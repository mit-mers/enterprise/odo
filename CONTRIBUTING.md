In order to make the process of contributing new code to this project easier,
please follow these guidelines:

1. Before beginning work, open a new issue on the Gitlab server. Be sure to
label it appropriately. The more detail you give in this step, the faster the
review will most likely be.
2. Keep each issue as small as makes sense. If you want to make a large change
that can be broken down into smaller, logically consistent chunks, please break
it apart and then reference the issues from each other if they are dependent.
3. If you perform your changes in the official repo (as opposed to a fork), keep
the changes in a branch of the form: `XX-summary-of-changes` where `XX` is the
issue number(s) that will be resolved by that branch.
4. As soon as you are ready to start receiving feedback, submit a WIP merge
request (click the option when making a merge request to mark it as a work in
progress). You may or may not get feedback before you're done, but you
definitely won't get any if you don't submit a WIP MR.
5. When you are finished with your changes, rebase your branch onto master and
push. You will need to force (`--force-with-lease` is recommended) the push if
the rebase changed any history.
6. Update the version number (in src/version.lisp) by incrementing the fourth
decimal point. And add a short summary of your change (if it's notable) to
CHANGELOG.md.
7. Remove the WIP status from the MR (or submit a MR for the first time).
8. After review, the reviewer will attempt to rebase again (if necessary). If
there are any non-trivial conflicts, the author will be responsible for rebasing
again before the branch is merged.
