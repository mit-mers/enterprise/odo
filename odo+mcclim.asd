;;;; Odo System Definition

#-:asdf3.1
(error "Requires ASDF >=3.1")

(defsystem #:odo+mcclim
  :version (:read-file-form "src/utils/version.lisp" :at (2 2))
  :description "Odo/McCLIM integration."
  :pathname "mcclim"
  :depends-on (#:bordeaux-threads #:clim #:mtk-mcclim-dot)
  :components
  ((:file "defs" :depends-on ("package"))
   (:file "inspector" :depends-on ("package"))
   (:file "package")
   (:file "state-plan" :depends-on ("package"))))
