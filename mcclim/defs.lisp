(in-package #:odo+mcclim)

(defgeneric odo-presentation-type-of (object)
  (:documentation
   "Returns a McCLIM presentation type for the given OBJECT."))

(defmethod odo-presentation-type-of (object)
  (odo-presentation-type-of (odo:type object)))

(defmethod odo-presentation-type-of ((object odo:object))
  (class-name (class-of object)))

(defclass graphical-view (clim:view)
  ()
  (:documentation
   "A view where detailed, graphical representations of Odo objects should be
drawn."))

(defclass detailed-view (clim:textual-view)
  ((child-view
    :initarg :child-view
    :initform (make-instance 'clim:textual-view)
    :reader child-view-of))
  (:documentation
   "A view where detailed, textual information of Odo objects is drawn."))
