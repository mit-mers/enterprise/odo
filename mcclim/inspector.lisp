(in-package #:odo+mcclim)

(defvar *default-odo-inspector*)

(clim:define-application-frame odo-inspector ()
  ((object))
  (:pointer-documentation t)
  (:panes
   (app :application
        :display-time :command-loop
        :display-function 'inspector-display
        :default-view (make-instance 'graphical-view)
        :height 400
        :width 600)
   (int :interactor
        :height 200
        :width 600))
  (:layouts
   (default (clim:vertically () app int))))

(define-odo-inspector-command (com-quit :name t) ()
  (when (and (boundp '*default-odo-inspector*)
             (eql *default-odo-inspector* clim:*application-frame*))
    (makunbound '*default-odo-inspector*))
  (clim:frame-exit clim:*application-frame*))

(defun inspector-display (frame stream)
  (when (slot-boundp frame 'object)
    (with-slots (object) frame
      (clim:present object (odo-presentation-type-of object) :stream stream))))

(defun ensure-odo-inspector (&key same-thread)
  (if (boundp '*default-odo-inspector*)
      *default-odo-inspector*
      (prog1
          (setf *default-odo-inspector* (clim:make-application-frame 'odo-inspector))
        (if same-thread
            (clim:run-frame-top-level *default-odo-inspector*)
            (bt:make-thread (lambda () (clim:run-frame-top-level *default-odo-inspector*)))))))

(defun odo-inspect (obj &key (frame (ensure-odo-inspector)))
  (with-slots (object) frame
    (setf object obj))
  (clim:redisplay-frame-panes frame))
