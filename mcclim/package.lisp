(uiop:define-package #:odo+mcclim
  (:use #:cl)
  ;; From defs
  (:export #:child-view-of
           #:detailed-view
           #:graphical-view
           #:odo-presentation-type-of)
  ;; From inspector
  (:export #:ensure-odo-inspector
           #:odo-inspect))

(in-package #:odo+mcclim)
