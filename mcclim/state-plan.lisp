(in-package #:odo+mcclim)

(defun maybe-inf (num)
  (case num
    (:infinity
     "∞")
    (:-infinity
     "-∞")
    (t
     num)))


;; Summary views

(clim:define-presentation-method clim:present (event
                                               (type odo:event)
                                               stream
                                               (view t)
                                               &key)
  (print-unreadable-object (event stream)
    (format stream "EVENT ~S" (odo:name event))))

(clim:define-presentation-method clim:present (state-plan
                                               (type odo:state-plan)
                                               stream
                                               (view t)
                                               &key)
  (print-unreadable-object (state-plan stream)
    (format stream "STATE-PLAN ~S" (odo:name state-plan))))

(clim:define-presentation-method clim:present (episode
                                               (type odo:episode)
                                               stream
                                               (view t)
                                               &key)
  (print-unreadable-object (episode stream)
    (format stream "EPISODE ~S" (odo:name episode))))


;; Detailed views

(clim:define-presentation-method clim:present :around (event
                                                       (type odo:event)
                                                       stream
                                                       (view detailed-view)
                                                       &key)
  (clim:with-text-style (stream (clim:make-text-style nil :bold :larger))
    (format stream "Event~%"))
  (clim:formatting-table (stream)
    (call-next-method)))

(clim:define-presentation-method clim:present (event
                                               (type odo:event)
                                               stream
                                               (view detailed-view)
                                               &key)
  (clim:formatting-row (stream)
    (clim:formatting-cell (stream)
      (format stream "Name:"))
    (clim:formatting-cell (stream)
      (format stream "~S" (odo:name event)))))

(clim:define-presentation-method clim:present :around (episode
                                                       (type odo:episode)
                                                       stream
                                                       (view detailed-view)
                                                       &key)
  (clim:with-text-style (stream (clim:make-text-style nil :bold :larger))
    (format stream "Episode~%"))
  (clim:formatting-table (stream)
    (call-next-method)))

(clim:define-presentation-method clim:present (episode
                                               (type odo:episode)
                                               stream
                                               (view detailed-view)
                                               &key)
  (clim:formatting-row (stream)
    (clim:formatting-cell (stream)
      (format stream "Name:"))
    (clim:formatting-cell (stream)
      (format stream "~S" (odo:name episode)))))

(clim:define-presentation-method clim:present :around (state-plan
                                                       (type odo:state-plan)
                                                       stream
                                                       (view detailed-view)
                                                       &key)
  (clim:with-text-style (stream (clim:make-text-style nil :bold :larger))
    (format stream "State Plan~%"))
  (clim:formatting-table (stream)
    (call-next-method)))

(clim:define-presentation-method clim:present (state-plan
                                               (type odo:state-plan)
                                               stream
                                               (view detailed-view)
                                               &key)
  (clim:formatting-row (stream)
    (clim:formatting-cell (stream)
      (format stream "Name:"))
    (clim:formatting-cell (stream)
      (format stream "~S" (odo:name state-plan))))
  (clim:formatting-row (stream)
    (clim:formatting-cell (stream)
      (format stream "Start event:"))
    (clim:formatting-cell (stream)
      (clim:present (odo:start-event state-plan)
                    'odo:event
                    :stream stream
                    :view (child-view-of view)))))


;; Graphical views

(clim:define-presentation-method clim:present ((e t)
                                               (type odo:event)
                                               stream
                                               (view graphical-view)
                                               &key)
  (clim:draw-circle* stream 0 0 10))

(clim:define-presentation-method clim:present ((sp t)
                                               (type odo:state-plan)
                                               stream
                                               (view graphical-view)
                                               &key)
  "Present a state plan, using the Dot/GraphViz backend to
FORMAT-GRAPH-FROM-ROOTS."
  (let ((constraints (odo:constraint-list sp))
        (outgoing-constraints-ht (make-hash-table :test 'eql))
        (incoming-constraints-ht (make-hash-table :test 'eql))
        (episodes (odo:goal-episode-list sp))
        (outgoing-episodes-ht (make-hash-table :test 'eql)))
    (dolist (c constraints)
      (let ((from (odo:argument-value (odo:expression c)
                                      'odo:from))
            (to (odo:argument-value (odo:expression c)
                                    'odo:to)))
        (push (cons to c) (gethash from outgoing-constraints-ht))
        (push (cons from c) (gethash to incoming-constraints-ht))))
    (dolist (ep episodes)
      (let ((from (odo:start-event ep))
            (to (odo:end-event ep)))
        (push (cons to ep) (gethash from outgoing-episodes-ht))))
    (clim:format-graph-from-roots
     (list (odo:start-event sp))
     (lambda (object stream)
       (clim:present object 'odo:event :stream stream))
     (lambda (event)
       (append
        (mapcar #'car (gethash event outgoing-constraints-ht))
        (mapcar #'car (gethash event outgoing-episodes-ht))))
     :stream stream
     :arc-drawer (make-instance 'mtk-mcclim-dot:dot-arc-drawer
                                :edge-label-printer
                                (lambda (drawer stream from to)
                                  (declare (ignore drawer))
                                  (let ((c (cdr (assoc to (gethash from outgoing-constraints-ht))))
                                        (ep (cdr (assoc to (gethash from outgoing-episodes-ht)))))
                                    (if ep
                                        (let ((c (odo:temporal-constraint ep)))
                                          (clim:with-output-as-presentation
                                              (stream ep 'odo:episode)
                                            (clim:draw-text* stream
                                                             (format nil "(~A~{ ~A~})~%[~A,~A]"
                                                                     (odo:activity-name ep)
                                                                     (odo:activity-args ep)
                                                                     (maybe-inf
                                                                      (if (null c)
                                                                          0
                                                                          (odo:argument-value
                                                                           (odo:expression c)
                                                                           'odo:lower-bound)))
                                                                     (maybe-inf
                                                                      (if (null c)
                                                                          :infinity
                                                                          (odo:argument-value
                                                                           (odo:expression c)
                                                                           'odo:upper-bound))))
                                                             0 0 :align-x :center)))
                                        (clim:draw-text* stream
                                                         (format nil "[~A,~A]"
                                                                 (maybe-inf
                                                                  (odo:argument-value
                                                                   (odo:expression c)
                                                                   'odo:lower-bound))
                                                                 (maybe-inf
                                                                  (odo:argument-value
                                                                   (odo:expression c)
                                                                   'odo:upper-bound)))
                                                         0 0 :align-x :center)))))
     :graph-type :dot-digraph
     :merge-duplicates t)))
