(uiop:define-package #:odo/examples/state-plan/glider-multi
    (:use #:cl
          #:anaphora
          #:odo))

(in-package #:odo/examples/state-plan/glider-multi)

(defun make-exogenous-state-plan (start-event x1 y1 z1 x2 y2 z2 dynamics-mode1 dynamics-mode2)
  (let ((exo-start-event (make-event :value -1 :name "exo-start")))
    (aprog1 (make-state-plan :name "exogenous"
                             :state-event exo-start-event)
      (let* ((initial-episode (make-episode :name "initial-conditions"
                                            :start-event exo-start-event
                                            :end-event start-event
                                            :end-constraints
                                            (list (make-state-constraint
                                                   (odo-equal (@ x1 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ y1 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ z1 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ dynamics-mode1 +current-time+) "default"))
                                                  (make-state-constraint
                                                   (odo-equal (@ x2 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ y2 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ z2 +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ dynamics-mode2 +current-time+) "default"))))))
        (state-space-add! it start-event)
        (add-goal-episode! it initial-episode)))))

(defun make-goal-state-plan (start-event x1 y1 z1 x2 y2 z2 dynamics-mode1 dynamics-mode2)
  (aprog1 (make-state-plan :name "goal"
                           :start-event start-event)
    (let* ((end-event (make-event))
           (end-reg1 (make-event :name "end-region1"))
           (end-reg2 (make-event :name "end-region2"))
           (end-in-region1 (make-episode
                           :start-event start-event
                           :end-event end-reg1
                           :end-constraints (list (make-state-constraint
                                  (geom-within (list (@ x1 +current-time+)
                                                     (@ y1 +current-time+))
                                               1)))))
           (end-in-region2 (make-episode
                            :start-event start-event
                            :end-event end-reg2
                            :end-constraints
                            (list (make-state-constraint
                                   (geom-within (list (@ x2 +current-time+)
                                                      (@ y2 +current-time+))
                                                2)))))
           (comm-range-and-rendezvous (make-episode
                                       :start-event start-event
                                       :end-event end-event
                                       :over-all-constraints
                                       (list (make-state-constraint
                                              (agent-separation (list (@ x1 +current-time+)
                                                                          (@ y1 +current-time+))
                                                                    (list (@ x2 +current-time+)
                                                                          (@ y2 +current-time+))
                                                                    :max 10.0)))
                                       :end-constraints
                                       (list (make-state-constraint
                                              (agent-separation (list (@ x1 +current-time+)
                                                                          (@ y1 +current-time+))
                                                                    (list (@ x2 +current-time+)
                                                                          (@ y2 +current-time+))
                                                                    :max 0.0)))))
           (avoid-obstacles (make-episode
                             :start-event start-event
                             :end-event end-event
                             :over-all-constraints
                             (list (make-state-constraint
                                    (avoids-obstacles (list (@ x1 +current-time+)
                                                            (@ y1 +current-time+))
                                                      3))
                                   (make-state-constraint
                                    (avoids-obstacles (list (@ x2 +current-time+)
                                                            (@ y2 +current-time+))
                                                      4)))))
           (dynamics (make-episode
                      :start-event start-event
                      :end-event end-event
                      :over-all-constraints
                      (list (make-state-constraint
                             (odo-equal (@ dynamics-mode1 +current-time+) "default"))
                            (make-state-constraint
                             (odo-equal (@ dynamics-mode2 +current-time+) "default")))))

           (tc1 (make-constraint (simple-temporal end-reg1 end-event
                                                  :lower-bound 0.001)))
           (tc2 (make-constraint (simple-temporal end-reg2 end-event
                                                  :lower-bound 0.001))))

      (state-space-add! it (list end-event end-reg1 end-reg2 x1 y1 z1 x2 y2 z2))
      (add-goal-episode! it end-in-region1)
      (add-goal-episode! it end-in-region2)
      (add-goal-episode! it comm-range-and-rendezvous)
      (add-goal-episode! it avoid-obstacles)
      (add-goal-episode! it dynamics)
      (add-constraint! it tc1)
      (add-constraint! it tc2))))

(defun make-multi-glider-state-plans ()
  (let* ((start-event (make-event :value 0 :name "start"))
         (x1 (make-state-variable '(real) :name "x1" :annotations '(state "x" vehicle "glider" instance "glider-1")))
         (y1 (make-state-variable '(real) :name "y1" :annotations '(state "y" vehicle "glider" instance "glider-1")))
         (z1 (make-state-variable '(real) :name "z1" :annotations '(state "z" vehicle "glider" instance "glider-1")))
         (dynamics-mode1 (make-state-variable '(member :members ("default" "yo" "thruster"))
                                             :name "dynamics-mode1"
                                             :annotations '(state "dynamics-mode" vehicle "glider" instance "glider-1")))
         (x2 (make-state-variable '(real) :name "x2" :annotations '(state "x" vehicle "glider" instance "glider-2")))
         (y2 (make-state-variable '(real) :name "y2" :annotations '(state "y" vehicle "glider" instance "glider-2")))
         (z2 (make-state-variable '(real) :name "z2" :annotations '(state "z" vehicle "glider" instance "glider-2")))
         (dynamics-mode2 (make-state-variable '(member :members ("default" "yo" "thruster"))
                                             :name "dynamics-mode2"
                                             :annotations '(state "dynamics-mode" vehicle "glider" instance "glider-2")))

         (exogenous-state-plan (make-exogenous-state-plan start-event x1 y1 z1 x2 y2 z2 dynamics-mode1 dynamics-mode2))
         (goal-state-plan (make-goal-state-plan start-event x1 y1 z1 x2 y2 z2 dynamics-mode1 dynamics-mode2)))
    (values exogenous-state-plan goal-state-plan)))
