(uiop:define-package #:odo/examples/state-plan/glider-psulu
    (:use #:cl
          #:anaphora
          #:odo))

(in-package #:odo/examples/state-plan/glider-psulu)

(defun make-exogenous-state-plan (start-event x y z dynamics-mode)
  (let ((exo-start-event (make-event :value -1 :name "exo-start")))
    (aprog1 (make-state-plan :name "exogenous"
                             :start-event exo-start-event)
      (let* ((initial-episode (make-episode :name "initial-conditions"
                                            :start-event exo-start-event
                                            :end-event start-event
                                            :end-constraints
                                            (list (make-state-constraint
                                                   (odo-equal (@ x +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ y +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ z +current-time+) 0))
                                                  (make-state-constraint
                                                   (odo-equal (@ dynamics-mode +current-time+) "default"))))))
        (state-space-add! it start-event)
        (add-goal-episode! it initial-episode)))))

(defun make-goal-state-plan (start-event x y z dynamics-mode)
  (aprog1 (make-state-plan :name "goal"
                           :start-event start-event)
    (let* ((end-event (make-event))
           (end-in-region (make-episode
                           :start-event start-event
                           :end-event end-event
                           :end-constraints
                           (list (make-state-constraint
                                  (geom-within (list (@ x +current-time+)
                                                     (@ y +current-time+))
                                               1)))))
           (avoid-obstacles (make-episode
                             :start-event start-event
                             :end-event end-event
                             :end-constraints
                             (list (make-state-constraint
                                    (avoids-obstacles (list (@ x +current-time+)
                                                            (@ y +current-time+))
                                                      2)))))
           (dynamics (make-episode
                      :start-event start-event
                      :end-event end-event
                      :over-all-constraints
                      (list (make-state-constraint
                             (odo-equal (@ dynamics-mode +current-time+) "default")))))
           (risk-on-plan (make-chance-constraint
                          :constraints (list end-in-region avoid-obstacles)
                          :failure-probability 0.01)))
      (state-space-add! it (list end-event x y z))
      (add-goal-episode! it end-in-region)
      (add-goal-episode! it dynamics)
      (add-goal-episode! it avoid-obstacles)
      (add-state-plan-chance-constraint! it risk-on-plan))))

(defun make-psulu-state-plans ()
  (let* ((start-event (make-event :value 0 :name "start"))
         (x (make-state-variable '(real) :name "x" :annotations '(state "x" vehicle "glider" instance "glider-1")))
         (y (make-state-variable '(real) :name "y" :annotations '(state "y" vehicle "glider" instance "glider-1")))
         (z (make-state-variable '(real) :name "z" :annotations '(state "z" vehicle "glider" instance "glider-1")))
         (dynamics-mode (make-state-variable '(member :members ("default" "yo" "thruster"))
                                             :name "dynamics-mode"
                                             :annotations '(state "dynamics-mode" vehicle "glider" instance "glider-1")))
         (exogenous-state-plan (make-exogenous-state-plan start-event x y z dynamics-mode))
         (goal-state-plan (make-goal-state-plan start-event x y z dynamics-mode)))
    (values exogenous-state-plan goal-state-plan)))
