(uiop:define-package #:odo/examples/state-plan/cps-simple
    (:use #:cl
          #:anaphora
          #:odo))

(in-package #:odo/examples/state-plan/cps-simple)

(defun make-cps-simple-state-plan ()
  (let* ((start-event (make-event :value 0 :name 'start)))
    (aprog1 (make-state-plan :name :cps-test-1
                             :start-event start-event)
      (let* ((ep1-start-event (make-event :name 'ep1-start))

             (ep1-end-event (make-event :name 'ep1-end))

             (ep2-start-event (make-event :name 'ep2-start))

             (ep2-end-event (make-event :name 'ep2-end))

             (rover-location (make-state-variable '((member "free-space" "region-a" "region-b"))
                                                  :name 'rover-location))
             (rover-x (make-state-variable '(real) :name 'rover-x))
             (rover-y (make-state-variable '(real) :name 'rover-y))

             (ep1 (make-episode :start-event ep1-start-event
                                :end-event ep1-end-event
                                :temporal-constraint (simple-temporal ep1-start-event
                                                                      ep1-end-event
                                                                      :lower-bound 1
                                                                      :upper-bound 2)
                                :over-all-constraints
                                (list (make-state-constraint
                                       (odo-equal (@ rover-location +current-time+) "region-a")))
                                :name 'ep1))
             (ep2 (make-episode :start-event ep2-start-event
                                :end-event ep2-end-event
                                :temporal-constraint (simple-temporal ep2-start-event
                                                                      ep2-end-event
                                                                      :lower-bound 1
                                                                      :upper-bound 2)
                                :over-all-constraints
                                (list (make-state-constraint
                                       (odo-equal (@ rover-location +current-time+) "region-b")))
                                :name 'ep2))

             (tc1 (make-constraint (simple-temporal start-event
                                                    ep1-start-event
                                                    :lower-bound 60)
                                   :name 'overall-time-bound)))

        ;; Add the other variables
        (state-space-add! it rover-location)

        (state-space-add! it rover-x)
        (state-space-add! it rover-y)

        (add-goal-episode! it ep1 ep2)
        (add-constraint! it tc1)))))
