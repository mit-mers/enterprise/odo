(uiop:define-package #:odo/examples/state-plan/spock-explore
    (:use #:cl
          #:anaphora
          #:odo))

(in-package #:odo/examples/state-plan/spock-explore)

(defun make-exogenous-state-plan (start-event x y z dive-angle dynamics-mode)
  (let ((exo-start-event (make-event :value -1 :name "exo-start")))
    (aprog1 (make-state-plan :name "exogenous"
                             :start-event exo-start-event)
      (let* ((initial-episode (make-episode :name "initial-conditions"
                                            :start-event exo-start-event
                                            :end-event start-event
                                            :end-constraints
                                            (list (make-state-constraint
                                                   (odo-equal (@ x +current-time+) -84.30058479309082))
                                                  (make-state-constraint
                                                   (odo-equal (@ y +current-time+) 8.93047963779774))
                                                  (make-state-constraint
                                                   (odo-equal (@ z +current-time+) -1010))
                                                  (make-state-constraint
                                                   (odo-equal (@ dive-angle +current-time+) 0.14))
                                                  (make-state-constraint
                                                   (odo-equal (@ dynamics-mode +current-time+) "default"))))))
        (state-space-add! it start-event)
        (add-goal-episode! it initial-episode)))))

(defun make-goal-state-plan (start-event x y z dive-angle dynamics-mode)
  (aprog1 (make-state-plan :name "goal"
                           :start-event start-event)
    (let* ((end-event (make-event))
           (episode (make-episode
                     :start-event start-event
                     :end-event end-event
                     :over-all-constraints
                     (list (make-state-constraint
                            (geom-within (list (@ x +current-time+)
                                               (@ y +current-time+))
                                         1))
                           (make-state-constraint
                            (avoids-obstacles (list (@ x +current-time+)
                                                    (@ y +current-time+))
                                              1801))
                           (make-state-constraint
                            (odo-equal (@ dynamics-mode +current-time+) "default"))
                           (make-state-constraint
                            (le (@ z +current-time+) -990))
                           (make-state-constraint
                            (ge (@ z +current-time+) -1010)))))
           (simple-temporal (make-constraint (simple-temporal start-event end-event :lower-bound 0 :upper-bound 7200)))
           (risk-on-plan (make-chance-constraint
                          :constraints (list episode)
                          :failure-probability 0.01)))
      (state-space-add! it (list end-event x y z dive-angle))
      (add-goal-episode! it episode)
      (add-state-plan-chance-constraint! it risk-on-plan)
      (add-constraint! it simple-temporal))))

(defun make-spock-state-plans ()
  (let* ((start-event (make-event :value 0 :name "start"))
         (x (make-state-variable '(real) :name "x" :annotations '(state "x" vehicle "glider" instance "glider-1")))
         (y (make-state-variable '(real) :name "y" :annotations '(state "y" vehicle "glider" instance "glider-1")))
         (z (make-state-variable '(real) :name "z" :annotations '(state "z" vehicle "glider" instance "glider-1")))
         (dive-angle (make-state-variable '(real) :name "diveAngle" :annotations '(state "diveAngle" vehicle "glider" instance "glider-1")))
         (dynamics-mode (make-state-variable '(member :members ("default" "yo" "thruster"))
                                             :name "dynamics-mode"
                                             :annotations '(state "dynamics-mode" vehicle "glider" instance "glider-1")))
         (exogenous-state-plan (make-exogenous-state-plan start-event x y z dive-angle dynamics-mode))
         (goal-state-plan (make-goal-state-plan start-event x y z dive-angle dynamics-mode)))
    (values exogenous-state-plan goal-state-plan)))
