;;; This file is where all sybmol exports should go that may shadow
;;; common symbols (including those in CL).

;; Do not lock for now. Has issues in model-api/application/expressions
(uiop:define-package #:odo/shadows
    (:use)
  (:import-from #:odo
                #:+
                #:-
                #:*
                #:/
                #:<
                #:<=
                #:>
                #:>=
                #:/=
                #:and
                #:every
                #:funcall
                #:lambda
                #:not
                #:or
                #:some
                #:xor)
  (:export
   #:+
   #:-
   #:*
   #:/
   #:=
   #:<
   #:<=
   #:>
   #:>=
   #:/=
   #:and
   #:every
   #:funcall
   #:lambda
   #:not
   #:or
   #:some
   #:xor))
