(in-package #:odo/impl)


;;; Define basic API implementation types.

(defclass odo-ref-impl/state-space ()
  ())

(defclass %state-space (annotation-base)
  ((object-map
    :initform (make-hash-table :test 'equal)
    :reader state-space-object-map)
   (domain-map
    :initarg :domain-map
    :reader state-space-domain-map)
   (variable-groups
    :initarg :variable-group-map
    :accessor state-space-variable-groups))
  (:documentation "A container for variables."))

(defmethod odo-impl/make-instance ((type odo:state-space)
                                   (impl odo-ref-impl/state-space)
                                   &key annotations)
  (make-instance '%state-space
                 :variable-group-map (make-hash-table :test #'equal)
                 :domain-map (make-hash-table :test #'equal)
                 :annotations annotations))

(defmethod odo:type ((obj %state-space))
  (make-instance 'odo:state-space))

(defmethod odo:state-space-p ((obj %state-space))
  t)

(defmethod odo:state-space-get ((self %state-space) name
                                &optional (missing-error-p t) missing-value)
  (multiple-value-bind (val exists-p)
      (gethash name (state-space-object-map self) missing-value)
    (when (and (not exists-p)
               missing-error-p)
      (odo/model-api::raise-state-space-error "Object ~S does not exist." name))
    val))

(defmethod odo:state-space-add-by-type! ((self %state-space) object
                                         (type odo:variable))
  (let ((name (odo:name object)))
    (ana:aif (gethash name (state-space-object-map self))
             (unless (eq it object)
               (odo/model-api::raise-state-space-error "An object with name ~S already exists."
                                                       name))
             (setf (gethash name (state-space-object-map self)) object)))
  self)

(defmethod odo:state-space-add-by-type! ((self %state-space) object
                                         (type odo:state-variable))
  (let ((name (odo:name object)))
    (ana:aif (gethash name (state-space-object-map self))
             (unless (eq it object)
               (odo/model-api::raise-state-space-error "An object with name ~S already exists."
                                                       name))
             (setf (gethash name (state-space-object-map self)) object)))
  self)

(defmethod odo:state-space-iterator ((state-space %state-space))
  (odo:make-hash-table-value-iterator (state-space-object-map state-space)))
