(in-package #:odo/impl)

(defclass odo-ref-impl/state-constraint ()
  ())

(defclass %state-constraint (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (expression
    :initarg :expression
    :reader odo:expression)))

(defmethod odo-impl/make-instance ((type odo:state-constraint)
                                   (impl odo-ref-impl/state-constraint)
                                   &key
                                     predicate
                                     name
                                     annotations)
  (make-instance '%state-constraint
                 :name name
                 :annotations annotations
                 :expression predicate))

(defmethod odo:type ((constraint %state-constraint))
  (make-instance 'odo:state-constraint))

(defmethod odo:state-constraint-p ((constraint %state-constraint))
  t)

(defmethod print-object ((constraint %state-constraint) stream)
  (utils:print-odo-object constraint stream))
