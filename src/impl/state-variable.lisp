(in-package #:odo/impl)

(defclass odo-ref-impl/state-variable ()
  ())

(defclass %state-variable (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (uncontrollable-p
    :initarg :uncontrollable-p
    :initform nil
    :reader odo:uncontrollable-p)
   (conditional-p
    :initarg :conditional-p
    :initform nil
    :reader odo:conditional-p)
   (range
    :initarg :range
    :reader odo:range)
   (time-domain
    :initarg :time-domain
    :reader odo:state-variable-time-domain)))

(defmethod odo-impl/make-instance ((type odo:state-variable)
                                   (impl odo-ref-impl/state-variable)
                                   &key
                                     range time-domain name
                                     conditional-p uncontrollable-p annotations)
  (make-instance '%state-variable
                 :name name
                 :conditional-p conditional-p
                 :uncontrollable-p uncontrollable-p
                 :range (odo:copy range)
                 :time-domain (odo:copy time-domain)
                 :annotations (copy-list annotations)))

(defmethod odo:state-variable-p ((dom %state-variable))
  t)

(defmethod odo:range-at ((self %state-variable) time)
  (declare (ignore time))
  (odo:range self))

(defmethod odo:type ((variable %state-variable))
  (make-instance 'odo:state-variable))

(defmethod print-object ((var %state-variable) stream)
  (utils:with-delimiter-printing (var stream)
    (write (odo:name var) :stream stream)
    (write-char #\space stream)
    (odo:print-odo-domain (odo:range var) stream)))
