(in-package #:odo/impl)

(defclass odo-ref-impl/automata ()
  ())

(defclass %automaton ()
  ((name
    :initarg :name
    :reader odo:name)
   (interface-var-map
    :initform (make-hash-table :test 'equal)
    :reader automaton-interface-var-map)
   (location-map
    :initform (make-hash-table :test 'equal)
    :reader automaton-location-map)
   (location-var
    :initform nil
    :accessor automaton-location-var)))

(defclass %primitive-automaton (%automaton)
  ((name
    :initarg :name
    :reader odo:name)
   (state-var-map
    :initform (make-hash-table :test 'equal)
    :reader automaton-state-var-map)
   (transition-map
    :initform (make-hash-table :test 'equal)
    :reader automaton-transition-map)
   (incoming-transitions
    :initform nil
    :reader odo:incoming-transitions
    :accessor %location-incoming-transitions)
   (outgoing-transitions
    :initform nil
    :reader odo:outgoing-transitions
    :accessor %location-outgoing-transitions)))

(defmethod odo-impl/make-instance ((type odo:primitive-automaton)
                                   (impl odo-ref-impl/automata)
                                   &key name)
  (make-instance '%primitive-automaton
                 :name name))

(defmethod odo:add-state-variable! ((self %primitive-automaton) state-var)
  (let ((value (alex:ensure-gethash (odo:name state-var)
                                    (automaton-state-var-map self)
                                    state-var)))
    (assert (eql value state-var))
    self))

(defmethod odo:get-state-variable ((self %primitive-automaton) variable-name
                                   &optional (missing-error-p t) missing-value)
  (multiple-value-bind (var exists-p)
      (gethash variable-name (automaton-state-var-map self) missing-value)
    (assert (or exists-p (not missing-error-p)))
    var))

(defmethod odo:state-variables ((self %primitive-automaton))
  (alex:hash-table-values (automaton-state-var-map self)))

(defmethod odo:add-interface-variable! ((self %automaton) interface-var)
  (let ((value (alex:ensure-gethash (odo:name interface-var)
                                    (automaton-interface-var-map self)
                                    interface-var)))
    (assert (eql value interface-var))
    self))

(defmethod odo:remove-interface-variable! ((self %automaton) interface-var)
  (assert (remhash (odo:name interface-var)
                   (automaton-interface-var-map self)))
  self)

(defmethod odo:get-interface-variable ((self %automaton) variable-name
                                       &optional (missing-error-p t) missing-value)
  (multiple-value-bind (var exists-p)
      (gethash variable-name (automaton-interface-var-map self) missing-value)
    (assert (or exists-p (not missing-error-p)))
    var))

(defmethod odo:interface-variables ((self %automaton))
  (alex:hash-table-values (automaton-interface-var-map self)))

(defmethod odo:add-location! :before ((self %primitive-automaton) loc)
  (assert (typep (odo:type loc) 'odo:primitive-location)))

(defmethod odo:add-location! ((self %automaton) loc)
  (let ((value (alex:ensure-gethash (odo:name loc)
                                    (automaton-location-map self)
                                    loc)))
    (assert (eql value loc))
    self))

(defmethod odo:locations ((self %automaton))
  (alex:hash-table-values (automaton-location-map self)))

(defmethod odo:location-variable ((self %primitive-automaton))
  "Lazily instantiate the state variable upon requests, with domain being the locations"
  (let* ((state-var (automaton-location-var self)))
    (if (and state-var
             (alex:set-equal (third (odo/model-api::domain-spec (odo:range state-var)))
                             (mapcar #'odo:name (odo:locations self))))
        state-var
        ;; instantiate new state variable
        (setf (automaton-location-var self)
              (odo:make-state-variable (list (append (list 'member) (mapcar #'odo:name (odo:locations self))))
                                       :name (intern (format nil "~a-LOCATION" (odo:name self))))))))

(defmethod odo:get-location ((self %automaton) location-name
                             &optional (missing-error-p t) missing-value)
  (multiple-value-bind (loc exists-p)
      (gethash location-name (automaton-location-map self) missing-value)
    (assert (or exists-p (not missing-error-p)))
    loc))

(defmethod odo:add-transition! ((self %primitive-automaton) transition)
  (let ((value (alex:ensure-gethash (odo:name transition)
                                    (automaton-transition-map self)
                                    transition)))
    (assert (eql value transition))
    (pushnew transition (%location-outgoing-transitions (odo:source transition)))
    (dolist (b (odo:branches transition))
      (pushnew transition (%location-incoming-transitions (odo:target b))))
    self))

(defmethod odo:get-transition ((self %primitive-automaton) transition-name
                               &optional (missing-error-p t) missing-value)
  (multiple-value-bind (loc exists-p)
      (gethash transition-name (automaton-transition-map self) missing-value)
    (assert (or exists-p (not missing-error-p)))
    loc))

(defmethod odo:transitions ((self %primitive-automaton))
  (alex:hash-table-values (automaton-transition-map self)))

(defmethod odo:remove-transition! ((self %primitive-automaton) transition)
  (let ((res (remhash (odo:name transition) (automaton-transition-map self))))
    (assert res)
    (setf (%location-outgoing-transitions (odo:source transition))
          (remove transition (%location-outgoing-transitions (odo:source transition))))
    (dolist (b (odo:branches transition))
      (setf (%location-incoming-transitions (odo:target b))
            (remove transition (%location-incoming-transitions (odo:target b)))))
    self))

(defmethod odo:type ((self %primitive-automaton))
  (make-instance 'odo:primitive-automaton))

(defmethod odo:timed-automaton-p ((self %primitive-automaton))
  t)

(defmethod odo:constraint-automaton-p ((self %primitive-automaton))
  t)

(defmethod odo:probabilistic-automaton-p ((self %primitive-automaton))
  nil)


;; Concurrent automata

(defclass %concurrent-automaton (%automaton)
  ((constraints
    :initform nil
    :reader odo:state-constraint-list
    :writer (setf concurrent-location-constraints))))

(defmethod odo-impl/make-instance ((type odo:concurrent-automaton)
                                   (impl odo-ref-impl/automata)
                                   &key name)
  (make-instance '%concurrent-automaton
                 :name name))

(defmethod odo:timed-automaton-p ((self %concurrent-automaton))
  t)

(defmethod odo:constraint-automaton-p ((self %concurrent-automaton))
  t)

(defmethod odo:probabilistic-automaton-p ((self %concurrent-automaton))
  nil)

(defmethod odo:add-state-constraint! ((self %concurrent-automaton) c)
  (pushnew c (slot-value self 'constraints)))

(defmethod odo:remove-state-constraint! ((self %concurrent-automaton) c)
  (when (member c (odo:state-constraint-list self))
    (setf (concurrent-location-constraints self) (remove c (odo:state-constraint-list self)))))

(defmethod odo:type ((self %concurrent-automaton))
  (make-instance 'odo:concurrent-automaton))

(defmethod odo:state-constraint-iterator ((automaton %concurrent-automaton))
  (odo:make-list-iterator (odo:state-constraint-list automaton)))



(defclass %primitive-location ()
  ((name
    :initarg :name
    :reader odo:name)
   (constraints
    :initform nil
    :reader odo:state-constraint-list
    :writer (setf primitive-location-constraints))
   (incoming-transitions
    :initform nil
    :reader odo:incoming-transitions
    :accessor %location-incoming-transitions)
   (outgoing-transitions
    :initform nil
    :reader odo:outgoing-transitions
    :accessor %location-outgoing-transitions)))

(defmethod odo-impl/make-instance ((type odo:primitive-location)
                                   (impl odo-ref-impl/automata)
                                   &key name)
  (make-instance '%primitive-location
                 :name name))

(defmethod odo:add-state-constraint! ((self %primitive-location) c)
  (pushnew c (slot-value self 'constraints)))

(defmethod odo:remove-state-constraint! ((self %primitive-location) c)
  (when (member c (odo:state-constraint-list self))
    (setf (primitive-location-constraints self) (remove c (odo:state-constraint-list self)))))

(defmethod odo:type ((self %primitive-location))
  (make-instance 'odo:primitive-location))

(defmethod odo:state-constraint-iterator ((automaton %primitive-location))
  (odo:make-list-iterator (odo:state-constraint-list automaton)))



;; Transitions

(defclass %transition ()
  ((name
    :initarg :name
    :reader odo:name)
   (source
    :initarg :source
    :reader odo:source)
   (branches
    :initarg :branches
    :reader odo:branches)
   (guard
    :initarg :guard
    :reader odo:guard)))

(defmethod odo-impl/make-instance ((type odo:automaton-transition)
                                   (impl odo-ref-impl/automata)
                                   &key source name branches guard)
  (make-instance '%transition
                 :guard guard :branches branches
                 :name name :source source))

(defmethod odo:type ((self %transition))
  (make-instance 'odo:automaton-transition))


;; Transition branch

(defclass %transition-branch ()
  ((target
    :initarg :target
    :reader odo:target)))

(defmethod odo-impl/make-instance ((type odo:automaton-transition-branch)
                                   (impl odo-ref-impl/automata)
                                   &key target)
  (make-instance '%transition-branch
                 :target target))

(defmethod odo:type ((self %transition-branch))
  (make-instance 'odo:automaton-transition-branch))
