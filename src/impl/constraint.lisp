(in-package #:odo/impl)

(defclass odo-ref-impl/constraint ()
  ())

(defclass %constraint (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (expression
    :initarg :expression
    :reader odo:expression)))

(defclass %contingent-constraint (%constraint) ())

(defmethod odo-impl/make-instance ((type odo:constraint)
                                   (impl odo-ref-impl/constraint)
                                   &key name
                                     expression
                                     contingent-p
                                     annotations)
  (assert (or (typep (odo:type expression) 'odo:application-expression)
              (typep (odo:type expression) 'odo:variable)
              (typep (odo:type expression) 'odo:conditional-variable)))
  (make-instance (if contingent-p
                     '%contingent-constraint
                     '%constraint)
                 :name name
                 :annotations annotations
                 :expression expression))

(defmethod odo:type ((constraint %constraint))
  (make-instance 'odo:constraint))

(defmethod odo:type ((constraint %contingent-constraint))
  (make-instance 'odo:contingent-constraint))

(defmethod odo:constraint-p ((constraint %constraint))
  t)

(defmethod print-object ((constraint %constraint) stream)
  (utils:print-odo-object constraint stream))
