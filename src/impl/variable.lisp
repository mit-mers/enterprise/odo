(in-package #:odo/impl)

(defclass odo-ref-impl/variable ()
  ())

(defclass %variable (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (domain
    :initarg :domain
    :reader variable-domain)))

(defclass %controllable-variable (%variable) ())

(defclass %uncontrollable-variable (%variable) ())

(defmethod odo-impl/make-instance ((type odo:controllable-variable)
                                   (impl odo-ref-impl/variable)
                                   &key name annotations domain)
  (make-instance '%controllable-variable
                 :annotations annotations
                 :name name
                 :domain domain))

(defmethod odo-impl/make-instance ((type odo:uncontrollable-variable)
                                   (impl odo-ref-impl/variable)
                                   &key name annotations domain)
  (make-instance '%uncontrollable-variable
                 :annotations annotations
                 :name name
                 :domain domain))

(defmethod odo:type ((var %controllable-variable))
  (make-instance 'odo:controllable-variable))

(defmethod odo:type ((var %uncontrollable-variable))
  (make-instance 'odo:uncontrollable-variable))

(defmethod odo:variable-p ((var %variable))
  t)

(defmethod odo:domain-p ((var %variable))
  t)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-var-fun-forwarder (name &key lambda-list &allow-other-keys)
    (multiple-value-bind (req optional rest key allow-other-keys)
        (alex:parse-ordinary-lambda-list lambda-list)
      (let ((optional-args (mapcar (lambda (x) (list (first x) (second x))) optional))
            (key-args (mapcar (lambda (x) (list (second (first x)) (second x))) key))
            (key-calls (mapcar #'first key)))
        `(defmethod ,name ((,(first req) %variable) ,@(rest req)
                           ,@(when optional (list* '&optional optional-args))
                           ,@(when key (list* '&key key-args))
                           ,@(when allow-other-keys (list '&allow-other-keys))
                           ,@(when rest (list '&rest rest)))
           ,(if rest
                `(apply ',name (variable-domain ,(first req))
                        ,@(rest req)
                        ,@(when optional (mapcar #'first optional))
                        ,@(when key (mapcan #'identity key-calls))
                        ,rest)
                `(,name (variable-domain ,(first req))
                        ,@(rest req)
                        ,@(when optional (mapcar #'first optional))
                        ,@(when key (mapcan #'identity key-calls)))))))))

(defmacro forward-var-funs ()
  `(progn
     ,@(mapcar (alex:curry #'apply #'make-var-fun-forwarder)
               (remove 'odo:domain-p odo:*domain-query-functions* :key #'first))))

(forward-var-funs)

(defmethod print-object ((var %variable) stream)
  (odo:print-odo-variable var stream))
