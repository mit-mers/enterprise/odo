(in-package #:odo/impl)


;; BDD
(defclass odo-ref-impl/bdd ()
  ())

(defclass %bdd (annotation-base)
  ((name
    :initarg :name
    :accessor odo:name)
   (expression
    :initarg :expression
    :accessor odo:expression)
   (child-low
    :initarg :child-low
    :accessor odo:child-low)
   (child-high
    :initarg :child-high
    :accessor odo:child-high)))

(defmethod odo-impl/make-instance ((type odo:bdd) (impl odo-ref-impl/bdd)
                                   &key name expression child-low child-high annotations)
  (make-instance '%bdd
                 :name name
                 :expression expression
                 :child-low child-low
                 :child-high child-high
                 :annotations annotations))

(defmethod odo:type ((self %bdd))
  (make-instance 'odo:bdd))

(defmethod odo:bdd-p ((n %bdd))
  t)

(defmethod odo:terminal-p ((bdd %bdd))
  ;; Returns values (T/NIL if BDD is terminal node and the BDD Node Expression)
  (let ((terminal-p (and (not (odo:child-low bdd))
                         (not (odo:child-high bdd)))))
        (values terminal-p
                (if terminal-p
                    (= (odo:expression bdd) 1)
                    nil))))

(defmethod odo:satisfiable-p ((bdd %bdd) &key &allow-other-keys)
  ;; If the 1 terminal node appears in a BDD's children, it is satisifiable
   (bdd-dfs bdd (lambda (n) (odo:terminal-p n)) :terminal-node-value 1))

(defmethod odo:entailed-p ((bdd %bdd) &key &allow-other-keys)
  ;; If the 0 terminal node DOES NOT appear in a BDD's children, it is entailed
  (not (bdd-dfs bdd (lambda (n) (odo:terminal-p n)) :terminal-node-value 0)))

(defmethod odo:scope ((bdd %bdd) &key state-space)
  (if (odo:terminal-p bdd)
      (list)
      (union (odo:scope (odo:expression bdd) :state-space state-space)
             (union (odo:scope (odo:child-low bdd) :state-space state-space)
                    (odo:scope (odo:child-high bdd) :state-space state-space)))))


;; BDD Children and Ancestors
(defun bdd-dfs (node fn &key search-node terminal-node-value)
  "Traverse all of the nodes reachable from a node in depth-first
order. If an optional `SEARCH-NODE` is supplied, terminate the search
when the node is found. If an optional `TERMINAL-NODE-VALUE` is found,
terminate the search when the terminal node with the specified value
is found."
  (let ((Q (list node))
        (visited (make-hash-table :test 'eql))
        n)
    (setf (gethash (odo:name node) visited) t)

    (loop :while Q :do
      (setf n (pop Q))

      (funcall fn n)

      (multiple-value-bind (terminal-p terminal-expression)
          (odo:terminal-p n)


        ;; If you've encounted the optional search-node or the desired terminal node, break and return
        (when (or (and search-node (odo:equal-p search-node n))
                  (and terminal-node-value
                       terminal-p
                       (if (= 1 terminal-node-value)
                           terminal-expression
                           (not terminal-expression))))

          (return-from bdd-dfs n)))

      (let ((children (if (odo:terminal-p n)
                          nil
                          (list (odo:child-low n) (odo:child-high n)))))
        (dolist (child children)
          (unless (gethash (odo:name child) visited)
            (setf (gethash (odo:name child) visited) t)
            (setf Q (cons child Q))))))))


(defun bdd-get-all-children (bdd)
  (let (children)
    (bdd-dfs bdd (lambda (n)
                   (push n children)))
    children))

(defun bdd-count-children (bdd)
  (let ((count 0))
    (bdd-dfs bdd (lambda (n)
                   (declare (ignore n))
                   (incf count)))
    count))


;; Writing to Dot Stream
(defmethod odo:bdd-to-dot-stream ((bdd %bdd) stream)
  (let ((nodes (bdd-get-all-children bdd)))
    (bdd-nodes-to-dot-stream nodes stream)))

(defun bdd-nodes-to-dot-stream (nodes stream)
  "Helper function that, given a list of `NODES`, writes them to
`STREAM`."
  (format stream "graph BDD{~%")
  ;; Generate all nodes
  (dolist (n nodes)
    (if (odo:terminal-p n)
        (format stream
                "  n~a [label=~a,shape=box];~%"
                (odo:name n)
                (odo:name n))
        (let (bdd-display-text)
          (if (odo:variable-p (odo:expression n))
              (setf bdd-display-text (format nil "~A" (odo:name (odo:expression n))))
              (setf bdd-display-text (format nil "~A" (odo:name (odo:expression n)))))
          (format stream
                "  n~a [label=\"~a\",shape=circle,fixedsize=true];~%"
                (odo:name n)
                bdd-display-text))))

  ;; Generate all edges
  (dolist (n nodes)
    (unless (odo:terminal-p n)
      (format stream
              "  n~a -- n~a [label=0,style=dashed];~%"
              (odo:name n)
              (odo:name (odo:child-low n)))
      (format stream
              "  n~a -- n~a [label=1];~%"
              (odo:name n)
              (odo:name (odo:child-high n)))))


  (format stream "}~%"))
