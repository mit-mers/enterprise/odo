(in-package #:odo/impl)

(defclass odo-ref-impl/state-plan ()
  ())

(defclass %state-plan (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (objective
    :initarg :objective
    :reader odo:objective)
   (features
    :initarg :features
    :reader odo:features)
   (state-space
    :initarg :state-space
    :reader odo:state-space)

   (start-event
    :accessor odo:start-event)

   (constraint-ht
    :initform (make-hash-table :test 'equal)
    :reader constraint-ht)
   (goal-episode-ht
    :initform (make-hash-table :test 'equal)
    :reader goal-episode-ht)
   (value-episode-ht
    :initform (make-hash-table :test 'equal)
    :reader value-episode-ht)
   (cc-ht
    :initform (make-hash-table :test 'equal)
    :reader cc-ht)))

(defmethod odo-impl/make-instance ((type odo:state-plan) (impl odo-ref-impl/state-plan)
                                   &key
                                     state-space
                                     name
                                     objective
                                     features
                                     annotations)
  (make-instance '%state-plan
                 :name name
                 :state-space state-space
                 :objective objective
                 :features features
                 :annotations annotations))

(defmethod odo:type ((self %state-plan))
  (make-instance 'odo:state-plan))

(defmethod odo:state-plan-p ((self %state-plan))
  t)

(defmethod odo:state-plan-episode-iterator ((self %state-plan))
  (odo:goal-episode-iterator self))

(defmethod odo:goal-episode-iterator ((self %state-plan))
  (odo:make-hash-table-value-iterator (goal-episode-ht self)))

(defmethod odo:value-episode-iterator ((self %state-plan))
  (odo:make-hash-table-value-iterator (value-episode-ht self)))

(defmethod odo:event-iterator ((self %state-plan))
  (let ((it (odo:state-space-iterator (odo:state-space self)))
        (next-event nil))
    (flet ((find-next-event ()
             (setf next-event nil)
             (loop
               :until (odo:it-done it)
               :for next := (odo:it-next it)
               :when (odo:event-p next)
                 :do (setf next-event next)
                     (return))))
      (find-next-event)
      (odo:make-iterator :next-fun (lambda ()
                                     (prog1 next-event
                                       (find-next-event)))
                         :done-fun (lambda ()
                                     (not next-event))))))

(defmethod odo:chance-constraint-iterator ((self %state-plan))
  (odo:make-hash-table-value-iterator (cc-ht self)))

(defmethod odo:constraint-iterator ((self %state-plan))
  (odo:make-hash-table-value-iterator (constraint-ht self)))

(defun %add-episode-scope (state-plan e)
  (let ((start-event (odo:start-event e))
        (end-event (odo:end-event e)))
    (odo:state-space-add! state-plan start-event)
    (odo:state-space-add! state-plan end-event)
    (dolist (sc (append (odo:end-constraints e)
                        (odo:start-constraints e)
                        (odo:once-constraints e)
                        (odo:over-all-constraints e)))
      (odo:state-space-add! state-plan (odo:scope (odo:expression sc))))))

(defmethod odo:add-episode! ((self %state-plan) &rest episodes)
  (apply #'odo:add-goal-episode! self episodes))

(defmethod odo:add-goal-episode! ((self %state-plan) &rest episodes)
  (dolist (e episodes)
    (multiple-value-bind (value exists-p) (gethash (odo:name e) (goal-episode-ht self))
      (assert (or (not exists-p)
                  (eql value e)))
      (setf (gethash (odo:name e) (goal-episode-ht self)) e))
    (%add-episode-scope self e))
  self)

(defmethod odo:add-value-episode! ((self %state-plan) &rest episodes)
  (dolist (e episodes)
    (multiple-value-bind (value exists-p) (gethash (odo:name e) (value-episode-ht self))
      (assert (or (not exists-p)
                  (eql value e)))
      (setf (gethash (odo:name e) (value-episode-ht self)) e))
    (%add-episode-scope self e))
  self)

(defmethod odo:get-constraint ((self %state-plan) name
                           &optional (missing-error-p t) missing-value)
  (multiple-value-bind (val exists-p)
      (gethash name (constraint-ht self) missing-value)
    (when (and (not exists-p)
               missing-error-p)
      (odo/model-api::raise-csp-error "Constraint ~S does not exist." name))
    val))

(defmethod odo:add-constraint! ((self %state-plan) (constraints list))
  (mapc (alex:curry #'odo:add-constraint! self) constraints)
  self)

(defmethod odo:add-constraint! ((self %state-plan) (constraints vector))
  (loop
    :for c :across constraints
    :do (odo:add-constraint! self c))
  self)

(defmethod odo:add-constraint! ((self %state-plan) constraint)
  (multiple-value-bind (value exists-p) (gethash (odo:name constraint) (constraint-ht self))
    (assert (or (not exists-p)
                (eql value constraint)))
    (odo:state-space-add! self (odo:scope (odo:expression constraint)))
    (setf (gethash (odo:name constraint) (constraint-ht self)) constraint))
  self)

(defmethod odo:remove-constraint! ((self %state-plan) c)
  (unless (odo:constraint-p c)
    (odo/model-api::raise-csp-error "~S does not follow the Odo constraint interface." c))
  (let ((name (odo:name c)))
    (ana:aif (odo:get-constraint self name nil)
             (remhash name (constraint-ht self))
             (odo/model-api::raise-csp-error "Cannot remove constraint with name ~S: does not exist in state plan." name)))
  self)

(defmethod odo:remove-constraint! ((self %state-plan) (c-vector vector))
  (loop
     :for c :across c-vector
     :do (odo:remove-constraint! self c))
  self)

(defmethod odo:remove-constraint! ((self %state-plan) (c-list list))
  (loop
     :for c :in c-list
     :do (odo:remove-constraint! self c))
  self)

(defmethod odo:add-chance-constraint! ((self %state-plan) &rest ccs)
  (dolist (cc ccs)
    (dolist (c (odo:constraint-list cc))
      (assert (eql (gethash (odo:name c) (goal-episode-ht self))
                   c)))
    (multiple-value-bind (value exists-p) (gethash (odo:name cc) (cc-ht self))
        (assert (or (not exists-p)
                    (eql value cc)))
      (setf (gethash (odo:name cc) (cc-ht self)) cc)))
  self)

(defmethod odo:state-space-add! ((self %state-plan) object)
  (odo:state-space-add! (odo:state-space self) object)
  self)

(defmethod odo:state-space-get ((self %state-plan) name &optional (missing-error-p t) missing-value)
  (odo:state-space-get (odo:state-space self) name missing-error-p missing-value))

(defmethod odo:state-space-iterator ((self %state-plan))
  (odo:state-space-iterator (odo:state-space self)))
