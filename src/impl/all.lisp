(in-package #:odo/impl)

(defclass odo-ref-impl (odo-ref-impl/action-models/pddl
                        odo-ref-impl/automata
                        odo-ref-impl/bdd
                        odo-ref-impl/chance-constraint
                        odo-ref-impl/conditional-variable
                        odo-ref-impl/constraint
                        odo-ref-impl/csp
                        odo-ref-impl/domain
                        odo-ref-impl/episode
                        odo-ref-impl/event
                        odo-ref-impl/expressions
                        odo-ref-impl/objective/attribute
                        odo-ref-impl/state-constraint
                        odo-ref-impl/state-plan
                        odo-ref-impl/state-space
                        odo-ref-impl/state-variable
                        odo-ref-impl/variable)
  ())

(eval-when (:load-toplevel)
  (unless *odo-implementation*
    (setf *odo-implementation* (make-instance 'odo-ref-impl))))
