(in-package #:odo/impl)

(defclass odo-ref-impl/expressions ()
  ())

(defclass %application-expression ()
  ((operator
    :initarg :operator
    :reader odo:operator)
   (arguments
    :initarg :arguments
    :reader odo:arguments)))

(defclass %lookup-expression (%application-expression)
  ())

(defmethod odo-impl/make-instance ((type odo:application-expression)
                                   (impl odo-ref-impl/expressions)
                                   &key operator arguments)
  (make-instance (if (eql operator 'odo:lookup)
                     '%lookup-expression
                     '%application-expression)
                 :operator operator
                 :arguments arguments))

(defmethod odo:type ((self %application-expression))
  (make-instance 'odo:application-expression))

(defmethod odo:type ((self %lookup-expression))
  (make-instance 'odo:lookup-expression))

(defmethod odo:expression-p ((self %application-expression))
  t)

(defmethod odo:application-expression-p ((self %application-expression))
  t)

(defmethod print-object ((obj %application-expression) stream)
  (utils:print-odo-object obj stream))
