(in-package #:odo/impl)

(defclass odo-ref-impl/event ()
  ())

(defclass %event-base ()
  ((observations
    :initarg :observations
    :reader odo:observations)
   (delayed-p
    :initarg :delayed-p
    :initform nil
    :reader odo:delayed-p)))

(defclass %event (%event-base %variable)
  ())

(defclass %controllable-event (%event %controllable-variable)
  ())

(defclass %uncontrollable-event (%event %uncontrollable-variable)
  ())

(defclass %conditional-event (%event-base %conditional-variable)
  ())

(defclass %controllable-conditional-event (%conditional-event %controllable-conditional-variable)
  ())

(defclass %uncontrollable-conditional-event (%conditional-event %uncontrollable-conditional-variable)
  ())

(defmethod odo-impl/make-instance ((type odo:event)
                                   (impl odo-ref-impl/event)
                                   &key
                                     name
                                     uncontrollable-p
                                     domain
                                     observations
                                     annotations)
  (make-instance (if uncontrollable-p
                     '%uncontrollable-event
                     '%controllable-event)
                 :name name
                 :domain domain
                 :observations observations
                 :annotations annotations))

(defmethod odo-impl/make-instance ((type odo:conditional-event)
                                   (impl odo-ref-impl/event)
                                   &key
                                     name
                                     uncontrollable-p
                                     domain
                                     observations
                                     annotations)
  (make-instance (if uncontrollable-p
                     '%uncontrollable-conditional-event
                     '%controllable-conditional-event)
                 :name name
                 :domain domain
                 :observations observations
                 :annotations annotations))

(defmethod odo:type ((self %controllable-conditional-event))
  (make-instance 'odo:controllable-conditional-event))

(defmethod odo:type ((self %controllable-event))
  (make-instance 'odo:controllable-event))

(defmethod odo:type ((self %uncontrollable-conditional-event))
  (make-instance 'odo:uncontrollable-conditional-event))

(defmethod odo:type ((self %uncontrollable-event))
  (make-instance 'odo:uncontrollable-event))

(defmethod odo:event-p ((self %event-base))
  t)

(defmethod odo:conditional-p ((self %conditional-event))
  t)

(defmethod odo:conditional-p ((self %event-base))
  nil)

(defmethod odo:uncontrollable-p ((self %event-base))
  nil)

(defmethod odo:uncontrollable-p ((self %uncontrollable-event))
  t)

(defmethod odo:uncontrollable-p ((self %uncontrollable-conditional-event))
  t)
