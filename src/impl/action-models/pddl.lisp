(in-package #:odo/impl)

(defclass odo-ref-impl/action-models/pddl ()
  ())


;; * Types

(defclass %pddl-type ()
  ((name
    :initarg :name
    :reader odo:name)
   (supertype
    :initarg :supertype
    :reader odo:supertype)))

(defmethod odo-impl/make-instance ((type odo:pddl-type)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-type
         initargs))

(defmethod odo:type ((self %pddl-type))
  (make-instance 'odo:pddl-type))


;; * Constants

(defclass %pddl-constant ()
  ((name
    :initarg :name
    :reader odo:name)
   (type
    :initarg :type
    :reader odo:pddl-constant-type)))

(defmethod odo-impl/make-instance ((type odo:pddl-constant)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-constant
         initargs))

(defmethod odo:type ((self %pddl-constant))
  (make-instance 'odo:pddl-constant))


;; * Parameters

(defclass %pddl-parameter ()
  ((name
    :initarg :name
    :reader odo:name)
   (type
    :initarg :type
    :reader odo:pddl-parameter-type)))

(defmethod odo-impl/make-instance ((type odo:pddl-parameter)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-parameter
         initargs))

(defmethod odo:type ((self %pddl-parameter))
  (make-instance 'odo:pddl-parameter))


;; * Predicates

(defclass %pddl-predicate ()
  ((name
    :initarg :name
    :reader odo:name)
   (parameters
    :initarg :parameters
    :reader odo:pddl-predicate-parameters)))

(defmethod odo-impl/make-instance ((type odo:pddl-predicate)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-predicate
         initargs))

(defmethod odo:type ((self %pddl-predicate))
  (make-instance 'odo:pddl-predicate))


;; * Functions

(defclass %pddl-function ()
  ((name
    :initarg :name
    :reader odo:name)
   (parameters
    :initarg :parameters
    :reader odo:pddl-function-parameters)
   (type
    :initarg :type
    :reader odo:pddl-function-type)))

(defmethod odo-impl/make-instance ((type odo:pddl-function)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-function
         initargs))

(defmethod odo:type ((self %pddl-function))
  (make-instance 'odo:pddl-function))


;; * Control Variables

(defclass %pddl-control-variable ()
  ((name
    :initarg :name
    :reader odo:name)
   (bounds
    :initarg :bounds)
   (parameters
    :initarg :parameters
    :reader odo:pddl-control-variable-parameters)))

(defmethod odo:bounds ((cv %pddl-control-variable) &key)
  (let ((bounds (slot-value cv 'bounds)))
    (values bounds t t)))

(defmethod odo-impl/make-instance ((type odo:pddl-control-variable)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-control-variable
         initargs))

(defmethod odo:type ((self %pddl-control-variable))
  (make-instance 'odo:pddl-control-variable))


;; * Actions

(defclass %pddl-action ()
  ((name
    :initarg :name
    :reader odo:name)
   (parameters
    :initarg :parameters
    :reader odo:pddl-action-parameters)
   (condition
    :initarg :condition
    :reader odo:condition)
   (effect
    :initarg :effect
    :reader odo:effect)))

(defclass %pddl-snap-action (%pddl-action)
  ())

(defclass %pddl-durative-action (%pddl-action)
  ((duration
    :initarg :duration
    :reader odo:pddl-action-duration)))

(defmethod odo-impl/make-instance ((type odo:pddl-snap-action)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-snap-action
         initargs))

(defmethod odo-impl/make-instance ((type odo:pddl-durative-action)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key)
  (apply #'make-instance
         '%pddl-durative-action
         initargs))

(defmethod odo:type ((self %pddl-durative-action))
  (make-instance 'odo:pddl-durative-action))

(defmethod odo:type ((self %pddl-snap-action))
  (make-instance 'odo:pddl-snap-action))


;; * Domains

(defclass %pddl-domain ()
  ((name
    :initarg :name
    :reader odo:name)
   (requirements
    :initarg :requirements
    :reader odo:pddl-domain-requirements)
   (types
    :initarg :types
    :reader %pddl-domain-types)
   (constants
    :initarg :constants
    :reader %pddl-domain-constants)
   (predicates
    :initarg :predicates
    :reader %pddl-domain-predicates)
   (functions
    :initarg :functions
    :reader %pddl-domain-functions)
   (control-variables
    :initarg :control-variables
    :reader %pddl-domain-control-variables)
   (actions
    :initarg :actions
    :reader %pddl-domain-actions)))

(defun make-pddl-hash-table (list key)
  (ana:aprog1 (make-hash-table :test 'equalp)
    (dolist (item list)
      (setf (gethash (string (funcall key item)) it) item))))

(defmethod odo-impl/make-instance ((type odo:pddl-domain)
                                   (impl odo-ref-impl/action-models/pddl)
                                   &rest initargs
                                   &key
                                     types constants predicates functions control-variables
                                     actions)
  (apply #'make-instance
         '%pddl-domain
         :types (make-pddl-hash-table types #'odo:name)
         :constants (make-pddl-hash-table constants #'odo:name)
         :predicates (make-pddl-hash-table predicates #'odo:name)
         :functions (make-pddl-hash-table functions #'odo:name)
         :control-variables (make-pddl-hash-table control-variables #'odo:name)
         :actions (make-pddl-hash-table actions #'odo:name)
         initargs))

(defmethod odo:type ((self %pddl-domain))
  (make-instance 'odo:pddl-domain))

(defmethod odo:pddl-domain-action ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-actions domain))
      (when errorp
        (error "Unable to find action with name ~a" (string name)))))

(defmethod odo:pddl-domain-actions ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-actions domain)))

(defmethod odo:pddl-domain-constant ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-constants domain))
      (when errorp
        (error "Unable to find constant with name ~a" (string name)))))

(defmethod odo:pddl-domain-constants ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-constants domain)))

(defmethod odo:pddl-domain-control-variable ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-control-variables domain))
      (when errorp
        (error "Unable to find control-variable with name ~a" (string name)))))

(defmethod odo:pddl-domain-control-variables ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-control-variables domain)))

(defmethod odo:pddl-domain-function ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-functions domain))
      (when errorp
        (error "Unable to find function with name ~a" (string name)))))

(defmethod odo:pddl-domain-functions ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-functions domain)))

(defmethod odo:pddl-domain-predicate ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-predicates domain))
      (when errorp
        (error "Unable to find predicate with name ~a" (string name)))))

(defmethod odo:pddl-domain-predicates ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-predicates domain)))

(defmethod odo:pddl-domain-type ((domain %pddl-domain) name &optional (errorp t))
  (or (gethash (string name) (%pddl-domain-types domain))
      (when errorp
        (error "Unable to find type with name ~a" (string name)))))

(defmethod odo:pddl-domain-types ((domain %pddl-domain))
  (alex:hash-table-values (%pddl-domain-types domain)))
