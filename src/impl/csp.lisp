(in-package #:odo/impl)

(defclass odo-ref-impl/csp ()
  ())

(defclass %csp (annotation-base)
  ((state-space
    :initarg :state-space
    :reader odo:state-space)
   (c-map
    :initarg :constraint-map
    :reader csp-c-map)
   (objective-goal
    :initarg :objective-goal
    :accessor odo:objective-goal)
   (objective-function
    :initarg :objective-function
    :accessor odo:objective-function)))

(defmethod odo-impl/make-instance ((type odo:csp)
                                   (impl odo-ref-impl/csp)
                                   &key state-space
                                     objective-function
                                     objective-goal
                                     annotations)
  (make-instance '%csp
                 :state-space state-space
                 :objective-function objective-function
                 :objective-goal objective-goal
                 :annotations annotations
                 :constraint-map (make-hash-table :test #'equal)))

(defmethod odo:type ((obj %csp))
  (make-instance 'odo:csp))

(defmethod odo:csp-p ((obj %csp))
  t)

(defmethod odo:get-constraint ((csp %csp) name
                               &optional (missing-error-p t) missing-value)
  (multiple-value-bind (val exists-p)
      (gethash name (csp-c-map csp) missing-value)
    (when (and (not exists-p)
               missing-error-p)
      (odo/model-api::raise-csp-error "Constraint ~S does not exist." name))
    val))

(defmethod odo:add-constraint! ((csp %csp) c)
  (unless (odo:constraint-p c)
    (odo/model-api::raise-csp-error "~S does not follow the Odo constraint interface." c))
  (let ((name (odo:name c)))
    (ana:aif (odo:get-constraint csp name nil)
             (unless (eq it c)
               (odo/model-api::raise-csp-error "A constraint with name ~S already exists."
                                               name))
             (progn
               (setf (gethash name (csp-c-map csp)) c)
               (odo:state-space-add! csp (odo:scope (odo:expression c))))))
  csp)

(defmethod odo:add-constraint! ((csp %csp) (c-vector vector))
  (loop
     :for c :across c-vector
     :do (odo:add-constraint! csp c))
  csp)

(defmethod odo:add-constraint! ((csp %csp) (c-list list))
  (loop
     :for c :in c-list
     :do (odo:add-constraint! csp c))
  csp)

(defmethod odo:remove-constraint! ((csp %csp) c)
  (unless (odo:constraint-p c)
    (odo/model-api::raise-csp-error "~S does not follow the Odo constraint interface." c))
  (let ((name (odo:name c)))
    (ana:aif (odo:get-constraint csp name nil)
             (remhash name (csp-c-map csp))
             (odo/model-api::raise-csp-error "Cannot remove constraint with name ~S: does not exist in CSP." name)))
  csp)

(defmethod odo:remove-constraint! ((csp %csp) (c-vector vector))
  (loop
     :for c :across c-vector
     :do (odo:remove-constraint! csp c))
  csp)

(defmethod odo:remove-constraint! ((csp %csp) (c-list list))
  (loop
     :for c :in c-list
     :do (odo:remove-constraint! csp c))
  csp)

(defmethod odo:constraint-iterator ((csp %csp))
  (let ((vals (alex:hash-table-values (csp-c-map csp))))
    (odo:make-iterator :next-fun (lambda ()
                                   (pop vals))
                       :done-fun (lambda ()
                                   (null vals)))))

(defmethod odo:state-space-list ((csp %csp))
  (odo:state-space-list (odo:state-space csp)))

(defmethod odo:state-space-iterator ((csp %csp))
  (odo:state-space-iterator (odo:state-space csp)))


(defmacro forward-csp-fun (name args &optional optional-args)
  (let ((prob (gensym)))
    `(defmethod ,name ((,prob %csp) ,@args ,@(when (not (null optional-args)) (list* '&optional optional-args)))
       (,name (odo:state-space ,prob) ,@args ,@(mapcar (lambda (x)
                                                         (if (listp x)
                                                             (first x)
                                                             x))
                                                       optional-args)))))

(forward-csp-fun odo:state-space-add! (var))

(forward-csp-fun odo:state-space-get (name) ((missing-error-p t) missing-val))

(forward-csp-fun odo:state-space-assigned-p ())
