(in-package #:odo/impl)

(defclass odo-ref-impl/chance-constraint ()
  ())

(defclass %chance-constraint (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (constraints
    :initarg :constraints
    :initform nil
    :reader chance-constraint-constraints)
   (failure-probability
    :initarg :failure-probability
    :reader chance-constraint-failure-probability)))

(defmethod odo-impl/make-instance ((type odo:chance-constraint)
                                   (impl odo-ref-impl/chance-constraint)
                                   &key name
                                     constraints
                                     failure-probability
                                     annotations
                                     &allow-other-keys)
  (make-instance '%chance-constraint
                 :name name
                 :constraints constraints
                 :failure-probability failure-probability
                 :annotations annotations))

(defmethod odo:type ((cc %chance-constraint))
  (make-instance 'odo:chance-constraint))

(defmethod odo:chance-constraint-p ((cc %chance-constraint))
  t)

(defmethod odo:constraint-list ((cc %chance-constraint))
  (chance-constraint-constraints cc))

(defmethod odo:failure-probability ((cc %chance-constraint))
  (chance-constraint-failure-probability cc))

(defmethod odo:constraint-iterator ((cc %chance-constraint))
  (let ((constraints (odo:constraint-list cc)))
    (odo:make-iterator
     :next-fun (lambda ()
                 (pop constraints))
     :done-fun (lambda ()
                 (null constraints)))))
