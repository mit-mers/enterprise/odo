(in-package #:odo/impl)

(defclass odo-ref-impl/conditional-variable ()
  ())

(defclass %conditional-variable (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (domain
    :initarg :domain
    :reader variable-domain)
   (activation-status
    :initarg :activation-status
    :reader odo:variable-activation-status)))

(defclass %controllable-conditional-variable (%conditional-variable) ())

(defclass %uncontrollable-conditional-variable (%conditional-variable) ())

(defmethod odo-impl/make-instance ((type odo:controllable-conditional-variable)
                                   (impl odo-ref-impl/conditional-variable)
                                   &key name annotations activation-status domain)
  (make-instance '%controllable-conditional-variable
                 :annotations annotations
                 :name name
                 :activation-status activation-status
                 :domain domain))

(defmethod odo-impl/make-instance ((type odo:uncontrollable-conditional-variable)
                                   (impl odo-ref-impl/conditional-variable)
                                   &key name annotations activation-status domain)
  (make-instance '%uncontrollable-conditional-variable
                 :annotations annotations
                 :name name
                 :activation-status activation-status
                 :domain domain))

(defmethod odo:domain-p ((dom %conditional-variable))
  t)

(defmethod odo:variable-p ((dom %conditional-variable))
  t)

(defmethod odo:conditional-variable-p ((var %conditional-variable))
  t)

(defmethod odo:type ((var %controllable-conditional-variable))
  (make-instance 'odo:controllable-conditional-variable))

(defmethod odo:type ((var %uncontrollable-conditional-variable))
  (make-instance 'odo:uncontrollable-conditional-variable))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-cv-fun-forwarder (name &key lambda-list &allow-other-keys)
    (multiple-value-bind (req optional rest key allow-other-keys)
        (alex:parse-ordinary-lambda-list lambda-list)
      (let ((optional-args (mapcar (lambda (x) (list (first x) (second x))) optional))
            (key-args (mapcar (lambda (x) (list (second (first x)) (second x))) key))
            (key-calls (mapcar #'first key)))
        `(defmethod ,name ((,(first req) %conditional-variable) ,@(rest req)
                           ,@(when optional (list* '&optional optional-args))
                           ,@(when key (list* '&key key-args))
                           ,@(when allow-other-keys (list '&allow-other-keys))
                           ,@(when rest (list '&rest rest)))
           ,(if rest
                `(apply ',name (variable-domain ,(first req))
                        ,@(rest req)
                        ,@(when optional (mapcar #'first optional))
                        ,@(when key (mapcan #'identity key-calls))
                        ,rest)
                `(,name (variable-domain ,(first req))
                        ,@(rest req)
                        ,@(when optional (mapcar #'first optional))
                        ,@(when key (mapcan #'identity key-calls)))))))))

(defmacro forward-cv-funs ()
  `(progn
     ,@(mapcar (alex:curry #'apply #'make-cv-fun-forwarder)
               (remove-if (lambda (x)
                            (member x '(odo:domain-p odo:assigned-p odo:assigned-value)))
                          odo:*domain-query-functions*
                          :key #'first))))

(forward-cv-funs)

(defmethod odo:assigned-p ((var %conditional-variable))
  (or (and (eql :active (odo:variable-activation-status var))
           (odo:assigned-p (variable-domain var)))
      (eql :inactive (odo:variable-activation-status var))))

(defmethod odo:assigned-value ((var %conditional-variable))
  (ecase (odo:variable-activation-status var)
    (:active
     (odo:assigned-value (variable-domain var)))
    (:inactive
     odo:+variable-inactive-value+)
    (:unknown
     (odo/model-api::raise-domain-error "Variable not assigned."))))

(defmethod print-object ((var %conditional-variable) stream)
  (odo:print-odo-conditional-variable var stream))
