(in-package #:odo/impl)

(defvar *odo-implementation* nil)

(defun call-with-odo-implementation (implementation thunk)
  (let ((*odo-implementation* implementation))
    (funcall thunk)))

(defmacro with-odo-implementation ((implementation) &body body)
  `(call-with-odo-implementation
    ,implementation
    (lambda ()
      ,@body)))

(defgeneric odo-impl/make-instance (type impl &key &allow-other-keys)
  (:method ((type symbol) impl &rest args)
    (apply #'odo-impl/make-instance (make-instance type) impl args)))

(defun odo:make-instance (type &rest args &key &allow-other-keys)
  (apply #'odo-impl/make-instance type *odo-implementation* args))

(utils:with-deprecation (:style-warning "0.5" :delete "0.6")
   (defgeneric odo:odo-make-instance
       (type &rest args &key &allow-other-keys)
     (:documentation "Deprecated. Use `MAKE-INSTANCE` instead."))
   (defmethod odo:odo-make-instance :before
              ((type t) &rest args &key &allow-other-keys)
     (declare (ignore args))))
 (defmethod odo:odo-make-instance (type &rest args &key &allow-other-keys)
   (apply 'odo:make-instance type args))
