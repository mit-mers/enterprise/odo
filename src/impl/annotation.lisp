(in-package #:odo/impl)

(defclass annotation-base ()
  ((annotations
    :initform nil
    :initarg :annotations
    :accessor annotations)))

(defmethod odo:annotation ((obj annotation-base) annotation-name &optional default-value)
  (getf (annotations obj) annotation-name default-value))

(defmethod (setf odo:annotation) (value (obj annotation-base) annotation-name &optional default-value)
  (setf (getf (annotations obj) annotation-name default-value) value))

(defmethod odo:annotation-iterator ((obj annotation-base))
  (let ((plist (annotations obj)))
    (odo:make-iterator :next-fun (lambda ()
                                   (values (pop plist) (pop plist)))
                       :done-fun (lambda ()
                                   (null plist)))))
