(in-package #:odo/impl)

(defclass odo-ref-impl/episode ()
  ())

(defclass %episode (annotation-base)
  ((name
    :initarg :name
    :reader odo:name)
   (conditional-p
    :initarg :conditional-p
    :reader odo:conditional-p)
   (start-event
    :initarg :start-event
    :reader odo:start-event)
   (end-event
    :initarg :end-event
    :reader odo:end-event)
   (temporal-constraint
    :initarg :temporal-constraint
    :reader odo:temporal-constraint)
   (over-all-constraints
    :initarg :over-all-constraints
    :reader odo:over-all-constraints)
   (start-constraints
    :initarg :start-constraints
    :reader odo:start-constraints)
   (end-constraints
    :initarg :end-constraints
    :reader odo:end-constraints)
   (once-constraints
    :initarg :once-constraints
    :reader odo:once-constraints)
   (activity-name
    :initarg :activity-name
    :reader odo:activity-name)
   (activity-args
    :initarg :activity-args
    :reader odo:activity-args)))

(defmethod odo-impl/make-instance ((type odo:episode)
                                   (impl odo-ref-impl/episode)
                                   &key
                                     name
                                     conditional-p
                                     start-event
                                     end-event
                                     temporal-constraint
                                     activity-name
                                     activity-args
                                     annotations
                                     over-all-constraints
                                     start-constraints
                                     end-constraints
                                     once-constraints)
    (make-instance '%episode
                   :name name
                   :conditional-p conditional-p
                   :start-event start-event
                   :end-event end-event
                   :temporal-constraint temporal-constraint
                   :over-all-constraints over-all-constraints
                   :start-constraints start-constraints
                   :end-constraints end-constraints
                   :once-constraints once-constraints
                   :activity-name activity-name
                   :activity-args activity-args
                   :annotations annotations))


(defmethod odo:type ((self %episode))
  (make-instance 'odo:episode))

(defmethod odo:episode-p ((self %episode))
  t)

;; TODO: Fix this
(defmethod odo:activation-status ((self %episode))
  :unknown)
