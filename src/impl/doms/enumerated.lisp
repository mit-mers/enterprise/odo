(in-package #:odo/impl)


;;; Define basic classes.

(defclass %enumerated-domain (base-domain)
  ((elements
    :initarg :members
    :reader elements
    :initform nil
    :documentation "A list of elements in this domain.")
   (test
    :initarg :test
    :reader test
    :initform #'equal
    :documentation "Equality test for this domain.")))

(defmethod odo-impl/make-instance ((type odo:pure-discrete-domain)
                                   (impl odo-ref-impl/domain)
                                   &key members test annotations)
  (make-instance '%enumerated-domain
                 :members members
                 :test test
                 :annotations annotations))


;;; Implement base API.

(defmethod odo:domain-p ((dom %enumerated-domain))
  t)

(defmethod odo:domain-type ((dom %enumerated-domain))
  (make-instance 'odo:pure-discrete-domain))

(defmethod odo:in-domain-p ((dom %enumerated-domain) el)
  (when (member el (elements dom) :test (test dom))
    t))

(defmethod odo:assigned-p ((dom %enumerated-domain))
  (null (rest (elements dom))))

(defmethod odo:assigned-value ((dom %enumerated-domain))
  (unless (odo:assigned-p dom)
    (error 'odo:domain-error))
  (first (elements dom)))


;;; Implement discrete API.

(defmethod odo:size ((dom %enumerated-domain))
  (length (elements dom)))

(defmethod odo:value-iterator ((dom %enumerated-domain))
  (let ((elements (elements dom)))
    (odo:make-iterator
     :next-fun (lambda ()
                 (pop elements))
     :done-fun (lambda ()
                 (null elements)))))


;;; Implement enumerated API.

(defmethod odo:domain-test ((dom %enumerated-domain))
  (test dom))
