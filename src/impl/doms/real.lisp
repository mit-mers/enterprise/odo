(in-package #:odo/impl)


;;; Define basic class.

(defclass %real-domain (base-domain)
  ((ranges
    :initarg :ranges
    :reader ranges
    :initform nil
    :documentation "A list of bounds on ranges for this domain.")))

(defmethod odo-impl/make-instance ((type odo:pure-real-domain)
                                   (impl odo-ref-impl/domain)
                                   &key ranges annotations)
  (make-instance '%real-domain
                 :ranges ranges
                 :annotations annotations))


;;; Implement base API.

(defmethod odo:domain-p ((dom %real-domain))
  t)

(defmethod odo:domain-type ((dom %real-domain))
  (make-instance 'odo:pure-real-domain))

(defmethod odo:in-domain-p ((dom %real-domain) el)
  (iter
    (for (values lb ub) :in-ranges dom)
    (unless (eql lb odo:+-infinity+)
      (never (< el lb)))
    (when (and (or (eql ub odo:+infinity+)
                   (<= el ub))
               (or (eql lb odo:+-infinity+)
                   (>= el lb)))
      (leave t))
    (finally (return nil))))

(defmethod odo:assigned-p ((dom %real-domain))
  (destructuring-bind ((min . min-closed) . (max . max-closed))
      (first (ranges dom))
    (and (null (rest (ranges dom)))
         (not (eql min odo:+-infinity+))
         (not (eql max odo:+infinity+))
         (= min max)
         min-closed
         max-closed)))

(defmethod odo:assigned-value ((dom %real-domain))
  (unless (odo:assigned-p dom)
    (error 'odo:domain-error))
  (car (car (first (ranges dom)))))


;;; Implement numeric domain API.

(defmethod odo:min-val ((numeric-dom %real-domain))
  (let ((cell (car (first (ranges numeric-dom)))))
    (values (car cell) (cdr cell))))

(defmethod odo:max-val ((numeric-dom %real-domain))
  (let ((cell (cdr (car (last (ranges numeric-dom))))))
    (values (car cell) (cdr cell))))


(defmethod odo:median ((numeric-dom %real-domain))
  (if (or (eql (odo:min-val numeric-dom) odo:+-infinity+)
          (eql (odo:max-val numeric-dom) odo:+infinity+))
      '*
      (let ((true-median (+ (odo:min-val numeric-dom)
                            (/ (- (odo:max-val numeric-dom)
                                  (odo:min-val numeric-dom))
                               2))))
        (iter
          (for (values lb ub) :in-ranges numeric-dom)
          (for previous-ub previous ub)
          (when (and (<= true-median ub)
                     (>= true-median lb))
            (leave true-median))
          (when (<= true-median lb)
            (leave previous-ub))))))

(defmethod odo:width ((numeric-dom %real-domain))
  (if (or (eql (odo:min-val numeric-dom) odo:+-infinity+)
          (eql (odo:max-val numeric-dom) odo:+infinity+))
      '*
      (- (odo:max-val numeric-dom) (odo:min-val numeric-dom))))

(defmethod odo:range-iterator ((dom %real-domain))
  (let ((current-range (ranges dom)))
    (odo:make-iterator
     :next-fun (lambda ()
                 (destructuring-bind ((min . min-closed) . (max . max-closed))
                     (pop current-range)
                   (values min max min-closed max-closed)))
     :done-fun (lambda ()
                 (null current-range)))))
