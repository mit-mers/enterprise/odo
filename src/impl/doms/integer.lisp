;;;; Contains the reference implementation of integer domains.

(in-package #:odo/impl)


;; Define basic class.

(defclass %integer-domain (base-domain)
  ((ranges
    :initarg :ranges
    :reader ranges
    :initform nil
    :documentation "A list of bounds on ranges for this domain.")))

(defmethod odo-impl/make-instance ((type odo:pure-integer-domain)
                                   (impl odo-ref-impl/domain)
                                   &key ranges annotations)
  (make-instance '%integer-domain :ranges ranges :annotations annotations))


;;; Implement base API.

(defmethod odo:domain-p ((dom %integer-domain))
  t)

(defmethod odo:domain-type ((dom %integer-domain))
  (make-instance 'odo:pure-integer-domain))

(defun to-lisp-typespec (dom)
  (list* 'or (iter
               (for (values lb ub) :in-ranges dom)
               (collecting `(integer ,(if (eql lb odo:+-infinity+)
                                          '* lb)
                                     ,(if (eql ub odo:+infinity+)
                                          '* ub))))))

(defmethod odo:in-domain-p ((dom %integer-domain) el)
  (typep el (to-lisp-typespec dom)))

(defmethod odo:assigned-p ((dom %integer-domain))
  (and (null (rest (ranges dom)))
       (not (eql odo:+-infinity+ (car (first (ranges dom)))))
       (not (eql odo:+infinity+ (cdr (first (ranges dom)))))
       (= (car (first (ranges dom))) (cdr (first (ranges dom))))))

(defmethod odo:assigned-value ((dom %integer-domain))
  (unless (odo:assigned-p dom)
    (error 'odo:domain-error))
  (car (first (ranges dom))))


;;; Implement discrete API.

(defmethod odo:size ((dom %integer-domain))
  (iter
    (for (values lb ub) :in-ranges dom)
    (when (or (eql lb odo:+-infinity+)
              (eql ub odo:+infinity+))
      (return '*))
    (sum (1+ (- ub lb)))))

(cl-coroutine:defcoroutine value-iterator-coroutine (dom)
  (iter
    (for (values lb ub) :in-ranges dom)
    (when (eql lb odo:+-infinity+)
      (odo/model-api::raise-domain-error
       "Can't generate use value iterator on an integer domain with no lower bound."))
    (if (eql ub odo:+infinity+)
        (iter (for v :from lb)
              (cl-coroutine:yield v))
        (iter (for v :from lb :to ub)
              (cl-coroutine:yield v)))))

(defmethod odo:value-iterator ((dom %integer-domain))
  (let ((coroutine (cl-coroutine:make-coroutine 'value-iterator-coroutine))
        (max-val (odo:max-val dom))
        (donep nil))
    (odo:make-iterator
     :next-fun (lambda ()
                 (ana:aprog1 (funcall coroutine dom)
                   (when (= it max-val)
                     (setf donep t))))
     :done-fun (lambda ()
                 donep))))


;;; Implement numeric domain API.

(defmethod odo:min-val ((numeric-dom %integer-domain))
  (values (car (first (ranges numeric-dom))) t))

(defmethod odo:max-val ((numeric-dom %integer-domain))
  (values (cdr (car (last (ranges numeric-dom)))) t))

(defmethod odo:median ((numeric-dom %integer-domain))
  (if (or (eql (odo:min-val numeric-dom) odo:+-infinity+)
          (eql (odo:max-val numeric-dom) odo:+infinity+))
      '*
      (let ((true-median (+ (odo:min-val numeric-dom)
                            (floor (- (odo:max-val numeric-dom)
                                      (odo:min-val numeric-dom))
                                   2))))
        (iter
          (for (values lb ub) :in-ranges numeric-dom)
          (for previous-ub previous ub)
          (when (and (<= true-median ub)
                     (>= true-median lb))
            (leave true-median))
          (when (<= true-median lb)
            (leave previous-ub))))))

(defmethod odo:width ((numeric-dom %integer-domain))
  (if (or (eql (odo:min-val numeric-dom) odo:+-infinity+)
          (eql (odo:max-val numeric-dom) odo:+infinity+))
      '*
      (- (odo:max-val numeric-dom) (odo:min-val numeric-dom))))

(defmethod odo:range-iterator ((dom %integer-domain))
  (let ((current-range (ranges dom)))
    (odo:make-iterator
     :next-fun (lambda ()
                 (multiple-value-prog1 (values (car (first current-range))
                                               (cdr (first current-range))
                                               t
                                               t)
                   (setf current-range (rest current-range))))
     :done-fun (lambda ()
                 (null current-range)))))


;;; Implement Integer API.

(defmethod odo:regret-min ((int-dom %integer-domain))
  (let* ((ranges (ranges int-dom))
         (first-range (first ranges))
         (second-range (second ranges)))
    (cond
      ((eql (car first-range) odo:+-infinity+)
       ;; Domain goes to negative infinity. Regret is 1.
       1)
      ((eql (cdr first-range) odo:+infinity+)
       ;; Domain goes to positive infinity. Regret is 1.
       1)
      ((= (car first-range)
          (cdr first-range))
       ;; The first range is just a single number.
       (if (null second-range)
           ;; Second range is empty, so domain is assigned. Regret
           ;; is zero.
           0
           ;; Second rnage not empty, regret is differnce between
           ;; lower bounds.
           (- (car second-range)
              (car first-range))))
      ;; First range is not just a number. Regret is 1.
      (t
       1))))

(defmethod odo:regret-max ((int-dom %integer-domain))
  (let* ((ranges (reverse (last (ranges int-dom) 2)))
         (high-range (first ranges))
         (low-range (second ranges)))
    (cond
      ((eql (car high-range) odo:+-infinity+)
       ;; Domain goes to negative infinity. Regret is 1.
       1)
      ((eql (cdr high-range) odo:+infinity+)
       ;; Domain goes to positive infinity. Regret is 1.
       1)
      ((= (car high-range)
          (cdr high-range))
       ;; The high range is just a single number.
       (if (null low-range)
           ;; Low range is empty, so domain is assigned. Regret
           ;; is zero.
           0
           ;; Low range not empty, regret is differnce between
           ;; upper bounds.
           (- (cdr high-range)
              (cdr low-range))))
      ;; high range is not just a number. Regret is 1.
      (t
       1))))
