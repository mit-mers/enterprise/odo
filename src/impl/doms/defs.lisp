(in-package #:odo/impl)


;;; Define the base domain class that the basic Odo implementation
;;; is built on.
(defclass base-domain (annotation-base)
  ()
  (:documentation "The base class for Odo's implementation of
domains."))

(defclass odo-ref-impl/domain ()
  ())

(defmethod print-object ((dom base-domain) stream)
  (odo:print-odo-domain dom stream))
