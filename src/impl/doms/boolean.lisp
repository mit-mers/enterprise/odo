(in-package #:odo/impl)


;;; Define basic classes.

(defclass %boolean-domain (base-domain)
  ((state
    :reader state
    :initarg :state
    :initform 'unknown
    :documentation "State of this variable. One of T, NIL, or 'UNKNOWN.")))

(defmethod odo-impl/make-instance ((type odo:pure-boolean-domain)
                                   (impl odo-ref-impl/domain)
                                   &key annotations true false)
  (make-instance '%boolean-domain
                 :state (cond
                          ((and true false)
                           'unknown)
                          (true
                           t)
                          (false
                           nil))
                 :annotations annotations))


;;; Implement base API.

(defmethod odo:domain-p ((dom %boolean-domain))
  t)

(defmethod odo:domain-type-spec ((dom %boolean-domain) &optional tightest)
  (if (and tightest
           (odo:assigned-p dom))
      `(eql ,(odo:assigned-value dom))
      'boolean))

(defmethod odo:domain-type ((dom %boolean-domain))
  (make-instance 'odo:pure-boolean-domain))

(defmethod odo:in-domain-p ((dom %boolean-domain) el)
  (when (typep el 'boolean)
    (ecase (state dom)
      ('unknown
       t)
      ((t)
       el)
      ((nil)
       (not el)))))

(defmethod odo:assigned-p ((dom %boolean-domain))
  (not (eq (state dom) 'unknown)))

(defmethod odo:assigned-value ((dom %boolean-domain))
  (unless (odo:assigned-p dom)
    (error 'odo:domain-error))
  (state dom))


;;; Implement discrete API.

(defmethod odo:size ((dom %boolean-domain))
  (ecase (state dom)
    ('unknown
     2)
    ((t nil)
     1)))

(defmethod odo:value-iterator ((dom %boolean-domain))
  (let ((vals (ecase (state dom)
                ('unknown
                 '(nil t))
                ((t)
                 '(t))
                ((nil)
                 '(nil)))))
    (odo:make-iterator
     :next-fun (lambda ()
                 (pop vals))
     :done-fun (lambda ()
                 (null vals)))))
