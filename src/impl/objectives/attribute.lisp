(in-package #:odo/impl)

(defclass odo-ref-impl/objective/attribute ()
  ())

(defclass %attribute-objective ()
  ((attributes-alist
    :initarg :attributes-alist
    :initform nil
    :reader attributes-alist)))

(defmethod odo-impl/make-instance ((type odo:attribute-objective-function)
                                   (impl odo-ref-impl/objective/attribute)
                                   &key attribute-values-alist)
  "Make a reference implementation instance of an attribute objective."
  (make-instance '%attribute-objective :attributes-alist (copy-list attribute-values-alist)))

(defmethod odo:type ((self %attribute-objective))
  (make-instance 'odo:attribute-objective-function))

(defmethod odo:evaluate-objective ((obj %attribute-objective) (csp t))
  "Evaluate the objective function by reducing the attributes alist, perofrming
lookups based on the `ASSIGNED-VALUE` of the variables."
  (let ((attributes-alist (attributes-alist obj)))
    (reduce #'+ attributes-alist
            :key (lambda (x)
                   (let ((var (car x))
                         (values-alist (cdr x)))
                     (alex:assoc-value values-alist (odo:assigned-value var)
                                       :test (odo:domain-test var)))))))

(defmethod odo:attribute-values-alist ((obj %attribute-objective))
  (attributes-alist obj))

(defmethod odo:attribute-objective-function-p ((obj %attribute-objective))
  t)
