(in-package #:odo/manual)

(defsection @odo (:title "Odo"
                  :package-symbol :odo)
  (odo asdf/system:system)
  "Odo provides an API for modeling variables, constraints, automata,
optimization problems, and so on. Additionally, it provides a reference
implementation of the API that is not particularly optimized for any use case.

Odo attempts to solve the proliferation of different APIs for modeling and
interacting with solvers. Prior to Odo, consumers of MERS constraint solvers
were intricately tied to solvers they chose to use; they had to import the
solver package and use its API to build up the problem and then interpret the
solution. Comparing different solvers was arduous, mostly due to different APIs
and the solver APIs exposing a little too much of the solvers' inner
workings (which also made improving solvers without API breakage difficult at
times).

In the Odo world, each solver is highly encouraged to implement its own classes
or structures for its internal representation of the problem being solved, but
wherever these structures are exposed to the user, they need to implement
methods for the Odo API. The primary reason for solvers writing their own
implementation is one of performance. Odo makes no commitment to structures
vs. classes, storing data in the Lisp image vs. foreign memory, etc. All of
those optimization details are left up to the discretion of the solver, based
on its specific needs.

This separation of API and implementation (as well as the existence of Odo's
reference implementation) does mean there are a few things users must be aware
of. First, a problem can be modeled without needing to know what is going to
solve it. This is a plus as the solver does not need to be loaded when modeling
and no extra argument needs to be passed around to the constructors to specify
which implementation should be used to create objects. However, this does mean
that when the solver is loaded and the problem handed to the solver, a one-time
cost must typically be paid to convert from the reference implementation to the
solver's implementation. (However, Odo does provide a way to create a problem
directly using the solver's implementation by executing the constructors in a
special dynamic scope). Second, because Odo needs to support solvers that copy
as well as ones that modify in place (and to support the copying to the solver
implementation), a reference to an Odo object (e.g., a variable) will not always
point to the \"correct\" thing. In general, if you are handed a state space or
problem object, use the lookup functions to get the correct object reference
instead of using one you've cached away somewhere.

### Public Packages ###

Odo has several packages within it. However, you **should only use symbols
exported from the packages listed below**. If a package is not listed below, no
guarantees are made on its stability (neither exported symbols nor continued
existence).

+ `ODO`

It is **strongly** recommended that you **do not** import the whole `ODO`
package into your package. Instead, import only the symbols you need and/or
prefix all odo symbols with `odo:`."

  (model::@odo/model section)
  (io::@odo/io section)
  (utils::@odo/utils section)
  (odo:make-instance function))
