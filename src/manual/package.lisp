(uiop:define-package #:odo/manual
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:local-nicknames (#:io #:odo/io)
                    (#:model #:odo/model-api)
                    (#:utils #:odo/utils)))

(cl:in-package #:odo/manual)

;; Make 40ants-doc happy... it looks for symbols in the locatives package,
;; regardless of their home package. It seems like this should be made better:
;; either DEFINE-LOCATIVE should import the symbol for you, or it should look
;; in the symbol's home package before falling back to the locatives package.

(import '(model::odo-function) :40ants-doc/locatives)
