(uiop:define-package #:odo/doc
    (:use #:cl)
  (:export #:generate-docs))

(in-package #:odo/doc)

(defun generate-docs ()
  (let ((40ants-doc/builder/printer:*document-uppercase-is-code* nil))
    (40ants-doc/builder:update-asdf-system-docs (list odo/manual::@odo)
                                                :odo)))

(defmethod 40ants-doc/locatives/base:locate-object (symbol (locative-type (eql 'odo/model-api::odo-function))
                                                    locative-args)
  (assert (endp locative-args))
  (or (odo/model-api::symbol-odo-function symbol)
      (40ants-doc/locatives/base:locate-error)))

(defmethod 40ants-doc/reference-api:canonical-reference ((obj odo/model-api::odo-function))
  (40ants-doc/reference:make-reference (odo/model-api::odo-function-name obj) 'odo/model-api::odo-function))

(defmethod 40ants-doc/commondoc/builder:to-commondoc ((obj odo/model-api::odo-function))
  (let* ((docstring (odo/model-api::odo-function-documentation obj))
         (children (when docstring
                     (40ants-doc/commondoc/markdown:parse-markdown docstring)))
         (reference (40ants-doc/reference-api:canonical-reference obj)))
    (40ants-doc/commondoc/bullet:make-bullet reference
                                             :children children)))

(defmethod 40ants-doc/locatives/base:locate-object (symbol (locative-type (eql 'odo/model-api::odo-function-argument))
                                                    locative-args)
  (assert (endp locative-args))
  (40ants-doc/reference:make-reference symbol 'odo/model-api::odo-function-argument))

(defmethod 40ants-doc/object-package::object-package ((object odo/model-api::odo-function))
  nil)
