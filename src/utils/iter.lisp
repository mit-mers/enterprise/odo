(in-package #:odo/utils)

(defsection @odo/utils/iter (:title "Iterators")
  "Several modeling aspects specify that iterators are returned. Those
iterators respond to the following functions."
  (odo:it-next function)
  (odo:it-done function)
  (odo:it-list function)
  (odo:make-hash-table-value-iterator function)
  (odo:make-iterator function)
  (odo:make-list-iterator function))

;; Inline the constructor to cut down on time needed to make an
;; iterator (something that probably happens very frequently in
;; propagation).
(declaim (inline odo:make-iterator))

(defstruct (iterator (:constructor odo:make-iterator))
  (next-fun nil :type function)
  (done-fun nil :type function))

(declaim (inline odo:it-next))
(defun odo:it-next (it)
  "Return the next item in the iterator. If ODO:IT-DONE is T at the time
of the call to ODO:IT-NEXT, the results are undefined."
  (declare (type iterator it))
  (funcall (iterator-next-fun it)))

(declaim (inline odo:it-done))
(defun odo:it-done (it)
  "Returns T iff all items from the iterator have been generated."
  (declare (type iterator it))
  (funcall (iterator-done-fun it)))

(declaim (inline odo:it-list))
(defun odo:it-list (it &optional multiple-values)
  "Given an iterator, return a new list of all its elements."
  (declare (type iterator it))
  (loop
     :until (odo:it-done it)
     :if multiple-values
     :collecting (multiple-value-list (odo:it-next it))
     :else
     :collecting (odo:it-next it)))

(declaim (inline odo:make-list-iterator))
(defun odo:make-list-iterator (list)
  "Given a list, make an iterator over its contents."
  (declare (type list list))
  (let ((list list))
    (odo:make-iterator
     :next-fun (lambda () (pop list))
     :done-fun (lambda () (null list)))))

(declaim (inline odo:make-hash-table-value-iterator))
(defun odo:make-hash-table-value-iterator (ht)
  "Given a hash table, make an iterator over its values."
  (declare (type hash-table ht))
  (odo:make-list-iterator (alex:hash-table-values ht)))
