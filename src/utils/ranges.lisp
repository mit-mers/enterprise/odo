(in-package #:odo/utils)

(defun canonicalize-ranges (ranges &key (number-type 'real)
                                          (error-fun #'error))
  (flet ((raise-error (&rest args)
           (apply error-fun args)))
    (unless (listp ranges)
      (raise-error "RANGES must be a list."))
    (let ((ranges (copy-tree ranges))
          (last-max :-infinity))
      (dolist (r ranges)
        (unless (consp r)
          (raise-error "RANGES must contain cons cells."))
        (when (numberp (car r))
          (setf (car r) (cons (car r) t)))
        (when (or (eql (car r) '*)
                  (eql (car r) :-infinity))
          (setf (car r) (cons :-infinity nil)))
        (when (numberp (cdr r))
          (setf (cdr r) (cons (cdr r) t)))
        (when (or (eql (cdr r) '*)
                  (eql (cdr r) :infinity))
          (setf (cdr r) (cons :infinity nil)))
        (destructuring-bind ((min . min-closed) . (max . max-closed))
            r
          (when (eql min '*)
            (setf min :-infinity))
          (when (eql max '*)
            (setf max :infinity))
          (cond
            ((eql min :-infinity)
             (unless (eql last-max :-infinity)
               (raise-error "negative infinity can only appear in the lowest lower bound spot."))
             (when min-closed
               (raise-error "Infinite bounds cannot be closed.")))
            ((not (typep min number-type))
             (raise-error "Domain's lower bound must be a ~A or negative infinity." number-type)))
          (cond
            ((eql max :infinity)
             (when (eql last-max :infinity)
               (raise-error "Infinity can only appear in the highest upper bound spot."))
             (when max-closed
               (raise-error "Infinite bounds cannot be closed.")))
            ((not (typep max number-type))
             (raise-error "Domain's upper bound must be a ~A or infinity." number-type)))
          (unless (or (eql min :-infinity)
                      (eql max :infinity)
                      (<= min max))
            (raise-error "Domain's lower bound must be <= its upper bound."))
          (unless (or (eql last-max :-infinity)
                      (< last-max min))
            (raise-error "Ranges must be ordered, non overlapping, and non contiguous."))
          (setf last-max max)))
      ranges)))

(defun canonicalize-integer-ranges (ranges &key (error-fun #'error))
  (setf ranges (canonicalize-ranges ranges :number-type 'integer
                                    :error-fun error-fun))
  (mapcar (lambda (r)
            (cons (if (cdar r)
                      (caar r)
                      (if (eql :-infinity (caar r))
                          :-infinity
                          (1+ (caar r))))
                  (if (cddr r)
                      (cadr r)
                      (if (eql :infinity (cadr r))
                          :infinity
                          (1- (cadr r))))))
          ranges))

(defun convert-values-to-ranges (values)
  (let ((values (sort (copy-list values) #'<)))
    (loop
       :with current-range := (cons nil nil)
       :for cell :on values
       :for this-val := (progn
                          (setf current-range (cons (car cell) (car cell)))
                          (car cell))
       :then (car cell)
       :for next-val := (first (cdr cell))
       :unless (eql next-val (1+ this-val))
       :collect current-range
       :and
       :do (setf (cdr current-range) this-val
                 current-range (cons next-val next-val)))))
