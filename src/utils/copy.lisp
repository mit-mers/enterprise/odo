(in-package #:odo/utils)

(defsection @odo/utils/copy (:title "Copying")
  (odo:copy generic-function)
  (odo:odo-copy generic-function)
  (odo:copy-by-type generic-function)
  (odo:odo-copy-by-type generic-function))

(defstruct odo-copy-context
  (var-map-fun (constantly nil))
  (cache (make-hash-table :test 'eq)))

(defgeneric odo:copy-by-type (obj type &key context)
  (:method (self (type (eql nil)) &key context)
    (cond
      ((listp self)
       (mapcar (alex:rcurry #'odo:copy :context context) self))
      ((vectorp self)
       (map 'vector (alex:rcurry #'odo:copy :context context) self))
      (t
       self)))
  (:method ((obj list) (type (eql nil)) &key context)
    (mapcar (alex:rcurry #'odo:copy :context context) obj))
  (:method ((obj string) (type (eql nil)) &key context)
    (declare (ignore context))
    (copy-seq obj))
  (:method ((obj vector) (type (eql nil)) &key context)
    (map 'vector (alex:rcurry #'odo:copy :context context) obj)))

(define-renamed-generic (odo:odo-copy-by-type odo:copy-by-type :redirect t) (obj type &key context))

(defgeneric odo:copy (obj &key var-map-fun context)
  (:documentation
   "Return a copy of OBJ. First consults the context to see if a copy already
exists in the provided context. If so, that copy is returned
directly. Otherwise, a copy is made and stored in the context.

If VAR-MAP-FUN is provided, it must be a function that takes a variable and
returns a variable instance or NIL. Its purpose is to allow copying of objects
like expressions, while using variables that have been already copied (but are,
for some reason, difficult to put into a hash-table for mapping).")
  (:method (obj &key var-map-fun context)
    (declare (ignore var-map-fun))
    (odo:copy-by-type obj (odo:type obj) :context context)))

(define-renamed-generic (odo:odo-copy odo:copy) (obj &key var-map-fun contect))

(defmethod odo:copy :around (obj &key
                                   (var-map-fun (constantly nil))
                                   (context (make-odo-copy-context :var-map-fun var-map-fun)))
  (let* ((cache (odo-copy-context-cache context))
         (value (gethash obj cache)))
    (unless value
      (setf value (call-next-method obj :context context))
      (setf (gethash obj cache) value))
    value))
