(in-package #:odo/utils)

(defvar *odo-pprint-unreadable* t
  "When using Lisp's pretty printer, should objects be printed with
  `PRINT-UNREADABLE-OBJECT`. If `T`, only the toplevel object is
  printed as unreadable. If `NIL`, none are. If `:ALWAYS`, every
  object is.")

(defun call-with-delimiter-printing (object stream thunk)
  (if *odo-pprint-unreadable*
      (let ((*odo-pprint-unreadable* (if (eql *odo-pprint-unreadable* :always)
                                         :always
                                         nil)))
        (print-unreadable-object (object stream :type t)
          (pprint-newline :linear stream)
          (funcall thunk object stream)))
      (pprint-logical-block (stream nil :prefix "(" :suffix ")")
        (funcall thunk object stream))))

(defmacro with-delimiter-printing ((object stream) &body body)
  `(call-with-delimiter-printing ,object ,stream
                                 (lambda (,object ,stream)
                                   ,@body)))

(defun string-sortable-p (obj)
  (or (typep obj 'alex:string-designator)
      (numberp obj)
      (and
       (listp obj)
       ;; Let's not handle nested lists.
       (every (lambda (x)
                (or (typep x 'alex:string-designator)
                    (numberp x)))
              obj))))

(defgeneric coerce-to-string (obj))

(defmethod coerce-to-string ((obj string))
  obj)

(defmethod coerce-to-string ((obj number))
  ;; This is safe to use because circularity detection doesn't care about
  ;; numbers.
  (format nil "~A" obj))

(defmethod coerce-to-string ((obj symbol))
  (string obj))

(defmethod coerce-to-string ((obj character))
  (string obj))

(defmethod coerce-to-string ((obj list))
  (mapcar #'coerce-to-string obj))

(defun string-sort (l &key (key #'identity))
  "Given a list L of `STRING-SORTABLE-P` objects, sort them."
  (let ((l (copy-list l)))
    (stable-sort l (alex:curry #'uiop:lexicographic< #'string<)
                 :key (alex:compose #'coerce-to-string #'alex:ensure-list key))))
