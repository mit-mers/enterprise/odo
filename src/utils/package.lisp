(uiop:define-package #:odo/utils
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:local-nicknames (#:alex #:alexandria))
  ;; copy
  (:export #:copy-odo-copy-context
           #:make-odo-copy-context
           #:odo-copy-context-var-map-fun)
  ;; log
  (:export #:setup-logger
           #:log-fatal
           #:log-error
           #:log-warn
           #:log-info
           #:log-debug
           #:log-trace)
  ;; plist
  (:export #:plist-equal)
  ;; pretty-printing
  (:export #:*odo-pprint-unreadable*
           #:with-delimiter-printing
           #:string-sortable-p
           #:string-sort)
  ;; printing
  (:export #:odo-print-unreadable-object
           #:print-odo-object
           #:print-odo-object-by-type)
  ;; ranges
  (:export #:canonicalize-ranges
           #:canonicalize-integer-ranges
           #:convert-values-to-ranges)
  ;; version
  (:export #:define-renamed-class
           #:define-renamed-generic
           #:with-deprecation))
