(in-package #:odo/utils)

(defsection @odo/utils/equal (:title "Equality")
  "Common Lisp's equality functions are, for better or worse, not extensible. So
Odo defines `ODO-EQUAL-P` as an extensible alternative."
  (odo:equal-p generic-function)
  (odo:odo-equal-p generic-function)
  (odo:equal-p-by-type generic-function)
  (odo:odo-equal-p-by-type generic-function))

(defgeneric odo:equal-p-by-type (left right left-type right-type)
  (:method (left right (left-type (eql nil)) (right-type (eql nil)))
    (if (and (or (listp left)
                 (vectorp left))
             (or (listp right)
                 (vectorp right)))
        (and (alex:length= left right)
             (every #'odo:equal-p left right))
        (equal left right)))
  (:method (left right left-type right-type)
    (declare (notinline odo:odo-equal-p-by-type))
    (if (> (length (compute-applicable-methods #'odo:odo-equal-p-by-type (list left right left-type right-type)))
           3)
        (odo:odo-equal-p-by-type left right left-type right-type)
        (equal left right))))

(define-renamed-generic (odo:odo-equal-p-by-type odo:equal-p-by-type) (left right left-type right-type))

(defgeneric odo:equal-p (left right))

(define-renamed-generic (odo:odo-equal-p odo:equal-p) (left right))

(defmethod odo:equal-p :around (left right)
  (or (eq left right)
      (call-next-method)))

(defmethod odo:equal-p (left right)
  (odo:equal-p-by-type left right (odo:type left) (odo:type right)))

(defmethod odo:equal-p ((left number) (right number))
  (= left right))
