(in-package #:odo/utils)

(defmacro odo-print-unreadable-object ((obj stream) &body body)
  `(pprint-logical-block (,stream nil)
     (print-unreadable-object (,obj ,stream)
       ,@body)))

(defgeneric print-odo-object-by-type (obj type stream))

(defgeneric print-odo-object (obj stream)
  (:method (obj stream)
    (print-odo-object-by-type obj (odo:type obj) stream)))
