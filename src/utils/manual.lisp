(in-package #:odo/utils)

(defsection @odo/utils (:title "Odo Utils"
                        :package-symbol :odo)
  "This section describes useful functions that are typically not tied
to any particular API or implementation."
  (@odo/utils/copy section)
  (@odo/utils/equal section)
  (@odo/utils/iter section))
