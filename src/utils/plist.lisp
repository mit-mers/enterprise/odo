(in-package #:odo/utils)

(defun plist-equal (plist-1 plist-2)
  (let ((default (gensym)))
    (and (alex:length= plist-1 plist-2)
         (alex:doplist (key val plist-1 t)
           (unless (equal (getf plist-2 key default) val)
             (return nil))))))
