(in-package #:odo/utils)

;; NOTE: If this form changes position in this file you *must* update odo.asd as
;; well.
(defparameter *version* "0.5.0")

(defun odo-version ()
  *version*)

(define-condition deprecated-arg-condition (warning)
  ((arg-name
    :initform :arg-name
    :reader deprecated-arg-condition-arg-name))
  (:report
   (lambda (c s)
     (format s "Argument ~S has been deprecated"
             (deprecated-arg-condition-arg-name c))))
  (:documentation "Used to report that a deprecated argument has been used."))

(defmacro with-deprecation ((&rest args
                             &key
                               style-warning
                               warning error delete)
                            &body definitions)
  (declare (ignore style-warning warning error delete))
  `(uiop:with-deprecation ((uiop:version-deprecation *version*
                                                     ,@args))
     ,@definitions))

(defmacro define-renamed-class (old-name new-name)
  (let ((var-name (intern (format nil "*DEPRECATED-CLASS-STYLE-WARNING-~A-NOTIFIED-P*" old-name))))
    `(progn
       (setf (find-class ',old-name) (find-class ',new-name))
       (defparameter ,var-name nil)
       (defmethod make-instance ((class (eql ',old-name)) &rest initargs)
         (declare (ignore initargs))
         (unless ,var-name
           (setf ,var-name t)
           (warn 'uiop:simple-style-warning :format-arguments (list ',old-name ',new-name)
                                            :format-control "Class ~A is deprecated, use ~A instead"))
         (call-next-method)))))

(defmacro define-renamed-generic ((old-name new-name &key redirect) lambda-list &rest options)
  (let* ((doc (second (find :documentation options :key #'first)))
         (end-of-required-args (or (position-if (lambda (x) (uiop:string-prefix-p "&" (string x))) lambda-list)
                                   (length lambda-list)))
         (required-args (subseq lambda-list 0 end-of-required-args))
         (non-required-args (subseq lambda-list end-of-required-args))
         (circle-detect (intern (format nil "*CIRCLE-DETECT-~A*" new-name))))
    `(progn
       (with-deprecation (:style-warning "0.5" :delete "0.6")
         (defgeneric ,old-name ,lambda-list
           (:documentation ,(format nil "Deprecated. Use `~A` instead.~@[~%~A~]" new-name doc)))
         (defmethod ,old-name :before (,@(mapcar (lambda (x) (list x t)) required-args) ,@non-required-args)
           (declare (ignore ,@(rest non-required-args)))))
       ,(if (null non-required-args)
            `(defmethod ,old-name (,@required-args)
               (,new-name ,@required-args))
            (if (eql (first non-required-args) '&optional)
                `(defmethod ,old-name (,@required-args &optional ,@(rest non-required-args))
                   (,new-name ,@required-args ,@(rest non-required-args)))
                `(defmethod ,old-name (,@required-args &rest args)
                   (apply ',new-name ,@required-args args))))
       ,(when redirect
          `(progn
             (defvar ,circle-detect nil)
             ,(if (null non-required-args)
                  `(defmethod ,new-name (,@required-args)
                     (declare (notinline ,old-name))
                     (when ,circle-detect (error ,(format nil "~A not implemented" new-name)))
                     (let ((,circle-detect t))
                       (,old-name ,@required-args)))
                  (if (eql (first non-required-args) '&optional)
                      `(defmethod ,new-name (,@required-args &optional ,@(rest non-required-args))
                         (declare (notinline ,old-name))
                         (when ,circle-detect (error ,(format nil "~A not implemented" new-name)))
                         (let ((,circle-detect t))
                           (,old-name ,@required-args ,@(rest non-required-args))))
                      `(defmethod ,new-name (,@required-args &rest args)
                         (declare (notinline ,old-name))
                         (when ,circle-detect (error ,(format nil "~A not implemented" new-name)))
                         (let ((,circle-detect t))
                           (apply ',old-name ,@required-args args))))))))))
