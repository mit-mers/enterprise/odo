(in-package #:odo/utils)

;;; Logging framework for ODO!

(defmacro setup-logger ()
  "This macro *must* be called in any package where the logging macros are used."
  (unless (uiop:featurep :odo/no-log)
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (,(uiop:find-symbol* :package-options :log)
         :category-separator "/"))))

(defmacro local-configure ()
  "Configure the root of the ODO logger. Wrapped in a macro to more easily deal
with :odo/no-log."
  (unless (uiop:featurep :odo/no-log)
    `(,(uiop:find-symbol* :config :log) (,(uiop:find-symbol* :category :log) '(odo))
       :tricky-console :sane :immediate-flush :own :notime)))

(local-configure)

(defsection @odo-dev/log (:title "Logging")
  "Logging framework for Odo.

If the symbol `:ODO/NO-LOG` is on the `*FEATURES*` list when this code is
compiled, all logging macros will turn into noops."
  (setup-logger macro)
  (log-fatal macro)
  (log-error macro)
  (log-warn macro)
  (log-info macro)
  (log-debug macro)
  (log-trace macro))

(defmacro forward-macro (docstring from to)
  `(defmacro ,from (&rest args)
     ,docstring
     (when (and (not (uiop:featurep :odo/no-log))
                (uiop:find-package* :log nil))
       (list* (uiop:find-symbol* ,to :log) args))))

(forward-macro "Emit a logging message a priority level :FATAL" log-fatal
               :fatal)
(defmacro log-error (&rest args)
  (when (and (not (uiop:featurep :odo/no-log))
             (uiop:find-package* :log nil))
    `(progn
       (,(uiop:find-symbol* :error :log) ,@args)
       (error ,@args))))
(forward-macro "Emit a logging message a priority level :WARN" log-warn
               :warn)
(forward-macro "Emit a logging message a priority level :INFO" log-info
               :info)
(forward-macro "Emit a logging message a priority level :DEBUG" log-debug
               :debug)
(forward-macro "Emit a logging message a priority level :TRACE" log-trace
               :trace)
