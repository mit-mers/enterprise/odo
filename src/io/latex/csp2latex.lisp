(uiop:define-package #:odo/io/latex/csp2latex
  (:use #:cl)
  (:export #:latex-print-variable
           #:latex-print-variables
           #:latex-print-constraint
           #:latex-print-constraints
           #:latex-print-objective
           #:latex-print-csp))

(in-package #:odo/io/latex/csp2latex)

;;;;;;;;;;;;;;;
;; Variables ;;
;;;;;;;;;;;;;;;

(defgeneric %latex-print-variable (stream var domain-type))

(defmethod %latex-print-variable (stream var (domain-type odo:pure-boolean-domain))
  "Print LaTeX representation of a boolean variable."
  (format stream "(\\textbf{bool}) & ~A & \\verb|~A| \\\\~%"
          (if (odo:assigned-p var)
            (format nil "$= ~A$" (odo:assigned-value var))
            (format nil "$\\lbrace \\top, \\bot \\rbrace$"))
          (odo:name var)))

(defmethod %latex-print-variable (stream var (domain-type odo:pure-integer-domain))
  "Print LaTeX representation of a integer variable."
  (format stream "(\\textbf{int}) & ~A & \\verb|~A| \\\\~%"
          (if (odo:assigned-p var)
            (format nil "$= ~A$" (odo:assigned-value var))
            (format nil "$\\lbrace ~A \\cdots ~A \\rbrace$" (odo:min-val var) (odo:max-val var)))
          (odo:name var)))

(defmethod %latex-print-variable (stream var (domain-type odo:pure-real-domain))
  "Print LaTeX representation of a real variable."
  (format stream "(\\textbf{real}) & ~A & \\verb|~A| \\\\~%"
          (if (odo:assigned-p var)
            (format nil "$= ~A$" (odo:assigned-value var))
            (format nil "$[~A, ~A]$" (odo:min-val var) (odo:max-val var)))
          (odo:name var)))

(defmethod %latex-print-variable (stream var (domain-type odo:pure-discrete-domain))
  "Print LaTeX representation of an enumerated variable."
  (format stream "(\\textbf{enum}) & ~A & \\verb|~A| \\\\~%"
          (if (odo:assigned-p var)
            (format nil "$= ~A$" (odo:assigned-value var))
            (format nil "~A" (odo:value-list var)))
          (odo:name var)))

(defmethod %latex-print-variable (stream (var t) domain-type)
  "Print the variable name and a warning that no pretty representation exists."
  (format t "Can't handle variables of type ~A~%" domain-type)
  (format stream "(???) & ??? & Can't handle ~A \\\\~%" domain-type))

(defun latex-print-variable (stream var)
  (%latex-print-variable stream var (odo:domain-type var)))

(defun latex-print-variables (stream csp)
  "Print the list of variables"
  (write-line "\\begin{center}" stream)
  (write-line "\\def\\arraystretch{1.5}" stream)
  (write-line "\\begin{longtable}{lll}" stream)
  (write-line "\\textbf{Type} & \\textbf{Scope} & \\textbf{Name} \\\\" stream)
  (loop for var in (odo:state-space-list csp) do
    (latex-print-variable stream var))
  (write-line "\\end{longtable}" stream)
  (write-line "\\end{center}" stream))


;;;;;;;;;;;;;;;;;
;; Constraints ;;
;;;;;;;;;;;;;;;;;

(defgeneric %latex-print-constraint (stream const pred-type))

(defmethod %latex-print-constraint (stream (const t) pred-type)
  "Print the constraint name and a warning that no pretty representation exists."
  (format t "LaTeX Ouput Unsupported for ~A" pred-type)
  (format stream "LaTeX Ouput Unsupported for ~A~%" pred-type))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:not)))
  (format stream "NOT (")
  (let ((pred (odo:argument-value const 'odo:x)))
    (latex-print-constraint stream pred))
  (format stream ")~%"))

(defun %latex-print-linear (stream const symb)
  "Print the LaTeX representation of a linear constraint."
  (let ((coeffs (odo:argument-value const 'odo:c))
        (vars (odo:argument-value const 'odo:x))
        (val (odo:argument-value const 'odo:b)))
    (write-var-list stream vars)
    (write-string "\\[" stream)
    (format stream "~A" (coef-pair (aref coeffs 0) "x_0"))
    (when (> (length coeffs) 1)
      (loop for idx from 1 to (1- (length coeffs)) do
        (format stream " ~A ~A"
                (if (< (aref coeffs idx) 0) "-" "+")
                (coef-pair (aref coeffs idx) (format nil "x_{~A}" idx)))))
    (format stream " ~A ~A\\]~%" symb val)))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:linear-<=)))
  "Print the LaTeX representation of a linear LE constraint."
  (%latex-print-linear stream const "\\leq"))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:linear-=)))
  "Print the LaTeX representation of a linear EQ constraint."
  (%latex-print-linear stream const "="))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:and)))
  "Print the LaTeX representation of a conjunction constraint."
  (write-line "The following must be true:" stream)
  (write-line "\\begin{itemize}" stream)
  (loop
    :for sub-pred :in (coerce (odo:argument-value const 'odo:values) 'list)
    :do (write-string "\\item" stream)
    (latex-print-constraint stream sub-pred))
  (write-line "\\end{itemize}" stream))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:cl-equalp)))
  "Print the LaTeX representation of an equality constraint."
  (format stream "~A $~A$ ~A~%"
          (if (odo:variable-p (odo:argument-value const 'odo:left))
              (format nil "\\verb|~A|" (odo:name (odo:argument-value const 'odo:left)))
              (odo:argument-value const 'odo:left))
          "="
          (if (odo:variable-p (odo:argument-value const 'odo:right))
              (format nil "\\verb|~A|" (odo:name (odo:argument-value const 'odo:right)))
              (odo:argument-value const 'odo:right))))

(defmethod %latex-print-constraint (stream const (pred-type odo:variable))
  "Print the LaTeX representation of a boolean predicate."
  (format stream "Boolean variable \\verb|~A| holds.~%" (odo:name const)))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:element)))
  "Print the LaTeX representation of an element constraint."
  (let ((elements (odo:argument-value const 'odo:elements))
        (index (odo:argument-value const 'odo:index))
        (value (odo:argument-value const 'odo:value)))
    (format stream "Assuming \\textbf{IDX} equals \\verb|~A|, then...~%"
            (odo:name index))
    (write-line "\\begin{center}" stream)
    (write-line "\\def\\arraystretch{1.5}" stream)
    (write-line "\\begin{tabular}{rc}" stream)
    (format stream "\\textbf{IDX} & \\verb|~A| \\\\ ~% \\hline~%" (odo:name value))
    (loop for idx from 0 to (1- (length elements)) do
      (format stream "~A & ~A \\\\~%"
              idx (if (odo:variable-p (aref elements idx))
                    (format nil "\\verb|~A|" (odo:name (aref elements idx)))
                    (aref elements idx))))
    (write-line "\\end{tabular}" stream)
    (write-line "\\end{center}" stream)))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:subcircuit)))
  "Print the LaTeX representation of a subcircuit constraint."
  (write-line "Subcircuit for the following variables:" stream)
  (write-line "\\begin{itemize}" stream)
  (loop for var in (coerce (odo:argument-value const 'odo:values) 'list) do
    (format stream "\\item \\verb|~A|~%" (odo:name var)))
  (write-line "\\end{itemize}" stream))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:<->)))
  "Print the LaTeX representation of a full-reif constraint."
  (write-line "(a) $\\leftrightarrow$ (b) where..." stream)
  (write-line "\\begin{enumerate}" stream)
  (write-string "\\item " stream)
  (latex-print-constraint stream (odo:argument-value const 'odo:left))
  (write-string "\\item " stream)
  (latex-print-constraint stream (odo:argument-value const 'odo:right))
  (write-line "\\end{enumerate}" stream))

(defmethod %latex-print-constraint (stream const (pred-type (eql 'odo:->)))
  "Print the LaTeX representation of a half-reif constraint."
  (write-line "(a) $\\rightarrow$ (b) where..." stream)
  (write-line "\\begin{enumerate}" stream)
  (write-string "\\item " stream)
  (latex-print-constraint stream (odo:argument-value const 'odo:left))
  (write-string "\\item " stream)
  (latex-print-constraint stream (odo:argument-value const 'odo:right))
  (write-line "\\end{enumerate}" stream))

(defun latex-print-constraint (stream const)
  (if (odo:variable-p const)
      (%latex-print-constraint stream const (odo:type const))
      (let ((predicate (if (odo:constraint-p const)
                           (odo:expression const)
                           const)))
        (%latex-print-constraint stream predicate
                                 (if (odo:variable-p predicate)
                                     (odo:type predicate)
                                     (odo:operator predicate))))))

(defun latex-print-constraints (stream csp)
  "Print the list of constraints"
  (write-line "\\begin{enumerate}" stream)
  (loop for const in (odo:constraint-list csp) do
    (format stream "\\vspace{1em} \\item (~A) " (odo:name const))
    (latex-print-constraint stream const))
  (write-line "\\end{enumerate}" stream))

(defun coef-pair (c v)
  (if (eql 1 (abs c))
    (format nil "~A" v)
    (format nil "~A \\cdot ~A" (abs c) v)))

(defun write-var-list (stream vars)
  (write-line "\\begin{itemize}" stream)
  (loop for idx from 0 to (1- (length vars)) do
    (format stream "\\item $x_{~A}$: \\verb|~A|~%" idx (odo:name (elt vars idx))))
  (write-line "\\end{itemize}" stream))


;;;;;;;;;;;;;;;
;; Objective ;;
;;;;;;;;;;;;;;;

(defun latex-print-objective (stream csp)
  "Print the objective function"
  (format stream "(\\textit{~A}) \\verb|~A|~%"
          (odo:objective-goal csp)
          (if (odo:variable-p (odo:objective-function csp))
            (odo:name (odo:objective-function csp))
            (odo:objective-function csp))))


;;;;;;;;;;;;;;;;;;;;
;; Problem Object ;;
;;;;;;;;;;;;;;;;;;;;

(defun latex-print-csp (stream csp &optional misc-latex)
  "Print the entire latex file for an odo csp object."
  (format stream "~%~%\\documentclass[12pt]{article}~%")
  (write-line "\\usepackage{amsmath}" stream)
  (write-line "\\usepackage{longtable}" stream)
  (write-line "\\title{Odo CSP}" stream)
  (write-line "\\author{}" stream)
  (write-line "\\date{\\today}" stream)
  (write-line "\\begin{document}" stream)
  (write-line "\\maketitle" stream)
  (when misc-latex
    (write-line "\\newpage" stream)
    (write-line misc-latex stream))
  (write-line "\\newpage\\section{Variables}" stream)
  (latex-print-variables stream csp)
  (write-line "\\newpage\\section{Constraints}" stream)
  (latex-print-constraints stream csp)
  (write-line "\\newpage\\section{Objective}" stream)
  (latex-print-objective stream csp)
  (format stream "\\end{document}~%")
  "Done writing")
