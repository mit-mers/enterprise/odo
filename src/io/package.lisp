(uiop:define-package #:odo/io
  (:use #:cl
        #:iterate)
  (:import-from #:40ants-doc #:defsection)
  (:import-from #:anaphora #:it)
  (:local-nicknames (#:alex #:alexandria)
                    (#:ana #:anaphora)
                    (#:pn #:parse-number)
                    (#:ss #:split-sequence)
                    (#:utils #:odo/utils)))
