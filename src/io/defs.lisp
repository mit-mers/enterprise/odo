(in-package #:odo/io)

(defsection @odo/io/defs (:title "I/O definitions")
  "Input/output for Odo objects."
  "#### Conditions ####"
  (odo:unknown-object-externalization condition)
  (odo:unknown-annotation-externalization condition)
  (odo:unknown-annotation-externalization-key (reader odo:unknown-annotation-externalization))
  (odo:unknown-object-externalization-object (reader odo:unknown-object-externalization))
  "#### Reading ####"
  (odo:deserialize-from-stream function)
  (odo:deserialize-from-file function)
  (odo:deserialize-from-string function)
  (odo:internalize function)
  "#### Writing ####"
  (odo:externalize function)
  (odo:serialize-to-stream function)
  (odo:serialize-to-file function)
  (odo:serialize-to-string function))


;; * Conditions

(define-condition odo:unknown-object-externalization ()
  ((object
    :initarg :object
    :reader odo:unknown-object-externalization-object))
  (:report
   (lambda (c s)
     (format s "Unable to externalize object:~%~S" (odo:unknown-object-externalization-object c))))
  (:documentation
   "Signaled when an object cannot be externalized. Typically has a USE-VALUE
restart associated with it."))

(define-condition odo:unknown-annotation-externalization (odo:unknown-object-externalization)
  ((key
    :initarg :key
    :reader odo:unknown-annotation-externalization-key))
  (:report
   (lambda (c s)
     (format s "Unable to externalize an object for an annotation with key ~S:~%~S"
             (odo:unknown-annotation-externalization-key c)
             (odo:unknown-object-externalization-object c))))
  (:documentation
   "Signaled when an object cannot be externalized as an annotation. This always
has the CONTINUE restart available to simply ignore it."))


;; * Encoders

(defvar *encoders-by-format* nil)

(defun register-encoder (encoder-name encoder-class format priority)
  (setf (alex:assoc-value (alex:assoc-value *encoders-by-format* format) encoder-name)
        (cons encoder-class priority)))

(defun find-encoder-class (format encoder)
  (let ((format-alist (alex:assoc-value *encoders-by-format* format)))
    (if (eql encoder :auto)
        (iter
          (for (nil . (class-name . priority)) :in format-alist)
          (finding class-name :minimizing priority))
        (car (alex:assoc-value format-alist encoder)))))

(defun make-encoder (format encoder)
  (let ((encoder (alex:ensure-list encoder)))
    (destructuring-bind (encoder-name &rest encoder-args) encoder
      (let ((encoder-class (find-encoder-class format encoder-name)))
        (unless encoder-class
          (error "Unable to find an encoder named ~S for format ~S" encoder-name format))
        (apply #'make-instance encoder-class encoder-args)))))


;; * Decoders

(defvar *decoders-by-format* nil)

(defun register-decoder (decoder-name decoder-class format priority)
  (setf (alex:assoc-value (alex:assoc-value *decoders-by-format* format) decoder-name)
        (cons decoder-class priority)))

(defun find-decoder-class (format decoder)
  (let ((format-alist (alex:assoc-value *decoders-by-format* format)))
    (if (eql decoder :auto)
        (iter
          (for (nil . (class-name . priority)) :in format-alist)
          (finding class-name :minimizing priority))
        (car (alex:assoc-value format-alist decoder)))))

(defun make-decoder (format decoder)
  (let ((decoder (alex:ensure-list decoder)))
    (destructuring-bind (decoder-name &rest decoder-args) decoder
      (let ((decoder-class (find-decoder-class format decoder-name)))
        (unless decoder-class
          (error "Unable to find an decoder named ~S for format ~S" decoder-name format))
        (apply #'make-instance decoder-class decoder-args)))))


;; * Common

(defun guess-file-format (pathname)
  (let ((pathname-type (pathname-type pathname)))
    (cond
      ((equal pathname-type "json")
       :json)
      ((equal pathname-type "fzn")
       :flatzinc)
      ((equal pathname-type "mzn")
       :minizinc)
      ((equal pathname-type "tex")
       :latex)
      (t
       (restart-case
           (error "Unable to determine format for file ~S" pathname)
         (use-value (value) value))))))


;; * Externalizing

(defgeneric externalize-object (encoder format object unknown-object-externalizer version))

(defun odo:externalize (obj &key
                              (format :jsown)
                              (encoder :auto)
                              version
                              ignore-unexternalizable-annotations
                              unknown-object-externalizer)
  "Externalize an object to another object that can more readily be consumed by
libraries without Odo support. Returns two values: the externalized object and
an opaque value that can be used by `INTERNALIZE` to make the internalized
object `ODO-EQUAL-P` to `OBJ`.

If the format is versioned and the encoder supports encoding multiple versions,
then VERSION specifies which to use. If NIL, the default version (typically the
latest) is used.

UNKNOWN-OBJECT-EXTERNALIZER can be used to externalize objects for which Odo has
no built-in exteralization. If provided, it must be a function that takes three
required arguments: the object being externalized, the encoder, and the format,
as well as keyword arguments that may vary by the format being used. The
function may decline to return an externalized form of the object by returning
`NIL`. To actually have the externalized form be `NIL`, `(VALUES NIL T)` must be
returned.

When IGNORE-UNEXTERNALIZABLE-ANNOTATIONS is T, all annotations that cannot be
externalized are silently discarded. If :WARN, a warning is additionally
produced."
  (handler-bind ((odo:unknown-annotation-externalization
                   (lambda (c)
                     (when (and ignore-unexternalizable-annotations
                                (find-restart 'continue c))
                       (when (eql ignore-unexternalizable-annotations :warn)
                         (warn "Ignoring annotation ~S" (odo:unknown-annotation-externalization-key c)))
                       (invoke-restart 'continue)))))
    (externalize-object (make-encoder format encoder) format obj unknown-object-externalizer version)))


;; * Internalizing

(defgeneric internalize-object (decoder format object meta))

(defun odo:internalize (obj &key
                              (format :jsown)
                              (decoder :auto)
                              meta)
  "Internalize an object. Returns the Odo object represented by OBJ."
  (internalize-object (make-decoder format decoder) format obj meta))


;; * Serializing

(defgeneric serialize-object-to-stream (encoder format object stream unknown-object-externalizer version))

(defun odo:serialize-to-stream (obj stream
                                &key
                                  (format :json)
                                  (encoder :auto)
                                  version
                                  ignore-unexternalizable-annotations
                                  unknown-object-externalizer)
  "Write an Odo object to the provided stream in the chosen format with the
chosen encoder. Returns an opaque value that can be used by the deserializers to
make the deserialized object `ODO-EQUAL-P` to `OBJ`.

If the format is versioned and the encoder supports encoding multiple versions,
then VERSION specifies which to use. If NIL, the default version (typically the
latest) is used.

UNKNOWN-OBJECT-EXTERNALIZER can be used to externalize objects for which Odo has
no built-in exteralization. If provided, it must be a function that takes three
required arguments: the object being externalized, the encoder, and the format,
as well as keyword arguments that may vary by the format being used. The
function may decline to return an externalized form of the object by returning
`NIL`. To actually have the externalized form be `NIL`, `(VALUES NIL T)` must be
returned.

When IGNORE-UNEXTERNALIZABLE-ANNOTATIONS is T, all annotations that cannot be
externalized are silently discarded. If :WARN, a warning is additionally
produced."
  (handler-bind ((odo:unknown-annotation-externalization
                   (lambda (c)
                     (when (and ignore-unexternalizable-annotations
                                (find-restart 'continue c))
                       (when (eql ignore-unexternalizable-annotations :warn)
                         (warn "Ignoring annotation ~S" (odo:unknown-annotation-externalization-key c)))
                       (invoke-restart 'continue)))))
    (serialize-object-to-stream (make-encoder format encoder) format obj stream
                                unknown-object-externalizer
                                version)))

(defun odo:serialize-to-file (obj pathname
                              &rest keys
                              &key if-exists if-does-not-exist
                                external-format
                                format
                                (encoder :auto)
                                version
                                ignore-unexternalizable-annotations
                                unknown-object-externalizer)
  "Write an Odo object to file designated by `PATHNAME`. If `FORMAT` is not
provided, the format is guessed by the file extension. The currently supported
formats are:

+ `:JSON` (*.json)

Returns an opaque value that can be used by the deserializers to make the
deserialized object `ODO-EQUAL-P` to `OBJ`.

If the format is versioned and the encoder supports encoding multiple versions,
then VERSION specifies which to use. If NIL, the default version (typically the
latest) is used.

UNKNOWN-OBJECT-EXTERNALIZER can be used to externalize objects for which Odo has
no built-in exteralization. If provided, it must be a function that takes three
required arguments: the object being externalized, the encoder, and the format,
as well as keyword arguments that may vary by the format being used. The
function may decline to return an externalized form of the object by returning
`NIL`. To actually have the externalized form be `NIL`, `(VALUES NIL T)` must be
returned.

When IGNORE-UNEXTERNALIZABLE-ANNOTATIONS is T, all annotations that cannot be
externalized are silently discarded. If :WARN, a warning is additionally
produced."
  (declare (ignore if-exists if-does-not-exist external-format))
  (alex:remove-from-plistf keys :format :encoder)

  (unless format
    (setf format (guess-file-format pathname)))

  (apply #'uiop:call-with-output-file pathname
         (lambda (stream)
           (prog1 (odo:serialize-to-stream obj stream
                                           :format format
                                           :encoder encoder
                                           :version version
                                           :unknown-object-externalizer unknown-object-externalizer
                                           :ignore-unexternalizable-annotations ignore-unexternalizable-annotations)
             (fresh-line stream)))
         keys))

(defun odo:serialize-to-string (obj
                                &key
                                  (format :json)
                                  (encoder :auto)
                                  version
                                  ignore-unexternalizable-annotations
                                  unknown-object-externalizer)
  "Write an Odo object to a string in the chosen format with the chosen
encoder. Returns two values. The first is the string, the second is an opaque
value that can be used by the deserializers to make the deserialized object
`ODO-EQUAL-P` to `OBJ`.

If the format is versioned and the encoder supports encoding multiple versions,
then VERSION specifies which to use. If NIL, the default version (typically the
latest) is used.

UNKNOWN-OBJECT-EXTERNALIZER can be used to externalize objects for which Odo has
no built-in exteralization. If provided, it must be a function that takes three
required arguments: the object being externalized, the encoder, and the format,
as well as keyword arguments that may vary by the format being used. The
function may decline to return an externalized form of the object by returning
`NIL`. To actually have the externalized form be `NIL`, `(VALUES NIL T)` must be
returned.

When IGNORE-UNEXTERNALIZABLE-ANNOTATIONS is T, all annotations that cannot be
externalized are silently discarded. If :WARN, a warning is additionally
produced."
  (let ((second-value nil))
    (values
     (with-output-to-string (stream)
       (setf second-value
             (odo:serialize-to-stream obj stream
                                      :format format
                                      :encoder encoder
                                      :version version
                                      :unknown-object-externalizer unknown-object-externalizer
                                      :ignore-unexternalizable-annotations ignore-unexternalizable-annotations)))
     second-value)))



(defgeneric deserialize-object-from-stream (decoder format type stream meta)
  (:method :around ((decoder t) (format t) (type t) (stream t) (meta t))
    (let ((result (call-next-method)))
      (unless (or (null type)
                  (odo:type-p result type))
        (error "Deserialized object not of the correct type"))
      result)))

(defun odo:deserialize-from-stream (stream
                                    &key
                                      (format :json)
                                      (decoder :auto)
                                      (type t)
                                      meta)
  "Construct an Odo object from the provided stream in the chosen format with
the chosen decoder.

`:TYPE` is a symbol naming the expected Odo type of the returned object or
NIL. If NIL, the decoder will attempt to guess the correct type.

`:META` is the value returned by the encoder that contains metadata about the
encoding process. It is necessary if you want gensyms to be round tripped to be
EQL."
  (deserialize-object-from-stream (make-decoder format decoder) format type stream meta))

(defun odo:deserialize-from-file (pathname
                                  &rest keys
                                  &key if-exists if-does-not-exist
                                    external-format
                                    format
                                    (decoder :auto)
                                    (type t)
                                    meta)
  "Construct an Odo object from a file designated by `PATHNAME`. If `FORMAT` is
not provided, the format is guessed by the file extension. The currently
supported formats are:

+ `:JSON` (*.json)
+ `:FLATZINC` (*.fzn)
+ `:MINIZINC` (*.mzn)

`:TYPE` is a symbol naming the expected Odo type of the returned object or
NIL. If NIL, the decoder will attempt to guess the correct type.

`:META` is the value returned by the encoder that contains metadata about the
encoding process. It is necessary if you want gensyms to be round tripped to be
EQL."
  (declare (ignore if-exists if-does-not-exist external-format))
  (alex:remove-from-plistf keys :format :decoder :meta :type)

  (unless format
    (setf format (guess-file-format pathname)))

  (apply #'uiop:call-with-input-file pathname
         (lambda (stream)
           (odo:deserialize-from-stream stream :format format :decoder decoder :meta meta :type type))
         keys))

(defun odo:deserialize-from-string (string
                                    &key
                                      (format :json)
                                      (decoder :auto)
                                      (type t)
                                      meta)
  "Construct an Odo object from a string representation of it using the chosen
decoder.

`:TYPE` is a symbol naming the expected Odo type of the returned object or
NIL. If NIL, the decoder will attempt to guess the correct type.

`:META` is the value returned by the encoder that contains metadata about the
encoding process. It is necessary if you want gensyms to be round tripped to be
EQL."
  (with-input-from-string (stream string)
    (odo:deserialize-from-stream stream :format format :decoder decoder :meta meta :type type)))

(defgeneric odo:deserialize (thing &key format decoder type meta)
  (:documentation
   "Construct an Odo object from THING (a string, pathname, or stream).

`:TYPE` is a symbol naming the expected Odo type of the returned object or
NIL. If NIL, the decoder will attempt to guess the correct type.

`:META` is the value returned by the encoder that contains metadata about the
encoding process. It is necessary if you want gensyms to be round tripped to be
EQL."))

(defmethod odo:deserialize ((thing string) &rest args &key format decoder (type t) meta)
  (declare (ignore format decoder type meta))
  (apply #'odo:deserialize-from-string thing args))

(defmethod odo:deserialize ((thing stream) &rest args &key format decoder (type t) meta)
  (declare (ignore format decoder type meta))
  (apply #'odo:deserialize-from-stream thing args))

(defmethod odo:deserialize ((thing pathname)
                            &rest args
                            &key format decoder (type t) meta if-exists if-does-not-exist)
  (declare (ignore format decoder type meta if-exists if-does-not-exist))
  (apply #'odo:deserialize-from-file thing args))
