(in-package #:odo/io/jsown)

(defsection @odo/io/jsown/decoder (:title "JSOWN Decoder")
  "This section describes the JSOWN decoder for deserializing Odo objects from a
JSON stream.")


;; * Decoding state

(defvar *decoder* nil
  "Bound to the current decoder in use. Used to keep from having to pass around
references to the decoder everywhere.")

(defvar *schema-version*)

(defparameter *default-schema-version* "0.3-0")

(defparameter *max-schema-version* (cl-semver:read-version-from-string "0.4-0"))

(defparameter *min-schema-version* (cl-semver:read-version-from-string "0.3-0"))

(defvar *event-specs* nil)


;; * Version handling

(defmacro vcase (&body forms)
  (flet ((process-form (form)
           (let ((condition (first form))
                 (body (rest form)))
             (if (listp condition)
                 (destructuring-bind (comparator version) condition
                   (ecase comparator
                     (>=
                      `((cl-semver:version>= *schema-version*
                                             (cl-semver:read-version-from-string ,version))
                        ,@body))))
                 (progn
                   (assert (eql condition t))
                   `(t ,@body))))))
    `(cond
       ,@(mapcar #'process-form forms))))


;; * Decoder object

(defclass jsown-decoder ()
  ((id-map
    :accessor decoder-id-map)
   (id->obj-table
    :initform (make-hash-table :test 'equal :size 256)
    :reader decoder-id->obj-table)))

(odo/io::register-decoder :jsown 'jsown-decoder :json 0)
(odo/io::register-decoder :jsown 'jsown-decoder :jsown 0)

(defparameter *decoder-annotations* '(:odo.mtk/true-id))

(defun decoder-get-obj (id)
  (gethash id (decoder-id->obj-table *decoder*)))

(defun get-true-id (external-id namespace annotations)
  (or (odo/io::id-map-external-id->true-id (decoder-id-map *decoder*) namespace external-id nil nil)
      (getf annotations :odo.mtk/true-id)
      external-id
      (gensym)))



(defgeneric parse-object-by-type (obj type))

(defmethod parse-object-by-type :around ((obj t) type)
  (multiple-value-bind (obj json-id) (call-next-method)
    (when json-id
      (setf (gethash (list type json-id) (decoder-id->obj-table *decoder*))
            obj))
    obj))

(defmethod parse-object-by-type (obj (type (eql :boolean-domain)))
  (let ((value (jsown:val-safe obj "value")))
    (if value
        (odo:make-boolean-domain :value value)
        (odo:make-boolean-domain))))

(defmethod parse-object-by-type (obj (type (eql :constraint)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val obj "$id"))
           (id (get-true-id json-id :constraint decoder-annotations)))
      (values (odo:make-constraint (parse-expression (jsown:val obj "expression"))
                                   :name id
                                   :annotations annotations)
              json-id))))

(defmethod parse-object-by-type (obj (type (eql :enumerated-domain)))
  (odo:make-enumerated-domain :members (jsown:val obj "values")))

(defmethod parse-object-by-type (obj (type (eql :episode)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val obj "$id"))
           (id (get-true-id json-id :constraint decoder-annotations))
           (start-event (decoder-get-obj (list :event (jsown:val obj "startEvent"))))
           (end-event (decoder-get-obj (list :event (jsown:val obj "endEvent"))))
           (activity-name (jsown:val-safe obj "activityName"))
           (activity-args (jsown:val-safe obj "activityArgs")))
      (values (odo:make-episode :name id
                                :start-event start-event
                                :end-event end-event
                                :temporal-constraint (ana:awhen (jsown:val-safe obj "duration")
                                                       (parse-duration it start-event end-event))
                                :start-constraints (mapcar (alex:rcurry #'parse-object :state-constraint)
                                                           (jsown:val-safe obj "startConstraints"))
                                :end-constraints (mapcar (alex:rcurry #'parse-object :state-constraint)
                                                         (jsown:val-safe obj "endConstraints"))
                                :over-all-constraints (mapcar (alex:rcurry #'parse-object :state-constraint)
                                                              (jsown:val-safe obj "overAllConstraints"))
                                :once-constraints (mapcar (alex:rcurry #'parse-object :state-constraint)
                                                          (jsown:val-safe obj "onceConstraints"))
                                :activity-name activity-name
                                :activity-args (mapcar #'parse-expression activity-args)
                                :annotations annotations)
              json-id))))

(defmethod parse-object-by-type (obj (type (eql :event)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val-safe obj "$id"))
           (true-id (get-true-id json-id :state-space decoder-annotations))
           (domain (parse-object (jsown:val obj "domain")))
           (conditional-p (jsown:val-safe obj "conditional"))
           (uncontrollable-p (jsown:val-safe obj "uncontrollable"))
           (delayed-p (jsown:val-safe obj "delayed")))
      (values (odo:make-event
               :name (or true-id (gensym))
               :conditional-p conditional-p
               :uncontrollable-p uncontrollable-p
               :delayed-p delayed-p
               :type (if (odo:integer-domain-p domain)
                         'integer
                         'real)
               :ranges (mapcar (lambda (cell)
                                 (cons (first cell) (second cell)))
                               (odo:range-list domain))
               :annotations annotations)
              json-id))))

(defmethod parse-object-by-type (obj (type (eql :real-domain)))
  (let ((value (jsown:val-safe obj "value"))
        (ranges (mapcar #'parse-range (jsown:val-safe obj "ranges"))))
    (if value
        (odo:make-real-domain :min value :max value)
        (odo:make-real-domain :ranges ranges))))

(defmethod parse-object-by-type (obj (type (eql :state-constraint)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val-safe obj "$id"))
           (id (get-true-id json-id :constraint decoder-annotations)))
      (values (odo:make-state-constraint (parse-expression (jsown:val obj "expression"))
                                         :name id
                                         :annotations annotations)
              json-id))))

(defmethod parse-object-by-type (obj (type (eql :state-plan)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val-safe obj "$id"))
           (true-id (get-true-id json-id :state-plan decoder-annotations))
           (raw-features (jsown:val obj "$features"))
           (features (mapcar #'parse-feature raw-features))
           (state-space (parse-object (jsown:val obj "stateSpace") :state-space))
           (constraints (mapcar (alex:rcurry #'parse-object :constraint) (jsown:val obj "constraints")))
           goal-episodes
           value-episodes)
      (vcase
        ((>= "0.4-0")
         (setf goal-episodes (mapcar (alex:rcurry #'parse-object :episode) (jsown:val obj "goalEpisodes"))
               value-episodes (mapcar (alex:rcurry #'parse-object :episode) (jsown:val obj "valueEpisodes"))))
        (t
         (setf goal-episodes (mapcar (alex:rcurry #'parse-object :episode) (jsown:val obj "episodes")))))
      (values
       (ana:aprog1 (odo:make-state-plan
                    :name (or true-id (gensym))
                    :features features
                    :state-space state-space
                    :start-event (decoder-get-obj (list :event (jsown:val obj "startEvent")))
                    :constraints constraints
                    :goal-episodes goal-episodes
                    :value-episodes value-episodes
                    :annotations annotations))
       json-id))))

(defmethod parse-object-by-type (obj (type (eql :state-space)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (declare (ignore decoder-annotations))
    (let ((variables (mapcar #'parse-object (jsown:val obj "variables")))
          (events (mapcar #'parse-object (jsown:val obj "events")))
          (state-variables (mapcar #'parse-object (jsown:val obj "stateVariables"))))
      (odo:make-state-space
       :variables (append variables events state-variables)
       :annotations annotations))))

(defmethod parse-object-by-type (obj (type (eql :state-variable)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val-safe obj "$id"))
           (true-id (get-true-id json-id :state-space decoder-annotations))
           (range (parse-object (jsown:val obj "range")))
           (time-domain (parse-object (jsown:val obj "timeDomain"))))
      (values (odo:make-state-variable
               range
               :name (or true-id (gensym))
               :time-domain-or-spec time-domain
               :annotations annotations)
              json-id))))

(defmethod parse-object-by-type (obj (type (eql :variable)))
  (multiple-value-bind (annotations decoder-annotations)
      (parse-annotations obj)
    (let* ((json-id (jsown:val-safe obj "$id"))
           (true-id (get-true-id json-id :state-space decoder-annotations))
           (domain (parse-object (jsown:val obj "domain")))
           (conditional-p (jsown:val-safe obj "conditional"))
           (uncontrollable-p (jsown:val-safe obj "uncontrollable")))
      (values (if conditional-p
                  (odo:make-conditional-variable
                   domain
                   :name (or true-id (gensym))
                   :uncontrollable-p uncontrollable-p
                   :annotations annotations)
                  (odo:make-variable
                   domain
                   :name (or true-id (gensym))
                   :uncontrollable-p uncontrollable-p
                   :annotations annotations))
              json-id))))

(defun parse-object (obj &optional expected-type)
  (let* ((type-string (jsown:val-safe obj "$type"))
         (type (if type-string
                   (alex:make-keyword (camel-case-to-lisp (jsown:val obj "$type")))
                   expected-type)))
    (assert type)
    (parse-object-by-type obj type)))


;; * Parse durations

(defun parse-duration (obj start-event end-event)
  (let ((type (jsown:val obj "$type")))
    (alex:eswitch (type :test #'equal)
      ("simpleDuration"
       (odo:make-constraint (odo:simple-temporal start-event end-event
                                                 :lower-bound (parse-expression (jsown:val obj "lowerBound"))
                                                 :upper-bound (parse-expression (jsown:val obj "upperBound")))))
      ("simpleContingentDuration"
       (odo:make-constraint (odo:simple-temporal start-event end-event
                                                 :lower-bound (parse-expression (jsown:val obj "lowerBound"))
                                                 :upper-bound (parse-expression (jsown:val obj "upperBound")))
                        :contingent-p t)))))


;; * Parsing expressions

(defun parse-application (obj)
  (let* ((raw-type (jsown:val obj "$type"))
         (application-type (subseq raw-type 0 (- (length raw-type) 11))))
    (alex:eswitch (application-type :test #'equal)
      ("avoidsObstacles"
       (odo:avoids-obstacles (parse-expression (jsown:val obj "point"))
                             (parse-expression (jsown:val obj "map"))))
      ("equal"
       (odo:equal (parse-expression (jsown:val obj "left"))
                  (parse-expression (jsown:val obj "right"))))
      ("ge"
       (odo:ge (parse-expression (jsown:val obj "numbers"))))
      ("geomWithin"
       (odo:geom-within (parse-expression (jsown:val obj "point"))
                    (parse-expression (jsown:val obj "geom"))))
      ("le"
       (odo:le (parse-expression (jsown:val obj "numbers"))))
      ("linear>="
       (odo:linear->= (parse-expression (jsown:val obj "c"))
                      (parse-expression (jsown:val obj "x"))
                      (parse-expression (jsown:val obj "b"))))
      ("linear<="
       (odo:linear-<= (parse-expression (jsown:val obj "c"))
                      (parse-expression (jsown:val obj "x"))
                      (parse-expression (jsown:val obj "b"))))
      ("agentSeparation"
       (odo:agent-separation     (parse-expression (jsown:val obj "p1"))
                                 (parse-expression (jsown:val obj "p2"))
                                 :max (or (parse-expression (jsown:val obj "max")) odo:+infinity+)
                                 :min (or (parse-expression (jsown:val obj "min")) odo:+-infinity+)))
      ("simpleTemporal"
       (odo:simple-temporal (parse-expression (jsown:val obj "from"))
                            (parse-expression (jsown:val obj "to"))
                            :lower-bound (or (parse-expression (jsown:val obj "lowerBound")) odo:+-infinity+)
                            :upper-bound (or (parse-expression (jsown:val obj "upperBound")) odo:+infinity+)))
      ("stateVar"
       (assert (jsown:val obj "currentTime"))
       (odo:@ (decoder-get-obj (list :state-variable (jsown:val obj "stateVar"))) odo:+current-time+)))))

(defun parse-expression (obj)
  (cond
    ((and (listp obj)
          (eql :obj (first obj)))
     (let ((var-name (jsown:val-safe obj "ref")))
       (cond
         ((alex:ends-with-subseq "Application" (jsown:val-safe obj "$type"))
          (parse-application obj))
         (var-name
          (or (gethash (list :event var-name) (decoder-id->obj-table *decoder*))
              (gethash (list :variable var-name) (decoder-id->obj-table *decoder*))
              (gethash (list :state-variable var-name) (decoder-id->obj-table *decoder*)))))))
    ((listp obj)
     (mapcar #'parse-expression obj))
    ((equal obj "Infinity")
     odo:+infinity+)
    ((equal obj "-Infinity")
     odo:+-infinity+)
    ((numberp obj)
     obj)
    ((stringp obj)
     obj)
    ((eql obj t)
     t)
    (t
     (error "Unknown: ~S" obj))))


;; * Utils

(defun parse-feature (f)
  (alex:switch (f :test #'equal)
    ("activities"
     :activities)
    ("chanceConstraints"
     :chance-constraints)))

(defun parse-range (r)
  (let* ((raw-bounds (jsown:val r "bounds"))
         (max-closed (jsown:val r "maxClosed"))
         (min-closed (jsown:val r "minClosed")))
    (destructuring-bind (min max) raw-bounds
      `(,(if (equal min "-Infinity") `(,odo:+-infinity+ . nil) `(,min . ,min-closed)) .
        ,(if (equal max "Infinity") `(,odo:+infinity+ . nil) `(,max . ,max-closed))))))

(defgeneric parse-annotation-value (true-id))

(defmethod parse-annotation-value ((true-id real))
  true-id)

(defmethod parse-annotation-value ((true-id string))
  true-id)

(defmethod parse-annotation-value ((obj (eql t)))
  obj)

(defmethod parse-annotation-value ((obj (eql nil)))
  obj)

(defmethod parse-annotation-value ((obj list))
  (if (eql (first obj) :obj)
      (let ((type (jsown:val obj "$type")))
        (cond
          ((equal type "symbol")
           (let* ((symbol-name-verbatim-p (jsown:val-safe obj "isSymbolNameVerbatim"))
                  (symbol-name (if symbol-name-verbatim-p
                                   (jsown:val obj "symbolName")
                                   (uiop:standard-case-symbol-name (jsown:val obj "symbolName"))))
                  (package-name-verbatim-p (jsown:val-safe obj "isPackageNameVerbatim"))
                  (raw-package-name (jsown:val-safe obj "packageName"))
                  (package-name (cond
                                  (package-name-verbatim-p
                                   raw-package-name)
                                  (raw-package-name
                                   (uiop:standard-case-symbol-name raw-package-name))
                                  (t
                                   nil))))
             (if package-name
                 (intern symbol-name (find-package package-name))
                 (make-symbol symbol-name))))
          (t
           (ana:aprog1 (make-hash-table :test 'equal
                                        :size (1- (length obj)))
             (jsown:do-json-keys (key val) obj
               (setf (gethash key it) (parse-annotation-value val)))))))
      (mapcar #'parse-annotation-value obj)))

(defun parse-annotations (obj)
  (let ((annotations nil)
        (decoder-annotations nil))
    (jsown:do-json-keys (key val) (jsown:val-safe obj "$annotations")
      (let ((key (alex:make-keyword (camel-case-to-lisp key)))
            (val (parse-annotation-value val)))
        (if (member key *decoder-annotations*)
            (progn
              (push val decoder-annotations)
              (push key decoder-annotations))
            (progn
              (push val annotations)
              (push key annotations)))))
    (values annotations decoder-annotations)))



(defmethod odo/io::deserialize-object-from-stream ((decoder jsown-decoder) (format (eql :json)) type stream meta)
  (declare (ignore type))
  (let* ((string (alex:read-stream-content-into-string stream))
         (obj (jsown:parse string)))
    (internalize-object decoder :jsown obj meta)))

(defmethod internalize-object ((decoder jsown-decoder) (format (eql :jsown)) obj meta)
  (if meta
      (setf (decoder-id-map decoder) (odo/io::copy-id-map meta))
      (setf (decoder-id-map decoder) (odo/io::make-id-map)))
  (let ((*decoder* decoder)
        (*schema-version* (cl-semver:read-version-from-string (jsown:val obj "$version"))))
    (unless (and (cl-semver:version<= *schema-version* *max-schema-version*)
                 (cl-semver:version>= *schema-version* *min-schema-version*))
      (error "Unsopported schema version ~A" (cl-semver:print-version-to-string *schema-version*)))
    (parse-object obj nil)))
