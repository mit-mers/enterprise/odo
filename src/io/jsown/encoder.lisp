(in-package #:odo/io/jsown)

(defsection @odo/io/jsown/encoder (:title "JSOWN Encoder")
  "This section describes the JSOWN encoder for serializing Odo objects to a
JSON stream.")


;; * Encoding state

(defvar *encoder* nil
  "Bound to the current encoder in use. Used to keep from having to pass around
references to the encoder everywhere.")

(defvar *format* nil
  "Bound to the current format.")

(defvar *unknown-object-externalizer* nil
  "Bound to the current user provided function for externalizing unknown
objects.")

(defvar *top-level-object-p* t
  "T when the current object being processed is the object for which
`SERIALIZE-OBJECT-TO-STREAM` was called.")

(defvar *encoding-annotation* nil
  "When encoding an annotation, this is set to the key of the annotation. Used
by ENCODE-UNKNOWN-OBJECT to signal the correct event.")

(defvar *schema-version*)

(defparameter *default-schema-version* "0.3-0")

(defparameter *max-schema-version* (cl-semver:read-version-from-string "0.4-0"))

(defparameter *min-schema-version* (cl-semver:read-version-from-string "0.3-0"))


;; * Unknown objects

(define-condition unknown-jsown-object-externalization (odo:unknown-object-externalization)
  ((path
    :initarg :path
    :reader unknown-jsown-object-externalization-path))
  (:report
   (lambda (c s)
     (format s "Unable to externalize object to a jsown object at path ~S:~%~S"
             (unknown-jsown-object-externalization-path c)
             (odo:unknown-object-externalization-object c))))
  (:documentation
   "Signals that an object cannot be converted to jsown format."))

(define-condition unknown-jsown-annotation-externalization (unknown-jsown-object-externalization
                                                            odo:unknown-annotation-externalization)
  ()
  (:documentation
   "Signals that an object cannot be converted to jsown format as an annotation."))

(defun encode-unknown-object (object path)
  (restart-case
      (flet ((signal-error ()
               (if *encoding-annotation*
                   (error 'unknown-jsown-annotation-externalization
                          :path path
                          :pbject object
                          :key *encoding-annotation*)
                   (error 'unknown-jsown-object-externalization
                          :path path
                          :object object))))
        (if *unknown-object-externalizer*
            (multiple-value-bind (result actually-nil-p)
                (funcall *unknown-object-externalizer*
                         object *encoder* *format* :path path)
              (if (or result actually-nil-p)
                  result
                  (signal-error))
              (signal-error))
            (signal-error)))
    (use-value (value)
      :report "Use a value"
      :interactive (lambda ()
                     (format *query-io* "Enter a value (unevaluated): ")
                     (force-output *query-io*)
                     (list (read *query-io*)))
      value)))


;; * Encoder object

(defclass jsown-encoder ()
  ((id-map
    :accessor encoder-id-map)))

(odo/io::register-encoder :jsown 'jsown-encoder :json 0)
(odo/io::register-encoder :jsown 'jsown-encoder :jsown 0)

(defun get-object-id (object)
  (odo/io::id-map-obj->external-id (encoder-id-map *encoder*) object))

(defgeneric annotation-value (value path))

(defmethod annotation-value ((value real) (path t))
  value)

(defmethod annotation-value ((value string) (path t))
  value)

(defmethod annotation-value ((value (eql nil)) (path t))
  :false)

(defmethod annotation-value ((value (eql t)) (path t))
  t)

(defmethod annotation-value ((value symbol) (path t))
  (let* ((symbol-name (symbol-name value))
         (symbol-package (symbol-package value))
         (package-name (when symbol-package (package-name symbol-package))))
    ;; The vast majority of the time, we'll be working with the normal read
    ;; case and not using escapes to cause some part of the names to be lower
    ;; case. To make things easier to read, we try to downcase the names and
    ;; then upcase them again when reading. But if our assumptions don't hold,
    ;; we'll add a flag to the object to say we shouldn't perform the upcase.
    (ana:aprog1 (jsown:new-js
                  ("$type" "symbol"))

      (if (equal (string-upcase symbol-name) symbol-name)
          (jsown:extend-js it
            ("symbolName" (string-downcase symbol-name)))
          (jsown:extend-js it
            ("symbolName" symbol-name)
            ("isSymbolNameVerbatim" t)))

      (when package-name
        (if (equal (string-upcase package-name) package-name)
            (jsown:extend-js it
              ("packageName" (string-downcase package-name)))
            (jsown:extend-js it
              ("packageName" package-name)
              ("isPackageNameVerbatim" t)))))))

(defmethod annotation-value ((value list) path)
  (mapcar (alex:rcurry #'annotation-value path) value))

(defmethod annotation-value ((value t) path)
  (encode-unknown-object value path))



(defun to-jsown-with-counter (path)
  (let ((x -1))
    (lambda (y)
      (to-jsown y :path (list* (incf x) path)))))

(defmethod to-jsown (obj &key path)
  "Given an Odo object, return a JSOWN object representing it using the current
`*ENCODER*`."
  (to-jsown-by-type obj (odo:type obj) :path path))

(defgeneric to-jsown-by-type (obj type &key path)
  (:documentation
   "Given an Odo object and the Odo type of this object, return a JSOWN object
representing it using the current `*ENCODER*`."))

;; Needed because compile time calls to this function exist in the same file.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun process-json-body-form (out-name form)
    (case (first form)
      ((when unless)
       (list* (first form) (second form)
              (mapcar (alex:curry #'process-json-body-form out-name) (rest (rest form)))))
      (vcase
        `(vcase
           ,@(mapcar (lambda (x) (list* (first x)
                                        (mapcar (alex:curry #'process-json-body-form out-name) (rest x))))
                     (rest form))))
      (t
       (list 'jsown:extend-js out-name form)))))

(defmacro define-jsown-encoder (type (self-name &key ((:path path-name) (gensym))) json-body &rest options)
  (let* ((toplevel-p-name (gensym "TOPLEVEL-P"))

         (id-pair (find :id json-body :key #'first))
         (id-form (second id-pair))
         (id-context (second (find :id-context options :key #'first)))
         (id-test (ecase id-context
                    (:toplevel toplevel-p-name)
                    ((nil) t)))
         (true-id-name (gensym "TRUE-ID"))
         (external-id-name (gensym "ID"))

         (type-pair (find :type json-body :key #'first))
         (type-form (second type-pair))

         (local-annotations-name (gensym "LOCAL-ANNOTATIONS"))

         (json-body-no-id-or-type (remove id-pair (remove type-pair json-body)))

         (out-name (gensym)))
    `(defmethod to-jsown-by-type (,self-name (,(gensym) ,type) &key ((:path ,path-name)))
       (let* ((,toplevel-p-name *top-level-object-p*)
              (*top-level-object-p* nil)
              (,local-annotations-name nil))
         (declare (ignorable ,toplevel-p-name))
         (let ((,out-name (jsown:new-js)))
           ,@(when id-form
               `((when ,id-test
                   (let ((,true-id-name ,id-form)
                         (,external-id-name (odo/io::id-map-obj->external-id (encoder-id-map *encoder*)
                                                                             ,self-name)))
                     (jsown:extend-js ,out-name ("$id" ,external-id-name))
                     (unless (equal ,true-id-name ,external-id-name)
                       (push ,true-id-name ,local-annotations-name)
                       (push :odo.mtk/true-id ,local-annotations-name))))))
           (jsown:extend-js ,out-name
             ,@(when type-form `(("$type" ,type-form))))

           ,@(mapcar (alex:curry #'process-json-body-form out-name) json-body-no-id-or-type)

           (let ((annotation-plist (append (odo:annotation-plist ,self-name)
                                           ,local-annotations-name)))
             (when annotation-plist
               (setf (jsown:val ,out-name "$annotations")
                     (plist-to-jsown annotation-plist (list* :$annotations ,path-name)))))
           ,out-name)))))

(define-jsown-encoder odo:constraint (self :path path)
  ((:id (odo:name self))
   (:type "constraint")

   ("expression" (expression-to-jsown (odo:expression self)
                                      :eval-type 'boolean
                                      :path (list* :expression path)))))

(define-jsown-encoder odo:episode (self :path path)
  ((:id (odo:name self))
   (:type "episode")

   ("startEvent" (get-object-id (odo:start-event self)))
   ("endEvent" (get-object-id (odo:end-event self)))
   (vcase
     ((>= "0.4-0")
      (when (odo:conditional-p self)
        ("conditional" t))))
   (when (odo:temporal-constraint self)
     ("duration" (duration-to-jsown (odo:temporal-constraint self))))
   (when (odo:start-constraints self)
     ("startConstraints" (mapcar (to-jsown-with-counter (list* :start-constraints path))
                                 (odo:start-constraints self))))
   (when (odo:end-constraints self)
     ("endConstraints" (mapcar (to-jsown-with-counter (list* :end-constraints path))
                               (odo:end-constraints self))))
   (when (odo:over-all-constraints self)
     ("overAllConstraints" (mapcar (to-jsown-with-counter (list* :over-all-constraints path))
                                   (odo:over-all-constraints self))))
   (when (odo:once-constraints self)
     ("onceConstraints" (mapcar (to-jsown-with-counter (list* :once-constraints path))
                                (odo:once-constraints self))))
   (when (odo:activity-name self)
     ("activityName" (odo:activity-name self))
     ("activityArgs" (expression-to-jsown (odo:activity-args self)
                                          :path (list* :activity-args path))))))

(define-jsown-encoder odo:event (self :path path)
  ((:id (odo:name self))
   (:type "event")

   ("domain" (domain-to-jsown self :path (list* :domain path)))
   (when (odo:uncontrollable-p self)
     ("uncontrollable" t))))

(define-jsown-encoder odo:state-constraint (self :path path)
  ((:id (odo:name self))
   (:type "stateConstraint")

   ("expression" (expression-to-jsown (odo:expression self)
                                      :path (list* :expression path)
                                      :eval-type 'boolean)))

  (:id-context :toplevel))

(define-jsown-encoder odo:state-plan (self :path path)
  ((:id (odo:name self))
   (:type "statePlan")

   ("$features"
    (mapcar #'state-plan-feature-to-string (odo:features self)))
   ("stateSpace"
    (to-jsown (odo:state-space self) :path (list* :state-space path)))
   ("startEvent"
    (get-object-id (odo:start-event self)))
   ("constraints"
    (mapcar (to-jsown-with-counter (list* :constraints path))
            (odo:constraint-list self)))
   (vcase
     ((>= "0.4-0")
      ("goalEpisodes"
       (mapcar (to-jsown-with-counter (list* :episodes path))
               (odo:goal-episode-list self)))
      ("valueEpisodes"
       (mapcar (to-jsown-with-counter (list* :episodes path))
               (odo:value-episode-list self))))
     (t
      ("episodes"
       (mapcar (to-jsown-with-counter (list* :episodes path))
               (odo:goal-episode-list self)))))))

(define-jsown-encoder odo:state-space (self :path path)
  ((:type "stateSpace")

   ;; This is a bit wasteful, but works for now. Consider adding functions like
   ;; STATE-SPACE-EVENTS, STATE-SPACE-VARIABLES, etc. Not doing it now as we
   ;; need to consider if STATE-SPACE-VARIABLES whould also return events.
   ("events"
    (mapcar (to-jsown-with-counter (list* :events path))
            (remove-if-not (lambda (x)
                             (odo:type-p x 'odo:event))
                           (odo:state-space-list self))))
   ("stateVariables"
    (mapcar (to-jsown-with-counter (list* :state-variables path))
            (remove-if-not (lambda (x)
                             (odo:type-p x 'odo:state-variable))
                           (odo:state-space-list self))))

   ("variables"
    (mapcar (to-jsown-with-counter (list* :variables path))
            (remove-if-not (lambda (x)
                             (and (odo:type-p x 'odo:variable)
                                  (not (odo:type-p x 'odo:event))))
                           (odo:state-space-list self))))))

(define-jsown-encoder odo:state-variable (self :path path)
  ((:id (odo:name self))
   (:type "stateVariable")

   ("timeDomain" (domain-to-jsown (odo:state-variable-time-domain self)
                                  :path (list* :time-domain path)))
   ("range" (domain-to-jsown (odo:range self)
                             :path (list* :range path)))))

(define-jsown-encoder odo:variable (self :path path)
  ((:id (odo:name self))
   (:type "variable")

   ("domain" (domain-to-jsown self :path (list* :domain path)))))


;; * Private Helpers

(defun maybe-infinity (num)
  (cond
    ((eql num odo:+-infinity+)
     "-Infinity")
    ((eql num odo:+infinity+)
     "Infinity")
    (t
     num)))

(defun domain-to-jsown (dom &key path)
  (etypecase (odo:domain-type dom)
    (odo:pure-boolean-domain
     (let ((domain-js (jsown:new-js
                        ("$type" "booleanDomain"))))
       (when (odo:assigned-p dom)
         (jsown:extend-js domain-js
           ("value" (if (odo:assigned-value dom)
                        t
                        :false))))
       domain-js))
    (odo:pure-integer-domain
     (let ((domain-js (jsown:new-js
                        ("$type" "integerDomain"))))
       (if (odo:assigned-p dom)
           (jsown:extend-js domain-js
             ("value" (odo:assigned-value dom)))
           (let ((ranges (odo:range-list dom)))
             (jsown:extend-js domain-js
               ("ranges" (mapcar #'range-to-jsown ranges)))))
       domain-js))
    (odo:pure-discrete-domain
     (let* ((values (odo:value-list dom))
            (counter -1))
       (jsown:new-js
         ("$type" "enumeratedDomain")
         ("values" (mapcar (lambda (v)
                             (incf counter)
                             (cond
                               ((or (stringp v)
                                    (numberp v)
                                    (eql v t))
                                v)
                               ((eql v nil)
                                :false)
                               ((listp v)
                                (error "Do not know how to translate lists at the moment."))
                               (t
                                (encode-unknown-object v (list* counter :values path)))))
                           values)))))
    (odo:pure-real-domain
     (let ((domain-js (jsown:new-js
                        ("$type" "realDomain"))))
       (if (odo:assigned-p dom)
           (jsown:extend-js domain-js
             ("value" (odo:assigned-value dom)))
           (let ((ranges (odo:range-list dom)))
             (jsown:extend-js domain-js
               ("ranges" (mapcar #'range-to-jsown ranges)))))
       domain-js))))

(defun duration-to-jsown (expr)
  (assert (odo:type-p expr 'odo:constraint))
  (let ((contingent-p (odo:type-p expr 'odo:contingent-constraint)))
    (let ((expr (odo:expression expr)))
      (ecase (odo:operator expr)
        (odo:simple-temporal
         (let ((lb (odo:argument-value expr 'odo:lower-bound))
               (ub (odo:argument-value expr 'odo:upper-bound)))
           (jsown:new-js
             ("$type" (if contingent-p
                          "simpleContingentDuration"
                          "simpleDuration"))
             ("lowerBound" (maybe-infinity lb))
             ("upperBound" (maybe-infinity ub)))))))))

(defun expression-to-jsown (expr &key (eval-type t) path)
  (expression-to-jsown-by-type expr (odo:type expr) :eval-type eval-type
                                                    :path path))

(defun plist-to-jsown (plist path)
  (ana:aprog1 (jsown:new-js)
    (loop
      :for key :in plist :by #'cddr
      :for value :in (rest plist) :by #'cddr
      :do
         (restart-case
             (let ((*encoding-annotation* key))
               (setf (jsown:val it
                                (lisp-to-camel-case (symbol-name key)))
                     (annotation-value value (list* key path))))
           (continue ()
             :report "Ignore and do not externalize annotation")))))


;; * Utils

(defun state-plan-feature-to-string (feature)
  (ecase feature
    (:activities
     "activities")
    (:chance-constraints
     "chanceConstraints")))

(defun jsown-bool (val)
  (if val t :false))

(defun range-to-jsown (range)
  (destructuring-bind (min-val max-val min-closed-p max-closed-p) range
    (jsown:new-js
      ("bounds" (list (maybe-infinity min-val)
                      (maybe-infinity max-val)))
      ("maxClosed" (jsown-bool max-closed-p))
      ("minClosed" (jsown-bool min-closed-p)))))




(defun known-application-to-jsown (function-def expr path)
  (let ((operator-name (symbol-name (odo:operator expr))))
    (when (alex:starts-with-subseq "odo-" operator-name :test #'equalp)
      (setf operator-name (subseq operator-name 4)))
    (ana:aprog1 (jsown:new-js
                  ("$type" (concatenate 'string
                                        (lisp-to-camel-case operator-name)
                                        "Application")))
      (multiple-value-bind (required-params optional-params rest-param keyword-params
                            allow-other-keys-p aux-params)
          (alex:parse-ordinary-lambda-list (odo/model-api::odo-function-lambda-list function-def))
        (declare (ignore allow-other-keys-p))
        ;; Optional gives me a headache right now
        (assert (null optional-params))
        (assert (null aux-params))
        (dolist (param (mapcar (lambda (x) (find-symbol (string x) :odo))
                               (append required-params (when rest-param (list rest-param)))))
          (setf (jsown:val it (lisp-to-camel-case (symbol-name param)))
                (expression-to-jsown (odo:argument-value expr param)
                                     :eval-type (odo/model-api::odo-function-argument-type function-def param)
                                     :path (list* (alex:make-keyword (symbol-name param)) path))))
        (dolist (param keyword-params)
          (destructuring-bind (keyword-name arg-name) (first param)
            (let ((arg-name (find-symbol (string arg-name) :odo)))
              (setf (jsown:val it (lisp-to-camel-case (symbol-name keyword-name)))
                    (expression-to-jsown (odo:argument-value expr arg-name)
                                         :eval-type (odo/model-api::odo-function-argument-type function-def arg-name)
                                         :path (list* keyword-name path))))))))))

(defun state-var-eval-to-jsown (expr)
  (let* ((state-var (odo:operator expr))
         (args (odo:arguments expr))
         (time (first args)))
    (assert (alex:length= 1 args))
    (assert (eql time odo:+current-time+))
    (jsown:new-js
      ("$type" "stateVarApplication")
      ("stateVar" (get-object-id state-var))
      ("currentTime" t))))

(defgeneric expression-to-jsown-by-type (expr expr-type &key eval-type path))

(defmethod expression-to-jsown-by-type (expr (expr-type odo:application-expression)
                                        &key eval-type path)
  (declare (ignore eval-type))
  (let* ((operator (odo:operator expr)))
    (cond
      ((odo:state-variable-p (odo:operator expr))
       (state-var-eval-to-jsown expr))
      ((odo/model-api::symbol-odo-function operator nil)
       (known-application-to-jsown (odo/model-api::symbol-odo-function operator nil) expr path))
      (t
       (jsown:new-js
         ("operator" operator)
         ("args" (expression-to-jsown (odo:arguments expr)
                                      :path (list* :args path))))))))

(defmethod expression-to-jsown-by-type (variable (type odo:variable)
                                        &key eval-type path)
  (declare (ignore eval-type path))
  (jsown:new-js
    ("ref" (get-object-id variable))))

(defmethod expression-to-jsown-by-type (variable (type odo:state-variable)
                                        &key eval-type path)
  (declare (ignore eval-type path))
  (jsown:new-js
    ("ref" (get-object-id variable))))

(defmethod expression-to-jsown-by-type (expr (type (eql nil))
                                        &key eval-type path)
  (cond
    ((listp expr)
     (let ((counter -1))
       (mapcar (lambda (x)
                 (expression-to-jsown x :eval-type eval-type
                                        :path (list* (incf counter) path)))
               expr)))
    ((stringp expr)
     expr)
    ((vectorp expr)
     (let ((counter -1))
       (map 'list (lambda (x)
                    (expression-to-jsown x :eval-type eval-type
                                           :path (list* (incf counter) path)))
            expr)))
    ((or (numberp expr) (odo:symbolic-infinity-p expr))
     (maybe-infinity expr))
    ((keywordp expr)
     (format nil ":~A" (string-downcase (symbol-name expr))))
    ((eql eval-type 'boolean)
     (jsown-bool expr))
    ((eql expr t)
     t)
    (t
     (encode-unknown-object expr path))))


(defmethod odo/io::serialize-object-to-stream ((self jsown-encoder) (format (eql :json)) obj stream
                                               unknown-object-externalizer
                                               version)
  "Serialize an object to json using the jsown encoder. Unfortunately jsown
doesn't serialize directly to a stream, so we have to create a string and then
write that to the stream."
  (multiple-value-bind (jsown meta)
      (externalize-object self :jsown obj unknown-object-externalizer version)
    (write-string (jsown:to-json jsown) stream)
    meta))

(defun version-to-string-without-patch (version)
  (format nil "~A.~A~@[-~A~]"
          (cl-semver:version-major version)
          (cl-semver:version-minor version)
          (cl-semver:version-pre-release version)))

(defmethod externalize-object ((self jsown-encoder) (format (eql :jsown)) obj
                               unknown-object-externalizer
                               version)
  (setf (encoder-id-map self) (odo/io::compute-id-map obj))
  (let* ((*encoder* self)
         (*format* format)
         (*unknown-object-externalizer* unknown-object-externalizer)
         (*schema-version* (cl-semver:read-version-from-string
                            (or version *default-schema-version*)))
         jsown)
    (unless (and (cl-semver:version<= *schema-version* *max-schema-version*)
                 (cl-semver:version>= *schema-version* *min-schema-version*))
      (error "Unsopported schema version ~A" (cl-semver:print-version-to-string *schema-version*)))
    (setf jsown (to-jsown obj))
    ;; Add in the schema version used.
    (jsown:extend-js jsown
      ("$version" (version-to-string-without-patch *schema-version*)))
    (values jsown (encoder-id-map self))))
