(uiop:define-package #:odo/io/jsown
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:import-from #:anaphora #:it)
  (:local-nicknames (#:alex #:alexandria)
                    (#:ana #:anaphora)))
