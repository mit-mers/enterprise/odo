(in-package #:odo/io)

(defsection @odo/io (:title "I/O APIs"
                     :package-symbol :odo)
  "This section describes the bulk of Odo's I/O functionality."
  (@odo/io/csp section)
  (@odo/io/defs section))
