(in-package #:odo/io)

(defsection @odo/io/csp (:title "CSP I/O")
  "Input/output for CSPs."
  "#### Writing ####"
  (odo:write-csp-to-stream function)
  (odo:write-csp-to-file function)
  (odo:write-flatzinc-solution function)
  "#### Reading ####"
  (odo:read-csp-from-file function))

(defun print-names-to-list (l)
  (flet ((w (obj)
           (with-output-to-string (s)
             (write obj :stream s))))
    (mapcar #'w l)))

(defun check-duplicate-names (csp)
  (let ((vars (print-names-to-list (mapcar #'odo:name
                                           (odo:state-space-list csp))))
        (constraints (print-names-to-list (mapcar #'odo:name
                                                  (odo:constraint-list csp)))))
    (or (not (alex:length= vars (remove-duplicates vars
                                                   :test #'equal)))
        (not (alex:length= constraints (remove-duplicates constraints
                                                          :test #'equal))))))

(defun odo:write-csp-to-stream (csp stream
                                &key
                                  (format :latex)
                                  (on-similar-name :error))
  "Write the `CSP` to `STREAM` in the provided `FORMAT`.

+ `FORMAT` is required. The currently supported formats are:

    + `:LATEX`

If the `:LATEX` output is used, the output is not guaranteed
to cover all forms of variable / constraints.

+ `ON-SIMILAR-NAME` specifies what to do if variable or constraint
names become indistinguishable in the selected format. It can be one
of `:ERROR`."
  (check-type on-similar-name (member :error))
  (ecase format
    (:latex
     (let ((*print-pretty* nil))
       (when (check-duplicate-names csp)
         (error "Names will become indistinguishable in file!"))
       (odo/io/latex/csp2latex:latex-print-csp stream csp))))

  (values))

(defun odo:write-csp-to-file (csp pathname
                              &rest keys
                              &key if-exists if-does-not-exist
                                external-format
                                format
                                (on-similar-name :error))
  "Write the `CSP` to file designated by `PATHNAME`. If `FORMAT`
is not provided, the format is guessed by the file extension. The
currently supported formats are:

+ `:LATEX` (*.tex)

If the `:LATEX` output is used, the output is not guaranteed to cover all forms
of variable / constraints.

+ `IF-EXISTS`, `IF-DOES-NOT-EXIST`, and `EXTERNAL-FORMAT` are passed
to `OPEN`.

+ `ON-SIMILAR-NAME` specifies what to do if variable or constraint
names become indistinguishable in the selected format. It can be one
of `:ERROR`."
  (declare (ignore if-exists if-does-not-exist external-format
                   on-similar-name))
  (alex:remove-from-plistf keys format)
  (let* ((write-args (alex:remove-from-plist keys
                                             :if-exists :if-does-not-exist
                                             :external-format))
         (open-args keys))
    (loop :for key :in write-args :by #'cddr
          :do (alex:remove-from-plistf open-args key))
    (unless format
      (setf format (guess-file-format pathname)))
    (apply #'uiop:call-with-output-file pathname
           #'(lambda (stream)
               (apply #'odo:write-csp-to-stream csp stream :format format
                      write-args)
               (terpri stream))
           open-args)))

(odo/utils:with-deprecation (:style-warning "0.5")
  (defun odo:read-csp-from-file (pathname &key file-format mzn2fzn
                                            data-pathname)
    "DEPRECATED. Use `DESERIALIZE-FROM-FILE` instead.

Return a CSP that is read from the file designated by
`PATHNAME`. If `FILE-FORMAT` is not provided, the format is guessed by
the file extension. The currently supported formats are:

+ `:FLATZINC` (*.fzn)
+ `:MINIZINC` (*.mzn)

If the minizinc parser is used, `MZN2FZN` specifies where to find the
`mzn2fzn` program. This is only needed if it is not on the PATH.

If the minizinc parser is used, `DATA-PATHNAME`, if provided, is a
pathname to a data file to be used during flattening."
    (let* ((file-format (or file-format (guess-file-format pathname)))
           (decoder (ecase file-format
                      (:flatzinc :flatzinc)
                      (:minizinc (list :minizinc :data-pathname data-pathname :minizinc (or mzn2fzn "minizinc"))))))
      (odo:deserialize-from-file pathname
                                 :format :file-format
                                 :decoder decoder
                                 :type 'odo:csp))))
