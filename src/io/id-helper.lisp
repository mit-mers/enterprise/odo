(in-package #:odo/io)

(defstruct (id-map (:copier %copy-id-map))
  (obj->external-id-map (make-hash-table))
  (external-id->true-id-map (make-hash-table :test 'equal)))

(defun copy-id-map (id-map)
  (make-id-map
   :obj->external-id-map (alex:copy-hash-table (id-map-obj->external-id-map id-map))
   :external-id->true-id-map (ana:aprog1 (make-hash-table :test 'equal)
                               (maphash
                                (lambda (k v)
                                  (setf (gethash k it) (copy-alist v)))
                                (id-map-external-id->true-id-map id-map)))))

(defun id-map-record-object! (id-map obj true-id external-id namespace)
  (setf (gethash obj (id-map-obj->external-id-map id-map))
        (list external-id
              (not (equal true-id external-id)))

        (alex:assoc-value (gethash external-id (id-map-external-id->true-id-map id-map))
                          namespace)
        true-id))

(defun id-map-obj->external-id (id-map obj)
  "Given an object, return the external ID used to represent it in the
serialized form."
  (multiple-value-bind (external-id exists-p) (gethash obj (id-map-obj->external-id-map id-map))
    (assert exists-p)
    (first external-id)))

(defun id-map-obj-external-id-different-p (id-map obj)
  "Returns T iff OBJ's external ID is different from its true id."
  (multiple-value-bind (external-id exists-p) (gethash obj (id-map-obj->external-id-map id-map))
    (assert exists-p)
    (second external-id)))

(defun id-map-external-id->true-id (id-map namespace id &optional (missing-error-p t) missing-value)
  "Given an external ID and a namespace, return the true ID of the object it
corresponds to."
  (flet ((does-not-exist ()
           (when missing-error-p
             (error "No such id in namespace"))
           missing-value))
    (multiple-value-bind (true-ids exists-p) (gethash id (id-map-external-id->true-id-map id-map))
      (if exists-p
          (let ((cell (assoc namespace true-ids)))
            (if cell
                (cdr cell)
                (does-not-exist)))
          (does-not-exist)))))

(defun create-incremented-id (base-id counter)
  "Given a base id, return a new id using the current value of counter. Noop if
counter is zero."
  (if (zerop counter)
      base-id
      (format nil "~A~~~A" base-id counter)))

(defun create-base-external-id (true-id)
  "Given a true id, create the ideal external id."
  (cond
    ((realp true-id)
     true-id)
    ((symbolp true-id)
     (string-downcase (symbol-name true-id)))
    (t
     (format nil "~A" true-id))))

(defun create-external-id (id-map namespace true-id)
  "Create a unique id within namesace based on a true-id."
  (loop
    :with base-external-id := (create-base-external-id true-id)
    :for counter :upfrom 0
    :for external-id := (create-incremented-id base-external-id counter)
    :for true-ids := (gethash external-id (id-map-external-id->true-id-map id-map))
    :unless (alex:assoc-value true-ids namespace)
      :return external-id))

(defgeneric augment-id-map-by-type (id-map obj type))

(defmethod augment-id-map-by-type (id-map self (type odo:constraint))
  (let* ((true-id (odo:name self))
         (external-id (create-external-id id-map :constraint true-id)))
    (id-map-record-object! id-map self true-id external-id :constraint)))

(defmethod augment-id-map-by-type (id-map self (type odo:episode))
  (let* ((true-id (odo:name self))
         (external-id (create-external-id id-map :constraint true-id)))
    (id-map-record-object! id-map self true-id external-id :constraint)))

(defmethod augment-id-map-by-type (id-map self (type odo:state-plan))
  (let* ((true-id (odo:name self))
         (external-id (create-external-id id-map :state-plan true-id)))
    (id-map-record-object! id-map self true-id external-id :state-plan))
  (augment-id-map id-map (odo:state-space self))
  (dolist (c (odo:constraint-list self))
    (augment-id-map id-map c))
  (dolist (e (odo:goal-episode-list self))
    (augment-id-map id-map e))
  (dolist (e (odo:value-episode-list self))
    (augment-id-map id-map e)))

(defmethod augment-id-map-by-type (id-map self (type odo:state-space))
  (dolist (obj (odo:state-space-list self))
    (augment-id-map id-map obj)))

(defmethod augment-id-map-by-type (id-map self (type odo:state-variable))
  (let* ((true-id (odo:name self))
         (external-id (create-external-id id-map :state-space true-id)))
    (id-map-record-object! id-map self true-id external-id :state-space)))

(defmethod augment-id-map-by-type (id-map self (type odo:variable))
  (let* ((true-id (odo:name self))
         (external-id (create-external-id id-map :state-space true-id)))
    (id-map-record-object! id-map self true-id external-id :state-space)))

(defgeneric augment-id-map (id-map obj))

(defmethod augment-id-map (id-map obj)
  (augment-id-map-by-type id-map obj (odo:type obj)))

(defun compute-id-map (obj)
  (ana:aprog1 (make-id-map)
    (augment-id-map it obj)))
