(uiop:define-package #:odo/io/pddl/decoder
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:local-nicknames (#:alex #:alexandria)))

(in-package #:odo/io/pddl/decoder)

(defsection @odo/io/pddl/decoder (:title "PDDL Decoder"))


;; * Decoder object

(defclass pddl-decoder ()
  ())

(odo/io::register-decoder :pddl 'pddl-decoder :pddl 0)


;; * Parsing forms

(defun parse-typed-list (list)
  (let (current-names
        out)
    (loop
      (unless list (return))
      (let ((sym (pop list)))
        (if (string-equal "-" sym)
            (loop
              :with type-name := (pop list)
              :for name :in (nreverse current-names)
              :do (push (cons name type-name) out)
              :finally
                 (setf current-names nil))
            (push sym current-names))))
    (when current-names
      (loop
        :with type-name := (intern (uiop:standard-case-symbol-name :object))
        :for name :in (nreverse current-names)
        :do (push (cons name type-name) out)))
    (nreverse out)))

(defun parse-pddl-requirements (domain-form)
  (let ((req-form (find :requirements (rest domain-form) :key #'first)))
    (rest req-form)))

(defun parse-pddl-types (domain-form)
  (let* ((types-form (find :types (rest domain-form) :key #'first))
         (names-alist (parse-typed-list (rest types-form))))
    (loop
      :for (name . type-name) :in names-alist
      :collect (odo:make-pddl-type name type-name))))

(defun parse-pddl-parameters (param-list)
  (let ((param-alist (parse-typed-list param-list)))
    (loop
      :for (name . type-name) :in param-alist
      :collect (odo:make-pddl-parameter name type-name))))

(defun parse-pddl-predicates (domain-form)
  (let ((predicates-form (find :predicates (rest domain-form) :key #'first)))
    (loop
      :for (name . args) :in (rest predicates-form)
      :collect (odo:make-pddl-predicate name :parameters (parse-pddl-parameters args)))))

(defun parse-pddl-functions (domain-form)
  (let ((functions-form (find :functions (rest domain-form) :key #'first)))
    (loop
      :for (name . args) :in (rest functions-form)
      :collect (odo:make-pddl-function name :parameters (parse-pddl-parameters args)))))

(defun parse-pddl-control-variable (control-variable-form)
  (destructuring-bind (name &key
                              ((:parameters parameters-form))
                              ((:bounds bounds-form)))
      (rest control-variable-form)
    (let ((bounds (list nil nil)))
      (labels ((recurse (form)
                 (cond
                   ((and (listp form)
                         (string-equal (first form) 'and))
                    (mapc #'recurse (rest form)))
                   ((and (listp form)
                         (string-equal (first form) '>=)
                         (string-equal (second form) '?value))
                    (setf (first bounds) (third form)))
                   ((and (listp form)
                         (string-equal (first form) '>=)
                         (string-equal (third form) '?value))
                    (setf (second bounds) (second form)))
                   ((and (listp form)
                         (string-equal (first form) '<=)
                         (string-equal (second form) '?value))
                    (setf (second bounds) (third form)))
                   ((and (listp form)
                         (string-equal (first form) '<=)
                         (string-equal (third form) '?value))
                    (setf (first bounds) (second form)))
                   ((and (listp form)
                         (string-equal (first form) '=)
                         (string-equal (second form) '?value))
                    (setf bounds (cons (third form) (third form))))
                   ((and (listp form)
                         (string-equal (first form) '=)
                         (string-equal (third form) '?value))
                    (setf bounds (list (second form) (second form))))
                   (t
                    (error "Can't parse bound-form form ~S" bounds-form)))))
        (recurse bounds-form))
      (odo:make-pddl-control-variable name bounds
                                      :parameters (parse-pddl-parameters parameters-form)))))

(defun parse-pddl-control-variables (domain-form)
  (let ((control-variable-forms (remove-if-not (alex:curry #'eql :control-variable)
                                               (rest domain-form)
                                               :key #'first)))
    (mapcar #'parse-pddl-control-variable control-variable-forms)))

(defun in-pddl-parameter-list-p (parameters name)
  (member name parameters :key #'odo:name
                          :test #'string-equal))

(defun parse-pddl-durative-action-duration (parameters duration-form)
  (parse-pddl-formula (list* (odo:make-pddl-parameter '?duration 'number) parameters) duration-form))

(defun parse-pddl-formula (parameters formula)
  (cond
    ((listp formula)
     (let ((operator
             ;; FIXME: Yuck.
             (alex:switch ((first formula) :test 'string-equal)
               ('and
                'odo:and)
               ('or
                'odo:or)
               ('increase
                'odo:pddl-increase)
               ('decrease
                'odo:pddl-decrease)
               ('at
                'odo:pddl-at)
               ('over
                'odo:pddl-over)
               ('>=
                'odo:ge)
               ('<=
                'odo:le)
               ('<
                'odo:lt)
               ('>
                'odo:gt)
               ('=
                'odo:=)
               ('+
                'odo:+)
               ('-
                'odo:-)
               ('*
                'odo:*)
               ('/
                'odo:/)
               (t
                (first formula)))))
       (cond
         ;; These first two have special parsing rules, they are akin to macros
         ;; in PDDL where a free symbol doesn't necessarily refer to a variable
         ;; binding.
         ((eql operator 'odo:pddl-over)
          (assert (string-equal (second formula) 'all))
          (odo:pddl-over :all (parse-pddl-formula parameters (third formula))))
         ((eql operator 'odo:pddl-at)
          (assert (or (string-equal (second formula) 'start)
                      (string-equal (second formula) 'end)))
          (odo:pddl-at (if (string-equal (second formula) 'start) :start :end)
                       (parse-pddl-formula parameters (third formula))))
         (t
          (apply #'odo:funcall operator (mapcar (alex:curry #'parse-pddl-formula parameters) (rest formula)))))))
    ((numberp formula)
     formula)
    ((string-equal formula "#T")
     (odo:lookup odo:+current-time+))
    ((in-pddl-parameter-list-p parameters formula)
     (odo:lookup formula))
    (t
     (error "Unable to parse ~S" formula))))

(defun parse-pddl-durative-action-condition (parameters condition-form)
  (let ((expr (parse-pddl-formula parameters condition-form)))
    expr))

(defun parse-pddl-durative-action-effect (parameters effect-form)
  (let ((expr (parse-pddl-formula parameters effect-form)))
    expr))

(defun parse-pddl-durative-action (action-form)
  (destructuring-bind (name &key ((:parameters parameters-form))
                              ((:duration duration-form))
                              ((:condition condition-form))
                              ((:effect effect-form)))
      (rest action-form)
    (let* ((parameters (parse-pddl-parameters parameters-form))
           (duration (parse-pddl-durative-action-duration parameters duration-form))
           (condition (parse-pddl-durative-action-condition parameters condition-form))
           (effect (parse-pddl-durative-action-effect parameters effect-form)))
      (odo:make-pddl-durative-action name
                                     :parameters parameters
                                     :duration duration
                                     :condition condition
                                     :effect effect))))

(defun parse-pddl-durative-actions (domain-form)
  (let ((durative-action-forms (remove-if-not (alex:curry #'eql :durative-action)
                                              (rest domain-form)
                                              :key #'first)))
    (mapcar #'parse-pddl-durative-action durative-action-forms)))


;; * Entrypoints

(defmethod odo/io::deserialize-object-from-stream ((decoder pddl-decoder) (format (eql :pddl)) type stream meta)
  (declare (ignore type))
  (let ((outer-package *package*)
        (domain-form))
    (uiop:with-safe-io-syntax (:package outer-package)
      (let ((*readtable* (copy-readtable nil)))
        (set-dispatch-macro-character #\# #\t
                                      (lambda (stream subchar prefix)
                                        (declare (ignore stream subchar prefix))
                                        (nth-value 0 (intern "#T"))))
        (setf domain-form (read stream))))
    (internalize-object decoder :pddl domain-form meta)))

(defmethod internalize-object ((decoder pddl-decoder) (format (eql :pddl)) obj meta)
  (declare (ignore meta))
  (assert (string-equal (first obj) 'define))
  (assert (and (listp (second obj))
               (alex:length= 2 (second obj))
               (string-equal (first (second obj)) 'domain)))
  (let* ((name (second (second obj)))
         (requirements (parse-pddl-requirements obj))
         (types (parse-pddl-types obj))
         (predicates (parse-pddl-predicates obj))
         (functions (parse-pddl-functions obj))
         (durative-actions (parse-pddl-durative-actions obj))
         (control-variables (parse-pddl-control-variables obj)))
    (odo:make-pddl-domain name requirements :types types
                                            :predicates predicates
                                            :functions functions
                                            :actions durative-actions
                                            :control-variables control-variables)))
