(uiop:define-package #:odo/io/pddl/encoder
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:import-from #:anaphora #:it)
  (:local-nicknames (#:alex #:alexandria)
                    (#:ana #:anaphora)))

(in-package #:odo/io/pddl/encoder)

(defsection @odo/io/pddl/encoder (:title "PDDL Encoder")
  "This section describes the PDDL encoder for serializing PDDL Domain objects
to a PDDL stream.")


;; * Encoder object

(defclass pddl-encoder ()
  ((domain-list
    :initform nil
    :accessor encoder-domain-list)))

(odo/io::register-encoder :pddl 'pddl-encoder :pddl 0)


(defvar *odo-symbol-to-pddl-symbol*
  '((odo:and . and)
    (odo:ge . >=)
    (odo:le . <=)
    (odo:pddl-at . at)
    (odo:pddl-over . over)
    (odo:pddl-increase . increase)
    (odo:pddl-decrease . decrease)
    (odo:* . *)
    (odo:not . not)))

(defun encode-types (types)
  (loop :for type :in types
        :collect (list (odo:name type)
                       '-
                       (odo:supertype type))))

(defun encode-predicates (predicates)
  (loop :for predicate :in predicates
        :collect (list* (odo:name predicate)
                        (encode-parameters (odo:pddl-predicate-parameters predicate)))))

(defun encode-functions (functions)
  (loop :for function :in functions
        :collect (list* (odo:name function)
                        (encode-parameters (odo:pddl-function-parameters function)))))

(defun encode-control-variables (control-variables)
  (loop :for var :in control-variables
        :collect (list :control-variable (odo:name var)
                       :bounds (append (list 'and)
                                       (encode-control-variable-bounds
                                        (odo:bounds var)))
                       :parameters (encode-parameters (odo:pddl-control-variable-parameters var)))))

(defun encode-control-variable-bounds (bounds)
  ;; Assert that there is lower and upper bound
  (assert (= (length bounds) 2))
  (let ((lb (first bounds))
        (ub (second bounds)))
    (list (list '>= '?value lb)
          (list '<= '?value ub))))

(defun encode-actions (actions)
  (loop :for action :in actions
        :collect (if (typep (odo:type action) 'odo:pddl-durative-action)
                     ;; PDDL durative actions
                     (list :durative-action (odo:name action)
                           :parameters (encode-parameters (odo:pddl-action-parameters action))
                           :duration (encode-odo-expression
                                      (odo:pddl-action-duration action))
                           :condition (encode-odo-expression (odo:condition action))
                           :effect (encode-odo-expression (odo:effect action)))
                     ;; PDDL snap actions
                     (list :durative-action (odo:name action)
                           :parameters (encode-parameters (odo:pddl-action-parameters action))
                           :condition (encode-odo-expression (odo:condition action))
                           :effect (encode-odo-expression (odo:effect action))))))

(defun encode-parameters (parameters)
  (loop
    :for cell :on parameters
    :for p := (car cell)
    :for next-p := (cadr cell)
    :for name := (odo:name p)
    :for type := (odo:pddl-parameter-type p)
    :for next-type := (when next-p (odo:pddl-parameter-type next-p))
    :when (and next-type (string-equal next-type type))
      :collect name
    :else
      :collect name
      :and :collect '-
      :and :collect type))

(defun encode-odo-expression (odo-expression)
  (cond
    ((numberp odo-expression)
     odo-expression)
    ((symbolp odo-expression)
     odo-expression)
    (:otherwise
     (encode-odo-expression-by-type odo-expression (odo:type odo-expression)))))

(defgeneric encode-odo-expression-by-type (expression type))

(defmethod encode-odo-expression-by-type (odo-expression (type odo:application-expression))
  (let ((operator (odo:operator odo-expression))
        (arguments (mapcar #'encode-odo-expression (odo:arguments odo-expression))))
    (if (alex:assoc-value *odo-symbol-to-pddl-symbol* operator)
        (list* (alex:assoc-value *odo-symbol-to-pddl-symbol* operator) arguments)
        (list* operator arguments))))

(defmethod encode-odo-expression-by-type (odo-expression (type odo:lookup-expression))
  (let ((name (odo:lookup-expression-name odo-expression)))
    (if (eql name odo:+current-time+)
        '|#T|
        name)))



(defun print-main-block (stream list)
  (pprint-logical-block (stream list :prefix "(" :suffix ")")
    (format stream "~A" (car list))
    (loop :for list :in (cdr list)
          :do (pprint-newline :mandatory stream)
          (format stream "~A" list))))

(defun print-domain-name-block (stream domain-name)
  (pprint-logical-block (stream domain-name :prefix "(" :suffix ")")
    (format stream "~A" (car domain-name))
    (format stream " ~A" (nth 1 domain-name))))

(defun print-requirements-block (stream requirements)
  (pprint-logical-block (stream requirements :prefix "(" :suffix ")")
    (format stream ":requirements")
    (format stream "~{ :~A~}" (cdr requirements))))

(defun print-types-block (stream types)
  (pprint-logical-block (stream types :prefix "(" :suffix ")")
    (format stream ":types")
    (write-char #\Space stream)
    (pprint-logical-block (stream (rest types))
      (loop
        (pprint-exit-if-list-exhausted)
        (let ((type (pprint-pop)))
          (pprint-logical-block (stream type)
            (format stream "~{~A~^ ~}" type)))
        (pprint-exit-if-list-exhausted)
        (pprint-newline :mandatory stream)))))

(defun print-predicates-block (stream predicates)
  (pprint-logical-block (stream predicates :prefix "(" :suffix ")")
    (format stream ":predicates")
    (write-char #\Space stream)
    (pprint-logical-block (stream (rest predicates))
      (loop
        (pprint-exit-if-list-exhausted)
        (write (pprint-pop) :stream stream)
        (pprint-exit-if-list-exhausted)
        (pprint-newline :mandatory stream)))))

(defun print-functions-block (stream functions)
  (pprint-logical-block (stream functions :prefix "(" :suffix ")")
    (format stream ":functions")
    (write-char #\Space stream)
    (pprint-logical-block (stream (rest functions))
      (loop
        (pprint-exit-if-list-exhausted)
        (write (pprint-pop) :stream stream)
        (pprint-exit-if-list-exhausted)
        (pprint-newline :mandatory stream)))))

(defun print-control-variable-block (stream control-variable)
  (pprint-logical-block (stream control-variable :prefix "(" :suffix ")")
    (format stream ":control-variable")
    (format stream " ~A" (nth 1 control-variable))
    (pprint-newline :mandatory stream)
    (print-field stream :bounds (nth 3 control-variable))
    (alex:when-let ((parameters (getf control-variable :parameters)))
      (pprint-newline :mandatory stream)
      (print-field stream :parameters parameters))))

(defun print-durative-action-block (stream durative-action)
  (pprint-logical-block (stream durative-action :prefix "(" :suffix ")")
    (format stream ":durative-action")
    (format stream " ~A" (nth 1 durative-action))
    (loop :for (field . idx) :in '((:parameters . 3)
                                   (:duration . 5)
                                   (:condition . 7)
                                   (:effect . 9))
          :do (pprint-newline :mandatory stream)
              (print-field stream field (nth idx durative-action)))))

(defun print-snap-action-block (stream action)
  (pprint-logical-block (stream action :prefix "(" :suffix ")")
    (format stream ":action")
    (format stream " ~A" (nth 1 action))
    (loop :for (field . idx) :in '((:parameters . 3)
                                   (:precondition . 5)
                                   (:effect . 7))
          :do (pprint-newline :mandatory stream)
              (print-field stream field (nth idx action)))))

(defun print-field (stream field obj)
  (pprint-logical-block (stream obj)
    (format stream ":~A" field)
    (format stream " ~A" obj)))

(defun print-nil-block (stream block)
  (declare (ignore block))
  (format stream "()"))

;; TODO: we should restrict obj to pddl-domain. but pddl-domain is not the same class as the impl of pddl-domain

(defmethod odo/io::serialize-object-to-stream ((self pddl-encoder) (format (eql :pddl)) obj stream
                                               unknown-object-externalizer
                                               version)
  "Serialize a PDDL Domain object to stream using the pddl encoder."
  (externalize-object self :pddl obj unknown-object-externalizer version)
  (with-standard-io-syntax
    (let ((*print-pprint-dispatch* (copy-pprint-dispatch))
          (*print-case* :downcase))
      (set-pprint-dispatch '(cons (eql :define)) 'print-main-block)
      (set-pprint-dispatch '(cons (eql :domain)) 'print-domain-name-block)
      (set-pprint-dispatch '(cons (eql :requirements)) 'print-requirements-block)
      (set-pprint-dispatch '(cons (eql :types)) 'print-types-block)
      (set-pprint-dispatch '(cons (eql :predicates)) 'print-predicates-block)
      (set-pprint-dispatch '(cons (eql :functions)) 'print-functions-block)
      (set-pprint-dispatch '(cons (eql :control-variable)) 'print-control-variable-block)
      (set-pprint-dispatch '(cons (eql :durative-action)) 'print-durative-action-block)
      (set-pprint-dispatch '(cons (eql :action)) 'print-snap-action-block)
      (set-pprint-dispatch '(eql nil) 'print-nil-block)
      (write (encoder-domain-list self) :stream stream :pretty t)))
  ;; return some meta data for deserialization
  nil)

(defmethod externalize-object ((self pddl-encoder) (format (eql :pddl)) obj
                               (unknown-object-externalizer t)
                               version)
  (declare (ignore version))

  (let* ((name (odo:name obj))
         (requirements (odo:pddl-domain-requirements obj))
         (types (odo:pddl-domain-types obj))
         (constants (odo:pddl-domain-constants obj))
         (control-variables (odo:pddl-domain-control-variables obj))
         (predicates (odo:pddl-domain-predicates obj))
         (functions (odo:pddl-domain-functions obj))
         (actions (odo:pddl-domain-actions obj)))

    ;; TODO: Add constants
    (declare (ignore constants))

    (let* (;; Define domain name
           (name-list (list :domain name))
           ;; Define requirements
           (requirements-list (push :requirements requirements))
           ;; Define types
           (types-list (let ((list (encode-types types)))
                         (when list
                           (push :types list))))
           ;; Define predicates
           (predicates-list (let ((list (encode-predicates predicates)))
                              (push :predicates list)))
           ;; Define functions
           (functions-list (let ((list (encode-functions functions)))
                             (push :functions list)))
           ;; Define control variables
           (control-variables-list (encode-control-variables control-variables))
           ;; Define actions
           (actions-list (encode-actions actions)))

      (loop :for list :in (reverse actions-list)
            :do (push list (encoder-domain-list self)))

      (loop :for list :in (reverse control-variables-list)
            :do (push list (encoder-domain-list self)))

      (loop :for list :in (list functions-list
                                predicates-list)
            :do (push list (encoder-domain-list self)))

      (when types-list
        (push types-list (encoder-domain-list self)))

      (loop :for list :in (list requirements-list
                                name-list)
            :do (push list (encoder-domain-list self)))

      (push :define (encoder-domain-list self)))))
