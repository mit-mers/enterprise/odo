(uiop:define-package #:odo/io/minizinc/m2o
    (:use #:cl
          #:alexandria)
    (:local-nicknames (#:io #:odo/io)))

(in-package #:odo/io/minizinc/m2o)

(defclass minizinc-decoder ()
  ((minizinc-path
    :initarg :minizinc-path
    :initarg :minizinc
    :initform "minizinc"
    :reader minizinc-path)
   (data-pathname
    :initarg :data-pathname
    :initform nil
    :reader data-pathname)))

(io::register-decoder :minizinc 'minizinc-decoder :minizinc 0)

(defmethod io::deserialize-object-from-stream ((decoder minizinc-decoder) (format t) (type t) (stream t) (meta t))
  (let ((command (list (minizinc-path decoder)
                       "-c"
                       "-I" (namestring (asdf:system-relative-pathname :odo "src/io/minizinc/mznlib/"))
                       "-g"
                       "--no-output-ozn"
                       "--output-fzn-to-stdout"
                       "--solver" "org.minizinc.mzn-fzn"))
        proc)
    (when (data-pathname decoder)
      (appendf command (list "-d" (namestring (data-pathname decoder)))))
    (appendf command (list "-"))
    (setf proc (uiop:launch-program command :input stream :output :stream
                                            :error-output :interactive
                                            :element-type 'character))
    (unwind-protect
         (odo/io/flatzinc/f2o::parse-flatzinc-stream (uiop:process-info-output proc) type)
      (uiop:wait-process proc))))
