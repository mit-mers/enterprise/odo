(uiop:define-package #:odo/io/flatzinc/f2o
  (:use #:cl)
  (:import-from #:40ants-doc #:defsection)
  (:import-from #:anaphora #:it)
  (:local-nicknames (#:alex #:alexandria)
                    (#:ana #:anaphora)
                    (#:io #:odo/io)
                    (#:pn #:parse-number)
                    (#:ss #:split-sequence)
                    (#:utils #:odo/utils)))

(in-package #:odo/io/flatzinc/f2o)
(utils:setup-logger)

(defsection @odo-dev/io/flatzinc/f2o (:title "F2O")
  "This section contains the f2o subsystem. It is responsible for parsing CSPs
  described in flatzinc into Odo CSPs. It uses esrap to define the flatzinc
  grammar and builds the resulting CSP during the production step of the esrap
  parsing. Building the CSP during the production step keeps all the logic
  localized and prevents us from needing a separate framework for building the
  CSP from the parse tree.

  One issue with using esrap is it parses PEGs (Parsing Expression Grammars),
  whereas the flatzinc spec is written as a CFG in BNF. This is an issue
  because there are some ambiguities when converting BNF to a PEG (PEG
  parsers always choose the first alternative that parses correctly, even if
  it becomes clear later on that the entire sentence can't be parsed). So
  ordering of alternatives is critical.

  Additionally, the flatzinc grammar has some issues in it. There are valid
  flatzinc files (even including in the flatzinc manual!) that are illegal
  under the provided grammar. So we try to annotate these issues where we
  find them.

  Many things in this section (see ~*known-predicates*~ and
  ~*known-annotations*~) require type definitions. Non-variable and vector types
  are identical to the corresponding lisp types. Variable types are of the form
  ~(variable domain-type)~ where a ~domain-type~ is recognized by Odo.")

;; * Configuration
;;   This section contains all of the configurable aspects of f2o. Namely, what
;;   annotations and predicates we recognize.

(defparameter *known-predicates*
  '(("array_bool_and"
     :signature ((vector (variable boolean) *) (variable boolean))
     :odo-type (odo:<-> odo:and)
     :odo-parameters (:apply :reif-var))
    ("array_bool_or"
     :signature ((vector (variable boolean) *) (variable boolean))
     :odo-type (odo:<-> odo:or)
     :odo-parameters (:apply :reif-var))
    ("array_float_element"
     :signature ((variable integer) (vector real *) (variable real))
     :odo-type odo:element
     :odo-parameters (1 0 2)
     :odo-additional (:one-indexed-p t))
    ("array_int_element"
     :signature ((variable integer) (vector integer *) (variable integer))
     :odo-type odo:element
     :odo-parameters (1 0 2)
     :odo-additional (:one-indexed-p t))
    ("array_var_float_element"
     :signature ((variable integer) (vector (variable real) *) (variable real))
     :odo-type odo:element
     :odo-parameters (1 0 2)
     :odo-additional (:one-indexed-p t))
    ("array_var_int_element"
     :signature ((variable integer) (vector (variable integer) *) (variable integer))
     :odo-type odo:element
     :odo-parameters (1 0 2)
     :odo-additional (:one-indexed-p t))
    ("bool_clause"
     :signature ((vector (variable boolean) *) (vector (variable boolean) *))
     :odo-type bool-clause
     :odo-parameters (:positive :negative))
    ("bool_eq_reif"
     :signature ((variable boolean) (variable boolean) (variable boolean))
     :odo-type (odo:<-> odo:cl-equalp)
     :odo-parameters (0 1 :reif-var))
    ("circuit"
     :signature ((vector (variable integer) *))
     :odo-type odo:circuit
     :odo-parameters (0)
     :odo-additional (:one-indexed-p t))
    ("episode"
     :signature ((variable real) (variable real) real real)
     :odo-type odo:episode)
    ("episode_imp"
     :signature ((variable real) (variable real) real real (variable boolean))
     :odo-type (odo:-> odo:episode))
    ("episode_reif"
     :signature ((variable real) (variable real) real real (variable boolean))
     :odo-type (odo:<-> odo:episode))
    ("float_eq"
     :signature ((variable real) (variable real))
     :odo-type odo:=
     :odo-parameters (0 1))
    ("float_eq_reif"
     :signature ((variable real) (variable real) (variable boolean))
     :odo-type (odo:<-> odo:=)
     :odo-parameters (0 1 :reif-var))
    ("float_le"
     :signature ((variable real) (variable real))
     :odo-type odo:lt
     :odo-parameters (0 1))
    ("float_lin_eq"
     :signature ((vector real *) (vector (variable real) *) real)
     :odo-type odo:linear-=
     :odo-parameters (0 1 2))
    ("float_lin_eq_reif"
     :signature ((vector real *) (vector (variable real) *) real (variable boolean))
     :odo-type (odo:<-> odo:linear-=)
     :odo-parameters (0 1 2 :reif-var))
    ("float_lin_le_reif"
     :signature ((vector real *) (vector (variable real) *) real (variable boolean))
     :odo-type (odo:<-> odo:linear-<=)
     :odo-parameters (0 1 2 :reif-var))
    ("float_lin_le"
     :signature ((vector real *) (vector (variable real) *) real)
     :odo-type odo:linear-<=
     :odo-parameters (0 1 2))
    ("int_eq_reif"
     :signature ((variable integer) (variable integer) (variable boolean))
     :odo-type (odo:<-> odo:=)
     :odo-parameters (0 1 :reif-var))
    ("int_le"
     :signature ((variable integer) (variable integer))
     :odo-type odo:lt
     :odo-parameters (0 1))
    ("int_lin_eq"
     :signature ((vector integer *) (vector (variable integer) *) integer)
     :odo-type odo:linear-=
     :odo-parameters (0 1 2))
    ("int_lin_eq_reif"
     :signature ((vector integer *) (vector (variable integer) *) integer (variable boolean))
     :odo-type (odo:<-> odo:linear-=)
     :odo-parameters (0 1 2 :reif-var))
    ("int_lin_ne"
     :signature ((vector integer *) (vector (variable integer) *) integer)
     :odo-type odo:linear-/=
     :odo-parameters (0 1 2))
    ("int_ne_reif"
     :signature ((variable integer) (variable integer) (variable boolean))
     :odo-type (odo:<-> odo:ne)
     :odo-parameters (0 1 :reif-var))
    ("int2float"
     :signature ((variable integer) (variable real))
     :odo-type odo:=
     :odo-parameters (0 1))
    ("stc"
     :signature ((variable real) (variable real) real real)
     :odo-type odo:simple-temporal
     :odo-parameters (0 1 :lower-bound :upper-bound))
    ("stc_imp"
     :signature ((variable real) (variable real) real real (variable boolean))
     :odo-type (odo:-> odo:simple-temporal)
     :odo-parameters (0 1 :lower-bound :upper-bound :reif-var))
    ("stc_reif"
     :signature ((variable real) (variable real) real real (variable boolean))
     :odo-type (odo:<-> odo:simple-temporal)
     :odo-parameters (0 1 :lower-bound :upper-bound :reif-var))
    ("subcircuit"
     :signature ((vector (variable integer) *))
     :odo-type odo:subcircuit
     :odo-parameters (0)
     :odo-additional (:one-indexed-p t)))
  "An alist that defines all the predicates f2o is able to parse from
  flatzinc. The keys are strings that name a flatzinc predicate. The values are
  plists that define how that predicate should be turned into an odo
  predicate. The known keywords are:

  + ~:signature~ :: A list describing the types of the flatzinc arguments the
                    predicate accepts.
  + ~:odo-type~ :: What the name of the corresponding predicate in odo is. If
                   the predicate corresponds to a full reif, this should be of
                   the form ~(full-reif pred-type)~.
  + ~:odo-parameters~ :: A list, of the same length as the signature, that
       specifies the names of the corresponding Odo arguments.
  + ~:odo-additional~ :: A plist that is added to
       ~make-predicate~/~make-constraint~.")

(defparameter *known-annotations*
  '(("activity"
     :signature (string))
    ("bool_search"
     :signature ((vector (variable boolean) *) annotation annotation annotation))
    ("complete")
    ("contingent")
    ("defines_var"
     :signature (variable))
    ("domain")
    ("first_fail")
    ("indomain_max")
    ("indomain_min")
    ("input_order")
    ("int_search"
     :signature ((vector (variable integer) *) annotation annotation annotation))
    ("is_defined_var")
    ("output_array"
     :signature (array-of-int-ranges))
    ("output_var")
    ("seq_search"
     :signature ((vector annotation *)))
    ("uncontrollable_var")
    ("var_is_introduced"))
  "An alist of known annotations and their definitions. The keys are strings that
  name a flatzinc annotation. The values are plists defining the annotation. The
  known keywords are:

  + ~:signature~ :: A list describing the types of the flatzinc arguments the
                    annotation accepts.")


(defun get-predicate-definition (predicate-name)
  "Get the definition of a known predicate. Returns two values: the first is the
definition, the second is T if the predicate exists and NIL otherwise."
  (let ((def-pair (assoc predicate-name *known-predicates* :test 'equal)))
    (values (cdr def-pair) (if def-pair t nil))))

(defun get-annotation-definition (annotation-name)
  "Get the definition of a known annotation. Returns two values: the first is the
definition, the second is T if the annotation exists and NIL otherwise."
  (let ((def-pair (assoc annotation-name *known-annotations* :test 'equal)))
    (values (cdr def-pair) (if def-pair t nil))))


;; * Definitions

(defvar *ss* nil
  "Bound to a state space instance during parsing.")
(defvar *annotations* nil
  "A plist for top level annotations")
(defvar *constraints* nil
  "A list to gather parsed constraints.")
(defvar *episodes* nil
  "A list to gather parsed episodes.")
(defvar *objective-goal* nil)
(defvar *objective-function* nil)

(defvar *rule-documentation* (make-hash-table)
  "A hash table that maps rule names (symbols) to documentation strings.")

(defmethod documentation ((rule symbol) (doc-type (eql 'f2o-rule)))
  (nth-value 0 (gethash rule *rule-documentation*)))

(defmethod (setf documentation) ((new-value string) (rule symbol) (doc-type (eql 'f2o-rule)))
  (setf (gethash rule *rule-documentation*) new-value))

(defmacro defrule (symbol expression &body options)
  "This shadows ~esrap:defrule~ and adds the ability to specify a
  ~:documentation~ option."
  (let ((documentation-option (find :documentation options :key #'first)))
    `(progn
       (esrap:defrule ,symbol ,expression
         ,@(remove documentation-option options))
       ,@(when documentation-option
           `((setf (documentation ',symbol 'f2o-rule) ,(second documentation-option)))))))

;; These need to be in the EVAL-WHEN so their definitions are available to
;; trivia.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defclass reference ()
    ((name
      :initarg :name))
    (:documentation
     "Represents a reference to a variable, parameter, or annotation. For many
     cases we can't know for sure at parse time what a reference should point
     to, so we delay figuring it out until production time."))

  (defclass indexed-variable-reference ()
    ((name
      :initarg :name)
     (index
      :initarg :index))
    (:documentation
     "Represents an reference into a variable or parameter array."))

  (defclass known-annotation ()
    ((name
      :initarg :name))
    (:documentation "Base class for known annotations. This is used to wrap
  annotations to prevent any ambiguity."))

  (defclass known-bare-annotation (known-annotation)
    ())

  (defclass known-annotation-with-args (known-annotation)
    ((args
      :initarg :args)))

  (defclass unknown-annotation ()
    ((name
      :initarg :name))
    (:documentation
     "Base class for unknown annotations. This is used to wrap annotations to
     prevent any ambiguity."))

  (defclass unknown-bare-annotation (unknown-annotation)
    ())

  (defclass unknown-annotation-with-args (unknown-annotation)
    ((args
      :initarg :args))))

(defun %get-variable-group (csp name)
  (let ((vector-spec (alex:assoc-value (odo:annotation csp :fzn/variable-vectors)
                                       name :test #'equal)))
    (when vector-spec
      (mapcar (lambda (spec)
                (trivia:ematch spec
                  ((list :var var-name)
                   (odo:state-space-get csp var-name))
                  ((list :const const)
                   const)))
              vector-spec))))

(defun resolve-flatzinc-arg (signature arg &key (by :ref))
  "Given the ~signature~ for ~arg~, resolve it. If ~by~ is ~:ref~, return names
of variables."
  ;; This function is probably the most hairy thing in f2o. We have to deal with
  ;; parameters, variable groups, etc.
  (labels ((ref-of (x)
             ;; Given some ~x~ that refers to a variable or constant, return a
             ;; reference to it.
             (if (odo:variable-p x)
                 ;; Easy case. Just return the variable.
                 x
                 (trivia:ematch x
                   ((satisfies stringp)
                    (cond
                      ((equal "infinity" x)
                       odo:+infinity+)
                      ((equal "-infinity" x)
                       odo:+-infinity+)
                      (t
                       ;; Given the name of a variable, wrap it in a reference form and
                       ;; call ref-of again.
                       (ref-of (make-instance 'reference :name x)))))
                   ((or (number) 't 'nil)
                    ;; The reference for a constant is itself.
                    x)
                   ((indexed-variable-reference name index)
                    ;; Get a reference to the entire array, get the element from
                    ;; that array, and look up its reference.
                    (ref-of (elt (ref-of (make-instance 'reference :name name)) index)))
                   ((reference name)
                    ;; Given a reference, look it up in the CSP.
                    (cond
                      ((equal "infinity" name)
                       odo:+infinity+)
                      ((equal "-infinity" name)
                       odo:+-infinity+)
                      (t
                       (ref-of
                        (or (alex:assoc-value (getf *annotations* :fzn/parameters) name
                                              :test #'equal)
                            (%get-variable-group *ss* name)
                            (odo:state-space-get *ss* name)))))))))
           (name-of (x)
             (if (odo:variable-p x)
                 ;; Return the name of the variable.
                 (odo:name x)
                 (trivia:ematch x
                   ((or (number) 't 'nil)
                    x)
                   ((indexed-variable-reference)
                    (name-of (ref-of x)))
                   ((reference)
                    (name-of (ref-of x))))))
           (refs-of (x)
             (trivia:ematch x
               ((satisfies listp)
                (mapcar #'ref-of x))
               ((trivia:vector*)
                (map 'list #'ref-of x))
               ((reference name)
                (refs-of
                 (or (alex:assoc-value (getf *annotations* :fzn/parameters) name
                                       :test #'equal)
                     (%get-variable-group *ss* name))))))
           (names-of (x)
             (mapcar #'name-of (refs-of x)))
           (ensure-var (variable-type x)
             (if (odo:variable-p x)
                 x
                 (trivia:ematch variable-type
                   ((or 'real 'integer)
                    (odo:make-variable variable-type :min x :max x))
                   ('boolean
                    (odo:make-variable 'boolean :value x)))))
           (ensure-parameter (x)
             (unless (or (numberp x)
                         (eql t x)
                         (eql nil x)
                         (odo:symbolic-infinity-p x))
               (error "This variable should not be here..."))
             x))
    (trivia:ematch signature

      ('string
       (assert (stringp arg))
       arg)

      ((list 'vector (list 'variable variable-type) '*)
       (ecase by
         (:name
          (names-of arg))
         (:ref
          (mapcar (alex:curry #'ensure-var variable-type) (refs-of arg)))))

      ((list 'vector (or 'integer 'real 'boolean) '*)
       (mapcar #'ensure-parameter (refs-of arg)))

      ((or 'integer 'real 'boolean)
       (ensure-parameter (ref-of arg)))

      ((or 'variable (list 'variable variable-type))
       (ecase by
         (:name
          (name-of arg))
         (:ref
          (ensure-var variable-type (ref-of arg)))))

      ((list 'vector 'annotation _)
       (mapcar (lambda (x)
                 (trivia:ematch x
                   ((known-annotation-with-args name args)
                    (cons name args))
                   ((known-bare-annotation name)
                    name)
                   ((reference)
                    (resolve-flatzinc-arg 'annotation x :by :name))))
               arg))

      ('annotation
       (trivia:ematch arg
         ((reference name)
          (multiple-value-bind (def exists-p)
              (get-annotation-definition name)
            (declare (ignore def))
            (unless exists-p
              (utils:log-warn "Unknown annotation ~a" name))
            (annotation-string-to-keyword name)))))

      ('array-of-int-ranges
       ;; This is a wonky one, used only by output_array as far as I know.
       (assert (every (lambda (x)
                        (and (listp x)
                             (alex:length= 2 x)
                             (integerp (first x))
                             (integerp (second x))
                             (or (zerop (first x))
                                 (plusp (first x)))
                             (>= (second x) (first x))))
                      arg))
       arg))))

(defun resolve-flatzinc-args (signature args &key (by :ref))
  (assert (alex:length= signature args))
  (mapcar (alex:rcurry #'resolve-flatzinc-arg :by by) signature args))

(defun merge-annotations! (existing new)
  "Given plists of existing and new annotations, return a new plist that merges
them."
  (setf existing (copy-list existing))
  (dolist (a (alex:ensure-list new))
    (trivia:ematch a
      ((known-bare-annotation name)
       (setf (getf existing name) t))
      ((known-annotation-with-args name args)
       (setf (getf existing name) args))
      ((unknown-bare-annotation name)
       (push (cons name t) (getf existing :fzn/unknown-annotations)))
      ((unknown-annotation-with-args name args)
       (push (cons name args) (getf existing :fzn/unknown-annotations)))))
  existing)


;; * esrap
;; ** Utilities

(defrule comment
    (and "%" (* (not #\Newline)) #\Newline)
  (:documentation
   "Comments start with a % character and extend to the end of the line.")
  (:constant nil))

(defrule whitespace
    (or #\Space #\Tab #\Newline
        comment)
  (:constant nil)
  (:error-report nil)
  (:documentation
   "Consumes one whitespace character (space, tab, or newline) or an entire comment."))

(defrule empty-string (and "")
  (:constant nil)
  (:documentation
   "Matches an empty string."))

(defrule spaced-comma
    (or (and (* whitespace) #\, (* whitespace))
        (and (* whitespace) #\,)
        (and #\, (* whitespace))
        #\,)
  (:documentation "Matches a comma optionally surrounded by spaces."))

(defmacro make-comma-separated-list-rule (rule-name)
  "Takes a ~RULE-NAME~ and produces a new rule with ~COM-SEP-~ prepended that
  corresponds to any number of instances of ~RULE-NAME~ separated by
  commas. Also supports a single instance that matches ~RULE-NAME~ with no
  commas. The new rule produces a list of whatever ~RULE-NAME~ produces."
  (let ((com-sep-name (intern (concatenate 'string "COM-SEP-" (symbol-name rule-name))))
        (with-comma-name (gensym)))
    `(progn
       (defrule ,with-comma-name (and (* (and ,rule-name spaced-comma)) ,rule-name)
         (:destructure (first-list last)
                       (append (mapcar #'first first-list)
                               (list last))))
       (defrule ,com-sep-name
           (or ,with-comma-name empty-string)))))

;; ** Basic flatzinc tokens

;;    Generally, things not defined by the flatzinc spec are prefixed with a =%=
;;    and rules with many options are broken into a rule for each option, suffixed
;;    with a =/= and a description of the option.

(defrule int-const
    (and (esrap:? (or "+" "-")) (+ (esrap:character-ranges (#\0 #\9))))
  (:lambda (args)
    (pn:parse-number (esrap:text args)))
  (:documentation
   "Parses an integer constant. Produces an integer."))

(make-comma-separated-list-rule int-const)

(defrule float-const
    (or (and (esrap:? (or "+" "-")) (+ (esrap:character-ranges (#\0 #\9)))
             "." (+ (esrap:character-ranges (#\0 #\9))) (esrap:? (and (or "e" "E") (+ (esrap:character-ranges (#\0 #\9))))))
        (and (esrap:? (or "+" "-")) (+ (esrap:character-ranges (#\0 #\9)))
             (or "e" "E") (+ (esrap:character-ranges (#\0 #\9)))))
  (:lambda (args)
    (pn:parse-number (esrap:text args) :float-format 'double-float))
  (:documentation
   "Parses a float constant. Produces an double-float."))

(defrule bool-const
    (or "true" "false")
  (:lambda (s)
    (if (equal s "true")
        t
        nil))
  (:documentation
   "Parses an boolean constant. Produces a ~t~ or ~nil~."))

(defrule %float-range
    (and float-const ".." float-const)
  (:lambda (l)
    (trivia:ematch l
      ((list lb _ ub)
       (list lb ub))))
  (:documentation
   "Matches a range of floats. Produces a list with two elements: the lower and
   upper bounds."))

(defrule %int-range
    (and int-const ".." int-const)
  (:lambda (l)
    (trivia:ematch l
      ((list lb _ ub)
       (list lb ub))))
  (:documentation
   "Matches a range of integers. Produces a list with two elements: the lower and
   upper bounds."))

(defrule index-set
    (or (and "1.." int-const)
        "int")
  (:lambda (args)
    (if (alex:length= 2 args)
        `(1 ,(second args))
        '(* *)))
  (:documentation
   "Matches an index set (1..INT-CONST or int). Produces either ~(1 UB)~ or ~(* *)~."))

(defrule string-const
    (and #\" (* (or string-escape (not (or #\\ #\")))) #\")
  (:lambda (l)
    (trivia:ematch l
      ((list _ contents _)
       (apply #'esrap:text contents))))
  (:documentation
   "Matches a string constant (string surrounded by double quotes). Produces the
   string with all escape sequences replaced by the corresponding
   character (this is Lisp, our strings are sensible)."))

(defrule string-escape
    (or string-escape/quote
        string-escape/newline
        string-escape/backslash)
  (:documentation
   "Matches a string escape sequence. Produces the character that the escape
   sequence represents."))

(defrule string-escape/quote
    (and #\\ #\")
  (:constant #\")
  (:documentation
   "An escaped double quote."))

(defrule string-escape/newline
    (and #\\ "n")
  (:constant #\Newline)
  (:documentation
   "An escaped newline."))

(defrule string-escape/backslash
    (and #\\ #\\)
  (:constant #\\)
  (:documentation
   "An escaped backslash."))

(defrule var-par-id
    (and (* #\_) (esrap:character-ranges (#\a #\z) (#\A #\Z))
         (* (esrap:character-ranges (#\a #\z) (#\A #\Z) (#\0 #\9) #\_)))
  (:function esrap:text)
  (:documentation
    "Matches a variable or parameter ID. Same as pred-ann-id, but allows leading
    underscores. Produces a string that matches the ID."))

(defrule pred-ann-id
    (and (esrap:character-ranges (#\a #\z) (#\A #\Z))
         (* (esrap:character-ranges (#\a #\z) (#\A #\Z) (#\0 #\9) #\_)))
  (:function esrap:text)
  (:documentation
    "Matches a predicate or annotation ID. Produces a string that matches the
    ID."))

;; ** Parameter types

(defrule par-type
    (or par-type/bool
        par-type/float
        par-type/int
        par-type/bool-vector
        par-type/float-vector
        par-type/int-vector)
  (:documentation
    "Matches a parameter declaration. Produces a type spec."))

(defrule par-type/bool
    "bool"
  (:constant 'boolean)
  (:documentation
    "Matches a boolean parameter. Produces ~boolean~."))

(defrule par-type/float
    "float"
  (:constant 'real)
  (:documentation
    "Matches a float parameter. Produces ~real~."))

(defrule par-type/int
    "int"
  (:constant 'integer)
  (:documentation
    "Matches an integer parameter. Produces ~integer~."))

(defrule par-type/bool-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "bool")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _)
       `(vector boolean ,length))))
  (:documentation
    "Matches an array of boolean parameters. Produces ~(vector boolean length)~."))

(defrule par-type/float-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "float")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _)
       `(vector real ,length))))
  (:documentation
    "Matches an array of float parameters. Produces ~(vector real length)~."))

(defrule par-type/int-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "int")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _)
       `(vector integer ,length))))
  (:documentation
    "Matches an array of integer parameters. Produces ~(vector integer length)~."))

;; ** Variable types

(defrule var-type
    (or var-type/bool
        var-type/float
        var-type/float-range
        var-type/int
        var-type/int-range
        var-type/bool-vector
        var-type/int-vector
        var-type/int-range-vector
        var-type/float-vector
        var-type/float-range-vector
        var-type/enumerated-int
        var-type/enumerated-int-vector)
  (:documentation
   "Matches a variable declaration. Produces a variable type spec."))

(defrule var-type/bool
    (and "var" (* whitespace) "bool")
  (:constant (list 'variable 'boolean))
  (:documentation
   "Matches a declaration of a boolean variable. Produces ~(variable boolean)~."))

(defrule var-type/float
    (and "var" (* whitespace) "float")
  (:constant (list 'variable 'real))
  (:documentation
   "Matches a declaration of a float variable. Produces ~(variable real)~."))

(defrule var-type/float-range
    (and "var" (* whitespace) %float-range)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ (list lb ub))
       `(variable (real :min ,lb :max ,ub)))))
  (:documentation
    "Matches a declaration of a float variable with a specified range. Produces
    ~(variable (real :min lb :max ub))~"))

(defrule var-type/int
    (and "var" (* whitespace) "int")
  (:constant (list 'variable 'integer))
  (:documentation
   "Matches a declaration of an integer variable. Produces ~(variable integer)~."))

(defrule var-type/int-range
    (and "var" (* whitespace) %int-range)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ (list lb ub))
       `(variable (integer :min ,lb :max ,ub)))))
  (:documentation
    "Matches a declaration of a float variable with a specified range. Produces
    ~(variable (integer :min lb :max ub))~"))

(defrule var-type/bool-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace) "bool")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ _)
       `(vector (variable boolean) ,length))))
  (:documentation
    "Matches a declaration of an array of boolean variables. Produces
    ~(vector (variable boolean) length)~."))

(defrule var-type/int-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace) "int")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ _)
       `(vector (variable integer) ,length))))
  (:documentation
    "Matches a declaration of an array of integer variables. Produces
    ~(vector (variable integer) length)~."))

(defrule var-type/int-range-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace) %int-range)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ (list lb ub))
       `(vector (variable (integer :min ,lb :max ,ub)) ,length))))
  (:documentation
    "Matches a declaration of an array of integer variables with a specified
    range. Produces ~(vector (variable (integer :min lb :max ub)) length)~."))

(defrule var-type/float-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace) "float")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ _)
       `(vector (variable real) ,length))))
  (:documentation
    "Matches a declaration of an array of integer variables. Produces
    ~(vector (variable real) length)~."))

(defrule var-type/float-range-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace) %float-range)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ (list lb ub))
       `(vector (variable (real :min ,lb :max ,ub)) ,length))))
  (:documentation
    "Matches a declaration of an array of integer variables with a specified
    range. Produces ~(vector (variable (real :min lb :max ub)) length)~."))

(defrule var-type/enumerated-int
    (and "var" (* whitespace) #\{ (* whitespace) com-sep-int-const (* whitespace) #\})
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ values _ _)
       `(variable (integer :values ,values)))))
  (:documentation
    "Matches a declaration of an enumerated integer variable. Produces
    ~(variable (integer :values values))~."))

(defrule var-type/enumerated-int-vector
    (and "array"
         (* whitespace) "[" (* whitespace) index-set (* whitespace) "]"
         (* whitespace) "of" (* whitespace) "var" (* whitespace)
         #\{ (* whitespace) com-sep-int-const (* whitespace) #\})
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ (list _ length) _ _ _ _ _ _ _ _ _ values _ _)
       `(vector (variable (integer :values ,values)) ,length))))
  (:documentation
    "Matches a declaration of an array of enumerated integer variables. Produces
    ~(vector (variable (integer :values values)) length)~."))

;; ** Expressions

(defrule expr
    (or bool-const
        float-const
        int-const
        ;; indexed-var-ref *must* come first because it is ambiguous with
        ;; a bare variable reference.
        %indexed-var-ref
        %ref
        var-par-id
        array-expr)
  (:documentation
    "Matches and produces an expression. This rule matches only things that can
    appear outside of annotation arguments."))
(make-comma-separated-list-rule expr)

(defrule array-expr
    (or array-expr/empty
        array-expr/filled)
  (:documentation
    "Matches and produces an array of expressions (potentially empty). Produces
    a list of the contained expressions."))

(defrule array-expr/empty
    (and "[" (* whitespace) "]")
  (:constant nil)
  (:documentation
    "Matches an empty array of expressions. Produces ~nil~."))

(defrule array-expr/filled
    (and "[" (* whitespace) com-sep-expr (* whitespace) "]")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ it _ _)
       it)))
  (:documentation
    "Matches an array of expr. Produces a list of the expressions."))

(defrule annotation-expr
    (or array-annotation-expr
        %int-range ; This rule isn't in the BNF, but is necessary for
                   ; output_array annotations.
        annotation/with-args
        string-const
        expr
        pred-ann-id)
  (:documentation
    "Matches and produces an expression. This rule matches the expr rule as well
    as adding annotations and string constants."))
(make-comma-separated-list-rule annotation-expr)

(defrule array-annotation-expr
    (or array-expr/empty
        array-annotation-expr/filled)
  (:documentation
    "Matches an array of annotation-expr. Produces a list of the expressions."))

(defrule array-annotation-expr/filled
    (and "[" (* whitespace) com-sep-annotation-expr (* whitespace) "]")
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ it _ _)
       it)))
  (:documentation
    "Matches an array of annotation-expr. Produces a list of the expressions."))

(defrule %ref
    var-par-id
  (:lambda (name)
    (make-instance 'reference :name name))
  (:documentation
    "Matches a variable or annotation reference. Produces an instance of
    ~reference~. An object is produced to help distinguish references from
    literal strings."))

(defrule %indexed-var-ref
    (and var-par-id (* whitespace) "[" (* whitespace) int-const (* whitespace) "]" )
  (:lambda (l)
    (trivia:ematch l
      ((list var-name _ _ _ index _ _)
       (make-instance 'indexed-variable-reference :name var-name :index index))))
  (:documentation
    "Matches an indexed reference into a variable. Produces an instance of
    ~indexed-variable-reference~. An object is used here instead of a list to
    prevent confusion with arrays of expressions (represented with lists), and
    with set constants (in the future)."))

;; ** Annotations

(defun annotation-string-to-keyword (name)
  "Convert an annotation string from flatzinc into a keyword. Uses ~:name~ if it
exists in the annotation definition. Otherwise, it has =fzn/= prepended to it,
underscores are replaced by dashes, and it is converted to a keyword."
  (let ((def (get-annotation-definition name)))
    (or (getf def :name)
        (alex:make-keyword (uiop:standard-case-symbol-name
                            (concatenate 'string "fzn/"
                                         (substitute #\- #\_  name)))))))

(defrule annotations
    (* (and (* whitespace) "::" (* whitespace) annotation))
  (:lambda (l)
    (reduce #'merge-annotations! l :key #'fourth :initial-value nil))
  (:documentation
    "Matches a set of annotations. Produces a list of annotations."))

(defrule annotation
    (or annotation/with-args
        ;; bare has to come second because it is ambiguous with /with-args.
        annotation/bare)
  (:documentation
    "Matches a single annotation. Produces an instance of ~annotation~ or
    ~unknown-annotation~."))

(defrule annotation/bare
    pred-ann-id
  (:lambda (name)
    ;; Figure out if this annotation is known or not.
    (multiple-value-bind (def exists-p)
        (get-annotation-definition name)
      (if exists-p
          (progn
            (assert (null (get def :signature))
                    nil
                    "Annotation ~S requires arguments" name)
            (make-instance 'known-bare-annotation
                           :name (annotation-string-to-keyword name)))
          (progn
            (utils:log-warn "Annotation ~S is not known" name)
            (warn "Annotation ~S is not known" name)
            (make-instance 'unknown-bare-annotation
                           :name name)))))
  (:documentation
    "Matches an annotation without arguments. Produces an instance of
    ~known-bare-annotation~ or ~unknown-bare-annotation~. If the annotation is
    unknown, a warning is issued."))

(defrule annotation/with-args
    (and pred-ann-id "(" (* whitespace) com-sep-annotation-expr (* whitespace) ")")
  (:lambda (l)
    (trivia:ematch l
      ((list name _ _ args _ _)
       (multiple-value-bind (def exists-p)
           (get-annotation-definition name)
         (if exists-p
             (let ((signature (getf def :signature)))
               (assert signature
                       nil
                       "Annotation ~S accepts no arguments" name)
               (make-instance 'known-annotation-with-args
                              :name (annotation-string-to-keyword name)
                              :args (resolve-flatzinc-args signature args :by :name)))
             (progn
               (utils:log-warn "Annotation ~S is not known" name)
               (warn "Annotation ~S is not known" name)
               (make-instance 'unknown-annotation-with-args
                              :name name :args args)))))))
  (:documentation
    "Matches an annotation with arguments. Produces an instance of
    ~known-annotation-with-arguments~ or ~unknown-annotation-with-arguments~. If
    the annotation is unknown, a warning is issued."))

;; ** Parameter Declarations

(defrule param-decl
    (and par-type (* whitespace) #\: (* whitespace) var-par-id (* whitespace) #\= (* whitespace) expr
         (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ _ _ name _ _ _ val _ _)
       (push (cons name val) (getf *annotations* :fzn/parameters))
       nil)))
  (:documentation
    "Matches a parameter declaration. Produces nothing meaningful. As an effect,
    the production step adds ~(name . val)~ to the ~:fzn/parameters~
    annotation on the CSP."))

;; ** Predicate Declarations

(defrule pred-decl
    (and "predicate" (* whitespace) pred-ann-id (* whitespace) #\( com-sep-pred-param #\) (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ name _ _ args _ _ _)
       (multiple-value-bind (def exists-p) (get-predicate-definition name)
         (assert exists-p
                 nil
                 "Predicate ~S is not recognized" name)
         (let ((def-signature (getf def :signature))
               (flatzinc-signature (mapcar #'first args)))
           (assert (equal flatzinc-signature def-signature)
                   nil
                   "Signatures do not match for predicate ~S" name)))
       nil)))
  (:documentation
    "Matches a top level predicate declaration. Produces nothing meaningful. As
    an effect, signals an error if the predicate is not known by f2o (see
    ~*KNOWN-PREDICATES*~)."))

(defrule pred-param
    (and pred-param-type (* whitespace) #\: (* whitespace) pred-ann-id)
  (:lambda (l)
    (trivia:ematch l
      ((list type _ _ _ name)
       (list type name))))
  (:documentation
    "Matches a predicate argument inside a predicate declaration. Returns a list
    with the first element being the type and the second element being the name
    of the argument."))
(make-comma-separated-list-rule pred-param)

(defrule pred-param-type
    (or par-pred-param-type var-pred-param-type)
  (:documentation
    "Matches and produces a type for an argument in a predicate declaration."))

(defrule par-pred-param-type
    (or par-type
        par-pred-param-type/float-range
        par-pred-param-type/int-range)
  (:documentation
    "Matches a parameter type declaration inside a predicate declaration."))

(defrule par-pred-param-type/float-range
    %float-range
  (:lambda (l)
    (list* 'real l))
  (:documentation
    "Matches a float-range parameter type. Produces ~(real lb ub)~."))

(defrule par-pred-param-type/int-range
    %int-range
  (:lambda (l)
    (list* 'integer l))
  (:documentation
    "Matches an int-range parameter type. Produces ~(integer lb ub)~."))

(defrule var-pred-param-type
    (or var-type)
  (:documentation
    "Matches a variable type declaration inside a predicate declaration."))

;; ** Variable Declarations

(defrule var-decl
    (and var-type (* whitespace) ":" (* whitespace) var-par-id (* whitespace)
         annotations (* whitespace)
         (esrap:? (and "=" (* whitespace) expr (* whitespace))) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list var-type _ _ _ var-name _ annotations _ equality _)
       (ana:awhen (getf annotations :fzn/unknown-annotations)
         (dolist (a it)
           (utils:log-warn "When parsing variable ~A, ignoring unknown annotation: ~S" var-name a)))
       (let ((uncontrollable-p (getf annotations :fzn/uncontrollable-var))
             (annotations (alex:remove-from-plist annotations :fzn/uncontrollable-var)))
         (trivia:ematch (alex:ensure-list var-type)
           ((list 'vector (list 'variable single-type) length)
            (trivia:ematch (alex:ensure-list single-type)
              ((list* type args)
               (if equality
                   ;; Well, this seems to be a "fake" vector that we won't enter
                   ;; directly into odo because odo doesn't support vector groups
                   ;; with mixed variables and constants. Instead, we make a
                   ;; variable group and register it in the annotations on the
                   ;; CSP.
                   (ana:aprog1 (mapcar
                                (lambda (x)
                                  (let ((resolved-by-name (resolve-flatzinc-arg
                                                           (list 'variable single-type)
                                                           x
                                                           :by :name)))
                                    (if (stringp resolved-by-name)
                                        (odo:state-space-get *ss* resolved-by-name)
                                        resolved-by-name)))
                                (third equality))
                     (push (cons var-name
                                 (loop
                                   :for x :in it
                                   :if (odo:variable-p x)
                                     :collect (list :var (odo:name x))
                                   :else
                                     :collect (list :const x)))
                           (getf *annotations* :fzn/variable-vectors))
                     (dolist (x it)
                       (when (odo:variable-p x)
                         (odo:state-space-add! *ss* x)))
                     (push (cons var-name annotations)
                           (getf *annotations* :fzn/variable-group-annotations)))
                   (ana:aprog1 (apply #'odo:make-variable-vector
                                      length type
                                      :annotations annotations
                                      :uncontrollable-p uncontrollable-p
                                      :name var-name
                                      args)
                     (push (cons var-name
                                 (loop
                                   :for x :across it
                                   :collect (list :var (odo:name x))))
                           (getf *annotations* *ss* :fzn/variable-vectors))
                     (odo:state-space-add! *ss* it))))))
           ((list 'variable (or (list* type args)
                                type))
            ;; The flatzinc spec is a little contradictory if equality works for
            ;; non array variables. The BNF says yes, but the text only shows it
            ;; for arrays. I'm going to trust the explanatory text instead of BNF.
            (assert (null equality))
            (ana:aprog1 (apply #'odo:make-variable type
                               :name var-name
                               :annotations annotations
                               :uncontrollable-p uncontrollable-p
                               args)
              (odo:state-space-add! *ss* it))))))))
  (:documentation
    "Matches a variable declaration. Produces the new variable(s) and as a side
    effect adds them to ~*CSP*~."))

(defun sort-fun (left right)
  (cond
    ((and (numberp left) (numberp right))
     (< left right))
    ((numberp left)
     t)
    ((numberp right)
     nil)
    (t t)))

(defun unwrap-predicate-args (args)
  (mapcan (lambda (x)
            (destructuring-bind (name . val) x
              (if (numberp name)
                  (list val)
                  (list name val))))
          args))

(defun form-predicate-args (evaluated-args arg-names)
  (let* ((apply-var-pos (position :apply arg-names))
         (apply-var (when apply-var-pos (elt evaluated-args apply-var-pos)))
         (arg-names (remove :apply arg-names))
         (evaluated-args (if apply-var-pos
                             (append (subseq evaluated-args 0 apply-var-pos)
                                     (subseq evaluated-args (1+ apply-var-pos)))
                             evaluated-args)))
    (append (unwrap-predicate-args
             (sort (mapcar #'cons arg-names evaluated-args) #'sort-fun :key #'car))
            (coerce apply-var 'list))))

;; ** Constraints
(defrule constraint
    (and "constraint" (* whitespace)
         pred-ann-id (* whitespace) "(" (* whitespace) com-sep-expr (* whitespace) ")"
         (* whitespace) annotations (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ name _ _ _ args _ _ _ annotations _ _)
       (ana:awhen (getf annotations :fzn/unknown-annotations)
         (dolist (a it)
           (utils:log-warn "When parsing constraint ~A, ignoring unknown annotation: ~S" name a)))
       (multiple-value-bind (def exists-p) (get-predicate-definition name)
         (unless exists-p
           (utils:log-error "Unknown predicate type ~S" name))
         (let* ((evaluated-args (resolve-flatzinc-args (getf def :signature) args
                                                       :by :ref))
                (odo-parameters (getf def :odo-parameters))
                (odo-pred-type (getf def :odo-type))
                (contingent-p (getf annotations :fzn/contingent))
                (annotations (alex:remove-from-plist annotations :fzn/contingent))
                (constraints ()))
           (trivia:ematch odo-pred-type
             ('odo:episode
              (let ((activity-name (first (getf annotations :fzn/activity)))
                    (annotations (alex:remove-from-plist annotations :fzn/activity)))
                (destructuring-bind (start-event end-event lower-bound upper-bound)
                    evaluated-args
                  (push (odo:make-episode :start-event start-event
                                          :end-event end-event
                                          :temporal-constraint (odo:make-constraint
                                                                (odo:simple-temporal
                                                                 start-event end-event
                                                                 :lower-bound lower-bound
                                                                 :upper-bound upper-bound)
                                                                :contingent-p contingent-p)
                                          :annotations annotations
                                          :activity-name activity-name)
                        constraints))))
             ((list 'odo:-> 'odo:episode)
              (let ((activity-name (first (getf annotations :fzn/activity)))
                    (annotations (alex:remove-from-plist annotations :fzn/activity)))
                (destructuring-bind (start-event end-event lower-bound upper-bound guard)
                    evaluated-args
                  (push (odo:make-episode :start-event start-event
                                          :end-event end-event
                                          :temporal-constraint (odo:make-constraint
                                                                (odo:simple-temporal
                                                                 start-event end-event
                                                                 :lower-bound lower-bound
                                                                 :upper-bound upper-bound)
                                                                :contingent-p contingent-p)
                                          :annotations annotations
                                          :activity-name activity-name)
                        constraints)
                  (push (odo:make-constraint (odo:-> guard (odo:activep (first constraints))))
                        constraints))))
             ((list 'odo:<-> 'odo:episode)
              (let ((activity-name (first (getf annotations :fzn/activity)))
                    (annotations (alex:remove-from-plist annotations :fzn/activity)))
                (destructuring-bind (start-event end-event lower-bound upper-bound guard)
                    evaluated-args
                  (push (odo:make-episode :start-event start-event
                                          :end-event end-event
                                          :temporal-constraint (odo:make-constraint
                                                                (odo:simple-temporal
                                                                 start-event end-event
                                                                 :lower-bound lower-bound
                                                                 :upper-bound upper-bound)
                                                                :contingent-p contingent-p)
                                          :annotations annotations
                                          :activity-name activity-name)
                        constraints)
                  (push (odo:make-constraint (odo:<-> guard (odo:activep (first constraints))))
                        constraints))))

             ((list 'odo:<-> type)
              (let* ((reif-var-pos (position :reif-var odo-parameters))
                     (reif-var (elt evaluated-args reif-var-pos))
                     (new-odo-parameters (remove :reif-var odo-parameters))
                     (new-evaluated-args (append (subseq evaluated-args 0 reif-var-pos)
                                                 (subseq evaluated-args (1+ reif-var-pos))))
                     (ordered-args (form-predicate-args new-evaluated-args new-odo-parameters))
                     (odo-pred-arglist (append
                                        ordered-args
                                        (getf def :odo-additional))))
                (cond
                  ((eql t reif-var)
                   ;; The reif variable is constantly true, so we can just add
                   ;; an unreified constraint.
                   (push (odo:make-constraint
                          (apply type odo-pred-arglist)
                          :contingent-p contingent-p
                          :annotation annotations)
                         constraints))
                  ((null reif-var)
                   ;; The reif variable is constantly false, so we can just add
                   ;; an unreified, negated constraint.
                   (push (odo:make-constraint
                          (odo:not (apply type odo-pred-arglist))
                          :contingent-p contingent-p
                          :annotations annotations)
                         constraints))
                  (t
                   ;; Need to make a full reif
                   (push (odo:make-constraint
                          (odo:<-> reif-var (apply type odo-pred-arglist))
                          :contingent-p contingent-p
                          :annotations annotations)
                         constraints)))))
             ((list 'odo:-> type)
              (let* ((reif-var-pos (position :reif-var odo-parameters))
                     (reif-var (elt evaluated-args reif-var-pos))
                     (new-odo-parameters (remove :reif-var odo-parameters))
                     (new-evaluated-args (append (subseq evaluated-args 0 reif-var-pos)
                                                 (subseq evaluated-args (1+ reif-var-pos))))
                     (ordered-args (form-predicate-args new-evaluated-args new-odo-parameters))
                     (odo-pred-arglist (append
                                        ordered-args
                                        (getf def :odo-additional))))
                (cond
                  ((eql t reif-var)
                   ;; The reif variable is constantly true, so we can just add
                   ;; an unreified constraint.
                   (push (odo:make-constraint
                          (apply type odo-pred-arglist)
                          :contingent-p contingent-p
                          :annotation annotations)
                         constraints))
                  (t
                   ;; Need to make a half reif
                   (push (odo:make-constraint
                          (odo:-> reif-var (apply type odo-pred-arglist))
                          :contingent-p contingent-p
                          :annotations annotations)
                         constraints)))))
             ('bool-clause
              (push (odo:make-constraint
                     (apply 'odo:and
                            (append (first evaluated-args)
                                    (mapcar #'odo:not (second evaluated-args))))
                     :contingent-p contingent-p
                     :annotations annotations)
                    constraints))
             (name
              (assert (symbolp name))
              (let ((odo-pred-arglist (append
                                       (form-predicate-args evaluated-args odo-parameters)
                                       (getf def :odo-additional))))
                (push (odo:make-constraint
                       (apply name odo-pred-arglist)
                       :contingent-p contingent-p
                       :annotations annotations)
                      constraints))))
           (dolist (constraint constraints)
             (if (odo:episode-p constraint)
                 (push constraint *episodes*)
                 (push constraint *constraints*)))
           constraints)))))
  (:documentation
    "Matches a constraint declaration. Produces the new constraint and as a side
    effect adds it to ~*CSP*~."))

;; ** Solving

(defrule solve-goal/satisfy
    (and "solve" (* whitespace) annotations (* whitespace) "satisfy" (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ annotations _ _ _ _)
       (list :satisfy annotations))))
  (:documentation
    "Matches a solve statement that specifies the problem should be
    satisfied. Returns a list of two elements. The first is ~:satisfy~, the
    second is a list of annotations on the solve statement."))

(defrule solve-goal/minimize
    (and "solve" (* whitespace) annotations (* whitespace) "minimize" (* whitespace)
         expr (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ annotations _ _ _ expr _ _)
       (list :minimize (resolve-flatzinc-arg 'variable expr :by :ref) annotations))))
  (:documentation
    "Matches a solve statement that specifies the problem should be
    minimized. Returns a list of three elements. The first is ~:minimize~, the
    second is the variable whose value should be minimized, and the third is a
    list of annotations on the solve statement."))

(defrule solve-goal/maximize
    (and "solve" (* whitespace) annotations (* whitespace) "maximize" (* whitespace)
         expr (* whitespace) #\;)
  (:lambda (l)
    (trivia:ematch l
      ((list _ _ annotations _ _ _ expr _ _)
       (list :maximize (resolve-flatzinc-arg 'variable expr :by :ref) annotations))))
  (:documentation
    "Matches a solve statement that specifies the problem should be
    maximized. Returns a list of three elements. The first is ~:maximize~, the
    second is the variable whose value should be maximized, and the third is a
    list of annotations on the solve statement."))

(defrule solve-goal
    (or solve-goal/satisfy
        solve-goal/minimize
        solve-goal/maximize)
  (:lambda (l)
    (flet ((handle-annotations (annotations)
             (ana:awhen (getf annotations :fzn/unknown-annotations)
               (dolist (a it)
                 (utils:log-warn "When parsing solve goal, ignoring unknown annotation: ~S" a)))
             (loop
               :for key :in annotations :by #'cddr
               :for val :in (rest annotations) :by #'cddr
               :do
                  (if (eql key :fzn/unknown-annotations)
                      (alex:appendf (getf *annotations* key) val)
                      (setf (getf *annotations* key) val)))))
      (trivia:ematch l
        ((list :minimize var annotations)
         (setf *objective-goal* :minimize
               *objective-function* var)
         (handle-annotations annotations))
        ((list :maximize var annotations)
         (setf *objective-goal* :maximize
               *objective-function* var)
         (handle-annotations annotations))
        ((list :satisfy annotations)
         (setf *objective-goal* :satisfy)
         (handle-annotations annotations)))))
  (:documentation
    "Matches a solve declaration. Modifies ~*CSP*~."))

;; ** Top level CSP
(defrule pred-decls
    (and (* (and pred-decl (* whitespace))))
  (:documentation
    "Matches a set of predicate declarations."))

(defrule param-decls
    (and (* (and param-decl (* whitespace))))
  (:documentation
    "Matches a set of parameter declarations."))

(defrule var-decls
    (and (* (and var-decl (* whitespace))))
  (:lambda (l)
    (mapcar #'first l))
  (:documentation
    "Matches a set of variable declarations."))

(defrule constraints
    (and (* (and constraint (* whitespace))))
  (:lambda (l)
    (mapcar #'first l))
  (:documentation
    "Matches a set of constraint declarations."))

(defrule flatzinc-csp
    (and (* whitespace)
         pred-decls
         (* whitespace)
         param-decls
         (* whitespace)
         var-decls
         (* whitespace)
         constraints
         (* whitespace)
         solve-goal
         (* whitespace))
  (:documentation
    "Matches an entire Flatzinc file."))

;; * Standard entry points

(defun parse-flatzinc-stream (stream return-type)
  "Given a stream containing a flatzinc file, parse it and return a CSP object
representing the contents of the file."
  (let ((*ss* (odo:make-state-space))
        (*annotations* nil)
        (*constraints* nil)
        (*episodes* nil)
        (*objective-goal* nil)
        (*objective-function* nil))
    (esrap:parse 'flatzinc-csp (uiop:slurp-stream-string stream))
    (if (or (not (null *episodes*))
            (subtypep return-type 'odo:state-plan))
        (odo:make-state-plan :state-space *ss*
                             :constraints *constraints*
                             :goal-episodes *episodes*
                             :annotations *annotations*)
        (odo:make-csp :state-space *ss* :constraints *constraints*
                      :objective-goal *objective-goal*
                      :objective-function *objective-function*
                      :annotations *annotations*))))

(defclass flatzinc-decoder ()
  ())

(io::register-decoder :flatzinc 'flatzinc-decoder :flatzinc 0)

(defmethod io::deserialize-object-from-stream ((decoder flatzinc-decoder) (format t) (type t) (stream t) (meta t))
  (parse-flatzinc-stream stream type))
