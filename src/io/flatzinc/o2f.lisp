(uiop:define-package #:odo/io/flatzinc/o2f
  (:use #:cl
        #:iterate)
  (:import-from #:40ants-doc #:defsection)
  (:local-nicknames (#:alex #:alexandria)))

(in-package #:odo/io/flatzinc/o2f)

(defsection @odo/io/flatzinc/o2f (:title "Odo to flatzinc")
  (odo:write-flatzinc-solution function))

(defgeneric %print-domain-to-fzn-stream (type var stream)
  (:documentation "Write a domain to STREAM, dispatching based on domain type."))

(defmethod %print-domain-to-fzn-stream ((type odo:pure-integer-domain) var stream)
  (format stream "~D" (odo:assigned-value var)))

(defmethod %print-domain-to-fzn-stream ((type odo:pure-real-domain) var stream)
  (flet ((float-to-string (f)
           (substitute #\e #\d (format nil "~f" f) :test #'char-equal)))
    (format stream "~A" (float-to-string (odo:assigned-value var)))))

(defun write-flatzinc-var-to-stream (var stream)
  "Given a variable, write its solution to stream."
  (let ((*print-pretty* nil))
    (format stream "~A = " (odo:name var))
    (%print-domain-to-fzn-stream (odo:domain-type var) var stream)
    (format stream ";~%")))

(defun write-flatzinc-array-to-stream (name dlist vars stream)
  "Given the name of a flatzinc array, its dimensions, and its variables, write
  it to stream."
  (let ((*print-pretty* nil))
    ;; write out the name and array dimensionality
    (format stream "~A = array~Dd(~{~{~D..~D~}, ~}[" name (length dlist) dlist)
    (loop
      :for pair :on vars
      :for var := (car pair)
      :for more-left-p := (cdr pair)
      :do (%print-domain-to-fzn-stream (odo:domain-type var) var stream)
      :if more-left-p
        :do (format stream ", ")
      :end)
    (format stream "]);~%")))

(defun write-flatzinc-line-to-stream (var-pair stream)
  "Write a line of flatzinc output."
  (if (odo:variable-p (cdr var-pair))
      (write-flatzinc-var-to-stream (cdr var-pair) stream)
      (write-flatzinc-array-to-stream (car var-pair) (car (cdr var-pair)) (cdr (cdr var-pair)) stream)))

(defun %get-variable-group (csp name)
  (let ((vector-spec (alex:assoc-value (odo:annotation csp :fzn/variable-vectors)
                                  name :test #'equal)))
    (when vector-spec
      (mapcar (lambda (spec)
                (trivia:ematch spec
                  ((list :var var-name)
                   (odo:state-space-get csp var-name))
                  ((list :const const)
                   const)))
              vector-spec))))

(defun odo:write-flatzinc-solution (csp &key stream)
  "Given a solved CSP, write its solution to STREAM in a flatzinc specified
format.

All output variables must have the ~(:fzn/output-var t)~ annotation and all
output variable groups must have the size of the output array stored in the
~:fzn/output-array~ annotation (stored in the ~:fzn/variable-group-annotations~
annotation on the CSP)."
  (cond
    ((null stream)
     (setf stream *standard-output*))
    ((eql t stream)
     (setf stream *terminal-io*)))
  (let* ((output-vars-alist))
    ;; Gather all the non-array output variables.
    (loop
      :with it := (odo:state-space-iterator csp)
      :until (odo:it-done it)
      :for variable := (odo:it-next it)
      :for output-annotation := (odo:annotation variable :fzn/output-var)
      :if output-annotation
        :do
           (assert (odo:assigned-p variable))
           (setf (alex:assoc-value output-vars-alist (odo:name variable) :test #'equal)
                 variable)
      :end)
    ;; Gather all the array output variables.
    (loop
      :for (group-name . group-annotations) :in (odo:annotation csp :fzn/variable-group-annotations)
      :for dlist := (first (getf group-annotations :fzn/output-array))
      :when dlist
        :do
           (setf (alex:assoc-value output-vars-alist group-name :test #'equal)
                 (cons dlist (coerce (%get-variable-group csp group-name) 'list))))
    (setf output-vars-alist (sort output-vars-alist #'string< :key #'car))
    (mapc (alex:rcurry #'write-flatzinc-line-to-stream stream) output-vars-alist))
  (format stream "----------~%")
  (values))
