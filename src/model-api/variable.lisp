(in-package #:odo/model-api)

(defsection @odo/model-api/variable (:title "Variables")
  "Variables are named instantiations of domains. They respond to the
same API as domains in addition to the following functions.

#### Types ####"
  (odo:variable class)
  (odo:odo-variable class)
  (odo:controllable-variable class)
  (odo:odo-controllable-variable class)
  (odo:uncontrollable-variable class)
  (odo:odo-uncontrollable-variable class)
  (odo:variable-p generic-function)
  (odo:odo-variable-p generic-function)
  "#### Constructors"
  (odo:make-variable function)
  (odo:make-integer-variable function)
  (odo:make-real-variable function)
  (odo:make-boolean-variable function)
  (odo:make-enumerated-variable function)
  "#### Equality testing"
  "#### Properties"
  "In addition to the `@ODO/MODEL-API/ANNOTATION` API, variables can
be queried for their name."
  (odo:variable-name generic-function)
  (odo:print-odo-variable function))


;;; Types

(defclass odo:variable (odo:object)
  ()
  (:documentation
   "A variable. Responds to:

+ `ODO-VARIABLE-P` with T
+ `VARIABLE-NAME`
+ `PRINT-ODO-VARIABLE`
+ `DOMAIN-TYPE` (and all functions the return value of this implies)"))

(utils:define-renamed-class odo:odo-variable odo:variable)

(defclass odo:controllable-variable (odo:variable)
  ()
  (:documentation "A variable that is assigned by the agent/solver/etc."))

(utils:define-renamed-class odo:odo-controllable-variable odo:controllable-variable)

(defclass odo:uncontrollable-variable (odo:variable)
  ()
  (:documentation "A variable that is assigned by Nature."))

(utils:define-renamed-class odo:odo-uncontrollable-variable odo:uncontrollable-variable)

(defgeneric odo:variable-p (obj)
  (:documentation "Returns T iff `OBJ` is an Odo variable.")
  (:method ((obj t))
    nil))

(utils:define-renamed-generic (odo:odo-variable-p odo:variable-p) (obj))


;;; Constructors

(defun odo:make-integer-variable (&key name uncontrollable-p annotations
                                    (min odo:+-infinity+) (max odo:+infinity+)
                                    (ranges (list (cons min max)))
                                    (values nil))
  "Instantiate and return a new variable with an integer domain.

See `MAKE-INTEGER-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-integer-domain
                 :ranges (utils:canonicalize-integer-ranges ranges)
                 :values values)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-variable
                           'odo:controllable-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :annotations annotations)))

(defun odo:make-real-variable (&key name uncontrollable-p annotations
                                 (min odo:+-infinity+) (max odo:+infinity+)
                                 (ranges (list (cons min max))))
  "Instantiate and return a new variable with a real domain.

See `MAKE-REAL-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-real-domain
                 :ranges (utils:canonicalize-ranges ranges :number-type 'real))))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-variable
                           'odo:controllable-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :annotations annotations)))

(defun odo:make-boolean-variable (&key name uncontrollable-p annotations
                                    (value :unknown)
                                    (true (ecase value
                                            ((:unknown t)
                                             t)
                                            ((nil)
                                             nil)))
                                    (false (ecase value
                                             ((:unknown nil)
                                              t)
                                             ((t)
                                              nil))))
  "Instantiate and return a new variable with an boolean domain.

See `MAKE-BOOLEAN-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-boolean-domain
                 :value value
                 :true true
                 :false false)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-variable
                           'odo:controllable-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :annotations annotations)))

(defun odo:make-enumerated-variable (&key name uncontrollable-p annotations
                                       (test #'equal) members)
  "Instantiate and return a new variable with an enumerated domain.

See `MAKE-ENUMERATED-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-enumerated-domain
                 :members members
                 :test test)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-variable
                           'odo:controllable-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :annotations annotations)))

(defun odo:make-variable (domain-or-spec
                          &rest
                            args
                          &key
                            name
                            uncontrollable-p
                            annotations
                          &allow-other-keys)
  "Instantiate and return a new variable.

If `DOMAIN-OR-SPEC` is an `ODO-DOMAIN`, then it is used as a template
to instantiate this variable. Note that implementations are *not*
allowed to just reference `DOMAIN-OR-SPEC`; it must be copied.

If `DOMAIN-OR-SPEC` is not an `ODO-DOMAIN`, then it and `ARGS` are
used to describe the domain as in `MAKE-DOMAIN`.

If `NAME` is not supplied, a name is autogenerated."
  (declare (ignore name annotations uncontrollable-p))
  (destructuring-bind (domain-type &rest domain-args)
      (if (odo:domain-p domain-or-spec)
          (list* (odo:domain-type domain-or-spec) (rest (domain-spec domain-or-spec)))
          (canonicalize-domain-description domain-or-spec))
    (let ((maker
           (etypecase domain-type
             (odo:pure-integer-domain
              #'odo:make-integer-variable)
             (odo:pure-real-domain
              #'odo:make-real-variable)
             (odo:pure-boolean-domain
              #'odo:make-boolean-variable)
             (odo:pure-discrete-domain
              #'odo:make-enumerated-variable))))
      (apply maker
             (nconc domain-args args)))))

(defmethod odo:copy-by-type (self (type odo:variable) &key context)
  (or (funcall (utils:odo-copy-context-var-map-fun context) self)
      (destructuring-bind (domain-type &rest args)
          (domain-spec self)
        (apply #'odo:make-variable domain-type
               :name (odo:name self)
               :annotations (copy-list (odo:annotation-plist self))
               :uncontrollable-p (typep type 'odo:uncontrollable-variable)
               args))))

(defmethod odo:equal-p-by-type (left right (left-type odo:variable) (right-type odo:variable))
  (and
   ;; Handle if one is uncontrollable and the other is not.
   (eql left-type right-type)
   (equal (odo:name left) (odo:name right))
   (odo:domain-equal left right)))


;;; Variable API

(utils:define-renamed-generic (odo:variable-name odo:name) (variable))

(defun odo:print-odo-variable (var stream)
  (utils:with-delimiter-printing (var stream)
    (write (odo:name var) :stream stream)
    (write-char #\space stream)
    (write-char #\( stream)
    (write-string
      (etypecase (odo:type var)
        (odo:controllable-variable
         (uiop:standard-case-symbol-name "controllable"))
        (odo:uncontrollable-variable
         (uiop:standard-case-symbol-name "uncontrollable")))
      stream)
    (write-char #\) stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (odo:print-odo-domain var stream)))
