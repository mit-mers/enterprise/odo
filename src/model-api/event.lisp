(in-package #:odo/model-api)

(defsection @odo/model-api/event (:title "Events")
  "An event is a special type of variable that represents an instant in time. It
is a variable that has a numeric (real or integer) domain. Additionally, it
contains a list of observations performed at the event.

#### Classes ####"
  (odo:event class)
  (odo:odo-event class)
  (odo:conditional-event class)
  (odo:odo-conditional-event class)
  (odo:controllable-event class)
  (odo:odo-controllable-event class)
  (odo:controllable-conditional-event class)
  (odo:odo-controllable-conditional-event class)
  (odo:uncontrollable-event class)
  (odo:odo-uncontrollable-event class)
  (odo:uncontrollable-delayed-event class)
  (odo:odo-uncontrollable-delayed-event class)
  (odo:uncontrollable-conditional-event class)
  (odo:odo-uncontrollable-conditional-event class)
  (odo:event-p generic-function)
  (odo:odo-event-p generic-function)
  "#### Constructors ####"
  (odo:make-event function)
  "#### Properties ####"
  (odo:event-name generic-function)
  (odo:event-conditional-p generic-function)
  (odo:event-uncontrollable-p generic-function)
  (odo:delayed-p generic-function)
  (odo:event-delayed-p generic-function)
  (odo:observations generic-function)
  (odo:event-observations generic-function))



(defclass odo:event (odo:variable)
  ()
  (:documentation
   "The event class for Odo."))

(utils:define-renamed-class odo:odo-event odo:event)

(defclass odo:conditional-event (odo:event odo:conditional-variable)
  ()
  (:documentation
   "Represents a conditional event."))

(utils:define-renamed-class odo:odo-conditional-event odo:conditional-event)

(defclass odo:controllable-event (odo:event odo:controllable-variable)
  ()
  (:documentation
   "An event that is scheduled by the agent/solver/etc."))

(utils:define-renamed-class odo:odo-controllable-event odo:controllable-event)

(defclass odo:controllable-conditional-event (odo:conditional-event odo:controllable-variable)
  ()
  (:documentation
   "A conditional event that is scheduled by the agent/solver/etc."))

(utils:define-renamed-class odo:odo-controllable-conditional-event odo:controllable-conditional-event)

(defclass odo:uncontrollable-event (odo:event odo:uncontrollable-variable)
  ()
  (:documentation
   "An event that is scheduled by Nature."))

(utils:define-renamed-class odo:odo-uncontrollable-event odo:uncontrollable-event)

(defclass odo:uncontrollable-delayed-event (odo:uncontrollable-event)
  ()
  (:documentation
   "An event that is scheduled by Nature and is observed after an uncertain delay."))

(utils:define-renamed-class odo:odo-uncontrollable-delayed-event odo:uncontrollable-delayed-event)

(defclass odo:uncontrollable-conditional-event (odo:conditional-event odo:uncontrollable-variable)
  ()
  (:documentation
   "A conditional event that is scheduled by Nature."))

(utils:define-renamed-class odo:odo-uncontrollable-conditional-event odo:uncontrollable-conditional-event)

(defgeneric odo:event-p (event)
  (:documentation
   "Returns T iff `EVENT` is an Odo episode.")
  (:method ((event t))
    nil))

(utils:define-renamed-generic (odo:odo-event-p odo:event-p) (event))



(utils:define-renamed-generic (odo:event-name odo:name) (event))

(utils:define-renamed-generic (odo:event-conditional-p odo:conditional-p) (event))

(utils:define-renamed-generic (odo:event-uncontrollable-p odo:uncontrollable-p) (event))

(defgeneric odo:delayed-p (event)
  (:documentation
   "Returns T iff observation of the event is delayed. If event observation is delayed,
the value must be an uncontrollable variable."))

(utils:define-renamed-generic (odo:event-delayed-p odo:delayed-p :redirect t) (event))

(defgeneric odo:observations (event)
  (:documentation
   "Returns a list of uncontrollable variables and events that are observed at
this event."))

(utils:define-renamed-generic (odo:event-observations odo:observations :redirect t) (event))


;;; Constructor
(defun odo:make-event (&key
                         (name (gensym "EV"))
                         conditional-p
                         uncontrollable-p
                         delayed-p

                         (type 'real)
                         value
                         (min (if value value 0))
                         (max (if value value odo:+infinity+))
                         (ranges (list (cons min max)))

                         observations
                         annotations)
  "Instantiate and return a new event.

`TYPE` must be either `REAL` (default) or `INTEGER`. The behavior is undefined
if more than one of `VALUE`, (`MIN` and `MAX`), and `RANGES` are
defined. `OBSERVATIONS` is a list of uncontrollable variables observed at this
event."
  (let ((domain (ecase type
                  (real
                   (odo:make-real-domain
                    :ranges (utils:canonicalize-ranges ranges)))
                  (integer
                   (odo:make-integer-domain
                    :ranges (utils:canonicalize-integer-ranges ranges))))))
    (if conditional-p
        (odo:make-instance 'odo:conditional-event
                           :name name
                           :uncontrollable-p uncontrollable-p
                           :domain domain
                           :observations observations
                           :annotations annotations)
        (odo:make-instance 'odo:event
                           :name name
                           :uncontrollable-p uncontrollable-p
                           :delayed-p delayed-p
                           :domain domain
                           :observations observations
                           :annotations annotations))))


;;; Copying

;; TODO: Handle type and friends!
(defmethod odo:copy-by-type (self (type odo:event) &key context)
  (or (funcall (utils:odo-copy-context-var-map-fun context) self)
      (odo:make-event :name (odo:name self)
                      :conditional-p (odo:conditional-p self)
                      :uncontrollable-p (odo:uncontrollable-p self)
                      :delayed-p (odo:delayed-p self)
                      :annotations (copy-list (odo:annotation-plist self)))))
