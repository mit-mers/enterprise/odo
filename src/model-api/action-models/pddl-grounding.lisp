(in-package #:odo/model-api)

(defsection @odo/model-api/action-models/pddl-grounding (:title "Ground PDDL Action Models")
  "PDDL Grounding"
  (ground-pddl-domain function))


;; Ground predicates, functions, actions, etc. in PDDL domain to objects

(defvar *name2type* nil)

(defvar *mangler-table* nil)

(defun ground-pddl-domain (domain objects)
  (let* ((pddl-types (odo:pddl-domain-types domain))
         (*name2type* (mapcar #'(lambda (type)
                                  (cons (odo:name type) type))
                              pddl-types))
         (*mangler-table* (make-hash-table :test #'equalp)))

    (let* ((predicates (odo:pddl-domain-predicates domain))
           (functions (odo:pddl-domain-functions domain))
           (control-variables (odo:pddl-domain-control-variables domain))
           (actions (odo:pddl-domain-actions domain))
           ;; Ground PDDL predicates, functions, control-variables and actions
           (grounded-predicates (compute-grounded-predicates predicates objects))
           (grounded-functions (compute-grounded-functions functions objects))
           (grounded-control-variables (compute-grounded-control-variables control-variables objects))
           (grounded-actions (compute-grounded-actions actions objects)))

      (values
       ;; Return grounded PDDL domain and the mangler table
       (odo:make-pddl-domain
        (odo:name domain)
        (remove :typing (odo:pddl-domain-requirements domain))
        :types nil
        :constants (odo:pddl-domain-constants domain)
        :control-variables grounded-control-variables
        :predicates grounded-predicates
        :functions grounded-functions
        :actions grounded-actions)
       *mangler-table*))))

(defun compute-grounded-predicates (predicates objects)
  (let ((grounded-predicates nil))
    (loop :for predicate :in predicates
          :for predicate-name := (odo:name predicate)
          :for parameters := (odo:pddl-predicate-parameters predicate)
          :for types := (mapcar #'odo:pddl-parameter-type parameters)
          :for grounded-arguments := (compute-arguments-with-matching-types objects types)
          :do (loop :for args :in grounded-arguments
                    :for mangled-name := (mangle-name predicate-name args)
                    :do (push (odo:make-pddl-predicate mangled-name)
                              grounded-predicates)))
    grounded-predicates))

(defun compute-grounded-functions (functions objects)
  (let ((grounded-functions nil))
    (loop :for function :in functions
          :for function-name := (odo:name function)
          :for parameters := (odo:pddl-function-parameters function)
          :for types := (mapcar #'odo:pddl-parameter-type parameters)
          :for grounded-arguments := (compute-arguments-with-matching-types objects types)
          :do (loop :for args :in grounded-arguments
                    :for mangled-name := (mangle-name function-name args)
                    :do (push (odo:make-pddl-function mangled-name)
                              grounded-functions)))
    grounded-functions))

(defun compute-grounded-control-variables (control-variables objects)
  (let ((grounded-control-variables nil))
    (loop :for variable :in control-variables
          :for variable-name := (odo:name variable)
          :for bounds := (odo:bounds variable)
          :for parameters := (odo:pddl-control-variable-parameters variable)
          :for types := (mapcar #'odo:pddl-parameter-type parameters)
          :for grounded-arguments := (compute-arguments-with-matching-types objects types)
          :do (loop :for args :in grounded-arguments
                    :for mangled-name := (mangle-name variable-name args)
                    :do (push (odo:make-pddl-control-variable mangled-name bounds)
                              grounded-control-variables)))
    grounded-control-variables))

(defun compute-grounded-actions (actions objects)
  (let ((grounded-actions nil))
    (loop :for action :in actions
          :for action-name := (odo:name action)
          :for parameters := (odo:pddl-action-parameters action)
          :for parameter-names := (mapcar #'odo:name parameters)
          :for types := (mapcar #'odo:pddl-parameter-type parameters)
          :for grounded-arguments := (compute-arguments-with-matching-types objects types)
          :do (loop :for args :in grounded-arguments
                    :for argmap := (mapcar #'cons parameter-names args)
                    :for mangled-name := (mangle-name action-name args)
                    :do (if (odo:type-p action 'odo:pddl-durative-action)
                            (push (odo:make-pddl-durative-action
                                   mangled-name
                                   :duration (odo:pddl-action-duration action)
                                   :condition (car (ground-args-in-odo-expression
                                                    (odo:condition action)
                                                    argmap))
                                   :effect (car (ground-args-in-odo-expression
                                                 (odo:effect action)
                                                 argmap)))
                                  grounded-actions)
                            (push (odo:make-pddl-snap-action
                                   mangled-name
                                   :condition (car (ground-args-in-odo-expression
                                                    (odo:condition action)
                                                    argmap))
                                   :effect (car (ground-args-in-odo-expression
                                                 (odo:effect action)
                                                 argmap)))
                                  grounded-actions))))
    grounded-actions))



(defun ground-args-in-odo-expression (odo-expression argmap)
  (cond
    ((numberp odo-expression)
     (cons odo-expression t))
    ((symbolp odo-expression)
     (cons odo-expression t))
    (:otherwise
     (ground-args-in-odo-expression-by-type odo-expression (odo:type odo-expression) argmap))))

(defgeneric ground-args-in-odo-expression-by-type (expression type argmap))

(defmethod ground-args-in-odo-expression-by-type (odo-expression (type odo:application-expression) argmap)
  (let* ((operator (odo:operator odo-expression))
         (arguments (mapcar #'(lambda (arg)
                                (ground-args-in-odo-expression arg argmap))
                            (odo:arguments odo-expression)))
         (parameterp (mapcar #'cdr arguments))
         (args (mapcar #'car arguments)))

    (if (some #'(lambda (x)
                  (equal x :parameter))
              parameterp)
        (progn
          (assert (every #'(lambda (x)
                             (equal x :parameter))
                         parameterp))
          (cons (odo:@ (mangle-name operator args)) t))
        (cons (apply #'odo:@ (push operator args)) t))))

(defmethod ground-args-in-odo-expression-by-type (odo-expression (type odo:lookup-expression) argmap)
  (let ((name (odo:lookup-expression-name odo-expression)))
    (if (alex:assoc-value argmap name :test #'equalp)
        (cons (alex:assoc-value argmap name :test #'equalp) :parameter)
        (cons (odo:lookup name) t))))



(defun compute-arguments-with-matching-types (objects types)
  (let ((arguments (if (> (length types) 1)
                       (compute-arguments-with-matching-types objects (cdr types))
                       (list nil)))
        (updated-arguments nil))
    (let* ((type (car types))
           (matching-objects (get-objects-of-type objects type)))
      (loop :for obj :in matching-objects
            :do (loop :for args :in arguments
                      :do (push (list* obj args) updated-arguments))))
    updated-arguments))

(defun get-objects-of-type (objects type)
  (loop :for (obj . obj-type) :in objects
        :if (pddl-subtypep obj-type type)
          :collect obj))

(defun pddl-subtypep (type1 type2)
  (or (string-equal type1 type2)
      (let ((pddl-type1 (alex:assoc-value *name2type* type1 :test #'string-equal)))
        (and (not (null pddl-type1)) ;; object type does not correspond to a pddl-type
             (not (null (odo:supertype pddl-type1)))
             (pddl-subtypep (odo:supertype pddl-type1)
                            type2)))))

(defun mangle-name (name args)
  (if (gethash (list* name args) *mangler-table*)
      (gethash (list* name args) *mangler-table*)
      (let ((symbol (intern (format nil "~A~{-~A~}" name args))))
        (setf (gethash (list* name args) *mangler-table*) symbol)
        symbol)))



;; (defun test-pddl-grounding ()
;;   (let ((domain (odo:deserialize (asdf:system-relative-pathname :odo "pddl-domain-parameter.pddl") :format :pddl))
;;         (objects '((my-world . "world")
;;                    (your-world . "world"))))
;;     (multiple-value-bind (grounded-domain mangler-table)
;;         (odo:ground-pddl-domain domain objects)
;;       (break)
;;       (format t "~A~%" mangler-table)
;;       (odo:serialize-to-file grounded-domain  (asdf:system-relative-pathname :odo "pddl-domain-tmp-3.pddl") :if-exists :supersede :format :pddl))))

;; (defun test-pddl-parameterized ()
;;   (let ((domain (odo:deserialize (asdf:system-relative-pathname :odo "pddl-domain-parameter.pddl") :format :pddl)))
;;     (odo:serialize-to-file domain  (asdf:system-relative-pathname :odo "pddl-domain-tmp-2.pddl") :if-exists :supersede :format :pddl)))
