(in-package #:odo/model-api)

(defsection @odo/model-api/action-models/pddl (:title "PDDL Action Models")
  "PDDL"
  (odo:make-pddl-control-variable function)
  (odo:make-pddl-domain function)
  (odo:make-pddl-durative-action function)
  (odo:make-pddl-function function)
  (odo:make-pddl-parameter function)
  (odo:make-pddl-predicate function)
  (odo:make-pddl-snap-action function)
  (odo:make-pddl-type function)
  (odo:pddl-action class)
  (odo:condition generic-function)
  (odo:pddl-action-condition generic-function)
  (odo:pddl-action-duration generic-function)
  (odo:effect generic-function)
  (odo:pddl-action-effect generic-function)
  (odo:pddl-action-name generic-function)
  (odo:pddl-action-parameters generic-function)
  (odo:pddl-assign odo-function)
  (odo:pddl-at odo-function)
  (odo:pddl-constant class)
  (odo:pddl-constant-name generic-function)
  (odo:pddl-constant-type generic-function)
  (odo:pddl-control-variable class)
  (odo:pddl-control-variable-bounds generic-function)
  (odo:pddl-control-variable-name generic-function)
  (odo:pddl-control-variable-parameters generic-function)
  (odo:pddl-decrease odo-function)
  (odo:pddl-domain class)
  (odo:pddl-domain-action generic-function)
  (odo:pddl-domain-actions generic-function)
  (odo:pddl-domain-constant generic-function)
  (odo:pddl-domain-constants generic-function)
  (odo:pddl-domain-control-variable generic-function)
  (odo:pddl-domain-control-variables generic-function)
  (odo:pddl-domain-function generic-function)
  (odo:pddl-domain-functions generic-function)
  (odo:pddl-domain-name generic-function)
  (odo:pddl-domain-predicate generic-function)
  (odo:pddl-domain-predicates generic-function)
  (odo:pddl-domain-requirements generic-function)
  (odo:pddl-domain-type generic-function)
  (odo:pddl-domain-types generic-function)
  (odo:pddl-durative-action class)
  (odo:pddl-function class)
  (odo:pddl-function-name generic-function)
  (odo:pddl-function-parameters generic-function)
  (odo:pddl-function-type generic-function)
  (odo:pddl-increase odo-function)
  (odo:pddl-over odo-function)
  (odo:pddl-parameter class)
  (odo:pddl-parameter-name generic-function)
  (odo:pddl-parameter-type generic-function)
  (odo:pddl-predicate class)
  (odo:pddl-predicate-name generic-function)
  (odo:pddl-predicate-parameters generic-function)
  (odo:pddl-snap-action class)
  (odo:pddl-type class)
  (odo:pddl-type-name generic-function)
  (odo:supertype generic-function)
  (odo:pddl-type-supertype generic-function))


;; Actions are lifted representations.

;; A PDDL action can be durative or not. It has a duration (maybe), conditions,
;; and effects.

(defclass odo:pddl-domain (odo:object)
  ())

(defclass odo:pddl-type (odo:object)
  ()
  (:documentation
   "Represents a type in a PDDL domain. A type has a name and a supertype. There
are two toplevel types (with NIL supertypes) - OBJECT and NUMBER."))

(defclass odo:pddl-constant (odo:object)
  ()
  (:documentation
   "Represents a constant value in a PDDL domain. Constants have a name and a
type."))

(defclass odo:pddl-parameter (odo:object)
  ()
  (:documentation
   "Represents an argument to a PDDL predicate, function, or action. Parameters
have a name and a type."))

(defclass odo:pddl-predicate (odo:object)
  ()
  (:documentation
   "Represents a boolean predicate in a PDDL domain. Predicates have a name and
a list of parameters."))

(defclass odo:pddl-function (odo:object)
  ()
  (:documentation
   "Represents a fluent in a PDDL domain. Functions have a name, list of
parameters, and a type."))

(defclass odo:pddl-control-variable (odo:object)
  ()
  (:documentation
   "A Scotty-specific extension to PDDL. Describes a continuous variable that
can be specified by the planner."))

(defclass odo:pddl-action (odo:object)
  ()
  (:documentation
   "Represents an action in a PDDL domain."))

(defclass odo:pddl-snap-action (odo:pddl-action)
  ()
  (:documentation
   "Represents a snap (instantaneous) action in a PDDL domain."))

(defclass odo:pddl-durative-action (odo:pddl-action)
  ()
  (:documentation
   "Represents a durative action in a PDDL domain."))



;; * Types

(utils:define-renamed-generic (odo:pddl-type-name odo:name) (pddl-type))

(defgeneric odo:supertype (pddl-type)
  (:documentation
   "Return the name of the supertype of PDDL-TYPE. Returns NIL if there is no
supertype."))

(utils:define-renamed-generic (odo:pddl-type-supertype odo:supertype :redirect t) (pddl-type))

(defun odo:make-pddl-type (name supertype)
  (odo:make-instance 'odo:pddl-type
                     :name name
                     :supertype supertype))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-type) (right-type odo:pddl-type))
  (and (string-equal (odo:name left) (odo:name right))
       (string-equal (odo:supertype left) (odo:supertype right))))


;; * Constants

(utils:define-renamed-generic (odo:pddl-constant-name odo:name) (pddl-constant))

(defgeneric odo:pddl-constant-type (pddl-constant)
  (:documentation
   "Returns the name of the type of PDDL-CONSTANT."))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-constant) (right-type odo:pddl-constant))
  (and (string-equal (odo:name left) (odo:name right))
       (string-equal (odo:pddl-constant-type left) (odo:pddl-constant-type right))))


;; * Parameters

(utils:define-renamed-generic (odo:pddl-parameter-name odo:name) (pddl-parameter))

(defgeneric odo:pddl-parameter-type (pddl-parameter)
  (:documentation
   "Returns the type of PDDL-PARAMETER. Returns either a symbol, which names the
type, or a list of symbols, which name a set of potential types (see PDDL's
EITHER type combinator)."))

(defun odo:make-pddl-parameter (name type)
  (odo:make-instance 'odo:pddl-parameter
                     :name name
                     :type type))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-parameter) (right-type odo:pddl-parameter))
  (and (string-equal (odo:name left) (odo:name right))
       (alex:set-equal (alex:ensure-list (odo:pddl-parameter-type left))
                       (alex:ensure-list (odo:pddl-parameter-type right))
                       :test #'string-equal)))


;; * Predicates

(utils:define-renamed-generic (odo:pddl-predicate-name odo:name) (pddl-predicate))

(defgeneric odo:pddl-predicate-parameters (pddl-predicate)
  (:documentation
   "Returns a list of parameters to PDDL-PREDICATE."))

(defun odo:make-pddl-predicate (name &key parameters)
  (odo:make-instance 'odo:pddl-predicate
                     :name name
                     :parameters parameters))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-predicate) (right-type odo:pddl-predicate))
  (and (string-equal (odo:name left) (odo:name right))
       (alex:length= (odo:pddl-predicate-parameters left) (odo:pddl-predicate-parameters right))
       (every #'odo:equal-p (odo:pddl-predicate-parameters left) (odo:pddl-predicate-parameters right))))


;; * Functions

(utils:define-renamed-generic (odo:pddl-function-name odo:name) (pddl-function))

(defgeneric odo:pddl-function-parameters (pddl-function)
  (:documentation
   "Returns a list of parameters to PDDL-FUNCTION."))

(defgeneric odo:pddl-function-type (pddl-function)
  (:documentation
   "Returns the name of the type of PDDL-FUNCTION."))

(defun odo:make-pddl-function (name &key parameters (type 'number))
  (odo:make-instance 'odo:pddl-function
                     :name name
                     :parameters parameters
                     :type type))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-function) (right-type odo:pddl-function))
  (and (string-equal (odo:name left) (odo:name right))
       (string-equal (odo:pddl-function-type left) (odo:pddl-function-type right))
       (alex:length= (odo:pddl-function-parameters left) (odo:pddl-function-parameters right))
       (every #'odo:equal-p (odo:pddl-function-parameters left) (odo:pddl-function-parameters right))))


;; * Control Variables

(utils:define-renamed-generic (odo:pddl-control-variable-name odo:name) (pddl-control-variable))

(utils:define-renamed-generic (odo:pddl-control-variable-bounds odo:bounds) (pddl-control-variable &key state-space))

(defgeneric odo:pddl-control-variable-parameters (pddl-control-variable))

(defun odo:make-pddl-control-variable (name bounds &key parameters)
  (odo:make-instance 'odo:pddl-control-variable
                     :name name
                     :bounds bounds
                     :parameters parameters))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-control-variable) (right-type odo:pddl-control-variable))
  (and (string-equal (odo:name left) (odo:name right))
       (equal (odo:bounds left) (odo:bounds right))
       (alex:length= (odo:pddl-control-variable-parameters left) (odo:pddl-control-variable-parameters right))
       (every #'odo:equal-p (odo:pddl-control-variable-parameters left) (odo:pddl-control-variable-parameters right))))


;; * Actions

(utils:define-renamed-generic (odo:pddl-action-name odo:name) (pddl-action))

(defgeneric odo:pddl-action-parameters (pddl-action)
  (:documentation
   "Returns the parameters of PDDL-ACTION."))

(defgeneric odo:pddl-action-duration (pddl-action)
  (:documentation
   "Returns the duration of PDDL-ACTION as an expression."))

(defgeneric odo:condition (pddl-action)
  (:documentation
   "Returns the condition that must be satisfied for PDDL-ACTION to be applied
as an expression."))

(utils:define-renamed-generic (odo:pddl-action-condition odo:condition :redirect t) (pddl-action))

(defgeneric odo:effect (pddl-action)
  (:documentation
   "Returns the effect of applying PDDL-ACTION as an expression."))

(utils:define-renamed-generic (odo:pddl-action-effect odo:effect :redirect t) (pddl-action))

(defun odo:make-pddl-snap-action (name &key parameters condition effect)
  (odo:make-instance 'odo:pddl-snap-action
                     :name name
                     :parameters parameters
                     :condition condition
                     :effect effect))

(defun odo:make-pddl-durative-action (name &key parameters condition effect duration)
  (odo:make-instance 'odo:pddl-durative-action
                     :name name
                     :parameters parameters
                     :duration duration
                     :condition condition
                     :effect effect))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-snap-action) (right-type odo:pddl-snap-action))
  (and (string-equal (odo:name left) (odo:name right))
       (alex:length= (odo:pddl-action-parameters left) (odo:pddl-action-parameters right))
       (every #'odo:equal-p (odo:pddl-action-parameters left) (odo:pddl-action-parameters right))
       (odo:equal-p (odo:effect left) (odo:effect right))
       (odo:equal-p (odo:condition left) (odo:condition right))))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-durative-action) (right-type odo:pddl-durative-action))
  (and (string-equal (odo:name left) (odo:name right))
       (alex:length= (odo:pddl-action-parameters left) (odo:pddl-action-parameters right))
       (every #'odo:equal-p (odo:pddl-action-parameters left) (odo:pddl-action-parameters right))
       (odo:equal-p (odo:pddl-action-duration left) (odo:pddl-action-duration right))
       (odo:equal-p (odo:effect left) (odo:effect right))
       (odo:equal-p (odo:condition left) (odo:condition right))))


;; * Expressions

(define-odo-function odo:pddl-at (time-specifier expression)
  (:documentation
   "States that EXPRESSION must hold at TIME-SPECIFIER. TIME-SPECIFIER may be
either :START or :END."))

(define-odo-function odo:pddl-over (time-interval expression)
  (:documentation
   "States that EXPRESSION must hold over the TIME-INTERVAL. TIME-INTERVAL may
be :ALL."))

(define-odo-function odo:pddl-increase (function delta)
  (:documentation
   "States that the FUNCTION increases by amount DELTA."))

(define-odo-function odo:pddl-decrease (function delta)
  (:documentation
   "States that the FUNCTION decreases by amount DELTA."))

(define-odo-function odo:pddl-assign (function value)
  (:documentation
   "States that the FUNCTION takes on VALUE."))


;; * Domains

(utils:define-renamed-generic (odo:pddl-domain-name odo:name) (pddl-domain))

(defgeneric odo:pddl-domain-requirements (pddl-domain)
  (:documentation
   "Return a list of keywords describing the requirements of PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-types (pddl-domain)
  (:documentation
   "Return a list of types defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-type (pddl-domain type-name &optional errorp)
  (:documentation
   "Return the type from PDDL-DOMAIN with name TYPE-NAME. If ERRORP is
non-NIL (default), an error is raised if the type is missing. Names are compared
via STRING-EQUAL."))

(defgeneric odo:pddl-domain-constants (pddl-domain)
  (:documentation
   "Return a list of constants defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-constant (pddl-domain constant-name &optional errorp)
  (:documentation
   "Return the constant from PDDL-DOMAIN with name CONSTANT-NAME. If ERRORP is
non-NIL (default), an error is raised if the constant is missing. Names are
compared via STRING-EQUAL."))

(defgeneric odo:pddl-domain-predicates (pddl-domain)
  (:documentation
   "Return a list of predicates defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-predicate (pddl-domain predicate-name &optional errorp)
  (:documentation
   "Return the predicate from PDDL-DOMAIN with name PREDICATE-NAME. If ERRORP is
non-NIL (default), an error is raised if the predicate is missing. Names are
compared via STRING-EQUAL."))

(defgeneric odo:pddl-domain-functions (pddl-domain)
  (:documentation
   "Return a list of functions defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-function (pddl-domain function-name &optional errorp)
  (:documentation
   "Return the function from PDDL-DOMAIN with name FUNCTION-NAME. If ERRORP is
non-NIL (default), an error is raised if the function is missing. Names are
compared via STRING-EQUAL."))

(defgeneric odo:pddl-domain-control-variables (pddl-domain)
  (:documentation
   "Return a list of control-variables defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-control-variable (pddl-domain control-variable-name &optional errorp)
  (:documentation
   "Return the control-variable from PDDL-DOMAIN with name
CONTROL-VARIABLE-NAME. If ERRORP is non-NIL (default), an error is raised if the
control-variable is missing. Names are compared via STRING-EQUAL."))

(defgeneric odo:pddl-domain-actions (pddl-domain)
  (:documentation
   "Return a list of actions defined in PDDL-DOMAIN."))

(defgeneric odo:pddl-domain-action (pddl-domain action-name &optional errorp)
  (:documentation
   "Return the action from PDDL-DOMAIN with name ACTION-NAME. If ERRORP is
non-NIL (default), an error is raised if the action is missing. Names are
compared via STRING-EQUAL."))

(defun odo:make-pddl-domain (name requirements
                             &key types
                               constants control-variables predicates
                               functions actions)
  (odo:make-instance 'odo:pddl-domain
                     :name name
                     :requirements requirements
                     :types types
                     :constants constants
                     :control-variables control-variables
                     :predicates predicates
                     :functions functions
                     :actions actions))

(defmethod odo:equal-p-by-type (left right (left-type odo:pddl-domain) (right-type odo:pddl-domain))
  (and (string-equal (odo:name left) (odo:name right))
       (alex:set-equal (odo:pddl-domain-requirements left) (odo:pddl-domain-requirements right))
       (alex:set-equal (odo:pddl-domain-types left) (odo:pddl-domain-types right)
                       :test #'odo:equal-p)
       (alex:set-equal (odo:pddl-domain-constants left) (odo:pddl-domain-constants right)
                       :test #'odo:equal-p)
       (alex:set-equal (odo:pddl-domain-control-variables left) (odo:pddl-domain-control-variables right)
                       :test #'odo:equal-p)
       (alex:set-equal (odo:pddl-domain-predicates left) (odo:pddl-domain-predicates right)
                       :test #'odo:equal-p)
       (alex:set-equal (odo:pddl-domain-functions left) (odo:pddl-domain-functions right)
                       :test #'odo:equal-p)
       (alex:set-equal (odo:pddl-domain-actions left) (odo:pddl-domain-actions right)
                       :test #'odo:equal-p)))
