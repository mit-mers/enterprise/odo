(in-package #:odo/model-api)

(defsection @odo/model-api/annotation (:title "Annotations")
  "Many Odo objects are able to be annotated using arbitrary
information provided by an outside source. This API defines how to
access this annotation information."
  (odo:annotation generic-function)
  (odo:annotation-iterator generic-function)
  (odo:annotation-plist generic-function))

(defgeneric odo:annotation (obj annotation-name &optional default-value)
  (:documentation "Get the annotation `ANNOTATION-NAME` associated
with `OBJ`. If it does not exist, return `DEFAULT-VALUE`. Annotation
names are compared with `EQL`.

A common annotation is `:SOURCE`, which commonly contains information
related to where the object came from.

`SETF`'able."))

(defgeneric (setf odo:annotation) (value obj annotation-name
                                   &optional default-value))

(defgeneric odo:annotation-iterator (object)
  (:documentation "Return an iterator following the `@ODO/UTILS/ITER`
API. Each call to the iterator returns two values. The first is the
annotation name. The second is the annotation value."))

(defgeneric odo:annotation-plist (object)
  (:documentation "Returns two values. The first value is a plist of
all annotations on the object. The second is T if the list is freshly
consed, NIL otherwise.")
  (:method (obj)
    (values (iter:iter
              (iter:with it = (odo:annotation-iterator obj))
              (iter:until (odo:it-done it))
              (iter:for (values key val) = (odo:it-next it))
              (iter:nconcing (list key val)))
            t)))
