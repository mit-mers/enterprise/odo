(in-package #:odo/model-api)

(defsection @odo/model-api/episode (:title "Episodes")
  "An episode represents both a temporal and state constraint. The temporal
constraint relates two events and the state constraint represents a relation
between state variables in a state plan that must hold at the start event, end
event, some time between the start and end, or every time between the start and
end. Additionally, an episode may have an activity associated with it (typically
representing a primitive action some agent can perform).

#### Classes"
  (odo:episode class)
  (odo:odo-episode class)
  (odo:episode-p generic-function)
  (odo:odo-episode-p generic-function)
  "#### Constructors ####"
  (odo:make-episode function)
  "#### Properties ####"
  (odo:activity-args generic-function)
  (odo:activity-name generic-function)
  (odo:temporal-constraint generic-function)
  (odo:start-constraints generic-function)
  (odo:end-constraints generic-function)
  (odo:over-all-constraints generic-function)
  (odo:once-constraints generic-function)
  (odo:episode-name generic-function)
  (odo:episode-conditional-p generic-function)
  (odo:episode-start-event generic-function)
  (odo:end-event generic-function)
  (odo:episode-end-event generic-function)
  (odo:episode-temporal-constraint generic-function)
  (odo:episode-start-constraints generic-function)
  (odo:episode-end-constraints generic-function)
  (odo:episode-over-all-constraints generic-function)
  (odo:episode-once-constraints generic-function)
  (odo:episode-activity-name generic-function)
  (odo:episode-activity-args generic-function))



(defclass odo:episode (odo:object)
  ()
  (:documentation
   "Represents an Odo episode"))

(utils:define-renamed-class odo:odo-episode odo:episode)

(defgeneric odo:episode-p (ep)
  (:documentation
   "Returns T iff `EP` is an Odo episode.")
  (:method ((ep t))
    nil))

(utils:define-renamed-generic (odo:odo-episode-p odo:episode-p) (ep))



(utils:define-renamed-generic (odo:episode-name odo:name) (ep))

(utils:define-renamed-generic (odo:episode-conditional-p odo:conditional-p) (ep))

(utils:define-renamed-generic (odo:episode-start-event odo:start-event) (ep))

(defgeneric odo:end-event (ep)
  (:documentation
   "Returns the end event of episode `EP`."))

(utils:define-renamed-generic (odo:episode-end-event odo:end-event :redirect t) (ep))

(defgeneric odo:temporal-constraint (ep)
  (:documentation
   "Returns the temporal constraint for the episode."))

(utils:define-renamed-generic (odo:episode-temporal-constraint odo:temporal-constraint :redirect t) (ep))

(defgeneric odo:start-constraints (ep)
  (:documentation
   "Returns the state and transition constraints that must hold at the start
event."))

(utils:define-renamed-generic (odo:episode-start-constraints odo:start-constraints :redirect t) (ep))

(defgeneric odo:end-constraints (ep)
  (:documentation
   "Returns the state and transition constraints that must hold at the end
event."))

(utils:define-renamed-generic (odo:episode-end-constraints odo:end-constraints :redirect t) (ep))

(defgeneric odo:over-all-constraints (ep)
  (:documentation
   "Returns the state and transition constraints that must hold over the entire
duration of the episode."))

(utils:define-renamed-generic (odo:episode-over-all-constraints odo:over-all-constraints :redirect t) (ep))

(defgeneric odo:once-constraints (ep)
  (:documentation
   "Returns the state and transition constraints that must hold at some point
during the duration of the episode."))

(utils:define-renamed-generic (odo:episode-once-constraints odo:once-constraints :redirect t) (ep))

(defgeneric odo:activity-name (ep)
  (:documentation
   "Returns the name of the activity associated with this episode. Returns `NIL`
if there is no activity."))

(utils:define-renamed-generic (odo:episode-activity-name odo:activity-name :redirect t) (ep))

(defgeneric odo:activity-args (ep)
  (:documentation
   "Returns a list of arguments for the activity associated with this
episode. Returns `NIL` if there is no activity. Arguments may be Odo variables,
in which case the actual value of the argument is the *evaluated* value of the
variable."))

(utils:define-renamed-generic (odo:episode-activity-args odo:activity-args :redirect t) (ep))


;;; Constructor
(defun odo:make-episode (&key
                           name
                           conditional-p
                           start-event
                           end-event
                           temporal-constraint
                           start-constraints
                           end-constraints
                           over-all-constraints
                           once-constraints
                           activity-name
                           activity-args
                           annotations
                         &allow-other-keys)
  "Instantiate and return a new episode."
  (odo:make-instance 'odo:episode
                     :name (or name (gensym (uiop:standard-case-symbol-name 'epi)))
                     :conditional-p conditional-p
                     :start-event start-event
                     :end-event end-event
                     :temporal-constraint temporal-constraint
                     :start-constraints start-constraints
                     :end-constraints end-constraints
                     :over-all-constraints over-all-constraints
                     :once-constraints once-constraints
                     :activity-name activity-name
                     :activity-args activity-args
                     :annotations annotations))

(defmethod odo:scope-by-type (ep (type odo:episode) &key state-space)
  (iter
    (for thing in (append (list (odo:temporal-constraint ep))
                          (odo:start-constraints ep)
                          (odo:end-constraints ep)
                          (odo:over-all-constraints ep)
                          (odo:once-constraints ep)))
    (unioning (odo:scope thing :state-space state-space))))


;;; Copy

(defmethod odo:copy-by-type (ep (type odo:episode) &key context)
  (odo:make-episode :name (odo:name ep)
                    :conditional-p (odo:conditional-p ep)
                    :start-event (odo:copy (odo:start-event ep) :context context)
                    :end-event (odo:copy (odo:end-event ep) :context context)
                    :temporal-constraint (odo:copy (odo:temporal-constraint ep) :context context)
                    :start-constraints (mapcar (lambda (x) (odo:copy x :context context))
                                               (odo:start-constraints ep))
                    :end-constraints (mapcar (lambda (x) (odo:copy x :context context))
                                             (odo:end-constraints ep))
                    :over-all-constraints (mapcar (lambda (x) (odo:copy x :context context))
                                                  (odo:over-all-constraints ep))
                    :once-constraints (mapcar (lambda (x) (odo:copy x :context context))
                                              (odo:once-constraints ep))
                    :activity-name (odo:activity-name ep)
                    :activity-args (mapcar (lambda (x) (odo:copy x :context context))
                                           (odo:activity-args ep))
                    :annotations (copy-list (odo:annotation-plist ep))))
