(in-package #:odo/model-api)

(defsection @odo/model-api/infinity (:title "Infinity")
  (odo:+infinity+ constant)
  (odo:+-infinity+ constant)
  (odo:symbolic-infinity-p function))

(defconstant odo:+infinity+ :infinity
  "A symbol representing positive infinity.")

(defconstant odo:+-infinity+ :-infinity
  "A symbol representing negative infinity.")

(defun odo:symbolic-infinity-p (maybe-infinity)
  (or (eql maybe-infinity odo:+infinity+)
      (eql maybe-infinity odo:+-infinity+)))
