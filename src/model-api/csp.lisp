(in-package #:odo/model-api)

(defsection @odo/model-api/csp (:title "Constraint Satisfaction Problem")
  "An Odo constraint satisfaction problem (CSP) consists of a state
space, a set of constraints, and an objective."
  (odo:make-csp function)
  (odo:csp class)
  (odo:odo-csp class)
  (odo:csp-p generic-function)
  (odo:odo-csp-p generic-function)
  "#### State Space ####

The first part of a CSP is its state space. Use the following function
to retrieve it:"
  (odo:csp-state-space generic-function)
  "Additionally, a CSP forwards state space functions to its state
space. Therefore, a CSP also responds to the following functions:

+ `STATE-SPACE-ADD!`
+ `STATE-SPACE-GET`
+ `STATE-SPACE-ASSIGNED-P`"
  "#### Constraints ####

Use the following functions to add and retrieve constraints:"
  (odo:add-constraint! generic-function)
  (odo:remove-constraint! generic-function)
  (odo:get-constraint generic-function)
  (odo:constraint-iterator generic-function)
  (odo:constraint-list generic-function)
  "#### Objective ####

A CSP has an objective associated with it. The objective can be
retrieved (and set) using the following functions:"
  (odo:objective-goal generic-function)
  (odo:objective-function generic-function)
  "#### Equality Testing ####"
  (odo:csp-equal function)
  "#### Conditions ####"
  (odo:csp-error condition)
  (odo:simple-csp-error condition))



(define-condition odo:csp-error ()
  ())

(define-condition odo:simple-csp-error (odo:csp-error simple-error)
  ())

(defun raise-csp-error (format-control &rest format-args)
  (error 'odo:simple-csp-error
         :format-control format-control
         :format-arguments format-args))



(defclass odo:csp (odo:object)
  ()
  (:documentation
   "Class describing the CSP type."))

(utils:define-renamed-class odo:odo-csp odo:csp)

(defgeneric odo:csp-p (obj)
  (:documentation "Returns T iff `OBJ` is an Odo CSP.")
  (:method ((obj t))
    nil))

(utils:define-renamed-generic (odo:odo-csp-p odo:csp-p) (obj))


;;; Define API for problems.

(utils:define-renamed-generic (odo:csp-state-space odo:state-space) (csp))

(defgeneric odo:objective-goal (csp)
  (:documentation "Returns the goal of the objective. One of
`:SATISFY`, `:MINIMIZE`, or `:MAXIMIZE`.

`SETF`'able."))

(defgeneric odo:objective-function (csp)
  (:documentation "If the `OBJECTIVE-GOAL` is `:MINIMIZE` or `:MAXIMIZE`, this
function returns what is being minimized or maximized.

Currently supported objects are:

+ variables with numeric domains

+ expressions (see `@ODO/MODEL-API/EXPRESSIONS`) that evaluate to reals

+ functions that take a single argument (a fully assigned CSP) and return a
numeric value

+ Objects that respond to `EVALUATE-OBJECTIVE`

`SETF`'able"))

(defgeneric (setf odo:objective-function) (value csp))

(defgeneric (setf odo:objective-goal) (value csp))

(defgeneric state-space (csp)
  (:documentation "Get the state space associated with `CSP`."))

(defgeneric odo:get-constraint (csp name &optional missing-error-p missing-value)
  (:documentation "Find and return the constraint in `CSP` with `NAME`.

If no constraint exists with `NAME`, and `MISSING-ERROR-P` is T (the
default), then a `CSP-ERROR` raised. Otherwise, `MISSING-VALUE` is
returned."))

(defgeneric odo:add-constraint! (csp constraint-or-seq)
  (:documentation "Add one or more constraints to the `CSP`.

If `CONSTRAINT-OR-SEQ` is a sequence, `ADD-CONSTRAINT!` is called on
each element of the sequence. If `CONSTRAINT-OR-SEQ` is an Odo
constraint, a `CSP-ERROR` is signalled if a constraint with the same
name already exists in the `CSP`. Adding the same constraint multiple
times does not raise an error, instead no change is made to the
`CSP`."))

(defgeneric odo:remove-constraint! (csp constraint-or-seq)
  (:documentation "Remove one or more constraints to the `CSP`.

If `CONSTRAINT-OR-SEQ` is a sequence, `REMOVE-CONSTRAINT!` is called
on each element of the sequence. If `CONSTRAINT-OR-SEQ` is an Odo
constraint, a `CSP-ERROR` is signalled if the constraint does not
exist in the `CSP`. The variables in the scope of the constraint
will not be removed from the `CSP`."))

(defun odo:make-csp (&key
                       (state-space (odo:make-state-space))
                       constraints
                       (objective-goal :satisfy)
                       objective-function
                       annotations)
  "Make and return an Odo problem.

+ If `STATE-SPACE` is provided, it becomes the backing state space for
this problem.

+ If `CONSTRAINTS` is provided, the constraints are added to the CSP
by `ADD-CONSTRAINT!`

+ `OBJECTIVE-GOAL` is a keyword as described by `OBJECTIVE-GOAL`

+ `OBJECTIVE-FUNCTION` is an object allowed by `OBJECTIVE-FUNCTION`

+ `ANNOTATIONS` is a plist of annotations for this CSP."
  (ana:aprog1
      (odo:make-instance 'odo:csp
                         :state-space state-space
                         :annotations annotations
                         :objective-goal objective-goal
                         :objective-function objective-function)
    (when constraints
      (odo:add-constraint! it constraints))))

(defun odo:csp-equal (csp-1 csp-2)
  "Returns T iff `CSP-1` and `CSP-2` are equal.

+ State spaces are equal,
+ All constraints are `CONSTRAINT-EQUAL`."
  (and
   (odo:csp-p csp-1)
   (odo:csp-p csp-2)
   (or
    (eql csp-1 csp-2)
    (and
     (odo:equal-p (odo:state-space csp-1)
                  (odo:state-space csp-2))
     (alex:set-equal (odo:constraint-list csp-1)
                     (odo:constraint-list csp-2)
                     :test #'odo:equal-p)
     (utils:plist-equal (odo:annotation-plist csp-1)
                        (odo:annotation-plist csp-2))
     (eql (odo:objective-goal csp-1) (odo:objective-goal csp-2))
     (if (eql (odo:objective-goal csp-1) :satisfy)
         t
         (odo:equal-p (odo:objective-function csp-1) (odo:objective-function csp-2)))))))

(defmethod odo:copy-by-type (self (type odo:csp) &key context)
  (let ((new-ss (odo:copy (odo:state-space self) :context context)))
    (ana:aprog1 (odo:make-csp
                 :state-space new-ss
                 :objective-goal (odo:objective-goal self)
                 :objective-function  (ana:awhen (odo:objective-function self)
                                        (cond
                                          ((functionp it)
                                           it)
                                          ((odo:attribute-objective-function-p it)
                                           (odo:make-attribute-objective-function
                                            :attribute-values-alist
                                            (mapcar (lambda (x)
                                                      (let* ((old-var (car x))
                                                             (var-name (odo:name old-var))
                                                             (new-var (odo:state-space-get new-ss var-name)))
                                                        (cons new-var (cdr x))))
                                                    (odo:attribute-values-alist it))))
                                          (t
                                           (odo:copy it :context context))))
                 :annotations (copy-list (odo:annotation-plist self)))
      (loop
        :with c-it := (odo:constraint-iterator self)
        :until (odo:it-done c-it)
        :for c := (odo:it-next c-it)
        :do
           (odo:add-constraint! it (odo:copy c :context context))))))




(defun print-problem-contents (prob stream)
  ;; This complicated sorting of constraints is due to odo#80 and SBCL#309090.
  (let* ((raw-constraints (copy-list (odo:constraint-list prob)))
         (string-constraints (remove-if-not #'utils:string-sortable-p raw-constraints
                                            :key #'odo:name))
         (non-string-constraints (remove-if #'utils:string-sortable-p raw-constraints
                                            :key #'odo:name))
         (constraints (append (utils:string-sort string-constraints :key #'odo:name)
                              non-string-constraints)))
    (prin1 :state-space stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (pprint-state-space stream (odo:state-space prob))
    (pprint-newline :mandatory stream)
    (prin1 :constraints stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (pprint-logical-block (stream constraints :prefix "(" :suffix ")")
      (pprint-exit-if-list-exhausted)
      (loop
         (write (pprint-pop) :stream stream)
         (pprint-exit-if-list-exhausted)
         (write-char #\space stream)
         (pprint-newline :mandatory stream)))
    (pprint-newline :mandatory stream)
    (prin1 :objective-goal stream)
    (write-char #\Space stream)
    (pprint-newline :fill stream)
    (prin1 (odo:objective-goal prob) stream)
    (unless (eql (odo:objective-goal prob) :satisfy)
      (write-char #\space stream)
      (pprint-newline :mandatory stream)
      (prin1 :objective-function stream)
      (write-char #\Space stream)
      (pprint-newline :fill stream)
      (let ((obj (odo:objective-function prob)))
        (cond
          ((odo:variable-p obj)
           (pprint-logical-block (stream nil :prefix "(" :suffix ")")
             (prin1 :var stream)
             (write-char #\space stream)
             (pprint-newline :fill stream)
             (write (odo:name obj)
                    :stream stream)))
          ((functionp obj)
           (pprint-logical-block (stream nil :prefix "(" :suffix ")")
             (prin1 :function stream)
             (write-char #\space stream)
             (pprint-newline :fill stream)
             (write obj
                    :stream stream)))
          ((odo:attribute-objective-function-p obj)
           (pprint-logical-block (stream nil :prefix "(" :suffix ")")
             (prin1 :attribute stream)
             (write-char #\space stream)
             (pprint-newline :fill stream)
             (write (mapcar (lambda (var-spec)
                              (cons (odo:name (car var-spec)) (cdr var-spec)))
                            (odo:attribute-values-alist obj))
                    :stream stream)))
          ((odo:expression-p obj)
           (pprint-logical-block (stream nil :prefix "(" :suffix ")")
             (prin1 :expression stream)
             (write-char #\space stream)
             (pprint-newline :fill stream)
             (utils:print-odo-object obj stream)))
          (t
           (write obj :stream stream)))))))

(defun pprint-problem (stream prob)
  (utils:with-delimiter-printing (prob stream)
    (print-problem-contents prob stream)))
