(in-package #:odo/model-api)

(defsection @odo/model-api/state-plan (:title "State Plans")
  "A state plan describes the evolution of some state space over time. It
consists of state variables and parameters, events (representing time points),
temporal constraints relating the events, and episodes representing time evolved
constraints on the state space and/or activities. Furthermore episodes can
represent goals that must be satisfied in any solution or values that are
guaranteed to be true.

There are many variants of state plans. Episodes can have activities associated
with them, there can be chance constraints, episodes can describe
\"qualitative\" constraints (this is a work in progress to define what
qualitative constraints truly are), etc. Instead of making a variant of the
state plan API for each flavor of state plan, a single API is used. In order to
advertise which portions of the API are implemented by any given state plan
object, *features* are used.

The defined features and their meanings are:

+ `:activities` :: If present, episodes are allowed to have an activity
associated with them.

+ `:chance-constrained` :: If present, the state plan is allowed to have chance
constraints.

+ `:conditional` :: If present, the state plan's episodes, events, variables,
constraints, and state variables are allowed to be conditional."
  (odo:state-plan class)
  (odo:odo-state-plan class)
  (odo:state-plan-p generic-function)
  (odo:odo-state-plan-p generic-function)
  "#### Constructors ####"
  (odo:make-state-plan function)
  "#### Modifiers ####"
  (odo:add-episode! generic-function)
  (odo:add-goal-episode! generic-function)
  (odo:add-value-episode! generic-function)
  (odo:add-chance-constraint! generic-function)
  (odo:add-state-plan-chance-constraint! generic-function)
  "#### Iterators ####"
  (odo:chance-constraint-iterator generic-function)

  (odo:state-plan-episode-iterator generic-function)
  (odo:goal-episode-iterator generic-function)
  (odo:state-plan-goal-episode-iterator generic-function)
  (odo:value-episode-iterator generic-function)
  (odo:state-plan-value-episode-iterator generic-function)
  (odo:event-iterator generic-function)
  (odo:state-plan-event-iterator generic-function)
  (odo:state-plan-chance-constraint-iterator generic-function)
  (odo:state-plan-constraint-iterator generic-function)
  "#### Properties ####"
  (odo:chance-constraint-list generic-function)

  (odo:state-plan-name generic-function)
  (odo:objective generic-function)
  (odo:state-plan-objective generic-function)
  (odo:features generic-function)
  (odo:state-plan-features generic-function)
  (odo:state-plan-feature-p generic-function)
  (odo:state-plan-episode-list generic-function)
  (odo:goal-episode-list generic-function)
  (odo:state-plan-goal-episode-list generic-function)
  (odo:value-episode-list generic-function)
  (odo:state-plan-value-episode-list generic-function)
  (odo:event-list generic-function)
  (odo:state-plan-event-list generic-function)
  (odo:state-plan-start-event generic-function)
  (odo:state-plan-chance-constraint-list generic-function)
  (odo:state-plan-constraint-list generic-function)
  (odo:state-plan-state-space generic-function))



(defclass odo:state-plan (odo:object)
  ()
  (:documentation
   "Represents an object following the Odo State Plan API."))

(utils:define-renamed-class odo:odo-state-plan odo:state-plan)

(defgeneric odo:state-plan-p (state-plan)
  (:documentation
   "Returns T iff `STATE-PLAN` is an Odo State Plan.")
  (:method ((state-plan t))
    nil))

(utils:define-renamed-generic (odo:odo-state-plan-p odo:state-plan-p) (state-plan))



(utils:define-renamed-generic (odo:state-plan-name odo:name) (state-plan))

(defgeneric odo:objective (state-plan)
  (:documentation
   "Returns the objective of the state plan."))

(utils:define-renamed-generic (odo:state-plan-objective odo:objective :redirect t) (state-plan))

(defgeneric odo:features (state-plan)
  (:documentation
   "Returns a list of features of the state plan."))

(utils:define-renamed-generic (odo:state-plan-features odo:features :redirect t) (state-plan))

(defgeneric odo:state-plan-feature-p (state-plan feature)
  (:documentation
   "Returns non-NIL iff `FEATURE` is included in the state plan's feature
list.")
  (:method (state-plan feature)
    (member feature (odo:features state-plan))))

(defgeneric odo:goal-episode-list (state-plan)
    (:documentation
     "Returns two values. The first is a list of the goal episodes in
`STATE-PLAN`. The second is T if the list is freshly consed, and NIL
otherwise.")
  (:method (state-plan)
    (values (odo:it-list (odo:goal-episode-iterator state-plan)) t)))

(utils:define-renamed-generic (odo:state-plan-goal-episode-list odo:goal-episode-list) (state-plan))

(defgeneric odo:value-episode-list (state-plan)
    (:documentation
     "Returns two values. The first is a list of the value episodes in
`STATE-PLAN`. The second is T if the list is freshly consed, and NIL
otherwise.")
  (:method (state-plan)
    (values (odo:it-list (odo:value-episode-iterator state-plan)) t)))

(utils:define-renamed-generic (odo:state-plan-value-episode-list odo:value-episode-list) (state-plan))

(utils:with-deprecation (:style-warning "0.5.0"
                         :warning "0.6.0"
                         :delete "0.7.0")
  (defgeneric odo:state-plan-episode-list (state-plan)
    (:documentation
     "*DEPRECATED* - Use `ODO:GOAL-EPISODE-LIST`

Returns two values. The first is a list of the episodes in `STATE-PLAN`. The
second is T if the list is freshly consed, and NIL otherwise."))
  (defmethod odo:state-plan-episode-list (state-plan)
    ;; Prevent deprecation warnings
    (declare (notinline odo:state-plan-episode-iterator))
    (values (odo:it-list (odo:state-plan-episode-iterator state-plan)) t)))

(defgeneric odo:event-list (state-plan)
  (:documentation
   "Returns two values. The first is a list of the events in `STATE-PLAN`. The
second is T if the list is freshly consed, and NIL otherwise.")
  (:method (state-plan)
    (values (odo:it-list (odo:event-iterator state-plan)) t)))

(utils:define-renamed-generic (odo:state-plan-event-list odo:event-list) (state-plan))

(defgeneric odo:chance-constraint-list (state-plan)
  (:documentation
   "Returns two values. The first is a list of the chance constraints in
`STATE-PLAN`. The second is T if the list is freshly consed, and NIL
otherwise.")
  (:method (state-plan)
    (values (odo:it-list (odo:chance-constraint-iterator state-plan)) t)))

(utils:define-renamed-generic (odo:state-plan-chance-constraint-list odo:chance-constraint-list)
    (state-plan))

(utils:define-renamed-generic (odo:state-plan-constraint-list odo:constraint-list)
    (state-plan))

(utils:define-renamed-generic (odo:state-plan-state-space odo:state-space) (state-plan))

(utils:define-renamed-generic (odo:state-plan-start-event odo:start-event)
    (state-plan)
  (:documentation
   "Returns the start event associated with this state plan. All other events
must occur after or simultaneously with the start event. SETF'able."))


;;; Constructor
(defun odo:make-state-plan (&key
                              name
                              objective
                              features
                              goal-episodes
                              value-episodes
                              constraints
                              start-event
                              chance-constraints
                              state-space
                              annotations
                              ;; DEPRECATED
                              episodes
                            &allow-other-keys)
  "Instantiate and return a new state-plan."
  (ana:aprog1 (odo:make-instance 'odo:state-plan
                                 :state-space (or state-space (odo:make-state-space))
                                 :name (or name (gensym (uiop:standard-case-symbol-name 'state-plan)))
                                 :objective objective
                                 :features features
                                 :annotations annotations)
    (let ((start-event (or start-event
                           (odo:make-event :value 0))))
      (odo:state-space-add! (odo:state-space it) start-event)
      (setf (odo:start-event it) start-event)
      (when episodes
        (warn 'deprecated-arg-condition :arg-name :episodes)
        (apply #'odo:add-episode! it episodes))
      (when goal-episodes
        (apply #'odo:add-goal-episode! it goal-episodes))
      (when value-episodes
        (apply #'odo:add-value-episode! it value-episodes))
      (when constraints
        (odo:add-constraint! it constraints))
      (when chance-constraints
        (apply #'odo:add-chance-constraint! it chance-constraints)))))


;;; Modifiers

(defgeneric odo:add-goal-episode! (state-plan &rest episodes)
  (:documentation
   "Add goal episodes to the `STATE-PLAN`."))

(defgeneric odo:add-value-episode! (state-plan &rest episodes)
  (:documentation
   "Add value episodes to the `STATE-PLAN`."))

(utils:with-deprecation (:style-warning "0.5.0"
                         :warning "0.6.0"
                         :delete "0.7.0")
  (defgeneric odo:add-episode! (state-plan &rest episodes)
    (:documentation
     "*DEPRECATED* - Use `ADD-GOAL-EPISODE!`.

Adds the provided episodes to `STATE-PLAN`."))
  (defmethod odo:add-episode! :before ((state-plan t) &rest episodes)
    (declare (ignore episodes))))

(defgeneric odo:add-chance-constraint! (state-plan &rest chance-constraints)
  (:documentation
   "Adds the provided chance constraints to `STATE-PLAN`."))

(utils:define-renamed-generic (odo:add-state-plan-chance-constraint! odo:add-chance-constraint! :redirect t)
                              (state-plan &rest chance-constraints))


;;; Iterators

(defgeneric odo:goal-episode-iterator (state-plan)
  (:documentation
   "Return an iterator following the `@ODO/UTILS/ITER` API. Each call to the
iterator returns a single goal episode of the state plan."))

(utils:define-renamed-generic (odo:state-plan-goal-episode-iterator odo:goal-episode-iterator :redirect t)
                              (state-plan))

(defgeneric odo:value-episode-iterator (state-plan)
  (:documentation
   "Return an iterator following the `@ODO/UTILS/ITER` API. Each call to the
iterator returns a single value episode of the state plan."))

(utils:define-renamed-generic (odo:state-plan-value-episode-iterator odo:value-episode-iterator :redirect t)
                              (state-plan))

(utils:with-deprecation (:style-warning "0.5.0"
                         :warning "0.6.0"
                         :delete "0.7.0")
  (defgeneric odo:state-plan-episode-iterator (state-plan)
    (:documentation
     "*DEPRECATED* - Use `GOAL-EPISODE-ITERATOR`

Return an iterator following the `@ODO/UTILS/ITER` API. Each call to the
iterator returns a single episode of the state plan."))
  (defmethod odo:state-plan-episode-iterator :before ((state-plan t))))

(defgeneric odo:event-iterator (state-plan)
  (:documentation
   "Return an iterator following the `@ODO/UTILS/ITER` API. Each call to the
iterator returns a single event of the state plan."))

(utils:define-renamed-generic (odo:state-plan-event-iterator odo:event-iterator :redirect t)
                              (state-plan))

(defgeneric odo:chance-constraint-iterator (state-plan)
  (:documentation
   "Return an iterator following the `@ODO/UTILS/ITER` API. Each call to the
iterator returns a single chance constraint of the state plan."))

(utils:define-renamed-generic (odo:state-plan-chance-constraint-iterator odo:chance-constraint-iterator
                                                                         :redirect t)
    (state-plan))

(utils:define-renamed-generic (odo:state-plan-constraint-iterator odo:constraint-iterator) (state-plan))


;;; Equality

(defmethod odo:equal-p-by-type (left right
                                (left-type odo:state-plan)
                                (right-type odo:state-plan))
  "State plans are equal if their names, features, state spaces, start events,
episodes, constraints, and chance constraints are equal."
  (and
   (eq left-type right-type)
   (equal (odo:name left) (odo:name right))
   (alex:set-equal (odo:features left) (odo:features right))
   (odo:equal-p (odo:state-space left) (odo:state-space right))
   (odo:equal-p (odo:start-event left) (odo:start-event right))
   (alex:set-equal (odo:goal-episode-list left) (odo:goal-episode-list right)
                   :test #'odo:equal-p)
   (alex:set-equal (odo:value-episode-list left) (odo:value-episode-list right)
                   :test #'odo:equal-p)
   (alex:set-equal (odo:constraint-list left) (odo:constraint-list right)
                   :test #'odo:equal-p)
   (alex:set-equal (odo:chance-constraint-list left) (odo:chance-constraint-list right)
                   :test #'odo:equal-p)))


;;; Copy

(defmethod odo:copy-by-type (sp (type odo:state-plan) &key context)
  (odo:make-state-plan :name (odo:name sp)
                       :objective (ana:awhen (odo:objective sp)
                                    (odo:copy it :context context))
                       :features (copy-list (odo:features sp))
                       :goal-episodes (mapcar (lambda (x) (odo:copy x :context context))
                                              (odo:goal-episode-list sp))
                       :value-episodes (mapcar (lambda (x) (odo:copy x :context context))
                                               (odo:value-episode-list sp))
                       :constraints (mapcar (lambda (x) (odo:copy x :context context))
                                            (odo:constraint-list sp))
                       :start-event (odo:copy (odo:start-event sp) :context context)
                       :chance-constraints (mapcar (lambda (x) (odo:copy x :context context))
                                                   (odo:chance-constraint-list sp))
                       :state-space (odo:copy (odo:state-space sp) :context context)
                       :annotations (copy-list (odo:annotation-plist sp))))
