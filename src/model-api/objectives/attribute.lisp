(in-package #:odo/model-api)

(defsection @odo/model-api/objectives/attribute (:title "Attribute objective function")
  "Attribute objective functions map the domain elements for a subset of the
variables in the CSP to values. The value of the objective is then the sum of
those values once all variables have been assigned."
  (odo:make-attribute-objective-function function)
  (odo:attribute-objective-function class)
  (odo:odo-attribute-objective-function class)
  (odo:attribute-objective-attribute-values-alist generic-function)
  (odo:attribute-objective-function-p generic-function)
  (odo:attribute-values-alist generic-function))

(defclass odo:attribute-objective-function (odo:object)
  ()
  (:documentation
   "Represents an attribute objective function Odo object."))

(utils:define-renamed-class odo:odo-attribute-objective-function odo:attribute-objective-function)

(defun odo:make-attribute-objective-function (&rest args &key attribute-values-alist)
  "Make an attribute objective function backed by the current implementation.

See `ATTRIBUTE-OBJECTIVE-ATTRIBUTE-VALUES-ALIST` for the format of
`ATTRIBUTE-VALUES-ALIST` argument."
  (declare (ignore attribute-values-alist))
  (apply #'odo:make-instance 'odo:attribute-objective-function args))

(defgeneric odo:attribute-values-alist (objective-function)
  (:documentation "Given an attribute objective function, return an alist of the
form:

    ((VARIABLE-INSTANCE . ((DOMAIN-ELEMENT . ATTRIBUTE-VALUE) ...)) ...)

The returned alist must *not* be modified."))

(utils:define-renamed-generic (odo:attribute-objective-attribute-values-alist odo:attribute-values-alist :redirect t) (objective-function))

(defgeneric odo:attribute-objective-function-p (obj)
  (:documentation "Returns T iff OBJ is an attribute objective function.")
  (:method ((obj t))
    nil))
