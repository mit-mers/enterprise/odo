(in-package #:odo/model-api)

(defgeneric odo:evaluate-objective (objective csp)
  (:documentation "Evaluate the objective function given an assigned CSP."))
