(in-package #:odo/model-api)

(defsection @odo/model-api/state-space (:title "State space")
  "A state space describes a set of variables. Additionally, a state space
allows you to store named variable groups for ease of use. Use the following
function to instantiate a state space."
  (odo:make-state-space function)
  "#### Types ####"
  (odo:state-space class)
  (odo:odo-state-space class)
  (odo:state-space-p generic-function)
  (odo:odo-state-space-p generic-function)
  "#### Variables ####

To add and retrieve objects from the state space, use the following functions:"
  (odo:state-space-add! generic-function)
  (odo:state-space-add-by-type! generic-function)
  (odo:state-space-get generic-function)
  (odo:state-space-iterator generic-function)
  (odo:state-space-list generic-function)
  (odo:state-space-assigned-p generic-function)
  "#### Conditions ####"
  (odo:state-space-error condition)
  "#### Writing ####"
  (odo:write-state-space-to-stream function)
  (odo:write-state-space-to-file function)
  "#### Externalizing ####"
  (odo:externalize-state-space generic-function))


;;; Types

(defclass odo:state-space (odo:object)
  ())

(utils:define-renamed-class odo:odo-state-space odo:state-space)

(defgeneric odo:state-space-p (obj)
  (:documentation "Return T iff `OBJ` is an Odo state space.")
  (:method ((obj t))
    nil))

(utils:define-renamed-generic (odo:odo-state-space-p odo:state-space-p) (obj))


;;; Define our conditions

(define-condition odo:state-space-error ()
  ())

(define-condition simple-state-space-error (odo:state-space-error simple-error)
  ())

(defun raise-state-space-error (format-control &rest format-args)
  (error 'simple-state-space-error
         :format-control format-control
         :format-arguments format-args))


;;; Define API for state spaces.

(defgeneric odo:state-space-add-by-type! (state-space object type)
  (:documentation
   "Add an object to the state-space. The Odo type of the object is provided as
the third argument. This is the primary method that implementations should
provide."))

(defmethod odo:state-space-add-by-type! ((state-space t) (object t) (type t))
  "Default method. Raises a state space error"
  (raise-state-space-error "State space ~S does not support adding ~S" state-space object))


(defgeneric odo:state-space-add! (state-space object)
  (:documentation
   "Add the given object to the state-space. Raises a `state-space-error` if the
state space does not support the object."))

(defmethod odo:state-space-add! (state-space object)
  "Default method. Simply calls `STATE-SPACE-ADD-BY-TYPE!`."
  (odo:state-space-add-by-type! state-space object (odo:type object)))

(defmethod odo:state-space-add! (state-space (object sequence))
  "If called with a sequence, add every object in the sequence to the state
space. This method is primarily future proofing for user defined
sequences. Lists and vectors have more specialized methods."
  (iter
    (for o :in-sequence object)
    (odo:state-space-add! state-space o))
  state-space)

(defmethod odo:state-space-add! (state-space (object list))
  "Add every object in the list to the state space."
  (loop
    :for o :in object
    :do (odo:state-space-add! state-space o))
  state-space)

(defmethod odo:state-space-add! (state-space (object vector))
  "Add every object in the vector to the state space."
  (loop
    :for o :across object
    :do (odo:state-space-add! state-space o))
  state-space)

(defgeneric odo:state-space-get (state-space name
                                 &optional missing-error-p missing-value)
  (:documentation "Find and return the object (variable, state-variable, event,
etc.) in `STATE-SPACE` with `NAME`.

If `MISSING-ERROR-P` is T (must be default on all specializations), a
`STATE-SPACE-ERROR` is raised if the variable does not exist. Otherwise,
`MISSING-VALUE` (must default to `NIL` in all specializations) is returned."))

(defgeneric odo:state-space-assigned-p (state-space)
  (:documentation "Returns T iff the controllable portion of `STATE-SPACE`
is fully assigned."))

(defmethod odo:state-space-assigned-p ((state-space t))
  (unless (odo:state-space-p state-space)
    (raise-state-space-error "Object does not follow the Odo state space API."))
  (loop :with iter := (odo:state-space-iterator state-space)
     :until (odo:it-done iter)
     :for var := (odo:it-next iter)
     :always (or (eq (make-instance 'odo:uncontrollable-variable)
                     (odo:type var))
                 (eq (make-instance 'odo:uncontrollable-conditional-variable)
                     (odo:type var))
                 (odo:assigned-p var))))

(defun odo:make-state-space (&key
                               variables
                               annotations)
  "Make and return an Odo state space.

+ If [`VARIABLES`][dislocated] is provided, the variables are added to
the state space with `STATE-SPACE-ADD!`

+ `ANNOTATIONS` is a plist of annotations for this state space."
  (ana:aprog1
      (odo:make-instance 'odo:state-space :annotations annotations)
    (when variables
      (odo:state-space-add! it variables))))

(defgeneric odo:state-space-iterator (state-space)
  (:documentation "Return an iterator following the `@ODO/UTILS/ITER` API. Each
call to the iterator returns a single object (variable, state-variable, event,
etc.) from the state space."))

(defgeneric odo:state-space-list (state-space)
  (:documentation "Returns two values. The first value is a list of all objects
in the state space. The second is T if the list is freshly consed, NIL
otherwise.")
  (:method (ss)
    (values (odo:it-list (odo:state-space-iterator ss)) t)))

(defmethod odo:copy-by-type (ss (type odo:state-space) &key context)
  (ana:aprog1 (odo:make-state-space
               :annotations (copy-list (odo:annotation-plist ss)))
    (loop
      :with var-it := (odo:state-space-iterator ss)
      :until (odo:it-done var-it)
      :do (multiple-value-bind (var)
              (odo:it-next var-it)
            (odo:state-space-add! it (odo:copy var :context context))))))

(defun print-state-space-contents (ss stream)
  ;; This complicated sorting of variables is due to odo#80 and SBCL#309090.
  (let* ((raw-vars (copy-list (odo:state-space-list ss)))
         (string-vars (remove-if-not #'utils:string-sortable-p raw-vars :key #'odo:name))
         (non-string-vars (remove-if #'utils:string-sortable-p raw-vars :key #'odo:name))
         vars)

    (setf string-vars (utils:string-sort string-vars
                                         :key #'odo:name))
    (setf vars (append string-vars non-string-vars))

    (prin1 :variables stream)
    (write-char #\space stream)
    (pprint-logical-block (stream vars :prefix "(" :suffix ")")
      (loop
         (write (pprint-pop) :stream stream)
         (pprint-exit-if-list-exhausted)
         (write-char #\space stream)
         (pprint-newline :mandatory stream)))))

(defun pprint-state-space (stream ss)
  (utils:with-delimiter-printing (ss stream)
    (print-state-space-contents ss stream)))

(defun odo:write-state-space-to-stream (state-space stream)
  "Write the `STATE-SPACE` to `STREAM`. The output format should not
be considered stable."
  (let ((*print-pretty* t)
        (utils:*odo-pprint-unreadable* nil)
        (*print-escape* nil))
    (pprint-logical-block (stream nil :prefix "(" :suffix ")")
      (princ 'state-space stream)
      (write-char #\space stream)
      (pprint-newline :mandatory stream)
      (print-state-space-contents state-space stream))))

(defun odo:write-state-space-to-file (state-space pathname
                                      &rest keys
                                      &key if-exists if-does-not-exist
                                        external-format)
  "Write the `STATE-SPACE` to file designated by `PATHNAME`. The
output format should not be considered stable."
  (declare (ignore if-exists if-does-not-exist external-format))
  (apply #'uiop:call-with-output-file pathname
         #'(lambda (stream)
             (odo:write-state-space-to-stream state-space stream)
             (terpri stream))
         keys))

(defgeneric odo:externalize-state-space (state-space external-format
                                         &key
                                           assigned-p
                                         &allow-other-keys)
  (:documentation "Convert `STATE-SPACE` into the specified `EXTERNAL-FORMAT`.

The returned objects must not share any structure with `STATE-SPACE` aside from
the name and value objects.

This function may return multiple VALUES depending on the `EXTERNAL-FORMAT`.

All `EXTERNAL-FORMAT`s that are keywords are reserved by the odo package.

The variables that are output in the externalization can be controlled with the
following keyword arguments:

+ `:ASSIGNED-P` if T, only externalizes variables that have been assigned."))

(defmethod odo:externalize-state-space (state-space (external-format (eql :alist))
                                        &key assigned-p)
  "Return an alist externalization. The keys are the variable names and the
values are lists of the values remaining in the variable's domain. If a variable
is assigned, then its domain is a list consisting of a single element."
  (flet ((filter (var)
           (or (not assigned-p)
               (odo:assigned-p var)))
         (get-value-list (var)
           (ecase (odo:variable-activation-status var)
             (:active
              (if (odo:assigned-p var)
                  (list (odo:assigned-value var))
                  (odo:value-list var)))
             (:inactive
              (list odo:+variable-inactive-value+))
             (:unknown
              (list* odo:+variable-inactive-value+
                     (odo:value-list var))))))
    (mapcar (lambda (var)
              (cons (odo:name var)
                    ;; Copy the list to break any structure sharing.
                    (copy-list (get-value-list var))))
            (remove-if-not #'filter (odo:state-space-list state-space)))))

(defmethod odo:equal-p-by-type (left right
                                (left-type odo:state-space)
                                (right-type odo:state-space))
  "Returns T iff `LEFT` and `RIGHT` are equal.

+ All variables are `VARIABLE-EQUAL`,
+ All domains are `DOMAIN-EQUAL`"
  (and
   (odo:state-space-p left)
   (odo:state-space-p right)
   (or
    (eql left right)
    (and
     (alex:set-equal (odo:state-space-list left)
                     (odo:state-space-list right)
                     :test #'odo:equal-p)
     (utils:plist-equal (odo:annotation-plist left)
                        (odo:annotation-plist right))))))
