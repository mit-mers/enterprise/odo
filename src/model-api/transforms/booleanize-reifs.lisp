(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/booleanize-reifs (:title "Booleanize Reif Predicates")
  (odo:booleanize-reifs class))

(defclass odo:booleanize-reifs (odo:transform-recurses-on-nested-expressions odo:csp-transform)
  ()
  (:documentation "This transform ensures that one side of every reif is
a (potentially negated) boolean."))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:booleanize-reifs)
                                                                 (operator (eql 'odo:->))
                                                                 (pred t)
                                                                 &key &allow-other-keys)
  ;; Recurse into both sides, transforming as necessary.
  (multiple-value-bind (temp-pred supporting-preds new-vars)
      (call-next-method)
    (let ((temp-left (odo:argument-value temp-pred 'odo:left))
          (temp-right (odo:argument-value temp-pred 'odo:right)))
      (if (or (odo:variable-p temp-left)
              (odo:variable-p temp-right))
          ;; At least one of the sides is a boolean, so we don't need to do any
          ;; further work, yay!
          (values temp-pred supporting-preds new-vars)
          ;; Grumble. Neither side is a boolean. So we need to introduce a new
          ;; var and pred.
          (let* ((new-var (odo:make-variable 'boolean :annotations (list :odo-transform/introduced/reif t))))
            ;; TODO Annotate this with :odo-transform/introduced/reif t
            (values (odo:-> new-var temp-right)
                    (list*
                     ;; TODO Annotate this with :odo-transform/introduced/reif t
                     (odo:<-> temp-left new-var)
                     supporting-preds)
                    (list* new-var new-vars)))))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:booleanize-reifs)
                                                                 (operator (eql 'odo:<->))
                                                                 (pred t)
                                                                 &key &allow-other-keys)
  ;; Recurse into both sides, transforming as necessary.
  (multiple-value-bind (temp-pred supporting-preds new-vars)
      (call-next-method)
    (let ((temp-left (odo:argument-value temp-pred 'odo:left))
          (temp-right (odo:argument-value temp-pred 'odo:right)))
      (if (or (odo:variable-p temp-left)
              (odo:variable-p temp-right))
          ;; At least one of the sides is a boolean, so we don't need to do any
          ;; further work, yay!
          (values temp-pred supporting-preds new-vars)
          ;; Grumble. Neither side is a boolean. So we need to introduce a new
          ;; var and pred.
          (let* ((new-var (odo:make-variable 'boolean :annotations (list :odo-transform/introduced/reif t))))
            ;; TODO Annotate this with :odo-transform/introduced/reif t
            (values (odo:<-> new-var temp-right)
                    (list*
                     ;; TODO Annotate this with :odo-transform/introduced/reif t
                     (odo:<-> temp-left new-var)
                     supporting-preds)
                    (list* new-var new-vars)))))))
