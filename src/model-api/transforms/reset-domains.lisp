(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/reset-domains (:title "Reset domains transform")
  (odo:reset-domains class)
  (odo:reset-domains/variables (reader odo:reset-domains))
  (odo:reset-domains/source-csp (reader odo:reset-domains))

  (odo:transform-csp/variable (method () (odo:reset-domains t))))

(defclass odo:reset-domains (odo:csp-variable-transform odo:csp-transform)
  ((variables
    :initarg :variables
    :reader odo:reset-domains/variables
    :documentation "A list of variable names to reset.")
   (source-csp
    :initarg :source-csp
    :reader odo:reset-domains/source-csp
    :documentation "The CSP that provides the original values of the variables
    to reset."))
  (:documentation "This transform takes a SOURCE-CSP and a list of VARIABLES or
variable names. The returned CSP is a copy of the input CSP except that the
domains of the specified VARIABLES are taken from SOURCE-CSP. VARIABLES can be
either a list of variable names or a list of variable objects from the input
CSP."))

(defmethod initialize-instance :after ((it odo:reset-domains) &key variables &allow-other-keys)
  "Make sure the variables list is a list of variable names."
  (flet ((ensure-name (x)
           (if (or (odo:variable-p x)
                   (odo:conditional-variable-p x))
               (odo:name x)
               x)))
    (setf (slot-value it 'variables) (mapcar #'ensure-name variables))))

(defun reset-variable (var orig-var)
  "Copy VAR, but take the domain from ORIG-VAR."
  (destructuring-bind (domain-type &rest args)
      (domain-spec orig-var)
    (if (odo:conditional-variable-p orig-var)
        (apply #'odo:make-conditional-variable domain-type
               :name (odo:name var)
               :annotations (copy-list (odo:annotation-plist var))
               :activation-status (odo:variable-activation-status orig-var)
               args)
        (apply #'odo:make-variable domain-type
               :name (odo:name var)
               :annotations (copy-list (odo:annotation-plist var))
               args))))

(defmethod odo:transform-csp/variable ((transform odo:reset-domains)
                                       var &key &allow-other-keys)
  "If VAR's name is in `RESET-DOMAINS/VARIABLES`, copy it, but take the domain
from the variable's counterpart in `RESET-DOMAINS/SOURCE-CSP`."
  (if (member (odo:name var) (odo:reset-domains/variables transform) :test #'equal)
      (reset-variable var (odo:state-space-get (odo:reset-domains/source-csp transform) (odo:name var)))
      (call-next-method)))
