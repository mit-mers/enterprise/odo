(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/temporal-to-linear (:title "Temporal constraints to linear constraints")
  (odo:temporal-to-linear class))

;; TODO: mgl-pax can't handle EQL specializers....
;;
;; (odo:transform-csp/application-expression-by-operator (method () (odo:temporal-to-linear (eql 'simple-temporal) t)))

(defclass odo:temporal-to-linear (odo:transform-recurses-on-nested-expressions
                                  odo:csp-transform)
  ()
  (:documentation "This transform turns temporal constraints into their
corresponding linear inequalities. This may introduce new conjunction
predicates, for instance, if both the lower and upper bounds are specified on a
simple temporal predicate."))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:temporal-to-linear)
                                                                 (operator (eql 'odo:simple-temporal))
                                                                 pred
                                                                 &key destination-csp
                                                                 &allow-other-keys)
  "Convert a simple temporal predicate that states [lb <= e2 - e1 <= ub] into a
conjunction that states [(e2 - e1 >= lb) ^ (e2 - e1 <= ub)]. If either the lower
bound or upper bound is not specified then only a single linear inequality is
returned instead of a conjunction."
  (flet ((map-fun (old-var)
           (if (odo:variable-p old-var)
               (odo:state-space-get destination-csp (odo:name old-var))
               old-var)))
    (let ((to-event (map-fun (odo:argument-value pred 'odo:to)))
          (from-event (map-fun (odo:argument-value pred 'odo:from)))
          (lower-bound (odo:argument-value pred 'odo:lower-bound))
          (upper-bound (odo:argument-value pred 'odo:upper-bound)))
      (cond
        ((and (not (eql lower-bound odo:+-infinity+))
              (not (eql upper-bound odo:+infinity+)))
         (odo:and (odo:linear->= (list 1 -1) (list to-event from-event) lower-bound)
                  (odo:linear-<= (list 1 -1) (list to-event from-event) upper-bound)))
        ((not (eql lower-bound odo:+-infinity+))
         (odo:linear->= (list 1 -1) (list to-event from-event) lower-bound))
        ((not (eql upper-bound odo:+infinity+))
         (odo:linear-<= (list 1 -1) (list to-event from-event) upper-bound))))))
