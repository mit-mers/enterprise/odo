(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/enums-to-ints (:title "Enum to integer transform")
  (odo:enums-to-ints class)

  (odo:enum-map-integer-value function)
  (odo:enum-map-orig-value function)

  (odo:transform-csp/annotation (method () (odo:enums-to-ints t t t)))

  (odo:transform-csp/variables (method () (odo:enums-to-ints t)))

  (odo:transform-csp/variable (method () (odo:enums-to-ints t)))

  ;; TODO: mgl-pax can't handle EQL specializers....
  ;;
  ;; (odo:transform-csp/application-expression-by-operator (method (:before) (odo:enums-to-ints t t)))
  ;; (odo:transform-csp/application-expression-by-operator (method () (odo:enums-to-ints (eql 'cl-equalp) t)))
  ;; (odo:transform-csp/application-expression-by-operator (method () (odo:enums-to-ints (eql 'all-diff-except) t)))
  ;; (odo:transform-csp/application-expression-by-operator (method () (odo:enums-to-ints (eql 'circuit) t)))
  ;; (odo:transform-csp/application-expression-by-operator (method () (odo:enums-to-ints (eql 'subcircuit) t)))
  ;; (odo:transform-csp/application-expression-by-operator (method () (odo:enums-to-ints (eql 'element) t)))

  (odo:transform-csp/objective (method () (odo:enums-to-ints t))))

(defclass odo:enums-to-ints (odo:transform-recurses-on-nested-expressions
                             odo:csp-variable-transform
                             odo:csp-transform)
  ()
  (:documentation "This transform converts every enum variable into an integer
variable. Additionally, it transforms predicates that compare an enum variable
to a value such that the value is an appropriate integer.

If applied to a CSP, the `:odo-transform/enum-map` annotation on the resulting
CSP contains an object that can be used to translate between original domain
values and integer values (see `ENUM-MAP-INTEGER-VALUE` and
`ENUM-MAP-ORIG-VALUE`).

Additionally, every transformed enum variable is annotated with the enum map
under `:ODO-TRANSFORM/ENUM-MAP` and the original domain test under
`:ODO-TRANSFORM/ORIGINAL-TEST`.

No attempt is made to compact the domains of the new integer variables, although
that may be done in the future.

If the objective function is a raw function, no attempt is made to wrap the
function in any translation layer, though this may be added in the future."))


;;; Enum maps

(defclass enum-map ()
  ((test-map
    :reader enum-map/test-map
    :initform (make-hash-table)
    :initarg :test-map
    :documentation "A hash map that has domain test functions as values and
vectors as keys. The vectors represent the integer that each value is mapped
to.")))

(defun copy-enum-map (enum-map)
  "Copy an ENUM-MAP."
  (let* ((old-ht (enum-map/test-map enum-map))
         (new-ht (make-hash-table :size (hash-table-size old-ht)
                                  :rehash-size (hash-table-rehash-size old-ht)
                                  :rehash-threshold (hash-table-rehash-threshold old-ht))))
    (maphash (lambda (k v)
               (setf (gethash k new-ht) (alex:copy-array v)))
             old-ht)
    (make-instance 'enum-map :test-map new-ht)))

(defun odo:enum-map-integer-value (enum-map domain-test orig-value
                                   &optional (error-on-missing-p t)
                                     missing-value)
  "Return the integer that corresponds to ORIG-VALUE in ENUM-MAP. Raises an
error if it is missing unless ERROR-ON-MISSING-P is NIL in which case
MISSING-VALUE is returned."
  (let* ((test (alex:ensure-function domain-test))
         (values-vector (gethash test (enum-map/test-map enum-map)))
         (idx (position orig-value values-vector :test test)))
    (cond
      (idx
       idx)
      (error-on-missing-p
       (error "Unable to find the index for value ~S" orig-value))
      (t
       missing-value))))

(defun odo:enum-map-orig-value (enum-map domain-test int-value
                                &optional (error-on-missing-p t)
                                  missing-value)
  "Return the original value that corresponds to an in integer in
ENUM-MAP. Raises an error if it is missing unless ERROR-ON-MISSING-P is NIL in
which case MISSING-VALUE is returned."
  (let* ((test (alex:ensure-function domain-test))
         (values-vector (gethash test (enum-map/test-map enum-map))))
    (cond
      ((< int-value (length values-vector))
       (aref values-vector int-value))
      (error-on-missing-p
       (error "No original value exists for integer value ~S" int-value))
      (t
       missing-value))))

(defun enum-map-ensure-test-vector! (enum-map domain-test)
  "Given an ENUM-MAP, ensure that a mapping vector exists for the given DOMAIN-TEST."
  (alex:ensure-gethash (alex:ensure-function domain-test)
                       (enum-map/test-map enum-map)
                       (make-array 16 :initial-element nil :adjustable t :fill-pointer 1)))

(defun enum-map-ensure-value! (enum-map domain-test orig-value)
  "Given an ENUM-MAP, ensure that a mapping for ORIG-VALUE exists and return it."
  (let ((underlying-vector (enum-map-ensure-test-vector! enum-map domain-test))
        (int-value (odo:enum-map-integer-value enum-map domain-test orig-value nil nil)))
    (unless int-value
      (setf int-value (length underlying-vector))
      (vector-push-extend orig-value underlying-vector (floor (* 1.5 int-value))))
    int-value))


;;; Transforming annotations

(defmethod odo:transform-csp/annotation ((transform odo:enums-to-ints)
                                         (csp t)
                                         key
                                         value
                                         &key &allow-other-keys)
  "If the key is `:ODO-TRANSFORM/ENUM-MAP`, copy the enum-map value."
  (if (eql :odo-transform/enum-map key)
      (list :odo-transform/enum-map (copy-enum-map value))
      (call-next-method)))


;;; Transforming variables

(defun member-domain-p (var)
  "Returns T iff VAR is an enumerated (member) domain."
  (and (odo:discrete-domain-p var t)
       (not (odo:integer-domain-p var t))
       (not (odo:boolean-domain-p var t))))

(defun copy-enum-variable-using-map (var enum-map)
  "Make an integer variable that represents the same domain as VAR. Value to
integer mappings are stored in ENUM-MAP."
  (let* ((variable-name (odo:name var))
         (domain-test (odo:domain-test var))
         (values (mapcar (lambda (val)
                           (enum-map-ensure-value! enum-map domain-test val))
                         (odo:value-list var))))
    (if (odo:conditional-variable-p var)
        (odo:make-conditional-variable 'integer
                                       :name variable-name
                                       :values values
                                       :activation-status (odo:variable-activation-status var)
                                       :annotations (list* :odo-transform/original-test domain-test
                                                           :odo-transform/enum-map enum-map
                                                           (copy-list (odo:annotation-plist var))))
        (odo:make-variable 'integer
                           :name variable-name
                           :values values
                           :annotations (list* :odo-transform/original-test domain-test
                                               :odo-transform/enum-map enum-map
                                               (copy-list (odo:annotation-plist var)))))))

(defmethod odo:transform-csp/variables ((transform odo:enums-to-ints) csp
                                        &rest args
                                        &key enum-map destination-csp
                                        &allow-other-keys)
  "Transforms all enumerated variables to integer variables. An enum-map can be
provided through the enum-map keyword or using the `:ODO-TRANSFORM/ENUM-MAP`
annotation on the destination-csp. In either case, the enum map is modified in
place. If neither approach is used, a new enum map is created.

Transformed variables are annotated with the enum-map under
`:ODO-TRANSFORM/ENUM-MAP` and the original domain test under
`:ODO-TRANSFORM/ORIGINAL-TEST`

The second value requests that the used enum map be added to the destination-csp
with the `:ODO-TRANSFORM/ENUM-MAP` annotation."
  (let ((enum-map (or enum-map
                      (odo:annotation destination-csp :odo-transform/enum-map)
                      (make-instance 'enum-map))))
    (multiple-value-bind (result new-annotations)
        (apply #'call-next-method transform csp :enum-map enum-map args)
      (values
       result
       (list* :odo-transform/enum-map enum-map
              new-annotations)))))

(defmethod odo:transform-csp/variable ((transform odo:enums-to-ints)
                                       variable
                                       &key enum-map &allow-other-keys)
  "If VARIABLE is an enumerated variable, transform it using the ENUM-MAP
keyword (modified in place). Otherwise, fall through to the next method. The
variable is annotated with the enum-map under `:ODO-TRANSFORM/ENUM-MAP` and the
original domain test under `:ODO-TRANSFORM/ORIGINAL-TEST`.

ENUM-MAP *must* be provided."
  (assert enum-map)
  (if (member-domain-p variable)
      (copy-enum-variable-using-map variable enum-map)
      (call-next-method)))


;;; Transforming predicates

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:enums-to-ints)
                                                                 (op (eql 'odo:equal))
                                                                 pred
                                                                 &key destination-csp &allow-other-keys)
  "If one side of the equality is an enumerated variable and the other side is a
value, convert the value to an appropriate integer."
  (let ((left (odo:argument-value pred 'odo:left))
        (right (odo:argument-value pred 'odo:right))
        (var-map-fun (lambda (old-var)
                       (odo:state-space-get destination-csp (odo:name old-var)))))
    (if (alex:xor (member-domain-p left)
                  (member-domain-p right))
        (let* ((orig-var (if (member-domain-p left)
                             left right))
               (orig-value (if (member-domain-p left)
                               right left))
               (new-var (funcall var-map-fun orig-var))
               (enum-map (odo:annotation new-var :odo-transform/enum-map))
               (test-fun (odo:annotation new-var :odo-transform/original-test))
               (new-value (odo:enum-map-integer-value enum-map test-fun orig-value)))
          (odo:equal new-var new-value))
        (call-next-method))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:enums-to-ints)
                                                                 (op (eql 'odo:all-diff-except))
                                                                 pred
                                                                 &key destination-csp &allow-other-keys)
  "If any of the variables in the predicate are enumerated variables, transform
the except slot to be the appropriate integer (and any constants in values)."
  (let* ((values (odo:argument-value pred 'odo:values))
         (except (odo:argument-value pred 'odo:except))
         (var-0 (find-if #'member-domain-p values))
         (var-map-fun (lambda (old-var)
                        (odo:state-space-get destination-csp (odo:name old-var)))))
    (if var-0
        (let* ((canonical-var (funcall var-map-fun var-0))
               (enum-map (odo:annotation canonical-var :odo-transform/enum-map))
               (test-fun (odo:annotation canonical-var :odo-transform/original-test))
               (new-except (odo:enum-map-integer-value enum-map test-fun except)))
          (odo:all-diff-except (map 'list (lambda (x)
                                            (if (odo:variable-p x)
                                                (funcall var-map-fun x)
                                                (odo:enum-map-integer-value enum-map test-fun x)))
                                values)
                           new-except))
        (call-next-method))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:enums-to-ints)
                                                                 (op (eql 'odo:circuit))
                                                                 pred
                                                                 &key destination-csp &allow-other-keys)
  "If the variables in the predicate are enumerated variables, reorder them and
provide a value for the sparse-vector-indices slot as necessary."
  (let ((old-values (odo:argument-value pred 'odo:values))
        (var-map-fun (lambda (old-var)
                       (odo:state-space-get destination-csp (odo:name old-var)))))
    (if (some #'member-domain-p old-values)
        (let* ((canonical-var (funcall var-map-fun (find-if #'member-domain-p old-values)))
               (enum-map (odo:annotation canonical-var :odo-transform/enum-map))
               (test-fun (odo:annotation canonical-var :odo-transform/original-test))
               (pairs nil)
               (old-indices (odo:argument-value pred 'odo:sparse-vector-indices)))
          (loop
            :for i :upfrom 0
            :for old-val :in (coerce old-values 'list)
            :for new-value := (if (odo:variable-p old-val)
                                  (funcall var-map-fun old-val)
                                  (odo:enum-map-integer-value enum-map test-fun old-val))
            :do (push (cons new-value (odo:enum-map-integer-value enum-map test-fun (elt old-indices i)))
                      pairs))
          (setf pairs (sort pairs #'< :key #'cdr))
          (odo:circuit (map 'list #'car pairs) :sparse-vector-indices (map 'vector #'cdr pairs)))
        (call-next-method))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:enums-to-ints)
                                                                 (op (eql 'odo:subcircuit))
                                                                 pred
                                                                 &key destination-csp &allow-other-keys)
  "If the variables in the predicate are enumerated variables, reorder them and
provide a value for the sparse-vector-indices slot as necessary."
  (let ((old-values (odo:argument-value pred 'odo:values))
        (var-map-fun (lambda (old-var)
                       (odo:state-space-get destination-csp (odo:name old-var)))))
    (if (some #'member-domain-p old-values)
        (let* ((canonical-var (funcall var-map-fun (find-if #'member-domain-p old-values)))
               (enum-map (odo:annotation canonical-var :odo-transform/enum-map))
               (test-fun (odo:annotation canonical-var :odo-transform/original-test))
               (pairs nil)
               (old-indices (odo:argument-value pred 'odo:sparse-vector-indices)))
          (loop
            :for i :upfrom 0
            :for old-val :in (coerce old-values 'list)
            :for new-value := (if (odo:variable-p old-val)
                                  (funcall var-map-fun old-val)
                                  (odo:enum-map-integer-value enum-map test-fun old-val))
            :do (push (cons new-value (odo:enum-map-integer-value enum-map test-fun (elt old-indices i)))
                      pairs))
          (setf pairs (sort pairs #'< :key #'cdr))
          (odo:subcircuit (map 'list #'car pairs) :sparse-vector-indices (map 'vector #'cdr pairs)))
        (call-next-method))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:enums-to-ints)
                                                                 (op (eql 'odo:element))
                                                                 pred
                                                                 &key destination-csp &allow-other-keys)
  "If the index value in the predicate is an enumerated variable, reorder the
elements and provide a value for the sparse-vector-indices slot as necessary."
  (let ((old-elements (odo:argument-value pred 'odo:elements))
        (old-index (odo:argument-value pred 'odo:index))
        (old-value (odo:argument-value pred 'odo:value))
        (var-map-fun (lambda (old-var)
                       (odo:state-space-get destination-csp (odo:name old-var)))))
    (if (member-domain-p old-index)
        (let* ((new-index (funcall var-map-fun old-index))
               (enum-map (odo:annotation new-index :odo-transform/enum-map))
               (test-fun (odo:annotation new-index :odo-transform/original-test))
               (old-indices (odo:argument-value pred 'odo:sparse-vector-indices))
               (pairs (mapcar #'cons (coerce old-elements 'list)
                              (mapcar (lambda (x)
                                        (odo:enum-map-integer-value enum-map test-fun x nil))
                                      (coerce old-indices 'list)))))
          (unless (and (every #'odo:variable-p old-elements))
            (error "This does not work if elements are not variables..."))
          (setf pairs (remove nil pairs :key #'cdr))
          (setf pairs (sort pairs #'< :key #'cdr))
          (let* ((new-elements (mapcar var-map-fun (mapcar #'car pairs)))
                 (enum-map-from-elements (odo:annotation (first new-elements)
                                                         :odo-transform/enum-map))
                 (test-fun-from-elements (odo:annotation (first new-elements)
                                                         :odo-transform/original-test))
                 (new-value (if (odo:variable-p old-value)
                               (funcall var-map-fun old-value)
                               (odo:enum-map-integer-value enum-map-from-elements test-fun-from-elements
                                                           old-value))))
            (odo:element new-elements
                         new-index
                         new-value
                         :sparse-vector-indices (mapcar #'cdr pairs))))
        (call-next-method))))

(defmethod odo:transform-csp/application-expression-by-operator :before ((transform odo:enums-to-ints)
                                                                         (type t) pred
                                                                         &key &allow-other-keys)
  "Currently, we can only handle predicates where every enumerated variable in
its scope as the same domain test. This raises an error if this condition is not
met."
  ;; First, take the easy way out of dealing with enumerated variables by
  ;; requiring that all enumerated variables in the scope of PRED have the same
  ;; test function.
  (let ((variables (odo:scope pred)))
    (loop
      :with test-fun := nil
      :for var :in variables
      :when (member-domain-p var)
        :do
           (unless test-fun
             (setf test-fun (odo:domain-test var)))
           (unless (eql test-fun (odo:domain-test var))
             (error "Can't handle predicates with enumerated variables of different test functions.")))))


;;; Objective function transforms

(defmethod odo:transform-csp/objective ((transform odo:enums-to-ints) csp
                                        &key destination-csp &allow-other-keys)
  "If the objective is an attribute objective function, replace all references
to enum values with their integers."
  (let ((orig-goal (odo:objective-goal csp))
        (orig-function (odo:objective-function csp)))
    (values
     orig-goal
     (ecase orig-goal
       ((:maximize :minimize)
        (cond
          ((odo:variable-p orig-function)
           (odo:state-space-get destination-csp (odo:name orig-function)))
          ((odo:attribute-objective-function-p orig-function)
           (odo:make-attribute-objective-function
            :attribute-values-alist
            (mapcar (lambda (x)
                      (let* ((old-var (car x))
                             (var-name (odo:name old-var))
                             (new-var (odo:state-space-get destination-csp var-name))
                             (enum-map (odo:annotation new-var :odo-transform/enum-map))
                             (test-fun (odo:annotation new-var :odo-transform/original-test)))
                        (if enum-map
                            (cons new-var
                                  (mapcar (lambda (pair)
                                            (cons (odo:enum-map-integer-value enum-map test-fun (car pair))
                                                  (cdr pair)))
                                          (cdr x)))
                            (cons new-var (cdr x)))))
                    (odo:attribute-values-alist orig-function))))
          (t
           orig-function)))
       (:satisfy)))))
