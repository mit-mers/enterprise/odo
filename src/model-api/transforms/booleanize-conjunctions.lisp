(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/booleanize-conjunctions (:title "Booleanize Conjunction Predicates")
  (odo:booleanize-conjunctions class))

(defclass odo:booleanize-conjunctions (odo:transform-recurses-on-nested-expressions odo:csp-transform)
  ()
  (:documentation "This transform converts every odo-and predicate into a
odo-and predicate with only boolean variables and negated boolean variables as
arguments. This may necessitate the introduction of new boolean variables and
predicates that reify the original predicates in the conjunction with the new
boolean variables."))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:booleanize-conjunctions)
                                                                 (operator (eql 'odo:and))
                                                                 pred
                                                                 &rest args)
  (let ((new-args nil)
        (new-supporting-preds nil)
        (new-vars nil))
    (dolist (arg (coerce (odo:arguments pred) 'list))
      (multiple-value-bind (this-new-arg this-new-supporting-preds this-new-vars)
          (apply #'odo:transform-csp/expression transform arg args)
        (setf new-supporting-preds (append this-new-supporting-preds new-supporting-preds))
        (setf new-vars (append this-new-vars new-vars))
        (if (or (odo:variable-p this-new-arg)
                (and (odo:expression-p this-new-arg)
                     (typep (odo:type this-new-arg) 'odo:application-expression)
                     (eql 'odo-not (odo:operator this-new-arg))
                     (odo:variable-p (odo:argument-value this-new-arg 'odo:x))))
            ;; We can just put this expression directly into the output.
            (push this-new-arg new-args)
            ;; We need to add a new variable and equivalence
            (let ((new-var (odo:make-variable 'boolean
                                              :annotations (list :odo-transform/introduced t))))
              ;; If the new argument is negated, try to keep the negation with
              ;; the conjunction.
              (if (and (odo:expression-p this-new-arg)
                       (typep (odo:type this-new-arg) 'odo:application-expression)
                       (eql 'odo:not (odo:operator this-new-arg)))
                  (progn
                    (push (odo:not new-var) new-args)
                    ;; TODO: Annotate this with :odo-transform/introduced/reif t
                    (push (odo:<-> (odo:argument-value this-new-arg 'odo:x)
                                   new-var)
                          new-supporting-preds))
                  (progn
                    (push new-var new-args)
                    (push (odo:<-> this-new-arg new-var) new-supporting-preds)))))))
    (values (apply #'odo:and (nreverse new-args))
            new-supporting-preds
            new-vars)))
