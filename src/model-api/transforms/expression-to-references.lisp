(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/expression-to-references
    (:title "Transform expressions to contain only references")
  (odo:expression-to-references class)

  (odo:transform-csp/objective (method () (odo:expression-to-references t))))

(defclass odo:expression-to-references (odo:csp-transform)
  ()
  (:documentation "This transform converts every variable in expressions to
reference the variable by name."))


;;; Objective function transforms

(defgeneric expression-convert-to-references (expression)
  (:method (self)
    (expression-convert-to-references-by-type self (odo:type self))))

(defgeneric expression-convert-to-references-by-type (expression type)
  (:method (self (type (eql nil)))
    self))

(defmethod expression-convert-to-references-by-type (self (type odo:variable))
  (odo:lookup (odo:name self)))

(defmethod expression-convert-to-references-by-type (self (type odo:application-expression))
  (odo:make-instance 'odo:application-expression
                     :operator (expression-convert-to-references (odo:operator self))
                     :arguments (mapcar #'expression-convert-to-references
                                        (odo:arguments self))))

(defmethod expression-convert-to-references-by-type (self (type odo:lookup-expression))
  (odo:make-instance 'odo:lookup-expression
                     :operator (expression-convert-to-references (odo:operator self))
                     :arguments (mapcar #'expression-convert-to-references
                                        (odo:arguments self))))

(defmethod odo:transform-csp/objective ((transform odo:expression-to-references) csp
                                        &key &allow-other-keys)
  "If the objective is an expression, copy it and replace all instances of
variables with variable references."
  (let ((orig-goal (odo:objective-goal csp))
        (orig-function (odo:objective-function csp)))
    (if (odo:expression-p orig-function)
        (values
         orig-goal
         (expression-convert-to-references orig-function))
        (call-next-method))))
