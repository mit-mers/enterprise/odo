(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/identity (:title "Identity transform")
  (odo:identity-transform class)

  (odo:transform-csp (method () (odo:identity-transform t))))

(defclass odo:identity-transform (odo:csp-transform)
  ()
  (:documentation "The identity transform returns a copy of the input CSP."))

(defmethod odo:transform-csp ((transform odo:identity-transform) csp &key &allow-other-keys)
  "Short circuit all the normal transform logic and call `ODO-COPY` directly."
  (odo:copy csp))
