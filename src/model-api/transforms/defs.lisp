(in-package #:odo/model-api)

(defsection @odo/model-api/transforms/defs (:title "Base transform definitions")
  "This section defines the base classes and generic functions that Odo
transforms are built upon. Additionally, some simple methods are defined for
the generic functions.

**WARNING:** Everything in this section is in beta and backwards incompatible
changes may be made. Be sure to pay attention to Odo issues if you rely on this
functionality."

  (odo:transform class)
  (odo:odo-transform class)
  (odo:csp-transform class)
  (odo:csp-variable-transform class)
  (odo:csp-expression-transform class)
  (odo:transform-recurses-on-nested-expressions class)

  (odo:transform-csp generic-function)
  (odo:transform-csp/annotation generic-function)
  (odo:transform-csp/annotations generic-function)
  (odo:transform-csp/constraint generic-function)
  (odo:transform-csp/constraints generic-function)
  (odo:transform-csp/expression generic-function)
  (odo:transform-csp/expression-by-type generic-function)
  (odo:transform-csp/application-expression-by-operator generic-function)
  (odo:transform-csp/variable generic-function)
  (odo:transform-csp/variables generic-function)

  (odo:transform-csp (method () (symbol t)))
  (odo:transform-csp (method () (list t)))
  (odo:transform-csp (method () (odo:csp-transform t)))

  (odo:transform-csp/annotations (method () (odo:csp-transform t t)))

  (odo:transform-csp/annotation (method () (odo:csp-transform t t t)))

  (odo:transform-csp/variables (method () (odo:csp-transform t)))

  (odo:transform-csp/variable (method () (odo:csp-transform t)))

  (odo:transform-csp/constraints (method () (odo:csp-transform t)))

  (odo:transform-csp/constraint (method () (odo:csp-transform t)))

  (odo:transform-csp/expression (method () (odo:csp-transform t)))

  (odo:transform-csp/expression-by-type (method () (odo:csp-transform t t)))
  (odo:transform-csp/expression-by-type (method () (odo:csp-transform odo:application-expression t)))
  (odo:transform-csp/application-expression-by-operator (method () (odo:csp-transform t t)))
  (odo:transform-csp/application-expression-by-operator (method () (odo:transform-recurses-on-nested-expressions t t)))

  (odo:transform-csp/objective (method () (odo:csp-transform t))))


;;; Base transform class hierarchy

(defclass odo:transform ()
  ()
  (:documentation "Base class for all Odo transforms."))

(utils:define-renamed-class odo:odo-transform odo:transform)

(defclass odo:csp-transform (odo:transform)
  ()
  (:documentation "Base class for all transformations on CSPs."))

(defclass odo:csp-variable-transform (odo:csp-transform)
  ()
  (:documentation "Base class for all CSP transformations that transform
individual variables."))

(defclass odo:csp-expression-transform (odo:csp-transform)
  ()
  (:documentation "Base class for all CSP transformations that transform
indvidual expressions."))

(defclass odo:transform-recurses-on-nested-expressions ()
  ()
  (:documentation "A mixin that indicates that transforming on nested
expressions."))


;;; Generic function definitions

(defgeneric odo:transform-csp (transform csp &rest args &key &allow-other-keys)
  (:documentation "Returns a new CSP representing a transformation of the input CSP.

The TRANSFORM can be one of:

+ An instance of `CSP-TRANSFORM`.

+ A symbol naming an Odo transform class. In this case, the transform will be
instantiated with no initargs.

+ A list of Odo transform objects or symbols.

The transforms generally occur as follows:

1. The CSP's annotations are transformed.
2. A new CSP is instantiated.
3. All variables are transformed using `TRANSFORM-CSP/VARIABLES`.
4. All constraints are transformed using `TRANSFORM-CSP/CONSTRAINTS`.
5. The objective function is transformed using `TRANSFORM-CSP/OBJECTIVE`."))

(defgeneric odo:transform-csp/annotations (transform csp annotation-plist &key &allow-other-keys)
  (:documentation "given a transform, csp, and plist of annotations from that
csp, return a plist of annotations that should be used applied to the output
CSP."))

(defgeneric odo:transform-csp/annotation (transform csp key value &key &allow-other-keys)
  (:documentation "Given a transform, csp, and annotation pair, return a list of
annotations that should be applied to the output CSP."))

(defgeneric odo:transform-csp/variables (transform csp &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and csp, return two values: a list of
transformed variables and a plist of annotations that should be applied to the
destination csp."))

(defgeneric odo:transform-csp/variable (transform variable &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and a variable, return the transformed
variable."))

(defgeneric odo:transform-csp/constraints (transform csp &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and csp, return three values: a list of
transformed constraints, a list of added variables, and a plist of annotations
that should be applied to the destination csp."))

(defgeneric odo:transform-csp/constraint (transform constraint &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and constraint, return three values: the
transformed constraint, a list of supporting constraints, and a list of new
variables."))

(defgeneric odo:transform-csp/expression (transform expression &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and expression, return three values: the
transformed expression, a list of supporting expressions that should be added as
top-level constraints, and a list of new variables."))

(defgeneric odo:transform-csp/expression-by-type (transform expression-type expression
                                                  &key destination-csp &allow-other-keys)
  (:documentation "Given a transform, expression type, and expression, return
three values: an expression that is equivalent to EXPRESSION, a list of
supporting expressions that should be added as top-level constraints, and a list
of new variables."))

(defgeneric odo:transform-csp/application-expression-by-operator (transform operator expression
                                                                  &key destination-csp &allow-other-keys)
  (:documentation "Given a transform, operator name, and an application
expression, return three values: an expression that is equivalent to EXPRESSION,
a list of supporting expressions that should be added as top-level constraints,
and a list of new variables."))

(defgeneric odo:transform-csp/objective (transform csp &key destination-csp &allow-other-keys)
  (:documentation "Given a transform and a csp, return three values: the new
`OBJECTIVE-GOAL`, the transformed `OBJECTIVE-FUNCTION`, and a plist of
annotations that should be applied to the destination csp."))


;;; Basic ODO:TRANSFORM-CSP implementations

(defmethod odo:transform-csp ((transform symbol) csp &rest args &key &allow-other-keys)
  "If the transform is just a symbol, instantiate the class it names and recall
the function."
  (apply #'odo:transform-csp (make-instance transform) csp args))

(defmethod odo:transform-csp ((transforms list) csp &rest args &key &allow-other-keys)
  "If the transform is a list, recall transform-csp on every element of the list
in order and return the resulting CSP."
  (let ((csp csp))
    (dolist (transform transforms csp)
      (setf csp (apply #'odo:transform-csp transform csp args)))))

(defmethod odo:transform-csp ((transform odo:csp-transform) csp &rest args &key &allow-other-keys)
  "A basic CSP transformation. First, transform the variables, then the
constraints, then the objective function."
  (let* ((new-annotations (apply #'odo:transform-csp/annotations
                                 transform csp
                                 (copy-list (odo:annotation-plist csp))
                                 args))
         (new-csp (odo:make-csp :annotations (copy-list new-annotations))))
    (flet ((apply-annotations (new-annotations)
             (iter
               (for prop in new-annotations :by #'cddr)
               (for value in (rest new-annotations) :by #'cddr)
               (setf (odo:annotation new-csp prop) value))))
      ;; Transform the variables
      (multiple-value-bind (new-vars new-annotations)
          (apply #'odo:transform-csp/variables transform csp :destination-csp new-csp
                 args)
        (odo:state-space-add! new-csp new-vars)
        (apply-annotations new-annotations))

      ;; Transform the constraints
      (multiple-value-bind (new-constraints new-variables new-annotations)
          (apply #'odo:transform-csp/constraints transform csp
                 :destination-csp new-csp args)
        (odo:state-space-add! new-csp new-variables)
        (odo:add-constraint! new-csp new-constraints)
        (apply-annotations new-annotations))

      ;; Transform the objective
      (multiple-value-bind (objective-goal objective-function new-annotations)
          (apply #'odo:transform-csp/objective transform csp :destination-csp new-csp args)
        (setf (odo:objective-goal new-csp) objective-goal)
        (setf (odo:objective-function new-csp) objective-function)
        (apply-annotations new-annotations)))
    new-csp))


;;; Basic TRANSFORM-CSP/ANNOTATIONS implementation

(defmethod odo:transform-csp/annotations ((transform odo:csp-transform) csp annotation-plist
                                          &rest args
                                          &key &allow-other-keys)
  "Call `TRANSFORM-CSP/ANNOTATION` on every key value pair."
  (loop
    :for key :in annotation-plist :by #'cddr
    :for value :in (rest annotation-plist) :by #'cddr
    :appending (apply #'odo:transform-csp/annotation transform csp key value args)))

(defmethod odo:transform-csp/annotations ((transforms list) csp annotation-plist
                                          &rest args
                                          &key &allow-other-keys)
  (let ((annotations annotation-plist))
    (dolist (transform transforms csp)
      (setf annotations (apply #'odo:transform-csp/annotations transform csp annotations args)))))

(defmethod odo:transform-csp/annotations ((transform symbol) csp annotation-plist
                                          &rest args
                                          &key &allow-other-keys)

  (apply #'odo:transform-csp/annotations (make-instance transform) csp annotation-plist args))


;;; Basic TRANSFORM-CSP/ANNOTATION implementation

(defmethod odo:transform-csp/annotation ((transform odo:csp-transform) (csp t) key value &key &allow-other-keys)
  "Return `(LIST KEY VALUE)`."
  (list key value))


;;; Basic TRANSFORM-CSP/VARIABLES implementation

(defmethod odo:transform-csp/variables ((transform odo:csp-transform) csp &rest args &key &allow-other-keys)
  "Call `TRANSFORM-CSP/VARIABLE` on every variable in the CSP and return the
results as a list."
  (iter
    (with it := (odo:state-space-iterator csp))
    (until (odo:it-done it))
    (for orig-var := (odo:it-next it))
    (for new-var := (apply #'odo:transform-csp/variable transform orig-var args))
    (collect new-var)))


;;; Basic TRANSFORM-CSP/VARIABLE implementations

(defmethod odo:transform-csp/variable ((transform odo:csp-transform) variable &key &allow-other-keys)
  "Just copy the variable."
  (odo:copy variable))


;;; Basic TRANSFORM-CSP/CONSTRAINTS implementation

(defmethod odo:transform-csp/constraints ((transform odo:csp-transform) csp &rest args &key &allow-other-keys)
  "Call `TRANSFORM-CSP/CONSTRAINT` on every variable in the CSP and return the
results as a list."
  (let ((new-constraints-out nil)
        (new-variables-out nil))
    (iter
      (with it := (odo:constraint-iterator csp))
      (until (odo:it-done it))
      (for constraint := (odo:it-next it))
      (for (values new-constraint new-supporting-constraints new-variables) :=
           (apply #'odo:transform-csp/constraint transform constraint args))
      (setf new-constraints-out (append new-supporting-constraints new-constraints-out))
      (push new-constraint new-constraints-out)
      (setf new-variables-out (append new-variables new-variables-out)))
    (values new-constraints-out new-variables-out)))


;;; Basic TRANSFORM-CSP/CONSTRAINT implementation

(defmethod odo:transform-csp/constraint ((transform odo:csp-transform) constraint
                                         &rest args
                                         &key &allow-other-keys)
  "Call `TRASNFORM-CSP/EXPRESSION` to get the new expressions and instantiate
them as constraints."
  (multiple-value-bind (primary-pred supporting-preds new-vars)
      (apply #'odo:transform-csp/expression transform (odo:expression constraint) args)
    (values
     (odo:make-constraint primary-pred
                          :name (odo:name constraint)
                          :contingent-p (typep (odo:type constraint)
                                               'odo:contingent-constraint)
                          :annotations (copy-list (odo:annotation-plist constraint)))
     (list (mapcar #'odo:make-constraint supporting-preds))
     new-vars)))

(defmethod odo:transform-csp/constraint ((transforms list) constraint
                                         &rest args
                                         &key &allow-other-keys)
  (let ((constraint constraint))
    (dolist (transform transforms constraint)
      (setf constraint (apply #'odo:transform-csp/constraint transform constraint args)))))

(defmethod odo:transform-csp/constraint ((transform symbol) constraint
                                         &rest args
                                         &key &allow-other-keys)

  (apply #'odo:transform-csp/annotations (make-instance transform) constraint args))


;;; Basic TRANSFORM-CSP/EXPRESSION implementation

(defmethod odo:transform-csp/expression ((transform odo:csp-transform) expression
                                         &rest args
                                         &key &allow-other-keys)
  "Just call `TRANSFROM-CSP/EXPRESSION-BY-TYPE`."
  (apply #'odo:transform-csp/expression-by-type transform
         (odo:type expression)
         expression args))


;;; Basic TRANSFORM-CSP/EXPRESSION-BY-TYPE implementation

(defmethod odo:transform-csp/expression-by-type ((transform odo:csp-transform) (type t) expression
                                                 &key destination-csp &allow-other-keys)
  "Fallback implementation, just copy the expression."
  (odo:copy expression :context (utils:make-odo-copy-context
                                 :var-map-fun (lambda (old-var)
                                                (odo:state-space-get destination-csp (odo:name old-var))))))

(defmethod odo:transform-csp/expression-by-type ((transform odo:csp-transform) (type odo:application-expression)
                                                 expression
                                                 &rest args)
  "Call `TRANSFORM-CSP/APPLICATION-EXPRESSION-BY-OPERATOR`."
  (apply #'odo:transform-csp/application-expression-by-operator
         transform (odo:operator expression)
         expression
         args))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:csp-transform) (operator t) expression
                                                                 &key destination-csp
                                                                 &allow-other-keys)
  "Fallback implementation, just copy the expression."
  (odo:copy expression :context (utils:make-odo-copy-context
                                 :var-map-fun (lambda (old-var)
                                                (odo:state-space-get destination-csp (odo:name old-var))))))

(defmethod odo:transform-csp/application-expression-by-operator ((transform odo:transform-recurses-on-nested-expressions)
                                                                 operator expression
                                                                 &rest args)
  "When the transform is defined to be applied to expressions, recurses on
nested expressions, and no other method applies, just bubble the transform
down."
  (let (new-args supporting vars)
    (dolist (a (odo:arguments expression))
      (multiple-value-bind (new-arg new-supporting new-vars)
          (apply #'odo:transform-csp/expression transform a args)
        (push new-arg new-args)
        (setf supporting (append new-supporting supporting))
        (setf vars (append new-vars vars))))
    (values (apply operator (nreverse new-args))
            supporting vars)))


;;; Basic TRANSFORM-CSP/OBJECTIVE implementation

(defmethod odo:transform-csp/objective ((transform odo:csp-transform) csp
                                        &key destination-csp &allow-other-keys)
  "Basic implementation, keep the goal the same and do our best to copy the
function."
  (let ((orig-goal (odo:objective-goal csp))
        (orig-function (odo:objective-function csp)))
    (values
     orig-goal
     (ecase orig-goal
       ((:maximize :minimize)
        (cond
          ((odo:variable-p orig-function)
           (odo:state-space-get destination-csp (odo:name orig-function)))
          ((odo:attribute-objective-function-p orig-function)
           (odo:make-attribute-objective-function
            :attribute-values-alist
            (mapcar (lambda (x)
                      (let* ((old-var (car x))
                             (var-name (odo:name old-var))
                             (new-var (odo:state-space-get destination-csp var-name)))
                        (cons new-var (cdr x))))
                    (odo:attribute-values-alist orig-function))))
          (t
           orig-function)))
       (:satisfy)))))
