(in-package #:odo/model-api)

(defsection @odo/model-api/generics (:title "Generics")
  (odo:name generic-function)
  (odo:bounds generic-function)
  (odo:constraint-iterator generic-function)
  (odo:constraint-list generic-function)
  (odo:start-event generic-function)
  (odo:state-space generic-function)
  (odo:uncontrollable-p generic-function))


(defvar *name-fallback-p* nil)

(defgeneric odo:name (thing)
  (:documentation "Return the name of THING. Names are EQUAL comparable.")
  (:method ((thing t))
    "Fallback function in case an implementation has not yet implemented NAME"
    ;; Prevent deprecation warnings.
    (declare (notinline odo:automaton-name odo:cc-name odo:constraint-name
                        odo:episode-name odo:event-name odo:pddl-action-name
                        odo:pddl-constant-name odo:pddl-control-variable-name
                        odo:pddl-domain-name odo:pddl-function-name odo:pddl-parameter-name
                        odo:pddl-predicate-name odo:pddl-type-name odo:primitive-location-name
                        odo:state-plan-name odo:transition-name
                        odo:variable-name odo:state-variable-name))
    ;; Prevent an infinite loop.
    (when *name-fallback-p* (error "ODO:NAME not implemented"))
    (let ((*name-fallback-p* t))
      (cond
        ((odo:type-p thing 'odo:automaton)
         (odo:automaton-name thing))
        ((odo:chance-constraint-p thing)
         (odo:cc-name thing))
        ((or (odo:constraint-p thing)
             (odo:state-constraint-p thing)
             (odo:transition-constraint-p thing))
         (odo:constraint-name thing))
        ((odo:episode-p thing)
         (odo:episode-name thing))
        ((odo:event-p thing)
         (odo:event-name thing))
        ((odo:type-p thing 'odo:pddl-action)
         (odo:pddl-action-name thing))
        ((odo:type-p thing 'odo:pddl-constant)
         (odo:pddl-constant-name thing))
        ((odo:type-p thing 'odo:pddl-control-variable)
         (odo:pddl-control-variable-name thing))
        ((odo:type-p thing 'odo:pddl-domain)
         (odo:pddl-domain-name thing))
        ((odo:type-p thing 'odo:pddl-function)
         (odo:pddl-function-name thing))
        ((odo:type-p thing 'odo:pddl-parameter)
         (odo:pddl-parameter-name thing))
        ((odo:type-p thing 'odo:pddl-predicate)
         (odo:pddl-predicate-name thing))
        ((odo:type-p thing 'odo:pddl-type)
         (odo:pddl-type-name thing))
        ((odo:type-p thing 'odo:primitive-location)
         (odo:primitive-location-name thing))
        ((odo:state-plan-p thing)
         (odo:state-plan-name thing))
        ((odo:type-p thing 'odo:automaton-transition)
         (odo:transition-name thing))
        ((odo:variable-p thing)
         (odo:variable-name thing))
        ((odo:state-variable-p thing)
         (odo:state-variable-name thing))
        (t
         (error "ODO:NAME not implemented"))))))

(defgeneric odo:constraint-iterator (thing)
  (:documentation "Return a an iterator following the
`@ODO/UTILS/ITER` API. Each call to the iterator returns a single
constraint.")
  (:method ((thing t))
    (declare (notinline odo:state-plan-constraint-iterator odo:cc-constraint-iterator))
    (cond
      ((odo:state-plan-p thing)
       (odo:state-plan-constraint-iterator thing))
      ((odo:chance-constraint-p thing)
       (odo:cc-constraint-iterator thing))
      (t
       (error "CONSTRAINT-ITERATOR not implemented")))))

(defgeneric odo:constraint-list (thing)
  (:documentation "Returns two values. The first value is a list of all
constraints in THING. The second is T if the list is freshly consed, NIL
otherwise.")
  (:method (thing)
    (values (odo:it-list (odo:constraint-iterator thing)) t)))

(defvar *circle-detect-start-event* nil)

(defgeneric odo:start-event (thing)
  (:documentation
   "Returns the start event associated with this object.")
  (:method (thing)
    (declare (notinline odo:episode-start-event odo:state-plan-start-event))
    (when *circle-detect-start-event* (error "START-EVENT not implemented"))
    (let ((*circle-detect-start-event* t))
      (cond
        ((odo:episode-p thing)
         (odo:episode-start-event thing))
        ((odo:state-plan-p thing)
         (odo:state-plan-start-event thing))
        (t
         (error "START-EVENT not implemented"))))))

(defvar *circle-detect-state-space* nil)

(defgeneric odo:state-space (thing)
  (:documentation
   "Return the state space of THING")
  (:method (thing)
    (declare (notinline odo:csp-state-space odo:state-plan-state-space))
    (when *circle-detect-state-space* (error "STATE-SPACE not implemented"))
    (let ((*circle-detect-state-space* t))
      (cond
        ((odo:csp-p thing)
         (odo:csp-state-space thing))
        ((odo:state-plan-p thing)
         (odo:state-plan-state-space thing))
        (t
         (error "STATE-SPACE not implemented"))))))

(defvar *circle-detect-uncontrollable-p* nil)

(defgeneric odo:uncontrollable-p (thing)
  (:documentation
   "Return T iff THING is uncontrollable")
  (:method (thing)
    (declare (notinline odo:event-uncontrollable-p))
    (when *circle-detect-uncontrollable-p* (error "UNCONTROLLABLE-P not implemented"))
    (let ((*circle-detect-uncontrollable-p* t))
      (cond
        ((odo:event-p thing)
         (odo:event-uncontrollable-p thing))
        (t
         (error "UNCONTROLLABLE-P not implemented"))))))

(defvar *circle-detect-bounds* nil)

(defgeneric odo:bounds (thing &key)
  (:documentation
   "Return the bounds of `THING`. Returns three values: ((`min-val`,
`max-val`), `min-closed`, `max-closed`).")
  (:method (thing &rest args &key state-space)
    (declare (notinline odo:pddl-control-variable-bounds)
             (ignore state-space))
    (when (eq *circle-detect-bounds* thing) (error "BOUNDS not implemented"))
    (let ((*circle-detect-bounds* thing))
      (cond
        ((odo:expression-p thing)
         (apply #'odo:bounds-by-type thing (odo:type thing) args))
        ((odo:type-p thing 'odo:pddl-control-variable)
         (values (odo:pddl-control-variable-bounds thing) t t))
        (t
         (error "BOUNDS not implemented"))))))
