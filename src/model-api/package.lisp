(uiop:define-package #:odo/model-api
  (:use #:cl
        #:iterate)
  (:import-from #:40ants-doc #:defsection)
  (:import-from #:anaphora #:it)
  (:local-nicknames (#:alex #:alexandria)
                    (#:ana #:anaphora)
                    (#:impl #:odo/impl)
                    (#:utils #:odo/utils)))
