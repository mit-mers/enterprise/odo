(in-package #:odo/model-api)

(defsection @odo/model-api/types (:title "Odo Type")
  "As Odo is designed to be a modeling API frontend for multiple
implementations, there needs to be a way to query what a specific object
represents, without imposing undue burdens on the class/structure hierarchy that
the backend uses. Ideally, the type querying method would be also be usable in
generic function dispatch.

The solution we have chosen is for Odo to define a class hierarchy and have
every object defined by Odo placed into this hierarchy. Every Odo object must
respond to the function `ODO-TYPE` with an instance of a class in this
hierarchy. The instance tells you the type of the original object and what
functions it responds to."

  (odo:object class)
  (odo:odo-object class)
  (odo:type generic-function)
  (odo:odo-type generic-function)
  (odo:type-p function)
  (odo:odo-type-p generic-function)
  (odo:negative-infinity type)
  (odo:positive-infinity type)
  (odo:odo-negative-infinity type)
  (odo:odo-positive-infinity type))

(deftype odo:negative-infinity ()
  `(eql ,odo:+-infinity+))

(deftype odo:positive-infinity ()
  `(eql ,odo:+infinity+))

(deftype odo:odo-negative-infinity ()
  `(eql ,odo:+-infinity+))

(deftype odo:odo-positive-infinity ()
  `(eql ,odo:+infinity+))

(defclass odo:object (cl-singleton-mixin:singleton-mixin)
  ()
  (:documentation
   "The base class for all Odo types."))

(utils:define-renamed-class odo:odo-object odo:object)

(defgeneric odo:type (obj)
  (:documentation
   "Returns an instance of a class descending from `ODO-OBJECT`. This class
represents what `OBJ` represents and which methods it responds to. See the
documentation for the subclasses of `ODO-OBJECT` for further info.")
  (:method ((obj t))
    (declare (notinline odo:odo-type))
    ;; This is unfortunate, but needed for backwards compatibility until we
    ;; fully remove ODO:ODO-TYPE. If there are only two methods for OBJ, return
    ;; NIL, otherwise the provider hasn't updated to ODO:TYPE yet, so we should
    ;; call ODO:ODO-TYPE.
    (if (> (length (compute-applicable-methods #'odo:odo-type (list obj))) 2)
        (odo:odo-type obj)
        nil)))

(utils:define-renamed-generic (odo:odo-type odo:type) (obj))

(defun odo:type-p (obj type)
  "Equivalent to `(typep (odo-type obj) type)`."
  (typep (odo:type obj) type))

(utils:define-renamed-generic (odo:odo-type-p odo:type-p) (obj type))
