(in-package #:odo/model-api)

(defsection @odo/model-api/bdd (:title "Binary Decision Diagrams")
  "A Binary Decision Diagram is a rooted, directed, acyclic graph that consists of a set of decision nodes and two terminal nodes. BDDs are used to represent Boolean functions.

Each decision node represents an `ODO-EXPRESSION` and has a low child (the expression does _not_ hold) and a high child (the expression _does_ hold). Each child of a decision node is itself a BDD Node, creating a recursive, treelike structure. Each path from root to leaf terminates in one of two _terminal_ nodes, which take the value of 1 if the Boolean formula represented by the path from root to leaf is satisfied (evaluates to `TRUE`) or 0 if the Boolean function evaluates to `FALSE`."
  "## Definitions ##"
  (odo:bdd class)
  (odo:bdd-p generic-function)
  (odo:make-bdd function)
  "## Properties ##"
  (odo:child-high generic-function)
  (odo:child-low generic-function)
  "##  API ##
Use the following functions to interact with BDD nodes:"
  (odo:terminal-p generic-function)
  (odo:satisfiable-p generic-function)
  (odo:entailed-p generic-function)
  "## Output ##"
  (odo:bdd-to-dot-stream generic-function))

;;; Define BDD Node
(defclass odo:bdd (odo:object)
  ()
  (:documentation
   "Represents a BDD.

Responds to:

- `NAME`
- `SCOPE`
- `EXPRESSION`
- `CHILD-HIGH`
- `CHILD-LOW`
- `TERMINAL-P`
- `SATISFIABLE-P`
- `ENTAILED-P`
- `BDD-TO-DOT-STREAM`"))

(defgeneric odo:bdd-p (odo:bdd)
  (:documentation "Returns T iff `OBJ` is a BDD.")
  (:method ((bdd t))
    nil))

(defun odo:make-bdd (&key name expression child-low child-high annotations)
  "Make and return an Odo BDD."
  (odo:make-instance 'odo:bdd
                     :name (or name (gensym (uiop:standard-case-symbol-name 'n)))
                     :expression expression
                     :child-low child-low
                     :child-high child-high
                     :annotations annotations))


;; ** BDD Properties

(defgeneric odo:child-low (bdd)
  (:documentation "Gets the low child (false, 0) if the assignment
  represented by `BDD` does not hold. The low child is itself a
  BDD."))

(defgeneric odo:child-high (bdd)
  (:documentation "Gets the high child (true, 1) if the assignment
  represented by `BDD` holds. The high child is itself a BDD."))

;; ** API
(defgeneric odo:terminal-p (bdd)
  (:documentation "Returns two values: `T` if `BDD` is itself a
terminal node and `NIL` otherwise, and `T` if `BDD` is the 1 terminal
node, and `NIL` if BDD is the 0 terminal node.  A node is terminal if
it has no children (i.e., both the `CHILD-LOW` and `CHILD-HIGH` of the
`BDD` are `NIL`)."))

(defgeneric odo:satisfiable-p (bdd &key &allow-other-keys)
  (:documentation "Returns `T` iff the Boolean function represented by
  the `BDD` is satisfiable; that is, can evaluate to `TRUE`. This
  occurs when one of the `BDD`'s children is the 1 terminal node, as
  a path from root to terminal node can result in a satisified
  propositional formula."))

(defgeneric odo:entailed-p (bdd &key &allow-other-keys)
  (:documentation "Returns `T` if and only if the Boolean function
  represented by the `BDD` is always true and `NIL` otherwise. A BDD
  Node is entailed if the 0 terminal node is not in any of its children,
  so no path from `BDD` to its terminal nodes will result in an
  unsatisfied propositional formula."))

;; Visual Output
(defgeneric odo:bdd-to-dot-stream (bdd stream)
  (:documentation "Illustrates the entire multi-rooted shared
structure as dot output to given stream. Warning: this function first
gets all children in a BDD, which may be exponentially large in the
worst case."))

(defmethod odo:equal-p-by-type (left right
                                (left-type odo:bdd)
                                (right-type odo:bdd))
  "Returns T iff `LEFT` and `RIGHT` are equal.

+ The `ID` is `EQUAL`
+ The `VARIABLE` is `ODO-EQUAL`
+ The `CHILD-LOW` is `ODO-EQUAL`
+ The `CHILD-HIGH` is `ODO-EQUAL`"
  (and
   (odo:bdd-p left)
   (odo:bdd-p right)
   (or
    (eql left right)
    (and (equal (odo:name left) (odo:name right))
         (odo:equal-p (odo:expression left) (odo:expression right))
         (odo:equal-p (odo:child-low left) (odo:child-low right))
         (odo:equal-p (odo:child-high left) (odo:child-high right))))))
