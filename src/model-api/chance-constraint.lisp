(in-package #:odo/model-api)

(defsection @odo/model-api/chance-constraint (:title "Chance Constraints")
  "A chance constraint is a conjunction of constraints that must hold
with some allowed probability of failure.

If a disjunctive chance constraint is required, one of the constraints
can be a disjunction constraint.

#### Classes ####"
  (odo:chance-constraint class)
  (odo:odo-chance-constraint class)
  (odo:chance-constraint-p generic-function)
  (odo:odo-chance-constraint-p generic-function)
  "#### Constructors ####"
  (odo:make-chance-constraint function)
  "#### Iterators ####"
  (odo:cc-constraint-iterator generic-function)
  "#### Properties ####"
  (odo:cc-name generic-function)
  (odo:cc-constraint-list generic-function)
  (odo:failure-probability generic-function)
  (odo:cc-failure-probability generic-function))



(defclass odo:chance-constraint (odo:object)
  ()
  (:documentation
   "Represents an object responding to the Odo chance constraint API."))

(utils:define-renamed-class odo:odo-chance-constraint odo:chance-constraint)

(defgeneric odo:chance-constraint-p (cc)
  (:documentation
   "Returns T iff `CC` is an Odo chance constraint.")
  (:method ((cc t))
    nil))

(utils:define-renamed-generic (odo:odo-chance-constraint-p odo:chance-constraint-p) (cc))



(utils:define-renamed-generic (odo:cc-name odo:name) (cc))

(utils:define-renamed-generic (odo:cc-constraint-list odo:constraint-list) (cc))

(defgeneric odo:failure-probability (cc)
  (:documentation
   "Returns the allowed failure-probability of `CC`."))

(utils:define-renamed-generic (odo:cc-failure-probability odo:failure-probability :redirect t) (cc))


;;; Constructor
(defun odo:make-chance-constraint (&key
                                     name
                                     constraints
                                     failure-probability
                                     annotations
                                   &allow-other-keys)
  "Instantiate and return a new chance-constraint."
  (odo:make-instance 'odo:chance-constraint
                     :name (or name (gensym (uiop:standard-case-symbol-name 'chance-constraint)))
                     :constraints constraints
                     :failure-probability failure-probability
                     :annotations annotations))


;;; Iterators

(utils:define-renamed-generic (odo:cc-constraint-iterator odo:constraint-iterator) (cc))
