(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/obstacle
    (:title "Obstacle Predicates")
  (odo:avoids-obstacles odo-function))

(define-odo-function odo:avoids-obstacles (point map)
  (:documentation
   "Returns T if the point is outside all obstacles on the specified map.")
  (:ftype (t t) boolean))
