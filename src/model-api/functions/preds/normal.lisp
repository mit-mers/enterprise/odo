(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/normal
    (:title "Normal function")
  (odo:normal-cdf-= odo-function))

(define-odo-function odo:normal-cdf-= (mu sigma x b &key (tolerance 1d-6))
  (:documentation
   "Returns T iff `b` is equal to the CDF of `N(mu, sigma)` evaluated at `x`.

If `x` or `b` are Odo variables, their assigned values are resolved.")
  (:ftype (number number number number &key (:tolerance number)))
  (:body
   (< (abs (- (cdf (/ (- x mu) sigma))
              b))
      tolerance)))

;; ERF approximation from aytonb
(defun erf (x)
  (let ((te (/ 1 (+ 1 (* 0.5 (abs x)))))
        (tau))
    (setf tau (* te (exp (+ (* -1 (expt x 2))
                            -1.26551223
                            (* 1.00002368 te)
                            (* 0.37409196 (expt te 2))
                            (* 0.09678418 (expt te 3))
                            (* -0.18628806 (expt te 4))
                            (* 0.27886807 (expt te 5))
                            (* -1.13520398 (expt te 6))
                            (* 1.48851587 (expt te 7))
                            (* -0.82215223 (expt te 8))
                            (* 0.17087277 (expt te 9))))))
    (cond
      ((>= x 0)
       (return-from erf (- 1 tau)))
      ((< x 0)
       (return-from erf (- tau 1))))))

(defun cdf (x)
  (/ (1+ (erf (/ x (sqrt 2)))) 2))
