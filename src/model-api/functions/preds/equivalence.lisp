(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/equivalence
    (:title "Equivalence")
  (odo:<-> odo-function))

(define-odo-function odo:<-> (left right)
  (:documentation
   "Returns T iff left <-> right.")
  (:ftype (boolean boolean) boolean)
  (:body
   (or (and left right)
       (and (not left) (not right)))))
