(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/implication
    (:title "Implication")
  (odo:-> odo-function))

(define-odo-function odo:-> (left right)
  (:documentation
   "Returns T iff left -> right.")
  (:ftype (boolean boolean) boolean)
  (:body
   (or right (not left))))
