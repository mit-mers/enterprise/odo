(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/numeric-equality
    (:title "Numeric equality functions")
  (odo:= odo-function))

(define-odo-function odo:= (&rest numbers)
  (:deprecated odo:odo-=)
  (:documentation
   "Compares all the numbers and returns t iff they are equal.")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'= numbers)))
