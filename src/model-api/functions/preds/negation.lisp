(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/negation
    (:title "Negation function")
  (odo:not odo-function))

(define-odo-function odo:not (x)
  (:deprecated odo:odo-not)
  (:documentation
   "Returns T iff the argument is non-NIL.")
  (:ftype (t) boolean)
  (:body
   (not x)))
