(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/temporal
    (:title "Temporal")
  (odo:simple-temporal odo-function)
  (odo:soft-simple-temporal odo-function)
  (odo:probabilistic-duration odo-function))

(define-odo-function odo:simple-temporal (from to
                                               &key
                                               (lower-bound odo:+-infinity+)
                                               (upper-bound odo:+infinity+))
  (:documentation
   "A simple-temporal predicate is satisfied if $lower-bound \\leq to - from
\\leq upper-bound$. If either lower- or upper-bound are not provided, they
default to the appropriate infinity.")
  (:ftype (real real
                &key (:lower-bound (or real odo:negative-infinity))
                (:upper-bound (or real odo:positive-infinity)))
          boolean)
  (:body
   (let ((duration (- to from)))
     (and
      (or (eql lower-bound odo:+-infinity+)
          (<= lower-bound duration))
      (or (eql upper-bound odo:+infinity+)
          (>= upper-bound duration))))))

(define-odo-function odo:looping-simple-temporal (from to number-loops &key
                                                       (lower-bound odo:+-infinity+)
                                                       (upper-bound odo:+infinity+))
  (:documentation
   "The difference between TO and FROM must be between NUMBER-LOOPS*LOWER-BOUND
and NUMBER-LOOPS*UPPER-BOUND (inclusive).")
  (:ftype (real real integer
                &key (:lower-bound (or real odo:negative-infinity))
                (:upper-bound (or real odo:positive-infinity)))
          boolean)
  (:body
   (let ((duration (- to from)))
     (and
      (or (eql lower-bound odo:+-infinity+)
          (<= (* number-loops lower-bound) duration))
      (or (eql upper-bound odo:+infinity+)
          (<= duration (* number-loops upper-bound)))))))

(define-odo-function odo:soft-simple-temporal (from to &key
                                                    (lower-bound odo:+-infinity+) (upper-bound odo:+infinity+)
                                                    (lower-bound-slack 0)
                                                    (upper-bound-slack 0))
  (:documentation
   "The soft version of simple-temporal, with optional slacks that are applied
to the bounds.")
  (:ftype (real real
                &key (:lower-bound (or real odo:negative-infinity))
                (:upper-bound (or real odo:positive-infinity))
                (:lower-bound-slack real)
                (:upper-bound-slack real))
          boolean)
  (:body
   (let ((duration (- to from))
         (lower-bound lower-bound)
         (upper-bound upper-bound))
     (when (and (numberp lower-bound) (numberp lower-bound-slack))
       (setf lower-bound (- lower-bound lower-bound-slack)))
     (when (and (numberp upper-bound) (numberp upper-bound-slack))
       (setf upper-bound (+ upper-bound upper-bound-slack)))
     (and
      (or (eql lower-bound odo:+-infinity+)
          (<= lower-bound duration))
      (or (eql upper-bound odo:+infinity+)
          (>= upper-bound duration))))))

(define-odo-function odo:probabilistic-duration (from to &key distribution)
  (:documentation
   "States that the duration between `FROM` and `TO` is drawn from a probability
`DISTRIBUTION`.")
  (:ftype (real real &key (:distribution list)) boolean)
  (:body
   (declare (ignore from to))
   (alex:switch ((first distribution) :test #'string-equal)
     ('normal
      (let* ((params (rest distribution))
             (mean (getf params :mean))
             (stdev (getf params :stdev)))
        (and mean
             stdev
             (> mean 0)
             (> stdev 0)
             (null (alex:remove-from-plist params :mean :stdev)))))
     (t
      nil))))
