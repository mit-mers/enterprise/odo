(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/equality
    (:title "Equality functions")
  (odo:cl-eql odo-function)
  (odo:cl-equal odo-function)
  (odo:cl-equalp odo-function)
  (odo:equal odo-function))

(define-odo-function odo:cl-eql (left right)
  (:documentation
   "Returns T iff `LEFT` and `RIGHT` are `EQL`.")
  (:ftype (t t) boolean)
  (:body
   (eql left right)))

(define-odo-function odo:cl-equal (x y)
  (:documentation
   "Returns T iff `LEFT` and `RIGHT` are `EQUAL`.")
  (:ftype (t t) boolean)
  (:body
   (equal x y)))

(define-odo-function odo:cl-equalp (left right)
  (:documentation
   "Returns T iff `LEFT` and `RIGHT` are `EQUALP`.")
  (:ftype (t t) boolean)
  (:body
   (equalp left right)))

(define-odo-function odo:equal (left right)
  (:nicknames odo/shadows:=)
  (:deprecated odo:odo-equal)
  (:documentation
   "Returns T iff `LEFT` and `RIGHT` are `ODO-EQUAL-P` to each other.")
  (:ftype (t t) boolean)
  (:body
   (odo:equal-p left right)))
