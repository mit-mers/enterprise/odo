(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/every
    (:title "Every function")
  (odo:every odo-function))

(define-odo-function odo:every (predicate &rest sets)
  (:deprecated odo:odo-every)
  (:documentation
   "Calls the predicate with arguments taken from every set (the arity of
predicate must match the number of sets) and returns T if the predicate
evaluates to T for every call.")
  (:ftype (function &rest t) boolean))
