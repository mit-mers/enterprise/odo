(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/all-diff
    (:title "All different")
  (odo:all-diff odo-function))

(define-odo-function odo:all-diff (values &key (test-fun #'equal))
  (:documentation
   "A function that returns T iff all the values of variables are different
under the provided test. If a test is not provided, a suitable one is
calculated (but it is recommended to always specify the test.)")
  (:ftype (sequence &key (:test-fun t)) boolean)
  (:body
   (with-inactive-filtered (values)
     (= (length values)
        (length (remove-duplicates values :test test-fun))))))
