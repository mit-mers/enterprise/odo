(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/subcircuit
    (:title "Subcircuit")
  (odo:subcircuit odo-function))

(define-odo-function odo:subcircuit (values &key
                                            one-indexed-p
                                            sparse-vector-indices)
  (:documentation
   "A subcircuit is satisfied if a path can be traced from any element of the
values sequence, through every other element, and back to the original
element. Every element not part of the path must point to itself.

If neither `ONE-INDEXED-P` nor `SPARSE-VECTOR-INDICES` is provided, all values
must be integer. The successor (next in the path) of any value is the value at
the matching index in the `VALUES` sequence.

If `ONE-INDEXED-P` is provided, all values must be integer and successors are
calculated as before, except the first value in the `VALUES` sequence is at
index 1, not 0.

If `SPARSE-VECTOR-INDICES` is provided, it must be a sequence the same length as
`VALUES`. Every element in the sequence names the value at the same index of
`VALUES`. This allows the values to be non integer or integer but non
contiguous.

It is an error to provide both `ONE-INDEXED-P` and `SPARSE-VECTOR-INDICES`.

For example, the following are valid subcircuits.

+ + `VALUES` :: `[2, 0, 3, 1]`
  + `SPARSE-VECTOR-INDICES` :: `NIL`
  + `ONE-INDEXED-P` :: `NIL`

+ + `VALUES` :: `[3, 1, 4, 2]`
  + `SPARSE-VECTOR-INDICES` :: `NIL`
  + `ONE-INDEXED-P` :: `T`

+ + `VARIABLES` :: `[3, 5, 8, 0]`
  + `SPARSE-VECTOR-INDICES` :: `[0, 3, 5, 8]`
  + `ONE-INDEXED-P` :: `NIL`

+ + `VARIABLES` :: `[3, 5, 'EXAMPLE, 0]`
  + `SPARSE-VECTOR-INDICES` :: `[0, 3, 5, 'EXAMPLE]`
  + `ONE-INDEXED-P` :: `NIL`

+ + `VALUES` :: [3, 0, 2, 1]
  + `SPARSE-VECTOR-INDICES` :: NIL
  + `ONE-INDEXED-P` :: `NIL`

+ + `VALUES` :: [3, 8, 5, 0]
  + `SPARSE-VECTOR-INDICES` :: [0, 3, 5, 8]
  + `ONE-INDEXED-P` :: `NIL`")
  (:ftype (sequence &key (:one-indexed-p boolean) (:sparse-vector-indices sequence)) boolean)
  (:body
   (let* ((sfun (make-index-lookup-fun one-indexed-p sparse-vector-indices))
          (num-values (length values))
          (visited-vec (make-array num-values
                                   :element-type 'bit
                                   :initial-element 0)))
     ;; First, mark everything pointing to itself as visited.
     (loop
       :for idx :below num-values
       :for raw-value := (elt values idx)
       :for next-idx := (funcall sfun raw-value)
       :when (= idx next-idx)
         :do (setf (aref visited-vec idx) 1))
     ;; Next, walk over the remaining values marking things visited as we see
     ;; them.
     (loop
       :with start-idx := (position 0 visited-vec)
       :while start-idx
       :for raw-value := (elt values start-idx) :then (elt values next-idx)
       :for next-idx := (funcall sfun raw-value)
       :always (and (integerp next-idx)
                    (<= next-idx num-values))
       :until (= (aref visited-vec next-idx) 1)
       :do (setf (aref visited-vec next-idx) 1)
       :finally (return (every #'plusp visited-vec))))))
