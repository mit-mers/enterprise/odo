(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/element
    (:title "Element")
  (odo:element odo-function))

(define-odo-function odo:element (elements index value
                                           &key one-indexed-p sparse-vector-indices
                                           (test-fun #'equal))
  (:documentation
   "The element predicate evaluates to T iff the `INDEX`'th element of
`ELEMENTS` is the same as `VALUE` as determined by `TEST-FUN`. If
`ONE-INDEXED-P` is true, then `ELEMENTS` is assumed to start at index 1. If
`SPARSE-VECTOR-INDICES` is provided, it must be a sequence the same length as
`ELEMENTS` and provide a name for every index of `ELEMENTS`. It is an error to
specify both `ONE-INDEXED-P` and `SPARSE-VECTOR-INDICES`.")
  (:predicate t)
  (:return-type boolean)
  (:body
   (let ((lookup-fun (make-index-lookup-fun one-indexed-p sparse-vector-indices)))
     (funcall test-fun (elt elements (funcall lookup-fun index)) value))))
