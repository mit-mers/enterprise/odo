(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/separation
    (:title "Agent Seperation Predicates")
  (odo:agent-separation odo-function))

(define-odo-function odo:agent-separation (p1 p2 &key (max odo:+infinity+) (min odo:+-infinity+))
  (:documentation
   "Returns T if the two points given are within max from each other and are at least min away from each other.")
  (:ftype (t t &key (:max number) (:min number)) boolean))
