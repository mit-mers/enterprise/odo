(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/all-diff-except
    (:title "All different except")
  (odo:all-diff-except odo-function))

(define-odo-function odo:all-diff-except (values except &key (test-fun #'equal))
  (:documentation
   "A function that returns T iff all the values are different under the
provided test, with the exception of the value of `EXCEPT`. If a test is not
provided, a suitable one is calculated (but it is recommended to always specify
the test.)")
  (:ftype (sequence t &key (:test-fun t)) boolean)
  (:body
   (with-inactive-filtered (values)
     (= (length values)
        (+ (count except values :test test-fun)
           (length (remove-duplicates (remove except values :test test-fun)
                                      :test test-fun)))))))
