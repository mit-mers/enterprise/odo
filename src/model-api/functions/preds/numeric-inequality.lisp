(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/numeric-inequality
    (:title "Numeric inequality functions")
  (odo:le odo-function)
  (odo:lt odo-function)
  (odo:ge odo-function)
  (odo:gt odo-function)
  (odo:ne odo-function))

(define-odo-function odo:le (&rest numbers)
  (:documentation
   "Returns T iff numbers are ordered from smallest to largest (or equal).")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'<= numbers)))

(define-odo-function odo:lt (&rest numbers)
  (:documentation
   "Returns T iff numbers are ordered from smallest to largest.")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'< numbers)))

(define-odo-function odo:ge (&rest numbers)
  (:documentation
   "Returns T iff numbers are ordered from largest to smallest (or equal).")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'>= numbers)))

(define-odo-function odo:gt (&rest numbers)
  (:documentation
   "Returns T iff numbers are ordered from largest to smallest.")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'> numbers)))

(define-odo-function odo:ne (&rest numbers)
  (:documentation
   "Returns T iff all numbers have different values.")
  (:ftype (&rest number) boolean)
  (:body
   (apply #'/= numbers)))
