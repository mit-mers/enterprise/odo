(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/cumulative
    (:title "Cumulative")
  (odo:cumulative odo-function))

(define-odo-function odo:cumulative (starts durations resources capacity)
  (:documentation
   "The cumulative constraint is used to model the resource usage of tasks and
constraining them to stay under a desired capacity.

There are N tasks.

`STARTS` must be a sequence of reals (of length N) denoting the start times of
the tasks.

`DURATIONS` must be a sequence of reals (of length N) denoting the duration of
each task.

`RESOURCES` must be a sequence of reals (of length N) denoting how many
resources are used for the duration of each task.

`CAPACITY` Is a single real specifying the maximum number of resources that can
be used at any time.")
  (:ftype (sequence sequence sequence real) boolean)
  (:body
   (let ((events nil)
         (n (length starts)))
     (when (and (every #'plusp durations)
                (every #'plusp resources))
       (loop
         :for i :upto (1- n)
         :for s := (elt starts i)
         :for d := (elt durations i)
         :for r := (elt resources i)
         :do
            (push r (alex:assoc-value events s :test #'=))
            (push (- r) (alex:assoc-value events (+ s d) :test #'=)))
       (setf events (sort events #'< :key #'car))
       (loop
         :for (nil . resources) :in events
         :for running-total := (reduce #'+ resources) :then (reduce #'+ resources :initial-value running-total)
         :always (<= running-total capacity))))))
