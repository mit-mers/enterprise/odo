(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/conjunction
    (:title "Conjunction")
  (odo:and odo-function))

(define-odo-function odo:and (&rest values)
  (:deprecated odo:odo-and)
  (:documentation
   "Returns T iff all `VALUES` are non-NIL.")
  (:ftype (&rest t) boolean)
  (:body
   (every #'identity values)))
