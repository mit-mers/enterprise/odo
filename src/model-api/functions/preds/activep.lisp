(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/activep
    (:title "Active")
  (odo:activep odo-function))

(define-odo-function odo:activep (x)
  (:documentation
   "Returns T iff `x` is active.")
  (:ftype (t) boolean)
  (:body
   (if (odo:conditional-p x)
       (eql :active (odo:activation-status x))
       (not (eql x odo:+variable-inactive-value+)))))
