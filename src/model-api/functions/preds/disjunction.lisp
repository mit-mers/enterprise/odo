(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/disjunction
    (:title "Disjunction")
  (odo:or odo-function))

(define-odo-function odo:or (&rest values)
  (:deprecated odo:odo-or)
  (:documentation
   "Returns T iff any arg in `VALUES` is non-NIL.")
  (:ftype (&rest t) boolean)
  (:body
   (some #'identity values)))
