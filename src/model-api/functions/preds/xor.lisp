(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/xor
    (:title "Xor")
  (odo:xor odo-function))

(define-odo-function odo:xor (&rest args)
  (:deprecated odo:odo-xor)
  (:documentation
   "Returns T iff an odd number of `ARGS` are non-NIL.")
  (:ftype (&rest t) boolean)
  (:body
   (oddp (count-if #'identity args))))
