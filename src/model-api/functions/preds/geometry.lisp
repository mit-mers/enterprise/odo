(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/geometry
    (:title "Geometry Predicates")
  (odo:geom-within odo-function))

(define-odo-function odo:geom-within (point geom)
  (:documentation
   "Returns T if the point is within the specified geometry.")
  (:ftype (t t) boolean))
