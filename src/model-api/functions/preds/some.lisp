(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/some
    (:title "Some function")
  (odo:some odo-function))

(define-odo-function odo:some (predicate &rest sets)
  (:deprecated odo:odo-some)
  (:documentation
   "Calls the predicate with arguments taken from every set (the arity of
predicate must match the number of sets) and returns T if the predicate
evaluates to T for any call.")
  (:ftype (function &rest t) boolean))
