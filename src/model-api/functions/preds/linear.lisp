(in-package #:odo/model-api)

(defsection @odo/model-api/functions/preds/linear
    (:title "Linear functions")
  (odo:linear-<= odo-function)
  (odo:linear-< odo-function)
  (odo:linear->= odo-function)
  (odo:linear-> odo-function)
  (odo:linear-= odo-function)
  (odo:linear-/= odo-function))

(define-odo-function odo:linear-<= (c x b)
  (:documentation
   "Returns T iff `c*x <= b`. If a variable is inactive, its value is assumed to
be zero. `c` and `x` must be the same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (<= (reduce #'+ (map 'list (lambda (coeff value)
                                (if (eql value odo:+variable-inactive-value+)
                                    0
                                    (* coeff value)))
                        c x))
       b)))

(define-odo-function odo:linear-< (c x b)
  (:documentation
   "Returns T iff `c*x < b`. If a variable is inactive, its value is assumed to
be zero. `c` and `x` must be the same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (< (reduce #'+ (map 'list (lambda (coeff value)
                               (if (eql value odo:+variable-inactive-value+)
                                   0
                                   (* coeff value)))
                       c x))
      b)))

(define-odo-function odo:linear->= (c x b)
  (:documentation
   "Returns T iff `c*x >= b`. If a variable is inactive, its value is assumed to
be zero. `c` and `x` must be the same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (>= (reduce #'+ (map 'list (lambda (coeff value)
                                (if (eql value odo:+variable-inactive-value+)
                                    0
                                    (* coeff value)))
                        c x))
       b)))

(define-odo-function odo:linear-> (c x b)
  (:documentation
   "Returns T iff `c*x > b`. If a variable is inactive, its value is assumed to
be zero. `c` and `x` must be the same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (> (reduce #'+ (map 'list (lambda (coeff value)
                               (if (eql value odo:+variable-inactive-value+)
                                   0
                                   (* coeff value)))
                       c x))
      b)))

(define-odo-function odo:linear-= (c x b)
  (:documentation
   "Returns T iff `c*x = b`. If a variable is inactive, its value is assumed to
be zero. `c` and `x` must be the same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (= (reduce #'+ (map 'list (lambda (coeff value)
                               (if (eql value odo:+variable-inactive-value+)
                                   0
                                   (* coeff value)))
                       c x))
      b)))

(define-odo-function odo:linear-/= (c x b)
  (:documentation
   "Returns T iff `c*x /= b`. Odo variables are allowed in `X` only. If a
variable is inactive, its value is assumed to be zero. `c` and `x` must be the
same length.")
  (:ftype (sequence sequence number) boolean)
  (:body
   (/= (reduce #'+ (map 'list (lambda (coeff value)
                                (if (eql value odo:+variable-inactive-value+)
                                    0
                                    (* coeff value)))
                        c x))
       b)))
