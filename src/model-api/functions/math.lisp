(in-package #:odo/model-api)

(defsection @odo/model-api/functions/math
    (:title "Simple math functions")

  (odo:+ odo-function)
  (odo:- odo-function)
  (odo:* odo-function)
  (odo:/ odo-function))

(define-odo-function odo:+ (&rest numbers)
  (:deprecated odo:odo-+)
  (:documentation
   "A function that adds any number of numbers together and returns the
result.")
  (:ftype (&rest number) number)
  (:derivative (thunk)
   (apply #'odo:+ (mapcar thunk numbers)))
  (:bounds (thunk)
   (let ((min-out 0)
         (max-out 0)
         (min-closed-out t)
         (max-closed-out t))
     (iter
       (for x :in numbers)
       (for (values min max min-closed max-closed) := (funcall thunk x))
       (incf min-out min)
       (incf max-out max)
       (unless min-closed
         (setf min-closed-out nil))
       (unless max-closed
         (setf max-closed-out nil)))
     (values min-out max-out min-closed-out max-closed-out)))
  (:body
   (apply #'+ numbers)))

(define-odo-function odo:- (number &rest more-numbers)
  (:deprecated odo:odo--)
  (:documentation
   "If given a single argument, negates it. Otherwise subtracts every subsequent
number from the first number.")
  (:ftype (number &rest number) number)
  (:derivative (thunk)
   (apply #'odo:- (funcall thunk number) (mapcar thunk more-numbers)))
  (:bounds (thunk)
   (multiple-value-bind (first-min first-max first-min-closed first-max-closed)
       (funcall thunk number)
     (if (null more-numbers)
         (values (- first-max) (- first-min)
                 first-max-closed first-min-closed)
         (let ((min-out first-min)
               (max-out first-max)
               (min-closed-out first-min-closed)
               (max-closed-out first-max-closed))
           (iter
             (for x :in more-numbers)
             (for (values min max min-closed max-closed) := (funcall thunk x))
             (decf min-out max)
             (decf max-out min)
             (unless max-closed
               (setf min-closed-out nil))
             (unless min-closed
               (setf max-closed-out nil)))
           (values min-out max-out min-closed-out max-closed-out)))))
  (:body
   (apply #'- number more-numbers)))

(define-odo-function odo:* (&rest numbers)
  (:deprecated odo:odo-*)
  (:documentation
   "Returns the product of all the numbers.")
  (:ftype (&rest number) number)
  (:derivative (thunk)
   (apply #'odo:+ (loop
                    :for n :in numbers
                    :collect (apply #'odo:* (funcall thunk n) (remove n numbers)))))
  (:body
   (apply #'* numbers)))

(define-odo-function odo:/ (number &rest more-numbers)
  (:deprecated odo:odo-/)
  (:documentation
   "If given a single argument, returns the reciprocal. Otherwise returns the
result of dividing the first argument by the product of every subsequent
argument.")
  (:ftype (number &rest number) number)
  (:derivative (thunk)
   (if (null more-numbers)
       (funcall #'odo:- (odo:/ (funcall thunk number)
                               (odo:* number number)))
       (apply #'odo:- (apply #'odo:/ (funcall thunk number) more-numbers)
              (loop
                :for n :in more-numbers
                :collect (apply #'odo:/ (odo:* number (funcall thunk n))
                                n more-numbers)))))
  (:body
   (apply #'/ number more-numbers)))
