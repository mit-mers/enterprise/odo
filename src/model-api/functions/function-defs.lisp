(in-package #:odo/model-api)

(40ants-doc/locatives/base:define-locative-type odo-function ()
  "Refers to an Odo function.")

(defclass odo-function ()
  ((name
    :initarg :name
    :reader odo-function-name)
   (lambda-list
    :initarg :lambda-list
    :reader odo-function-lambda-list)
   (documentation
    :initarg :documentation
    :reader odo-function-documentation)
   (body-function-name
    :initarg :body-function-name
    :reader odo-function-body-function-name)
   (bounds-function
    :initarg :bounds-function
    :reader odo-function-bounds-function)
   (derivative-function
    :initarg :derivative-function
    :reader odo-function-derivative-function)
   (ftype
    :initarg :ftype
    :reader odo-function-ftype)
   (nicknames
    :initarg :nicknames
    :reader odo-function-nicknames)
   (deprecated-names
    :initarg :deprecated-names
    :reader odo-function-deprecated-names)))

(defun extract-lambda-list-names (lambda-list)
  (multiple-value-bind (required-params optional-params rest-param keyword-params
                        allow-other-keys-p aux-params)
      (alex:parse-ordinary-lambda-list lambda-list)
    (declare (ignore allow-other-keys-p))
    (append required-params
            (mapcar #'first optional-params)
            (remove nil (mapcar #'third optional-params))
            (when rest-param (list rest-param))
            (mapcar (alex:compose #'second #'first) keyword-params)
            (remove nil (mapcar #'third keyword-params))
            (mapcar #'first aux-params))))

(defun make-body-name (name)
  (intern (concatenate 'string "%" (string name))))

(defun make-body-fun (name lambda-list body)
  `(defun ,name ,lambda-list
     ,@(if body
           body
           `((declare (ignore ,@(extract-lambda-list-names lambda-list)))
             (error "Not implemented!")))))

(defun make-argument-type-funs (instance-name lambda-list ftype)
  (let ((out nil))
    (multiple-value-bind (required-params optional-params rest-param keyword-params
                          allow-other-keys-p aux-params)
        (alex:parse-ordinary-lambda-list lambda-list)
      (declare (ignore allow-other-keys-p))
      ;; Optional gives me a headache right now
      (assert (null optional-params))
      (assert (null aux-params))
      (loop
        :for i :upfrom 0
        :for param :in required-params
        :do
           (push `(defmethod odo-function-argument-type ((self (eql ,instance-name)) (arg-name (eql ',(find-symbol (string param) :odo))))
                    ',(elt (type-r:function-type-args-types ftype) i))
                 out))
      (when rest-param
        (push `(defmethod odo-function-argument-type ((self (eql ,instance-name)) (arg-name (eql ',(find-symbol (string rest-param) :odo))))
                 ',(elt (type-r:function-type-args-types ftype)
                        (1+ (position '&rest (type-r:function-type-args-types ftype)))))
              out))
      (when keyword-params
        (let ((key-types (nthcdr (1+ (position '&key (type-r:function-type-args-types ftype)))
                                 (type-r:function-type-args-types ftype))))
          (loop
            :for param :in keyword-params
            :for (keyword-name arg-name) := (first param)
            :do
               (push `(defmethod odo-function-argument-type ((self (eql ,instance-name)) (arg-name (eql ',(find-symbol (string arg-name) :odo))))
                        ',(first (alex:assoc-value key-types keyword-name)))
                     out)))))
    out))

(defmacro define-odo-function (name lambda-list &rest options)
  (flet ((get-option (name)
           (rest (find name options :key #'first))))
    (let ((body-name (make-body-name name))
          (derivative-function (get-option :derivative))
          (bounds-function (get-option :bounds)))
      `(progn
         (let ((function-instance
                 (make-instance 'odo-function
                                :name ',name
                                :lambda-list ',lambda-list
                                :body-function-name ',body-name
                                :documentation ,(first (get-option :documentation))
                                :ftype '(function ,@(get-option :ftype))
                                :derivative-function ,(if derivative-function
                                                          `(lambda (,@(first derivative-function)
                                                                    ,@lambda-list)
                                                             ,@(rest derivative-function))
                                                          `(lambda (&rest args)
                                                             (declare (ignore args))
                                                             (error "Derivative not implemented!")))
                                :bounds-function ,(if bounds-function
                                                      `(lambda (,@(first bounds-function)
                                                                ,@lambda-list)
                                                         ,@(rest bounds-function))
                                                      `(lambda (&rest args)
                                                         (declare (ignore args))
                                                         (error "Bounds not implemented!")))
                                :nicknames ,(when (get-option :nicknames)
                                              `',(get-option :nicknames))
                                :deprecated-names ,(when (get-option :deprecated)
                                                     `',(get-option :deprecated)))))
           (setf (symbol-odo-function ',name)
                 function-instance)
           ,@(let ((ftype (get-option :ftype)))
               (when ftype
                 (make-argument-type-funs 'function-instance lambda-list `(function ,@ftype))))
           (dolist (nick ,(when (get-option :nicknames)
                            `',(get-option :nicknames)))
             (setf (symbol-odo-function nick)
                   function-instance))
           (dolist (nick ,(when (get-option :deprecated)
                            `',(get-option :deprecated)))
             (setf (symbol-odo-function nick)
                   function-instance)))
         ,(make-body-fun body-name lambda-list (get-option :body))
         ,(make-application-fun name lambda-list (get-option :nicknames) (get-option :deprecated))
         ,@(make-application-argument-value-funs name lambda-list)))))

(defgeneric odo-function-p (function)
  (:documentation
   "Returns T iff `FUNCTION` is an Odo function.")
  (:method ((function t))
    nil))

(defgeneric odo-function-argument-type (function arg-name))

(defun make-forwarding-lambda-list (lambda-list)
  (multiple-value-bind (required-params optional-params rest-param keyword-params
                        allow-other-keys-p aux-params)
      (alex:parse-ordinary-lambda-list lambda-list)
    ;; Optional gives me a headache right now
    (assert (null optional-params))
    (assert (null aux-params))
    (let ((out-lambda-list (reverse required-params))
          (out-apply-list (reverse required-params))
          (rest-arg (or rest-param (gensym)))
          (ignorable nil))

      (if (or rest-param keyword-params)
          (progn
            (push '&rest out-lambda-list)
            (push rest-arg out-lambda-list)
            (push rest-arg out-apply-list))
          (push nil out-apply-list))

      (when keyword-params
        (push '&key out-lambda-list))

      (dolist (p keyword-params)
        (destructuring-bind ((keyword-name name) value suppliedp) p
          (if suppliedp
              (push p out-lambda-list)
              (push (list (list keyword-name name) value) out-lambda-list))
          (push name ignorable)
          (when suppliedp
            (push suppliedp ignorable))))

      (when allow-other-keys-p
        (push '&allow-other-keys out-lambda-list))
      (values (nreverse out-lambda-list) (nreverse out-apply-list) ignorable))))

(defun make-application-fun (name lambda-list nicknames deprecated)
  (multiple-value-bind (lambda-list apply-list ignorable)
      (make-forwarding-lambda-list lambda-list)
    `(progn
       (defun ,name ,lambda-list
         (declare (ignorable ,@ignorable))
         (odo:make-instance 'odo:application-expression
                            :operator ',name
                            :arguments (list* ,@apply-list)))
       ,@(mapcar (lambda (nick)
                   `(setf (symbol-function ',nick) (symbol-function ',name)))
                 nicknames)
       ,@(loop :for dep :in deprecated
               :collect `(utils:with-deprecation (:style-warning "0.5" :delete "0.6")
                           (defun ,dep (&rest args)
                             (apply #',name args)))))))

(defun make-application-argument-value-funs (name lambda-list)
  (let ((out nil))
    (multiple-value-bind (required-params optional-params rest-param keyword-params
                          allow-other-keys-p aux-params)
        (alex:parse-ordinary-lambda-list lambda-list)
      (declare (ignore allow-other-keys-p))
      ;; Optional gives me a headache right now
      (assert (null optional-params))
      (assert (null aux-params))
      (loop
        :for i :upfrom 0
        :for param :in required-params
        :do (push `(defmethod odo:argument-value-by-type (self (type (eql ',name))
                                                          (argument-name (eql ',(find-symbol (string param) :odo))))
                     (nth ,i (odo:arguments self)))
                  out))
      (push `(defmethod odo:argument-value-by-type (self (type (eql ',name)) argument-name)
               (let ((real-name (uiop:find-symbol* (string argument-name) :odo nil)))
                 (if (null real-name)
                     (call-next-method)
                     (odo:argument-value self real-name))))
            out)
      (when rest-param
        (push `(defmethod odo:argument-value-by-type (self (type (eql ',name))
                                                      (argument-name (eql ',(find-symbol (string rest-param) :odo))))
                 (nthcdr ,(length required-params) (odo:arguments self)))
              out))
      (loop
        :for ((keyword-name arg-name) value nil) :in keyword-params
        :do (push `(defmethod odo:argument-value-by-type (self (type (eql ',name))
                                                          (argument-name (eql ',(find-symbol (string arg-name) :odo))))
                     (getf (nthcdr ,(length required-params) (odo:arguments self))
                           ,keyword-name ,value))
                  out)))
    out))
