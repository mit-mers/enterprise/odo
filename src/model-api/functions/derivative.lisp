(in-package #:odo/model-api)

(defsection @odo/model-api/functions/derivative
    (:title "Derivatives")

  (odo:d/dx odo-function)
  (odo:d/dn odo-function)
  (odo:d/dt odo-function))

(define-odo-function odo:d/dx (function x)
  (:documentation
   "Returns the derivative of a function, taken with respect to an argument
named by x.")
  (:ftype (function symbol) function))

(define-odo-function odo:d/dn (function n)
  (:documentation
   "Returns the derivative of a function, taken with respect to the n'th
argument.")
  (:ftype (function real) function))

(define-odo-function odo:d/dt (function)
  (:documentation
   "Returns the derivative of a function, taken with respect to
time. Semantically the same as `(d/dx function 'time)`.")
  (:ftype (function) function))
