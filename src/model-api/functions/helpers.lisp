(in-package #:odo/model-api)

(defun make-binding (binding-desc)
  (destructuring-bind (binding-name &optional (outer-name binding-name))
      (alex:ensure-list binding-desc)
    `(,binding-name (remove odo:+variable-inactive-value+ ,outer-name))))

(defun make-bindings-list (bindings)
  (mapcar #'make-binding bindings))

(defmacro with-inactive-filtered (bindings &body body)
  `(let ,(make-bindings-list bindings)
     ,@body))

(defun make-index-lookup-fun (one-indexed-p sparse-vector-indices)
  (assert (not (and one-indexed-p sparse-vector-indices)))
  (cond
    (one-indexed-p
     #'1-)
    (sparse-vector-indices
     (let ((ht (make-hash-table :test 'equal)))
       (dotimes (i (length sparse-vector-indices))
         (setf (gethash (elt sparse-vector-indices i) ht) i))
       (lambda (x)
         (gethash x ht))))
    (t
     #'identity)))
