(in-package #:odo/model-api)


(defsection @odo/model-api/automata
    (:title "Automata")
  "Every automaton consists of a set of locations and transitions between the
locations. Further variants may add additional information."
  (odo:automaton class)
  (odo:odo-automaton class)
  (odo:primitive-automaton class)
  (odo:odo-primitive-automaton class)
  (odo:concurrent-automaton class)
  (odo:odo-concurrent-automaton class)
  (odo:hierarchical-automaton class)
  (odo:odo-hierarchical-automaton class)
  (odo:hierarchical-automaton-all class)
  (odo:odo-hierarchical-automaton-all class)
  (odo:hierarchical-automaton-xor class)
  (odo:odo-hierarchical-automaton-xor class)
  (odo:primitive-location class)
  (odo:odo-primitive-location class)
  (odo:automaton-transition class)
  (odo:odo-automaton-transition class)
  (odo:automaton-transition-branch class)
  (odo:odo-automaton-transition-branch class)

  (odo:make-primitive-automaton function)
  (odo:make-concurrent-automaton function)
  (odo:make-primitive-location function)
  (odo:make-automaton-transition function)
  (odo:make-automaton-transition-branch function)

  (odo:timed-automaton-p generic-function)
  (odo:constraint-automaton-p generic-function)
  (odo:probabilistic-automaton-p generic-function)

  (odo:automaton-name generic-function)
  (odo:location-variable generic-function)
  (odo:automaton-location-variable generic-function)
  (odo:locations generic-function)
  (odo:automaton-locations generic-function)
  (odo:add-location! generic-function)
  (odo:add-automaton-location! generic-function)
  (odo:get-location generic-function)
  (odo:get-automaton-location generic-function)
  (odo:interface-variables generic-function)
  (odo:automaton-interface-variables generic-function)
  (odo:add-interface-variable! generic-function)
  (odo:add-automaton-interface-variable! generic-function)
  (odo:remove-interface-variable! generic-function)
  (odo:remove-automaton-interface-variable! generic-function)
  (odo:get-interface-variable generic-function)
  (odo:get-automaton-interface-variable generic-function)
  (odo:state-variables generic-function)
  (odo:automaton-state-variables generic-function)
  (odo:add-state-variable! generic-function)
  (odo:add-automaton-state-variable! generic-function)
  (odo:get-state-variable generic-function)
  (odo:get-automaton-state-variable generic-function)
  (odo:clock-parameters generic-function)
  (odo:automaton-clock-parameters generic-function)
  (odo:clock-variables generic-function)
  (odo:automaton-clock-variables generic-function)
  (odo:transitions generic-function)
  (odo:automaton-transitions generic-function)
  (odo:add-transition! generic-function)
  (odo:add-automaton-transition! generic-function)
  (odo:get-transition generic-function)
  (odo:get-automaton-transition generic-function)
  (odo:remove-transition! generic-function)
  (odo:remove-automaton-transition! generic-function)

  (odo:primitive-location-name generic-function)

  (odo:location-name generic-function)
  (odo:outgoing-transitions generic-function)
  (odo:location-outgoing-transitions generic-function)
  (odo:incoming-transitions generic-function)
  (odo:location-incoming-transitions generic-function)
  (odo:state-constraint-iterator generic-function)
  (odo:state-constraint-list generic-function)
  (odo:location-constraints generic-function)
  (odo:add-state-constraint! generic-function)
  (odo:add-location-constraint! generic-function)
  (odo:remove-state-constraint! generic-function)
  (odo:remove-location-constraint! generic-function)

  (odo:transition-name generic-function)
  (odo:source generic-function)
  (odo:transition-source generic-function)
  (odo:branches generic-function)
  (odo:transition-branches generic-function)
  (odo:guard generic-function)
  (odo:transition-guard generic-function)
  (odo:clock-resets generic-function)
  (odo:transition-clock-resets generic-function)

  (odo:target generic-function)
  (odo:branch-target generic-function)
  (odo:probability generic-function)
  (odo:branch-probability generic-function))

(defclass odo:automaton (odo:object)
  ()
  (:documentation
   "Base class in the odo type hierarchy for an automaton."))

(utils:define-renamed-class odo:odo-automaton odo:automaton)

(defclass odo:primitive-automaton (odo:automaton)
  ()
  (:documentation
   "Represents a primitive automaton."))

(utils:define-renamed-class odo:odo-primitive-automaton odo:primitive-automaton)

(defclass odo:concurrent-automaton (odo:automaton)
  ()
  (:documentation
   "Represents a concurrent automaton."))

(utils:define-renamed-class odo:odo-concurrent-automaton odo:concurrent-automaton)

(defclass odo:hierarchical-automaton (odo:automaton)
  ()
  (:documentation
   "Represents a hierarchical automaton."))

(utils:define-renamed-class odo:odo-hierarchical-automaton odo:hierarchical-automaton)

(defclass odo:hierarchical-automaton-all (odo:hierarchical-automaton)
  ()
  (:documentation
   "Represents a hierarchical automaton where every contained location must be
marked or none must be marked."))

(utils:define-renamed-class odo:odo-hierarchical-automaton-all odo:hierarchical-automaton-all)

(defclass odo:hierarchical-automaton-xor (odo:hierarchical-automaton)
  ()
  (:documentation
   "Represents a hierarchical automaton where at most one contained location can
be marked."))

(utils:define-renamed-class odo:odo-hierarchical-automaton-xor odo:hierarchical-automaton-xor)

(defun odo:make-primitive-automaton (&key name)
  (odo:make-instance 'odo:primitive-automaton
                     :name (or name (gensym))))

(defun odo:make-concurrent-automaton (&key name)
  (odo:make-instance 'odo:concurrent-automaton
                     :name (or name (gensym))))

(defgeneric odo:timed-automaton-p (automaton)
  (:documentation
   "Returns T iff the automaton is timed."))

(defgeneric odo:constraint-automaton-p (automaton)
  (:documentation
   "Returns T iff the automaton has modal constraints."))

(defgeneric odo:probabilistic-automaton-p (automaton)
  (:documentation
   "Returns T iff the automaton has probabilistic transitions."))

(utils:define-renamed-generic (odo:automaton-name odo:name) (automaton))

(defgeneric odo:location-variable (automaton)
  (:documentation
   "If the automaton is primitive, returns a single finite domain state variable
whose domain is the union of ~(nil)~ and the name of every location contained in
the automaton, representing which location (if any) is marked/active.

If the automaton is concurrent or hierarchical-all, returns a single boolean
state variable, representing if the automaton, and all of its locations, are
marked/active.

If the automaton is hierarchical-xor, returns a single finite domain state
variable whose domain is the union of ~(nil)~ and the name of every location
contained in the automaton, representing which location (if any) is
marked/active."))

(utils:define-renamed-generic (odo:automaton-location-variable odo:location-variable :redirect t) (automaton))

(defgeneric odo:locations (automaton)
  (:documentation
   "Returns the locations of automaton. If the automaton is primitive, it returns a
list of ~odo-primitive-location~ objects. Otherwise, returns a list of
~odo-automaton~ objects."))

(utils:define-renamed-generic (odo:automaton-locations odo:locations :redirect t) (automaton))

(defgeneric odo:add-location! (automaton location))

(utils:define-renamed-generic (odo:add-automaton-location! odo:add-location! :redirect t) (automaton location))

(defgeneric odo:get-location (automaton location-name
                              &optional missing-error-p missing-value)
  (:documentation
   "Returns the location contained within ~automaton~ with the name ~location-name~
(compared using ~equal~).

If the automaton is primitive, it returns an ~odo-primitive-location~
object. Otherwise, returns an ~odo-automaton~ object.

If ~missing-error-p~ is true (must be the default), an error is signaled if the
location does not exist. Otherwise ~missing-value~ is returned."))

(utils:define-renamed-generic (odo:get-automaton-location odo:get-location)
                              (automaton location-name &optional missing-error-p missing-value))

(defgeneric odo:interface-variables (automaton)
  (:documentation
   "Returns a list of ~odo-state-variable~ objects representing connections
between this automaton and other automata/the world/the controller/etc."))

(utils:define-renamed-generic (odo:automaton-interface-variables odo:interface-variables :redirect t) (automaton))

(defgeneric odo:add-interface-variable! (automaton state-variable))

(utils:define-renamed-generic (odo:add-automaton-interface-variable! odo:add-interface-variable! :redirect t) (automaton state-variable))

(defgeneric odo:remove-interface-variable! (automaton state-variable))

(utils:define-renamed-generic (odo:remove-automaton-interface-variable! odo:remove-interface-variable! :redirect t)
                              (automaton state-variable))

(defgeneric odo:get-interface-variable (automaton variable-name
                                        &optional missing-error-p missing-value)
  (:documentation
   "Returns the interface variable contained within ~automaton~ with the name
~variable-name~ (compared using ~equal~).

If ~missing-error-p~ is true (must be the default), an error is signaled if the
variable does not exist. Otherwise ~missing-value~ is returned."))

(utils:define-renamed-generic (odo:get-automaton-interface-variable odo:get-interface-variable :redirect t)
                              (automaton variable-name &optional missing-error-p missing-value))

(defgeneric odo:state-variables (automaton)
  (:documentation
   "If the automaton is primitive, returns a list of ~odo-state-variable~ objects
representing the state of the automaton.

Errors otherwise."))

(utils:define-renamed-generic (odo:automaton-state-variables odo:state-variables :redirect t) (automaton))

(defgeneric odo:add-state-variable! (automaton state-variable))

(utils:define-renamed-generic (odo:add-automaton-state-variable! odo:add-state-variable! :redirect t) (automaton state-variable))

(defgeneric odo:get-state-variable (automaton variable-name
                                    &optional missing-error-p missing-value)
  (:documentation
   "If the automaton is primitive, returns the state variable contained within
~automaton~ with the name ~variable-name~ (compared using ~equal~).

If ~missing-error-p~ is true (must be the default), an error is signaled if the
variable does not exist. Otherwise ~missing-value~ is returned.

If the automaton is not primitive an error is signaled."))

(utils:define-renamed-generic (odo:get-automaton-state-variable odo:get-state-variable)
                              (automaton variable-name &optional missing-error-p missing-value))

(defgeneric odo:clock-variables (automaton)
  (:documentation
   "If the automaton is timed, returns a list of ~odo-state-variable~ objects
representing the clock variables available inside the automaton.

Errors otherwise."))

(utils:define-renamed-generic (odo:automaton-clock-variables odo:clock-variables :redirect t) (automaton))

(defgeneric odo:clock-parameters (automaton)
  (:documentation
   "If the automaton is timed, returns a list of ~odo-state-variable~ objects
representing the clock parameters available inside the automaton.

Errors otherwise."))

(utils:define-renamed-generic (odo:automaton-clock-parameters odo:clock-parameters :redirect t) (automaton))

(defgeneric odo:transitions (automaton)
  (:documentation
   "Return a list of transitions defined for this automaton."))

(utils:define-renamed-generic (odo:automaton-transitions odo:transitions :redirect t) (automaton))

(defgeneric odo:add-transition! (automaton transition))

(utils:define-renamed-generic (odo:add-automaton-transition! odo:add-transition! :redirect t) (automaton transition))

(defgeneric odo:get-transition (automaton transition-name
                                &optional missing-error-p missing-value))

(utils:define-renamed-generic (odo:get-automaton-transition odo:get-transition :redirect t)
                              (automaton transition &optional missing-error-p missing-value))

(defgeneric odo:remove-transition! (automaton transition))

(utils:define-renamed-generic (odo:remove-automaton-transition! odo:remove-transition! :redirect t)
                              (automaton transition))


;; * Primitive locations

(defclass odo:primitive-location (odo:object)
  ()
  (:documentation
   "A type representing primitive locations in an automaton."))

(utils:define-renamed-class odo:odo-primitive-location odo:primitive-location)

(utils:define-renamed-generic (odo:primitive-location-name odo:name) (location))

(defun odo:make-primitive-location (&key name)
  (odo:make-instance 'odo:primitive-location
                     :name (or name (gensym))))


;; * Location functions

;; A location can be either a primitive location or an automaton contained
;; inside a hierarchical automaton.

(utils:define-renamed-generic (odo:location-name odo:name) (location))

(defgeneric odo:outgoing-transitions (location)
  (:documentation
   "Returns a list of transitions with this location as the source."))

(utils:define-renamed-generic (odo:location-outgoing-transitions odo:outgoing-transitions :redirect t)
                              (location))

(defgeneric odo:incoming-transitions (location)
  (:documentation
   "Returns a list of transitions with this location as a target."))

(utils:define-renamed-generic (odo:location-incoming-transitions odo:incoming-transitions :redirect t)
                              (location))

(defgeneric odo:state-constraint-iterator (thing)
  (:documentation
   "Return an iterator of state constraint objects for THING."))

(defgeneric odo:state-constraint-list (thing)
  (:documentation
   "Returns a list of state constraint objects for THING."))

(utils:define-renamed-generic (odo:location-constraints odo:state-constraint-list :redirect t) (location))

(defgeneric odo:add-state-constraint! (location state-constraint)
  (:documentation
   "Add a state-constraint to location."))

(utils:define-renamed-generic (odo:add-location-constraint! odo:add-state-constraint! :redirect t)
                              (location state-constraint))

(defgeneric odo:remove-state-constraint! (location state-constraint)
  (:documentation
   "Remove a state-constraint from location and return T if constraint is removed, NIL if not found."))

(utils:define-renamed-generic (odo:remove-location-constraint! odo:remove-state-constraint! :redirect t)
                              (location state-constraint))


;; * Transitions

(defclass odo:automaton-transition (odo:object)
  ()
  (:documentation
   "A type representing a transition in an odo automaton."))

(utils:define-renamed-class odo:odo-automaton-transition odo:automaton-transition)

(utils:define-renamed-generic (odo:transition-name odo:name) (transition))

(defgeneric odo:source (transition)
  (:documentation
   "Returns the location the transition starts in."))

(utils:define-renamed-generic (odo:transition-source odo:source :redirect t) (transition))

(defgeneric odo:branches (transition)
  (:documentation
   "Returns a list of ~odo-automaton-transition-branch~ objects describing
different destinations of the transition."))

(utils:define-renamed-generic (odo:transition-branches odo:branches :redirect t) (transition))

(defgeneric odo:guard (transition)
  (:documentation
   "Returns a state-constraint describing the transition's guard or ~t~ if
there is no guard."))

(utils:define-renamed-generic (odo:transition-guard odo:guard :redirect t) (transition))

(defgeneric odo:clock-resets (transition)
  (:documentation
   "Returns a state-constraint describing the transition's clock resets. Errors
if transition is not part of a timed automaton."))

(utils:define-renamed-generic (odo:transition-clock-resets odo:clock-resets :redirect t) (transition))

(defun odo:make-automaton-transition (source &key name branches (guard t))
  (odo:make-instance 'odo:automaton-transition
                     :source source
                     :name (or name (gensym))
                     :branches branches
                     :guard guard))


;; * Branches

(defclass odo:automaton-transition-branch (odo:object)
  ()
  (:documentation
   "A type representing the target of a transition."))

(utils:define-renamed-class odo:odo-automaton-transition-branch odo:automaton-transition-branch)

(defgeneric odo:target (branch)
  (:documentation
   "Returns the location the branch ends in."))

(utils:define-renamed-generic (odo:branch-target odo:target :redirect t) (branch))

(defgeneric odo:probability (branch)
  (:documentation
   "Returns the probability the transition will follow this branch. Errors if
the transition is not part of a probabilistic automaton."))

(utils:define-renamed-generic (odo:branch-probability odo:probability :redirect t) (branch))

(defun odo:make-automaton-transition-branch (target)
  (odo:make-instance 'odo:automaton-transition-branch
                     :target target))
