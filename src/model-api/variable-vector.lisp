(in-package #:odo/model-api)

(defsection @odo/model-api/variable-vector (:title "Variable Vectors")
  "In addition to modeling single variables, Odo provides support for
modeling vectors of variables.

Not that the object returned by `MAKE-VARIABLE-ARRAY` is a lisp
vector. No special object should be returned.

#### Type ####"
  (odo:variable-vector-p generic-function)
  "#### Constructor ####"
  (odo:make-variable-vector function))


;;; Create arrays.

(defun map-indices (function shape &optional indices)
  (if shape
      (iter
        (for i from 0 below (pop shape))
        (map-indices function shape (list* i indices)))
      (funcall function indices)))

(defun odo:make-variable-vector (shape
                                 domain-or-spec
                                 &rest args
                                 &key
                                   name
                                   uncontrollable-p
                                   annotations
                                 &allow-other-keys)
  "Instantiate and return an array of Odo variables.

`NAME` is the base name for every variable in the array.

`SHAPE` must be a single number or a list with a single number,
appropriate for passing to `MAKE-ARRAY`.

`DOMAIN-OR-SPEC` must be an Odo domain or a specification for a domain
accepted by `MAKE-DOMAIN`.

`ANNOTATIONS` is a plist of annotations attached to every variable.

The name of every variable is generated from `NAME` by making `NAME`
the first element of a list and the indices of the variable as the
rest of the list.

Any remaining arguments are passed directly to `MAKE-VARIABLE`."
  (declare (ignore annotations uncontrollable-p))
  (unless name
    (setf name (gensym (uiop:standard-case-symbol-name 'vec))))
  (unless (or (numberp shape)
              (and (listp shape)
                   (alex:length= 1 shape)))
    (error "Odo currently only supports vectors (one-d arrays)."))
  (alex:remove-from-plistf args :name)
  (let (storage)
    (setf storage (make-array shape))
    (map-indices #'(lambda (indices)
                     (setf (apply #'aref storage indices)
                           (apply #'odo:make-variable
                                  domain-or-spec
                                  :name (list* name indices)
                                  args)))
                 (alex:ensure-list shape))
    storage))

;;; Define variable array API.

(defgeneric odo:variable-vector-p (obj &key type)
  (:documentation "Returns T iff `OBJ` is an array of Odo variables.")
  (:method ((obj t) &key type proper)
    (declare (ignore type proper))
    nil))

(defmethod odo:variable-vector-p ((obj vector) &key type)
  (and (every (lambda (x)
                (or (odo:variable-p x)
                    (odo:conditional-variable-p x)))
              obj)
       (or (null type)
           (eql type '*)
           (every (lambda (x)
                    (typep x type))
                  obj))))
