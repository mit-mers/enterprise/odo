(in-package #:odo/model-api)

(defsection @odo/model (:title "Modelling API"
                        :package-symbol :odo)
  "This package provides the bulk of Odo's functionality by providing
the API for modeling variables, constraints, state spaces, constraint
satisfaction problems, etc.

The available APIs are:"
  (@odo/model-api/generics section)
  (@odo/model-api/types section)
  (@odo/model-api/infinity section)
  (@odo/model-api/state-space section)
  (@odo/model-api/csp section)
  (@odo/model-api/doms section)
  (@odo/model-api/variable section)
  (@odo/model-api/state-variable section)
  (@odo/model-api/conditional-variable section)
  (@odo/model-api/variable-vector section)
  (@odo/model-api/functions section)
  (@odo/model-api/constraint section)
  (@odo/model-api/annotation section)
  (@odo/model-api/objectives section)
  (@odo/model-api/transforms section)
  (@odo/model-api/expressions section)
  (@odo/model-api/automata section)
  (@odo/model-api/chance-constraint section)
  (@odo/model-api/conditional section)
  (@odo/model-api/episode section)
  (@odo/model-api/event section)
  (@odo/model-api/state-constraint section)
  (@odo/model-api/state-plan section)
  (@odo/model-api/transition-constraint section)
  (@odo/model-api/action-models section)
  (@odo/model-api/bdd section))

(defsection @odo/model-api/functions (:title "Odo Functions"
                                      :package-symbol :odo)
  "This API allows Odo to represent functions in a way that is inspectable and
interpretable by other libraries. It contains the ability to represent function
calls as well as lambda functions.

Future work will port all users of the predicate and expressions APIs over to
this API instead (this API adds lambda functions and more extensibility than the
expressions API and predicates are just functions that return a boolean
value). Additionally, type checking and derivation functionality will be added."
  (@odo/model-api/functions/derivative section)
  (@odo/model-api/functions/math section)
  (@odo/model-api/functions/preds section))

(defsection @odo/model-api/objectives (:title "Odo Objective Functions")
  (odo:evaluate-objective generic-function)
  (@odo/model-api/objectives/attribute section))

(defsection @odo/model-api/transforms (:title "Transforms")
  "This section contains the various transforms that Odo can perform on
objects. The transforms API is currently in beta and backwards incompatible
changes may be made without warning. Be sure to pay attention to the issues on
Odo to know when changes are made."
  (@odo/model-api/transforms/defs section)
  (@odo/model-api/transforms/booleanize-conjunctions section)
  (@odo/model-api/transforms/booleanize-disjunctions section)
  (@odo/model-api/transforms/booleanize-reifs section)
  (@odo/model-api/transforms/enums-to-ints section)
  (@odo/model-api/transforms/expression-to-references section)
  (@odo/model-api/transforms/identity section)
  (@odo/model-api/transforms/reset-domains section)
  (@odo/model-api/transforms/temporal-to-linear section))

(defsection @odo/model-api/expressions (:title "Expressions")
  "Expressions represent a statement that can be evaluated to produce some
value. Modeling frequently involves writing expressions involving Odo variables
or state variables that cannot be evaluated until the variables are
assigned. This API allow modelers to represent these expressions in an
unevaluated form that can be evaluated and/or parsed by solvers."
  (@odo/model-api/expressions/defs section)
  (@odo/model-api/expressions/application section)
  (@odo/model-api/expressions/time section))

(defsection @odo/model-api/action-models (:title "Action Models")
  (@odo/model-api/action-models/pddl section))
