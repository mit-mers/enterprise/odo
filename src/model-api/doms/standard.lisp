(in-package #:odo/model-api)

;; TODO: Maybe remove from Odo exports??
;; (defsection @odo-dev/model-api/doms/standard ()
;;   (odo:define-domain-query macro)
;;   (odo:*domain-query-functions* variable))

(defsection @odo/model-api/doms/standard (:title "Standard Domains"
                                          :package-symbol :odo)
  "This section describes the standard, no-frills domains that
represent a set of values. Other concepts, such as conditional domains
can be built on top of these domains.

##### Types

These functions and types are used to examine the types of values
contained within domains. The classes below are not meant to be
subclassed to provide the domain functionality. They are currently
singletons, but that is not guaranteed to hold in the future."
  (odo:domain-type generic-function)
  (odo:domain class)
  (odo:odo-domain class)
  (odo:discrete-domain class)
  (odo:pure-discrete-domain class)
  (odo:real-domain class)
  (odo:pure-real-domain class)
  (odo:integer-domain class)
  (odo:pure-integer-domain class)
  (odo:boolean-domain class)
  (odo:pure-boolean-domain class)

  "In addition to testing the return value of `DOMAIN-TYPE`, the
following functions can be used to determine the types of values in
domains."
  (odo:integer-domain-p generic-function)
  (odo:boolean-domain-p generic-function)
  (odo:real-domain-p generic-function)
  (odo:discrete-domain-p generic-function)

  "##### Queries

The following functions are used to query the state of
domains. Additionally, domains must respond to the
`@ODO/MODEL-API/ANNOTATION` API."

  (odo:assigned-p generic-function)
  (odo:assigned-value generic-function)
  (odo:domain-test generic-function)
  (odo:domain-type-spec generic-function)
  (odo:in-domain-p generic-function)
  (odo:max-val generic-function)
  (odo:median generic-function)
  (odo:min-val generic-function)
  (odo:domain-p generic-function)
  (odo:odo-domain-p generic-function)
  (odo:range-iterator generic-function)
  (odo:range-list generic-function)
  (odo:regret-max generic-function)
  (odo:regret-min generic-function)
  (odo:size generic-function)
  (odo:value-iterator generic-function)
  (odo:value-list generic-function)
  (odo:width generic-function)

  "##### Printing"
  (odo:print-odo-domain function)
  "##### Conditions"
  (odo:domain-error condition))


;;; Define our conditions

(define-condition odo:domain-error ()
  ()
  (:documentation "The base condition for all model errors on
domains."))

(define-condition simple-domain-error (odo:domain-error simple-error)
  ()
  (:documentation "A catch-all error type for simple errors that don't
have their own conditions yet."))

(declaim (inline raise-domain-error))
(defun raise-domain-error (format-control &rest format-args)
  (error 'simple-domain-error
         :format-control format-control
         :format-arguments format-args))


;;; Define the helpers

(defvar odo:*domain-query-functions* nil
  "An alist mapping domain querying function names to plists with
their properties.")

(defun %domain-query-methods (options)
  (mapcar #'second (remove-if-not (lambda (x) (eql :method x)) options :key #'first)))

(defmacro define-domain-query (name lambda-list &body options)
  "Defines a domain query with appropriate defaults and registers it
in `*DOMAIN-QUERY-FUNCTIONS*`."
  (flet ((%parse-lambda-list (lambda-list)
           (multiple-value-bind (reqs optionals rest keys)
               (alex:parse-ordinary-lambda-list lambda-list)
             (let ((out (if rest (list rest) nil)))
               (alex:appendf out
                             reqs
                             (mapcar #'first optionals)
                             (mapcar (alex:compose #'second #'first) keys))
               out))))
    (let* ((default-value (find :default-value options :key #'first))
           (arg-names (%parse-lambda-list lambda-list))
           (default-method (find :default-method options :key #'first)))
      (cond
        (default-value
         (setf options (remove default-value options))
            (push `(:method ,lambda-list
                     (declare (ignorable ,@arg-names))
                     ,(second default-value))
                  options))
        (default-method
         (setf options (remove default-method options))
            (push `(:method ,@(rest default-method))
                  options))
        (t
         (push `(:method ,lambda-list
                  (declare (ignorable ,@arg-names))
                  (if (odo:domain-p ,(first lambda-list))
                      (raise-domain-error
                       "Domain ~A does not support the query ~A."
                       ,(first lambda-list)
                       ',name)
                      (error "The first argument to ~A must be an Odo domain."
                             ',name)))
               options)))
      `(progn
         (setf (getf (alex:assoc-value odo:*domain-query-functions* ',name) :lambda-list)
               ',lambda-list)
         (defgeneric ,name ,lambda-list
           ,@options)))))


;;; Types and type related functions

(define-domain-query odo:domain-type (dom)
  (:documentation "Returns an instance of a class descending from
`ODO-DOMAIN`. This class represents what type of elements `DOM`
contains and which methods it responds to. See the documentation for
classes in this section for further info."))

(defclass odo:domain (odo:object)
  ()
  (:documentation "The base class for all Odo domain types.

Signifies that the object responds to:

+ `ASSIGNED-P`
+ `ASSIGNED-VALUE`
+ `DOMAIN-TEST`
+ `DOMAIN-TYPE-SPEC`
+ `IN-DOMAIN-P`
+ `DOMAIN-P` returns `T`
+ `SIZE`"))

(utils:define-renamed-class odo:odo-domain odo:domain)

(defclass odo:discrete-domain (odo:domain)
  ()
  (:documentation "Signifies that the domain contains some discrete
elements.

The domain responds to:

+ `VALUE-ITERATOR`
+ `VALUE-LIST`"))

(defclass odo:pure-discrete-domain (odo:discrete-domain)
  ()
  (:documentation "Signifies that the domain contains *only*
discretely valued elements."))

(defclass odo:real-domain (odo:domain)
  ()
  (:documentation "Signifies that the domain contains some real-valued
elements.

The domain must respond to:

+ `MAX-VAL`
+ `MEDIAN`
+ `MIN-VAL`
+ `RANGE-ITERATOR`
+ `RANGE-LIST`
+ `WIDTH`"))

(defclass odo:pure-real-domain (odo:real-domain)
  ()
  (:documentation "Signifies that the domain contains only real-valued
elements."))

(defclass odo:integer-domain (odo:real-domain odo:discrete-domain)
  ()
  (:documentation "Signifies that the domain contains some
integer-valued elements.

The domain must respond to:

+ `REGRET-MAX`
+ `REGRET-MIN`"))

(defclass odo:pure-integer-domain (odo:integer-domain odo:pure-real-domain odo:pure-discrete-domain)
  ()
  (:documentation "Signifies that the domain contains only integer
valued elements."))

(defclass odo:boolean-domain (odo:discrete-domain)
  ()
  (:documentation "Signifies that the domain contains the elements `T`
and/or `NIL`."))

(defclass odo:pure-boolean-domain (odo:boolean-domain odo:pure-discrete-domain)
  ()
  (:documentation "Signifies that the domain contains only the
elements `T` and/or `NIL`."))

(define-domain-query odo:domain-p (obj)
  (:documentation "Returns T iff `OBJ` is an Odo domain.")
  (:default-value nil))

(utils:define-renamed-generic (odo:odo-domain-p odo:domain-p) (obj))

(define-domain-query odo:domain-type-spec (dom &optional tightest)
  (:documentation "Returns two values. The first is a type spec for
the elements stored in `DOM`. If `TIGHTEST` is non-NIL, every effort
should be made to ensure that the typespec contains only the elements
in `DOM`. Otherwise the type spec may only be a super type of the
elements. If the second returned value is `T`, the returned typespec
is the tightest possible.

For instance, a domain defined over the integers `[1, 2, 5, 6]` may
return `(VALUES 'INTEGER NIL)` or `(VALUES '(INTEGER 1 6) NIL)` if
`TIGHTEST` is NIL. If `TIGHTEST` is `T`, the implementation should try
to return: `(VALUES '(OR (INTEGER 1 2) (INTEGER 5 6)) T)`")
  (:default-method (dom &optional tightest)
      (unless (odo:domain-p dom)
        (error "The first argument to ~A must be an Odo domain." 'odo:domain-type-spec))
    (typecase (odo:domain-type dom)
      (odo:pure-integer-domain
       (if tightest
           `(or ,@(mapcar (lambda (x) `(integer ,(if (eql (first x) odo:+-infinity+)
                                                     '* (first x))
                                                ,(if (eql (second x) odo:+infinity+)
                                                     '* (second x))))
                          (odo:range-list dom)))
           (let ((min-val (odo:min-val dom))
                 (max-val (odo:max-val dom)))
             `(integer ,(if (eql min-val odo:+-infinity+) '* min-val)
                       ,(if (eql max-val odo:+infinity+) '* max-val)))))
      (odo:pure-real-domain
       (if tightest
           `(or ,@(mapcar (lambda (x)
                            (destructuring-bind (min max min-closed max-closed) x
                              `(real ,(cond
                                        (min-closed
                                         min)
                                        ((eql odo:+-infinity+ min)
                                         '*)
                                        (t
                                         (list min)))
                                     ,(cond
                                        (max-closed
                                         max)
                                        ((eql odo:+infinity+ max)
                                         '*)
                                        (t
                                         (list max))))))
                          (odo:range-list dom)))
           (multiple-value-bind (min min-closed) (odo:min-val dom)
             (multiple-value-bind (max max-closed) (odo:max-val dom)
               `(real ,(cond
                         (min-closed
                          min)
                         ((eql odo:+-infinity+ min)
                          '*)
                         (t
                          (list min)))
                      ,(cond
                         (max-closed
                          max)
                         ((eql odo:+infinity+ max)
                          '*)
                         (t
                          (list max))))))))
      (odo:pure-boolean-domain
       (if (and tightest
                (odo:assigned-p dom))
           `(eql ,(odo:assigned-value dom))
           'boolean))
      (odo:pure-discrete-domain
       `(member ,@(odo:value-list dom)))
      (t
       (raise-domain-error "Domain ~A does not support the query ~A."
                           dom 'odo:domain-type-spec)))))

(define-domain-query odo:integer-domain-p (dom &optional pure)
  (:documentation "Returns `T` if `DOM` contains integer values. If
`PURE` is non-`NIL`, returns `T` if `DOM` contains *only* integer
values.")
  (:default-method (dom &optional pure)
      (when (odo:domain-p dom)
        (if pure
            (typep (odo:domain-type dom) 'odo:pure-integer-domain)
            (typep (odo:domain-type dom) 'odo:integer-domain)))))

(define-domain-query odo:real-domain-p (dom &optional pure)
  (:documentation "Returns `T` if `DOM` contains real values. If
`PURE` is non-`NIL`, returns `T` if `DOM` contains *only* real
values.")
  (:default-method (dom &optional pure)
      (when (odo:domain-p dom)
        (if pure
            (typep (odo:domain-type dom) 'odo:pure-real-domain)
            (typep (odo:domain-type dom) 'odo:real-domain)))))

(define-domain-query odo:boolean-domain-p (dom &optional pure)
  (:documentation "Returns `T` if `DOM` contains boolean values. If
`PURE` is non-`NIL`, returns `T` if `DOM` contains *only* bolean
values.")
  (:default-method (dom &optional pure)
      (when (odo:domain-p dom)
        (if pure
            (typep (odo:domain-type dom) 'odo:pure-boolean-domain)
            (typep (odo:domain-type dom) 'odo:boolean-domain)))))

(define-domain-query odo:discrete-domain-p (dom &optional pure)
  (:documentation "Returns `T` if `DOM` contains discrete values. If
`PURE` is non-`NIL`, returns `T` if `DOM` contains *only* discrete
values.")
  (:default-method (dom &optional pure)
      (when (odo:domain-p dom)
        (if pure
            (typep (odo:domain-type dom) 'odo:pure-discrete-domain)
            (typep (odo:domain-type dom) 'odo:discrete-domain)))))


;;; Declare the API for standard domains.

(define-domain-query odo:in-domain-p (dom el)
  (:documentation "Return T iff `EL` is in the domain of `DOM`."))

(define-domain-query odo:assigned-p (dom)
  (:documentation "Return T iff `DOM` is assigned (contains one element)."))

(define-domain-query odo:assigned-value (dom)
  (:documentation "Return the assigned value of `DOM`. If `DOM` is not
assigned, raises a `DOMAIN-ERROR`."))

(define-domain-query odo:min-val (numeric-dom)
  (:documentation "Returns two values: the minimum value of
`NUMERIC-DOM`. and a boolean that is true iff the lower bound of the
domain is closed.

If the minimum value is negative infinity, returns `(VALUES +-INFINITY+ NIL)`."))

(define-domain-query odo:max-val (numeric-dom)
  (:documentation "Returns two values: the maximum value of
`NUMERIC-DOM`. and a boolean that is true iff the upper bound of the
domain is closed.

If the maximum value is infinity, returns `(VALUES +INFINITY+ NIL)`."))

(define-domain-query odo:median (numeric-dom)
  (:documentation "Return the median value of the domain. (Greatest
element not greater than the median).

If the domain has infinite `WIDTH`, returns `'*`."))

(define-domain-query odo:width (numeric-dom)
  (:documentation "Return the width of the domain.

If the width is infinite, returns `'*`."))

(define-domain-query odo:range-list (numeric-dom)
  (:documentation "Returns two values. The first is a list of the
ranges in the domain. The second is `T` if the list is freshly
consed, `NIL` otherwise.

Signals an error if the size of `DOM` is infinite.")
  (:default-method (numeric-dom)
    (values (odo:it-list (odo:range-iterator numeric-dom) t) t)))

(define-domain-query odo:range-iterator (numeric-dom)
  (:documentation "Return an iterator over the ranges of permissible
values of `NUMERIC-DOM`.

Each call to `IT-NEXT` returns multiple values. The first value is the lower
bound of the range (or `+-INFINITY+`). The second value is the upper bound of
the range (or `+INFINITY+`). The third value is T iff the lower bound is
closed. The fourth value is T iff the upper bound is closed. Ranges are
guaranteed to be provided in sorted order from lowest to highest."))

(define-domain-query odo:size (dom)
  (:documentation "Return the number of elements in `DOM`. If the size
is infinite, returns `'*`"))

(define-domain-query odo:value-list (dom)
  (:documentation "Returns two values. The first is a list of the
values in the domain. The second is `T` if the list is freshly
consed, `NIL` otherwise.

Signals an error if the size of `DOM` is infinite.")
  (:default-method (dom)
      (values (odo:it-list (odo:value-iterator dom)) t)))

(define-domain-query odo:value-iterator (dom)
  (:documentation "Return an object following the `@ODO/UTILS/ITER` API that
iterates over the values of `DOM`. Is allowed to signal an error if
the size of `DOM` is infinite (but is not required to)."))

(define-domain-query odo:regret-min (int-dom)
  (:documentation "Return the regret of the minimum value of the domain."))

(define-domain-query odo:regret-max (int-dom)
  (:documentation "Return the regret of the maximum value of the domain."))

(define-domain-query odo:domain-test (dom)
  (:documentation "Returns the function used to compare elements of
this domain.")
  (:default-method (dom)
      (unless (odo:domain-p dom)
        (error "The first argument to ~A must be an Odo domain."
               'odo:domain-test))
    (etypecase (odo:domain-type dom)
      (odo:pure-real-domain
       #'=)
      (odo:pure-boolean-domain
       #'eql))))


;;; Iterate drivers

(defmacro-driver (FOR range IN-RANGES dom)
  "All the ranges in the domain of `DOM`."
  (let ((iterator (gensym))
        (kwd (if generate 'generate 'for)))
    `(progn
       (with ,iterator = (odo:range-iterator ,dom))
       (,kwd ,range next (if (odo:it-done ,iterator)
                             (terminate)
                             (odo:it-next ,iterator))))))

(defmacro-driver (FOR val IN-DOMAIN var)
  "All the values in the domain of `VAR`."
  (let ((iterator (gensym))
        (kwd (if generate 'generate 'for)))
    `(progn
       (with ,iterator = (odo:value-iterator ,var))
       (,kwd ,val next (if (odo:it-done ,iterator)
                           (terminate)
                           (odo:it-next ,iterator))))))


;;;; Printing

(defun print-numeric-domain (dom type stream)
  (utils:with-delimiter-printing (dom stream)
    (let ((range-list nil))
      (iter (for (values lb ub) :in-ranges dom)
            (push (cons lb ub) range-list))
      (setf range-list (nreverse range-list))
      (write type :stream stream)
      (write-char #\space stream)
      (pprint-newline :fill stream)
      (if (= (length range-list) 1)
          (progn
            (prin1 :min stream)
            (write-char #\space stream)
            (prin1 (car (first range-list)) stream)
            (write-char #\space stream)
            (pprint-newline :fill stream)
            (prin1 :max stream)
            (write-char #\space stream)
            (prin1 (cdr (first range-list)) stream))
          (progn
            (prin1 :ranges stream)
            (write-char #\space stream)
            (prin1 range-list stream))))))

(defgeneric %print-odo-domain (stream dom dom-type))

(defmethod %print-odo-domain (stream dom (dom-type odo:pure-boolean-domain))
  (utils:with-delimiter-printing (dom stream)
    (cond
      ((= 2 (odo:size dom))
       (write 'boolean :stream stream))
      ((odo:in-domain-p dom t)
       (write 'boolean :stream stream)
       (write-char #\space stream)
       (pprint-newline :linear stream)
       (prin1 :value stream)
       (write-char #\space stream)
       (princ t stream))
      (t
       (write 'boolean :stream stream)
       (write-char #\space stream)
       (pprint-newline :linear stream)
       (prin1 :value stream)
       (write-char #\space stream)
       (princ nil stream)))))

(defmethod %print-odo-domain (stream dom (dom-type odo:pure-integer-domain))
  (print-numeric-domain dom 'integer stream))

(defmethod %print-odo-domain (stream dom (dom-type odo:pure-real-domain))
  (print-numeric-domain dom 'real stream))

(defmethod %print-odo-domain (stream dom (dom-type odo:pure-discrete-domain))
  (utils:with-delimiter-printing (dom stream)
    (write 'member :stream stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (prin1 :test stream)
    (write-char #\space stream)
    (let ((test (odo:domain-test dom)))
      (cond
        ((eql test #'eq)
         (write 'eq :stream stream))
        ((eql test #'eql)
         (write 'eql :stream stream))
        ((eql test #'equal)
         (write 'equal :stream stream))
        ((eql test #'equalp)
         (write 'equalp :stream stream))
        (t
         (write (odo:domain-test dom) :stream stream))))
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (prin1 :members stream)
    (write-char #\space stream)
    (write (odo:value-list dom) :stream stream)))

(defun odo:print-odo-domain (odo-domain stream)
  "Print the `ODO-DOMAIN` to `STREAM`."
  (%print-odo-domain stream odo-domain (odo:domain-type odo-domain)))
