(in-package #:odo/model-api)

;; TODO: Remove from odo exports??
(defsection @odo-dev/model-api/doms (:title "Domains Dev")
  (odo:canonicalize-domain-description function)
  (odo:domain-spec function))

(defsection @odo/model-api/doms (:title "Domains"
                                 :package-symbol :odo)
  "A domain describes a set of possible values. This section describes
the valid domain types and the functions that can be used to query
domains.

Each concrete domain type can be instantiated with the following
functions."
  (odo:make-boolean-domain function)
  (odo:make-enumerated-domain function)
  (odo:make-integer-domain function)
  (odo:make-real-domain function)
  (odo:make-domain function)
  (odo:domain-equal function)
  (@odo/model-api/doms/standard section))

(defun canonicalize-domain-description (type-spec)
  "Given a type spec, return a canonicalized form that can be passed
directly to the appropriate constructors."
  (let (domain-type
        (extra-args nil))
    (cond
      ((typep type-spec 'odo:domain)
       (setf domain-type type-spec))
      ((symbolp type-spec)
       ;; No extra arguments have been provided, so just look up the
       ;; symbol naming the domain and return it.
       (setf domain-type (ecase type-spec
                           ((boolean odo:boolean-domain :boolean-domain)
                            (make-instance 'odo:pure-boolean-domain))
                           ((integer odo:integer-domain :integer-domain)
                            (make-instance 'odo:pure-integer-domain))
                           ((real odo:real-domain :real-domain)
                            (make-instance 'odo:pure-real-domain))
                           ((member odo:discrete-domain :discrete-domain)
                            (make-instance 'odo:pure-discrete-domain)))))
      ((listp type-spec)
       ;; Extra arguments may have been provided. Destructure the list
       ;; and try to figure out the extra arguments.
       (destructuring-bind (type-spec &rest list-args)
           type-spec
         (ecase type-spec
           (real
            (setf domain-type (make-instance 'odo:pure-real-domain))
            (when list-args
              (destructuring-bind (lb &optional (ub odo:+infinity+ ubp))
                  list-args
                (setf extra-args (list* :min lb extra-args))
                (when ubp
                  (setf extra-args (list* :max ub extra-args))))))
           (integer
            (setf domain-type (make-instance 'odo:pure-integer-domain))
            (when list-args
              (destructuring-bind (lb &optional (ub odo:+infinity+ ubp))
                  list-args
                (setf extra-args (list* :min lb extra-args))
                (when ubp
                  (setf extra-args (list* :max ub extra-args))))))
           (member
            (setf domain-type (make-instance 'odo:pure-discrete-domain))
            (when list-args
              (setf extra-args (list* :test #'equal
                                      :members list-args
                                      extra-args)))))))
      (t
       (error "Unable to canonicalize with spec ~S" type-spec)))
    (list* domain-type extra-args)))

(defun odo:make-boolean-domain (&key annotations
                                  (value :unknown)
                                  (true (ecase value
                                          ((:unknown t)
                                           t)
                                          ((nil)
                                           nil)))
                                  (false (ecase value
                                           ((:unknown nil)
                                            t)
                                           ((t)
                                            nil))))
  "Make and return a boolean domain.

+ If `TRUE` is non-NIL, then `T` is included in the domain.
+ If `FALSE` is non-NIL, then `NIL` is included in the domain.
+ If `VALUE` is provided, then the domain contains only that value.

Results are undefined if (`VALUE` and (`TRUE` or `FALSE`)) are provided.

If both `TRUE` and `FALSE` are `NIL`, a `DOMAIN-ERROR` is signalled.

`ANNOTATIONS` is a plist of annotations for this domain."
  (unless (or true false)
    (error 'odo:domain-error))
  (odo:make-instance 'odo:pure-boolean-domain
                     :annotations annotations
                     :true true
                     :false false))

(defun odo:make-enumerated-domain (&key
                                     members
                                     (test #'equal)
                                     annotations)
  "Make and return an enumerated finite domain.

`MEMBERS` is a list of elements in the domain.

`TEST` is the equality test for the domain.

`ANNOTATIONS` is a plist of annotations for this domain."
  (unless (and members
               (listp members))
    (raise-domain-error "Domain elements must be provided."))
  (unless (alex:length= (remove-duplicates members :test test)
                        members)
    (raise-domain-error "Domain elements must be unique."))

  (odo:make-instance 'odo:pure-discrete-domain
                     :members members
                     :test (alex:ensure-function test)
                     :annotations annotations))

(defun odo:make-integer-domain (&key
                                  (min odo:+-infinity+)
                                  (max odo:+infinity+)
                                  (values nil)
                                  (ranges (list (cons min max)))
                                  annotations)
  "Make and return an integer domain.

There are three ways to describe an integer domain:

1. Provide a `MIN` and/or `MAX` value.

2. Provide a list of `RANGES`. The ranges must be cons cells. The
`CAR` of each cell is the lower bound of the range. The `CDR` of each
cell is the upper bound of the range.

3. Provide a list of integer `VALUES` the domain can take.

If none of the above is explicitly provided, the integer domain is
created with the largest possible domain. Behavior is undefined if
more than one way is used to describe the integer's domain.

`ANNOTATIONS` is a plist of annotations for this domain."

  (odo:make-instance 'odo:pure-integer-domain
                     :ranges (utils:canonicalize-integer-ranges
                              (if values
                                  (utils:convert-values-to-ranges values)
                                  ranges)
                              :error-fun #'raise-domain-error)
                     :annotations annotations))

(defun odo:make-real-domain (&key
                               (min odo:+-infinity+)
                               (max odo:+infinity+)
                               (ranges (list (cons min max)))
                               annotations)
  "Make and return a real domain.

The domain can be instantiated by providing bounds (min and/or max) or
ranges. If none of `MIN`, `MAX`, or `RANGES` is provided, the returned
domain has the largest domain possible. Results are undefined if
`MIN`/`MAX` and `RANGES` are provided.

`MIN` must be a number, `'*`, or `+-INFINITY+`.

`MAX` must be a number, `'*`, or `+INFINITY+`.

`RANGES`, if provided, must be a list of cons cells. The `CAR` of each
cell is the lower bound of the range, either a number or a cons cell,
the `CDR` of each cell is the upper bound of the range, wither a
number or a cons cell. If the bounds are numbers, that bound is
assumed to be closed. If the bound is a cons cell, the `CAR` is the
bound and the `CDR` is a boolean, if T the range is closed, if `NIL`
it is open.

`ANNOTATIONS` is a plist of annotations for this domain."

  (odo:make-instance 'odo:pure-real-domain
                     :ranges (utils:canonicalize-ranges ranges
                                                        :number-type 'real
                                                        :error-fun #'raise-domain-error)
                     :annotations annotations))

(defun odo:make-domain (type-spec &rest args
                        &key
                          annotations
                        &allow-other-keys)
  "Make a domain based on a simple typespec. Determines the type of
domain to instantiate based on `TYPE-SPEC`, passes any additional
arguments directly to the appropriate `MAKE-*-DOMAIN` function.

`TYPE-SPEC` can be one of:

+ `BOOLEAN`, `BOOLEAN-DOMAIN`, or `:BOOLEAN-DOMAIN`- Make a boolean
domain (`MAKE-BOOLEAN-DOMAIN`)

+ `INTEGER`, `INTEGER-DOMAIN`, or `:INTEGER-DOMAIN` - Make an integer
domain (`MAKE-INTEGER-DOMAIN`).

+ `(INTEGER LB)` - Make an integer domain with `:MIN` set to `LB`.

+ `(INTEGER LB UB)` - Make an integer domain with `:MIN` set to `LB`
and `:MAX` set to `UB`.

+ `REAL`, `REAL-DOMAIN`, or `:REAL-DOMAIN` - Make a real
domain (`MAKE-REAL-DOMAIN`).

+ `(REAL LB)` - Make a real domain with `:MIN` set to `LB`.

+ `(REAL LB UB)` - Make a real domain with `:MIN` set to `LB` and
`:MAX` set to `UB`.

+ `MEMBER`, `DISCRETE-DOMAIN` or `:DISCRETE-DOMAIN` - Make an
enumerated finite domain (`MAKE-ENUMERATED-DOMAIN`)

+ `(MEMBER &REST ELEMENTS)` Make an enumerated finite domain with
`:ELEMENTS` set to `ELEMENTS` and `:TEST` set to `#'EQUAL`."
  (declare (ignore annotations))
  (destructuring-bind (domain-type &rest extra-args)
      (canonicalize-domain-description type-spec)
    (let ((maker
           (etypecase domain-type
             (odo:pure-integer-domain
              #'odo:make-integer-domain)
             (odo:pure-real-domain
              #'odo:make-real-domain)
             (odo:pure-boolean-domain
              #'odo:make-boolean-domain)
             (odo:pure-discrete-domain
              #'odo:make-enumerated-domain))))
      (apply maker (nconc extra-args args)))))

(defun integer-ranges (int)
  (iter
    (for (values lb ub) :in-ranges int)
    (collect (cons lb ub))))

(defun real-ranges (dom)
  (iter
    (for (values lb ub lb-closed ub-closed) :in-ranges dom)
    (collect (cons (cons lb lb-closed)
                   (cons ub ub-closed)))))

(defgeneric %domain-spec (dom dom-type))

(defmethod %domain-spec (dom (dom-type odo:pure-integer-domain))
  (list 'integer :ranges (integer-ranges dom)))

(defmethod %domain-spec (dom (dom-type odo:pure-real-domain))
  (list 'real :ranges (real-ranges dom)))

(defmethod %domain-spec (dom (dom-type odo:pure-boolean-domain))
  (list 'boolean
        :true (odo:in-domain-p dom t)
        :false (odo:in-domain-p dom nil)))

(defmethod %domain-spec (dom (dom-type odo:pure-discrete-domain))
  (multiple-value-bind (members newly-consed) (odo:value-list dom)
    (list 'member
          :members (if newly-consed
                       members
                       (copy-list members))
          :test (odo:domain-test dom))))

(defun domain-spec (domain)
  (%domain-spec domain (odo:domain-type domain)))

(defmethod odo:copy-by-type (domain (type odo:domain) &key context)
  (declare (ignore context))
  (destructuring-bind (domain-type &rest domain-args)
      (domain-spec domain)
    (apply #'odo:make-domain domain-type
           :annotations (copy-list (odo:annotation-plist domain))
           domain-args)))

(defun odo:domain-equal (dom-1 dom-2)
  "Returns T if `DOM-1` and `DOM-2` have the same domain."
  (and
   (odo:domain-p dom-1)
   (odo:domain-p dom-2)
   (or
    (eql dom-1 dom-2)
    (and
     (eql (odo:domain-type dom-1)
          (odo:domain-type dom-2))
     (utils:plist-equal (odo:annotation-plist dom-1)
                        (odo:annotation-plist dom-2))
     (typecase (odo:domain-type dom-1)
       (odo:pure-boolean-domain
        (and
         (eql (odo:in-domain-p dom-1 t)
              (odo:in-domain-p dom-2 t))
         (eql (odo:in-domain-p dom-1 nil)
              (odo:in-domain-p dom-2 nil))))
       (odo:pure-real-domain
        (iter
          (with dom-1-it = (odo:range-iterator dom-1))
          (with dom-2-it = (odo:range-iterator dom-2))
          (initially (when (or (odo:it-done dom-1-it)
                               (odo:it-done dom-2-it))
                       (return (= (odo:it-done dom-1-it)
                                  (odo:it-done dom-2-it)))))
          (for (values lb-1 ub-1 lb-closed-1 ub-closed-1) = (odo:it-next dom-1-it))
          (for (values lb-2 ub-2 lb-closed-2 ub-closed-2) = (odo:it-next dom-2-it))
          (always (eql (odo:it-done dom-1-it)
                       (odo:it-done dom-2-it)))
          (until (odo:it-done dom-1-it))
          (always (eql lb-1 lb-2))
          (always (eql ub-1 ub-2))
          (always (eql lb-closed-1 lb-closed-2))
          (always (eql ub-closed-1 ub-closed-2))))
       (odo:pure-discrete-domain
        (and
         (eql (odo:domain-test dom-1)
              (odo:domain-test dom-2))
         (alex:set-equal (odo:value-list dom-1)
                         (odo:value-list dom-2)
                         :test (odo:domain-test dom-1)))))))))
