(in-package #:odo/model-api)

(defsection @odo/model-api/conditional
    (:title "Conditional API")
  "Many Odo objects can be *conditional*. That is, their inclusion in the
problem specification is conditional on some guard being true. Any conditional
object must respond to this API."
  (odo:odo-conditional-p generic-function)
  (odo:activation-status generic-function))

(defgeneric odo:conditional-p (x)
  (:documentation
   "Returns T iff `X` is conditional.")
  (:method ((x t))
    (declare (notinline odo:episode-conditional-p odo:event-conditional-p))
    (cond
      ((odo:episode-p x)
       (odo:episode-conditional-p x))
      ((odo:event-p x)
       (odo:event-conditional-p x))
      (t
       nil))))

(utils:define-renamed-generic (odo:odo-conditional-p odo:conditional-p) (x))

(defgeneric odo:activation-status (x)
  (:documentation
   "Given a conditional Odo object, return the object's activation status. The
status is one of the following values:

+ `:ACTIVE`
+ `:INACTIVE`
+ `:UNKNOWN`"))
