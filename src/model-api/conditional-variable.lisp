(in-package #:odo/model-api)

(defsection @odo/model-api/conditional-variable (:title "Conditional Variables")
  "Conditional variables are just like variables, but they are only considered
to be present in the problem if specific conditions are met. This activation
condition is represented as a `VARIABLE-ACTIVE-PREDICATE`. Iff the predicate is
true, the variable is active.

The variable's activation status can be active, inactive, or unknown.

In addition to the standard variable API, conditional variables respond to:

+ `ODO-CONDITIONAL-VARIABLE-P`
+ `VARIABLE-ACTIVATION-STATUS`

Additionally, if the variable is inactive, `ASSIGNED-P` will return T and
`ASSIGNED-VALUE` will return the value of the constant
`+VARIABLE-INACTIVE-VALUE+`.

#### Constants ####"
  (odo:+variable-inactive-value+ constant)
  "#### Types"
  (odo:conditional-variable class)
  (odo:odo-conditional-variable class)
  (odo:controllable-conditional-variable class)
  (odo:odo-controllable-conditional-variable class)
  (odo:uncontrollable-conditional-variable class)
  (odo:odo-uncontrollable-conditional-variable class)
  "#### Functions"
  (odo:conditional-variable-p generic-function)
  (odo:odo-conditional-variable-p generic-function)
  (odo:variable-activation-status generic-function)
  (odo:print-odo-conditional-variable function)
  "#### Constructors"
  (odo:make-conditional-variable function)
  (odo:make-conditional-integer-variable function)
  (odo:make-conditional-real-variable function)
  (odo:make-conditional-boolean-variable function)
  (odo:make-conditional-enumerated-variable function))


;;; Constants

(defconstant odo:+variable-inactive-value+ 'inactive
  "Value representing that the variable is inactive when queried by `VARIABLE-VALUE`.")


;;; Types

(defclass odo:conditional-variable (odo:variable)
  ()
  (:documentation
   "Represents a conditional variable. Must respond to:

+ `ODO-CONDITIONAL-VARIABLE-P` with T
+ `VARIABLE-ACTIVATION-STATUS`"))

(utils:define-renamed-class odo:odo-conditional-variable odo:conditional-variable)

(defclass odo:controllable-conditional-variable (odo:conditional-variable
                                                 odo:controllable-variable)
  ()
  (:documentation
   "A controllable, conditional variable."))

(utils:define-renamed-class odo:odo-controllable-conditional-variable odo:controllable-conditional-variable)

(defclass odo:uncontrollable-conditional-variable (odo:conditional-variable
                                                   odo:uncontrollable-variable)
  ()
  (:documentation
   "An uncontrollable, conditional variable."))

(utils:define-renamed-class odo:odo-uncontrollable-conditional-variable odo:uncontrollable-conditional-variable)


;;; Functions

(defgeneric odo:conditional-variable-p (obj)
  (:documentation "Returns T iff `OBJ` is an Odo conditional
  variable.")
  (:method ((obj t))
    nil))

(utils:define-renamed-generic (odo:odo-conditional-variable-p odo:conditional-variable-p) (obj))

(defgeneric odo:variable-activation-status (variable)
  (:documentation "Given an Odo conditional variable, return the variable's
activation status. The status is one of the following values:

+ `:ACTIVE`
+ `:INACTIVE`
+ `:UNKNOWN`")
  (:method ((obj t))
    (when (and (odo:variable-p obj)
               (not (odo:conditional-variable-p obj)))
      :active)))


;;; Constructors

(defun odo:make-conditional-integer-variable (&key name uncontrollable-p annotations
                                                (activation-status :unknown)
                                                (min odo:+-infinity+) (max odo:+infinity+)
                                                (ranges (list (cons min max)))
                                                (values nil))
  "Instantiate and return a new conditional variable with an integer domain.

See `MAKE-INTEGER-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-integer-domain
                 :ranges (utils:canonicalize-integer-ranges ranges)
                 :values values)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-conditional-variable
                           'odo:controllable-conditional-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'cvar)))
                       :domain domain
                       :activation-status activation-status
                       :annotations annotations)))

(defun odo:make-conditional-real-variable (&key name uncontrollable-p annotations
                                             (activation-status :unknown)
                                             (min odo:+-infinity+) (max odo:+infinity+)
                                             (ranges (list (cons min max))))
  "Instantiate and return a new conditional variable with a real domain.

See `MAKE-REAL-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-real-domain
                 :ranges (utils:canonicalize-ranges ranges :number-type 'real))))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-conditional-variable
                           'odo:controllable-conditional-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :activation-status activation-status
                       :annotations annotations)))

(defun odo:make-conditional-boolean-variable (&key name uncontrollable-p annotations
                                                (activation-status :unknown)
                                                (value :unknown)
                                                (true (ecase value
                                                        ((:unknown t)
                                                         t)
                                                        ((nil)
                                                         nil)))
                                                (false (ecase value
                                                         ((:unknown nil)
                                                          t)
                                                         ((t)
                                                          nil))))
  "Instantiate and return a new conditional variable with an boolean domain.

See `MAKE-BOOLEAN-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-boolean-domain
                 :value value
                 :true true
                 :false false)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-conditional-variable
                           'odo:controllable-conditional-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :activation-status activation-status
                       :annotations annotations)))

(defun odo:make-conditional-enumerated-variable (&key name uncontrollable-p annotations
                                                   (activation-status :unknown)
                                                   (test #'equal) members)
  "Instantiate and return a new conditional variable with an enumerated domain.

See `MAKE-ENUMERATED-DOMAIN` for a description of the arguments."
  (let ((domain (odo:make-enumerated-domain
                 :members members
                 :test test)))
    (odo:make-instance (if uncontrollable-p
                           'odo:uncontrollable-conditional-variable
                           'odo:controllable-conditional-variable)
                       :name (or name (gensym (uiop:standard-case-symbol-name 'var)))
                       :domain domain
                       :activation-status activation-status
                       :annotations annotations)))

(defun odo:make-conditional-variable (domain-or-spec
                                      &rest
                                        args
                                      &key
                                        name
                                        uncontrollable-p
                                        annotations
                                      &allow-other-keys)
  "Instantiate and return a new conditional variable.

If `DOMAIN-OR-SPEC` is an `ODO-DOMAIN`, then it is used as a template
to instantiate this variable. Note that implementations are *not*
allowed to just reference `DOMAIN-OR-SPEC`; it must be copied.

If `DOMAIN-OR-SPEC` is not an `ODO-DOMAIN`, then it and `ARGS` are
used to describe the domain as in `MAKE-DOMAIN`.

If `NAME` is not supplied, a name is autogenerated."
  (declare (ignore name annotations uncontrollable-p))
  (destructuring-bind (domain-type &rest domain-args)
      (if (odo:domain-p domain-or-spec)
          (list* (odo:domain-type domain-or-spec) (rest (domain-spec domain-or-spec)))
          (canonicalize-domain-description domain-or-spec))
    (let ((maker
           (etypecase domain-type
             (odo:pure-integer-domain
              #'odo:make-conditional-integer-variable)
             (odo:pure-real-domain
              #'odo:make-conditional-real-variable)
             (odo:pure-boolean-domain
              #'odo:make-conditional-boolean-variable)
             (odo:pure-discrete-domain
              #'odo:make-conditional-enumerated-variable))))
      (apply maker
             (append domain-args args)))))



(defmethod odo:copy-by-type (self (type odo:conditional-variable) &key context)
  (or (funcall (utils:odo-copy-context-var-map-fun context) self)
      (destructuring-bind (domain-type &rest args)
          (domain-spec self)
        (apply #'odo:make-conditional-variable domain-type
               :name (odo:name self)
               :annotations (copy-list (odo:annotation-plist self))
               :activation-status (odo:variable-activation-status self)
               :uncontrollable-p (typep type 'odo:uncontrollable-conditional-variable)
               args))))

(defmethod odo:equal-p-by-type (left right
                                (left-type odo:conditional-variable)
                                (right-type odo:conditional-variable))
  (and
   (eql (odo:variable-activation-status left)
        (odo:variable-activation-status right))
   (call-next-method)))

(defun odo:print-odo-conditional-variable (var stream)
  (utils:with-delimiter-printing (var stream)
    (write (odo:name var) :stream stream)
    (write-char #\space stream)
    (write-char #\( stream)
    (write-string
      (etypecase (odo:type var)
       (odo:controllable-conditional-variable
        (uiop:standard-case-symbol-name "controllable"))
       (odo:uncontrollable-conditional-variable
        (uiop:standard-case-symbol-name "uncontrollable")))
      stream)
    (write-char #\) stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (odo:print-odo-domain var stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (prin1 :conditional stream)
    (write-char #\space stream)
    (prin1 t stream)
    (write-char #\space stream)
    (pprint-newline :fill stream)
    (prin1 :activation-status stream)
    (write-char #\space stream)
    (prin1 (odo:variable-activation-status var) stream)))
