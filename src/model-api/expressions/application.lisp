(in-package #:odo/model-api)

(defsection @odo/model-api/expressions/application
    (:title "Application Expressions")
  "Application expressions represent function calls and are described by an
operator and a list of arguments.

When constructing an application expression, it is allowed for simplification
to happen, including complete evaluation (e.g., `(odo-+ 1 2)` is allowed to
return 3) or folding (e.g., `(odo-+ x 1 2)` is allowed to return the same
result as `(odo-+ x 3)`)."

  (odo:application-expression class)
  (odo:odo-application-expression class)
  (odo:lookup-expression class)
  (odo:odo-lookup-expression class)
  (odo:application-expression-p generic-function)
  (odo:odo-application-expression-p generic-function)
  (odo:operator generic-function)
  (odo:application-operator generic-function)
  (odo:arguments generic-function)
  (odo:application-arguments generic-function)
  (odo:argument-value generic-function)
  (odo:application-argument-value generic-function)
  (odo:argument-value-by-type generic-function)
  (odo:application-argument-value-by-type generic-function)
  (odo:lookup function)
  (odo:lookup-expression-name generic-function)
  (odo:funcall function)
  (odo:odo-funcall generic-function)
  (odo:@ function))

(defclass odo:application-expression (odo:expression)
  ())

(utils:define-renamed-class odo:odo-application-expression odo:application-expression)

(defclass odo:lookup-expression (odo:application-expression)
  ())

(utils:define-renamed-class odo:odo-lookup-expression odo:lookup-expression)

(defgeneric odo:application-expression-p (expr)
  (:documentation
   "Returns T if EXPR is an Odo application expression.")
  (:method ((expr t))
    nil))

(utils:define-renamed-generic (odo:odo-application-expression-p odo:application-expression-p)
                              (expr))

(defgeneric odo:operator (application-expression)
  (:documentation
   "Returns the operator that is applied to the arguments. One of:

+ Symbol :: Names a global function.

+ State variable :: Treats the state variable as a function of one argument.

+ Function

+ Odo lambda expression

+ An Odo application that evaluates to one of the above."))

(progn
 (utils:with-deprecation (:style-warning "0.5" :delete "0.6")
   (defgeneric odo:application-operator
       (application-expression)
     (:documentation "Deprecated. Use `OPERATOR` instead. NOTE that `OPERATOR`
may return different results. For instance, if `APPLICATION-OPERATOR` returns
`ODO:ODO-OR`, `OPERATOR` will return `ODO:OR`."))
   (defmethod odo:application-operator :before ((application-expression t))
     (declare (ignore))))
 (defmethod odo:application-operator (application-expression)
   (let ((true-result (odo:operator application-expression)))
     (or (and ))
     (if (symbolp true-result)
         (or (alex:when-let* ((fun (symbol-odo-function true-result nil))
                              (deprecated-names (odo-function-deprecated-names fun)))
               (first deprecated-names))
             true-result)
         true-result)))
 (progn
  (defvar *circle-detect-operator* nil)
  (defmethod odo:operator (application-expression)
    (declare (notinline odo:application-operator))
    (when *circle-detect-operator* (error "OPERATOR not implemented"))
    (let ((*circle-detect-operator* t))
      (odo:application-operator application-expression)))))

(defgeneric odo:arguments (application-expression)
  (:documentation
   "A list of arguments the operator is applied to."))

(utils:define-renamed-generic (odo:application-arguments odo:arguments :redirect t) (application-expression))

(defgeneric odo:lookup-expression-name (lookup)
  (:method (self)
    (first (odo:arguments self))))

(defgeneric odo:argument-value-by-type (application type argument-name))

(utils:define-renamed-generic (odo:application-argument-value-by-type odo:argument-value-by-type :redirect t)
                              (application type argument-name))

(defgeneric odo:argument-value (application argument-name)
  (:documentation
   "Returns the value of argument `ARGUMENT-NAME` in `APPLICATION`. Like
`SLOT-VALUE` for application expressions.")
  (:method (application argument-name)
    (odo:argument-value-by-type application (odo:operator application) argument-name)))

(utils:define-renamed-generic (odo:application-argument-value odo:argument-value)
                              (application argument-name))

(defun odo:@ (operator &rest args)
  "Synonym for ODO-FUNCALL."
  (odo:make-instance 'odo:application-expression
                     :operator operator
                     :arguments args))

(defun odo:funcall (operator &rest args)
  "Create an expression representing the application of OPERATOR to ARGS."
  (odo:make-instance 'odo:application-expression
                     :operator operator
                     :arguments args))

(utils:define-renamed-generic (odo:odo-funcall odo:funcall) (operator &rest args))

(defun odo:lookup (name)
  (odo:make-instance 'odo:lookup-expression
                     :operator 'odo:lookup
                     :arguments (list name)))

(defmethod odo:expression-to-sexp-by-type (self (type odo:application-expression))
  (list* (odo:operator self) (mapcar #'odo:expression-to-sexp (odo:arguments self))))

(defmethod odo:derivative-by-type (self (type odo:application-expression) &rest args)
  (let* ((operator (symbol-odo-function (odo:operator self)))
         (def operator)
         (arguments (odo:arguments self))
         (derivative-function (odo-function-derivative-function def)))
    (apply derivative-function (lambda (x)
                                 (apply #'odo:derivative x args))
           arguments)))

(defmethod odo:eval-expression-by-type (self (type odo:application-expression) &rest args)
  (let* ((operator (symbol-odo-function (odo:operator self)))
         (def operator)
         (arguments (odo:arguments self))
         (eval-fun (odo-function-body-function-name def)))
    (apply eval-fun (mapcar (alex:rcurry (alex:curry #'apply #'odo:eval-expression) args) arguments))))

(defmethod odo:derivative-by-type (self (type odo:lookup-expression)
                                   &key odo-variable-name)
  (cond
    (odo-variable-name
     (if (equal odo-variable-name
                (odo:lookup-expression-name self))
         1
         0))
    (t
     (error "can't handle this yet in lookup derivative"))))

(defmethod odo:scope-by-type (self (type odo:application-expression) &key state-space)
  (remove-duplicates (append
                      (when (odo:application-expression-p (odo:operator self))
                        (mapcan (alex:rcurry #'odo:scope :state-space state-space) (odo:operator self)))
                      (when (odo:state-variable-p (odo:operator self))
                        (list (odo:operator self)))
                      (mapcan (alex:rcurry #'odo:scope :state-space state-space) (odo:arguments self)))))

(defmethod odo:scope-by-type (self (type odo:lookup-expression) &key state-space)
  (odo:state-space-get state-space (odo:lookup-expression-name self)))

(defmethod odo:eval-expression-by-type (self (type odo:lookup-expression) &key state-space)
  (odo:eval-expression (odo:state-space-get state-space (odo:lookup-expression-name self))))

(defmethod odo:bounds-by-type (self (type odo:lookup-expression) &key state-space)
  (odo:bounds (odo:state-space-get state-space (odo:lookup-expression-name self))))

(defmethod odo:copy-by-type (self (type odo:application-expression) &rest args)
  (odo:make-instance 'odo:application-expression
                     :operator (apply #'odo:copy (odo:operator self) args)
                     :arguments (mapcar (lambda (x)
                                          (apply #'odo:copy x args))
                                        (odo:arguments self))))

(defmethod odo:copy-by-type (self (type odo:lookup-expression) &rest args)
  (odo:make-instance 'odo:lookup-expression
                     :operator (apply #'odo:copy
                                      (odo:operator self)
                                      args)
                     :arguments (mapcar (lambda (x)
                                          (apply #'odo:copy x args))
                                        (odo:arguments self))))

(defmethod odo:equal-p-by-type (left right (left-type odo:application-expression)
                                (right-type odo:application-expression))
  (and
   (eql (odo:operator left) (odo:operator right))
   (alex:length= (odo:arguments left) (odo:arguments right))
   (every #'odo:equal-p (odo:arguments left) (odo:arguments right))))

(defmethod odo:equal-p-by-type (left right (left-type odo:lookup-expression) (right-type odo:lookup-expression))
  (equal (odo:lookup-expression-name left) (odo:lookup-expression-name right)))

(defmethod utils:print-odo-object-by-type (obj (type odo:application-expression) stream)
  (utils:odo-print-unreadable-object (obj stream)
    (write 'odo:application-expression :stream stream)
    (write-char #\Space stream)
    (pprint-newline :fill stream)
    (let ((output (list* (odo:operator obj) (odo:arguments obj))))
      (write output :stream stream))))

(defmethod utils:print-odo-object-by-type (obj (type odo:lookup-expression) stream)
  (utils:odo-print-unreadable-object (obj stream)
    (write 'odo:lookup-expression :stream stream)
    (write-char #\Space stream)
    (pprint-newline :fill stream)
    (write (odo:lookup-expression-name obj) :stream stream)))
