(in-package #:odo/model-api)

(defsection @odo/model-api/expressions/defs
    (:title "Expression Definitions")
  (odo:expression class)
  (odo:odo-expression class)
  (odo:expression-p generic-function)
  (odo:odo-expression-p generic-function)
  (odo:eval-expression generic-function)
  (odo:eval-expression-by-type generic-function)
  (odo:expression-bounds generic-function)
  (odo:bounds-by-type generic-function)
  (odo:expression-bounds-by-type generic-function)
  (odo:derivative generic-function)
  (odo:derivative-by-type generic-function)
  (odo:expression-derivative generic-function)
  (odo:expression-derivative-by-type generic-function)
  (odo:scope generic-function)
  (odo:scope-by-type generic-function)
  (odo:expression-scope generic-function)
  (odo:expression-scope-by-type generic-function)
  (odo:expression-to-sexp generic-function)
  (odo:expression-to-sexp-by-type generic-function))

(defclass odo:expression (odo:object)
  ()
  (:documentation
   "The root odo-type for expressions."))

(utils:define-renamed-class odo:odo-expression odo:expression)

(defgeneric odo:expression-p (obj)
  (:documentation
   "Returns T iff the object is an odo expression.")
  (:method (obj)
    (or (odo:variable-p obj)
        (odo:conditional-variable-p obj))))

(utils:define-renamed-generic (odo:odo-expression-p odo:expression-p) (obj))

(defgeneric odo:derivative-by-type (expression type &key odo-variable-name)
  (:documentation
   "Compute an expression denoting the derivative of `EXPRESSION` with respect
to the variable named by `ODO-VARIABLE-NAME`.")
  (:method ((self t) (type (eql nil)) &key &allow-other-keys)
    0)
  (:method (self (type odo:variable) &key odo-variable-name &allow-other-keys)
    (assert odo-variable-name)
    (if (equal odo-variable-name (odo:name self))
        1
        0)))

(utils:define-renamed-generic (odo:expression-derivative-by-type odo:derivative-by-type :redirect t)
                              (expression type &key odo-variable-name))

(defgeneric odo:derivative (expression &key odo-variable-name)
  (:documentation
   "Compute an expression denoting the derivative of `EXPRESSION` with respect
to the variable named by `ODO-VARIABLE-NAME`.")
  (:method (self &rest args)
    (apply #'odo:derivative-by-type self (odo:type self) args)))

(utils:define-renamed-generic (odo:expression-derivative odo:derivative)
                              (expression &key odo-variable-name))

(defgeneric odo:bounds-by-type (expression type &key state-space)
  (:documentation
   "Compute the bounds of `EXPRESSION`. Returns three values: ((`min-val`,
`max-val`), `min-closed`, `max-closed`).")
  (:method (self (type (eql nil)) &key state-space)
    (declare (ignore state-space))
    (if (numberp self)
        (values (list self self) t t)
        (error "Unknown how to compute bounds!")))
  (:method (self (type odo:variable) &key state-space)
    (declare (ignore state-space))
    (if (or (odo:integer-domain-p self t)
            (odo:real-domain-p self t))
        (multiple-value-bind (min-val min-closed) (odo:min-val self)
          (multiple-value-bind (max-val max-closed) (odo:max-val self)
            (values (list min-val max-val) min-closed max-closed))))))

(utils:with-deprecation (:style-warning "0.5" :delete "0.6")
  (defgeneric odo:expression-bounds-by-type (expression type &key state-space)
    (:documentation "Deprecated. Use `BOUNDS-BY-TYPE` instead."))
  (defvar *circle-detect-expression-bounds-by-type* nil)
  (defmethod odo:expression-bounds-by-type :before ((expression t) (type t) &key state-space)
    (declare (ignore state-space))))
(defmethod odo:expression-bounds-by-type ((expression t) (type t) &key state-space)
  (let ((raw (multiple-value-list (odo:bounds-by-type expression type :state-space state-space))))
    (values (first (first raw)) (second (first raw)) (third raw) (fourth raw))))

(utils:define-renamed-generic (odo:expression-bounds odo:bounds) (expression &key state-space))

(defgeneric odo:scope-by-type (expression type &key state-space)
  (:documentation
   "Return all Odo variables and state variables in the scope of
`EXPRESSION`.")
  (:method (self (type (eql nil)) &key state-space)
    (cond
      ((listp self)
       (mapcan (alex:rcurry #'odo:scope :state-space state-space) self))
      ((vectorp self)
       (loop
         :for el :across self
         :nconcing (odo:scope el :state-space state-space)))
      (t nil)))
  (:method (self (type odo:variable) &key &allow-other-keys)
    (list self))
  (:method (self (type odo:state-variable) &key &allow-other-keys)
    (list self)))

(utils:define-renamed-generic (odo:expression-scope-by-type odo:scope-by-type :redirect t)
                              (expression type &key state-space))

(defgeneric odo:scope (expression &key state-space)
  (:documentation
   "Return all Odo variables and state variables in the scope of
`EXPRESSION`.")
  (:method (expression &key state-space)
    (odo:scope-by-type expression (odo:type expression) :state-space state-space)))

(utils:define-renamed-generic (odo:expression-scope odo:scope) (expression &key state-space))

(defgeneric odo:expression-to-sexp-by-type (obj type)
  (:documentation
   "Convert an expression into a sexp.")
  (:method (self (type (eql nil)))
    self)
  (:method (self (type odo:variable))
    self))

(defgeneric odo:expression-to-sexp (obj)
  (:documentation
   "Convert an expression into a sexp.")
  (:method (obj)
    (odo:expression-to-sexp-by-type obj (odo:type obj))))

(defgeneric odo:eval-expression-by-type (expression type &key state-space)
  (:documentation
   "Evaluate an expression.")
  (:method (self (type (eql nil)) &key &allow-other-keys)
    self)
  (:method ((self list) (type (eql nil)) &rest args &key &allow-other-keys)
    (mapcar (lambda (item)
              (apply #'odo:eval-expression item args))
            self))
  (:method ((self vector) (type (eql nil)) &rest args &key &allow-other-keys)
    (map 'vector (lambda (item)
                   (apply #'odo:eval-expression item args))
         self))
  (:method (self (type odo:constraint) &key &allow-other-keys)
    self)
  (:method (self (type odo:episode) &key &allow-other-keys)
    self)
  (:method (self (type odo:variable) &key &allow-other-keys)
    (odo:assigned-value self)))

(defgeneric odo:eval-expression (expression &key state-space)
  (:documentation
   "Evaluate an expression.")
  (:method (self &rest args)
    (apply #'odo:eval-expression-by-type self (odo:type self) args)))
