(in-package #:odo/model-api)

(defsection @odo/model-api/expressions/time
    (:title "Time")
  "This section describes the symbolic time functionality of Odo expressions."
  (odo:+current-time+ constant)
  (odo:+previous-time+ constant)

  (odo:*current-time* variable)
  (odo:*previous-time* variable))

(defvar odo:*current-time*)

(setf (documentation 'odo:*current-time* 'variable)
      "Should be dynamically bound to the current time value when evaluating
      expressions with current time as a free variable. See `+current-time+`.")

(defconstant odo:+current-time+ '%current-time%
  "The symbol that represents the current time in expressions. Used primarily
by state and transition constraints as those require expressions that take time
as a free variable.

When evaluated, it evaluates to the current value of `*current-time*`.")

(defvar odo:*previous-time*)

(setf (documentation 'odo:*previous-time* 'variable)
      "Should be dynamically bound to the previous time value when evaluating
      expressions with previous time as a free variable. See
      `+previous-time+`.")

(defconstant odo:+previous-time+ '%previous-time%
  "The symbol that represents the previous time in expressions. Used primarily
by transition constraints as those require expressions that take time
as a free variable.

When evaluated, it evaluates to the current value of `*previous-time*`.")

(defmethod eval-expression ((self (eql odo:+current-time+)) &key &allow-other-keys)
  odo:*current-time*)

(defmethod eval-expression ((self (eql odo:+previous-time+)) &key &allow-other-keys)
  odo:*previous-time*)
