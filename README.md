# Odo #
[![build status](https://git.mers.csail.mit.edu/mars-toolkit/odo/badges/master/build.svg)](https://git.mers.csail.mit.edu/mars-toolkit/odo/commits/master) [![coverage report](https://git.mers.csail.mit.edu/mars-toolkit/odo/badges/master/coverage.svg)](https://git.mers.csail.mit.edu/mars-toolkit/odo/commits/master)

## Owners ##
* Eric Timmons (etimmons@mit.edu)

## Description ##

This project exists to provide a unified modelling front-end for MERS solvers.

It consists of two top-level systems: `odo` and `odo+mcclim`. `odo` is the
primary system and is likely the one you are looking for.

`odo+mcclim` provides integration
with [McCLIM](https://github.com/McCLIM/McCLIM) for use in CL-based GUIs. Note
that McCLIM is licensed under the LGPLv2. This means that any binary you
distribute with McCLIM (and by extension `odo+mcclim`) is subject to additional
requirements. Rule of thumb: don't do it unless you really know what you're
doing.

## External Dependencies ##

- [Minizinc](https://www.minizinc.org/). See also [scripts/minizinc-docker.org](./scripts/minizinc-docker.org) for a script to run Minizinc in a [Docker container](https://hub.docker.com/r/minizinc/minizinc). (Optional: this is a soft dependency and is only required if you want to compile Minizinc to Odo)
- [jsonschema](https://json-schema.org/) (Optional)

## Documentation ##

The documentation is located at
https://mars-toolkit.mers-pages.csail.mit.edu/odo/

To generate a local copy, run:

```common-lisp
(asdf:load-system "odo/doc")
(odo/doc:generate-docs)
```

## Code structure ##

The Odo package is defined in `src/package.lisp`. Any new exported symbols
should be added to that package defining form. There are several other packages
defined that are then used to implement functionality. Every file should be
`in-package` one of these implementation packages. NO file should include an
`(in-package :odo)` form! This ensures that the only symbols present in the Odo
package are the exported symbols.

## Versioning ##

The odo project uses v2 of
the [semantic versioning](http://semver.org/spec/v2.0.0.html) scheme with the
exception that pre-release versions are specified after another dot instead of
a hyphen. So 0.3.0.1 would indicate a pre-release version of 0.3.1 or
0.4.0. This is due to limitations in ASDF's handling of version numbers.
