(in-package #:odo/test)

(defun test-csp (csp)
  (is (odo:csp-p csp))
  (let ((all-diff-c (odo:make-constraint (odo:all-diff (odo:make-variable-vector 2 'integer :name 'x))
                                         :name 'c))
        (equality-c (odo:make-constraint (odo:cl-equalp (odo:make-variable 'integer :name 'y) 5)
                                         :name 'b)))
    (signals error (odo:get-constraint csp 'c))
    (is (eql 'unknown (odo:get-constraint csp 'c nil 'unknown)))
    (signals error (odo:state-space-get csp '(x 0)))
    (odo:add-constraint! csp all-diff-c)
    (is (eql all-diff-c (odo:get-constraint csp 'c)))
    (finishes (odo:state-space-get csp '(x 0)))

    (signals error (odo:get-constraint csp 'b))
    (is (eql 'unknown (odo:get-constraint csp 'b nil 'unknown)))
    (signals error (odo:state-space-get csp 'y))
    (odo:add-constraint! csp equality-c)
    (is (eql equality-c (odo:get-constraint csp 'b)))
    (finishes (odo:state-space-get csp 'y))

    (odo:remove-constraint! csp equality-c)
    (signals error (odo:get-constraint csp 'b))
    (is (eql 'unknown (odo:get-constraint csp 'b nil 'unknown)))
    (odo:add-constraint! csp equality-c)
    (finishes (odo:get-constraint csp 'b))

    (is (eql :satisfy (odo:objective-goal csp)))
    (is (null (odo:objective-function csp)))
    (setf (odo:objective-goal csp) :maximize)
    (is (eql :maximize (odo:objective-goal csp)))
    (setf (odo:objective-function csp) (odo:state-space-get csp '(x 0)))
    (is (eql (odo:state-space-get csp '(x 0))
             (odo:objective-function csp)))
    (let ((copied-csp (odo:copy csp))
          (objective-lambda (lambda (csp)
                              (odo:assigned-value (odo:state-space-get csp '(x 0))))))
      (is (odo:csp-equal copied-csp csp))
      (is (not (eql copied-csp csp)))
      ;; Test setting the objective function to a function in the copy so we
      ;; don't try to dump the function to a file.
      (finishes
        (setf (odo:objective-function copied-csp)
              objective-lambda)
        ;; Try copying the CSP again, to make sure the function gets copied over
        ;; correctly.
        (is (eql objective-lambda (odo:objective-function (odo:copy copied-csp))))
        ;;
        (let ((*print-readably* nil))
          (print copied-csp (make-broadcast-stream)))))

    ;; Try making the objective function an expression.
    (let ((copied-csp (odo:copy csp))
          (objective-expression (odo:* 5 (odo:lookup '(x 0)))))
      (is (odo:csp-equal copied-csp csp))
      (is (not (eql copied-csp csp)))
      ;; Test setting the objective function to a function in the copy so we
      ;; don't try to dump the function to a file.
      (finishes
        (setf (odo:objective-function copied-csp)
              objective-expression)
        ;; Try copying the CSP again, to make sure the function gets copied over
        ;; correctly.
        (is (equal (odo:expression-to-sexp objective-expression)
                   (odo:expression-to-sexp (odo:objective-function (odo:copy copied-csp)))))
        ;;
        (let ((*print-readably* nil))
          (print copied-csp (make-broadcast-stream)))))))

(defun test-attribute-objective (csp)
  (let ((x (odo:make-variable 'integer :min 3 :max 3
                                       :name 'x))
        (y (odo:make-variable 'integer :min 1 :max 1
                                       :name 'y)))
    (odo:state-space-add! csp x)
    (odo:state-space-add! csp y)
    (setf (odo:objective-function csp)
          (odo:make-attribute-objective-function :attribute-values-alist `((,x . ((1 . 1) (2 . 2) (3 . 3)))
                                                                           (,y . ((0 . 0) (1 . 10))))))
    (setf (odo:objective-goal csp) :maximize)

    (is (= 13 (odo:evaluate-objective (odo:objective-function csp) csp)))))

(defun test-csp-implementation (fun)
  (let ((csp (funcall fun)))
    (test-state-space csp)
    (setf csp (funcall fun))
    (test-csp csp))
  (let ((csp (funcall fun)))
    (test-attribute-objective csp))
  (let ((csp (funcall fun))
        (x (odo:make-variable 'member :members '("aba" "ccc")
                                      :name :x)))
    (odo:state-space-add! csp x)
    (odo:add-constraint! csp (odo:make-constraint (odo:cl-equalp x "aba")
                                                  :name :c1))))
