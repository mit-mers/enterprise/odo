(in-package #:odo/test)

(defparameter *expressions-test-cases*
  `((,(odo:make-variable 'integer :name :x)
     :d/dx ((:x 1)
            (:y 0)))
    ((odo:+ 2 3)
     :value 5)
    ((odo:+ 3 ,(odo:make-variable '(integer 1 1)))
     :value 4)
    ((odo:+ 3 (odo:lookup :x))
     :state-space ((:x (integer 1 1)))
     :value 4
     :d/dx ((:x (odo:+ 0 1))))

    ((odo:- (odo:lookup :x))
     :state-space ((:x (integer 4 4)))
     :value -4
     :d/dx ((:x (odo:- 1))))
    ((odo:- 5 3)
     :value 2)
    ((odo:- (odo:lookup :x) (odo:lookup :y))
     :state-space ((:x (integer 15 15))
                   (:y (integer 10 10)))
     :value 5
     :d/dx ((:x (odo:- 1 0))
            (:y (odo:- 0 1))))

    ((odo:* 4 (odo:lookup :x))
     :state-space ((:x (integer -1 -1)))
     :value -4
     :d/dx ((:x (odo:+ (odo:* 0 (odo:lookup :x))
                           (odo:* 1 4)))))
    ((odo:* 4 (odo:lookup :x) (odo:lookup :x))
     :state-space ((:x (integer 10 10)))
     :value 400
     :d/dx ((:x (odo:+ (odo:* 0 (odo:lookup :x) (odo:lookup :x))
                           (odo:* 1 4 (odo:lookup :x))
                           (odo:* 1 4 (odo:lookup :x))))))
    ((odo:* 4 (odo:lookup :x) (odo:lookup :y))
     :state-space ((:x (integer 10 10))
                   (:y (integer 15 15)))
     :value 600
     :d/dx ((:x (odo:+ (odo:* 0 (odo:lookup :x) (odo:lookup :y))
                       (odo:* 1 4 (odo:lookup :y))
                       (odo:* 0 4 (odo:lookup :x))))
            (:y (odo:+ (odo:* 0 (odo:lookup :x) (odo:lookup :y))
                       (odo:* 0 4 (odo:lookup :y))
                       (odo:* 1 4 (odo:lookup :x))))))
    ((odo:* 5.6d0)
     :value 5.6d0)

    ((odo:/ (odo:lookup :x))
     :state-space ((:x (integer 2 2)))
     :value 1/2
     :d/dx ((:x (odo:- (odo:/ 1 (odo:* (odo:lookup :x) (odo:lookup :x)))))))
    ((odo:/ 2 (odo:lookup :x))
     :state-space ((:x (integer 2 2)))
     :value 1
     :d/dx ((:x (odo:- (odo:/ 0 (odo:lookup :x))
                       (odo:/ (odo:* 2 1) (odo:lookup :x) (odo:lookup :x))))))))

(defun evaluate-expr-test-case (expr &key sexp value d/dx state-space)
  (let ((state-space-desc state-space)
        (state-space (odo:make-state-space)))
    (is (equal sexp (odo:expression-to-sexp expr)))
    (when state-space-desc
      (dolist (spec state-space-desc)
        (destructuring-bind (name domain-spec) spec
          (odo:state-space-add! state-space (odo:make-variable domain-spec :name name)))))
    (when value
      (is (= value (odo:eval-expression expr :state-space state-space))))
    (when d/dx
      (dolist (d d/dx)
        (destructuring-bind (name expected)
            d
          (is (equal expected (odo:expression-to-sexp (odo:derivative expr :odo-variable-name name)))))))))

(defun run-expr-test-case (test-case)
  (let* ((sexp (car test-case))
         (results (cdr test-case))
         (expr (eval sexp)))
    (apply #'evaluate-expr-test-case expr :sexp sexp results)))

(defun test-expressions-implementation ()
  (dolist (test-case *expressions-test-cases*)
    (run-expr-test-case test-case)))
