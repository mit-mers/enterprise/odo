(in-package #:odo/test)


(defun setup-bdd-tests ()
  "Set up simple BDD tests. Create several BDD nodes, one of which
represents a simple constraint."
  (let* ((x (odo:make-boolean-variable :name 'x))
         (c1 (odo:equal x t))
         (n1 (odo:make-bdd :name 0
                           :expression 1
                           :child-low nil
                           :child-high nil))
         (n2 (odo:make-bdd :name 1
                           :expression 0
                           :child-low nil
                           :child-high nil))
         (n3 (odo:make-bdd :name 3
                           :expression c1
                           :child-low n1
                           :child-high n2)))

    (values x c1 n1 n2 n3)))


(defun test-bdd ()
  (multiple-value-bind (x c1 n1 n2 n3) (setup-bdd-tests)
    ;; Type Checking
    (is (odo:bdd-p n1))

    ;; Terminality
    (is (odo:terminal-p n1))
    (is (odo:terminal-p n2))
    (is (not (odo:terminal-p n3)))

    ;; Names
    (is (= (odo:name n1) 0))
    (is (= (odo:name n2) 1))

    ;; Scope
    (is (equal (odo:scope n1) (list)))
    (is (equal (odo:scope n3) (list x)))

    ;; Test BDD Expression
    (is (odo:equal-p (odo:expression n3)
                     (odo:equal (odo:make-boolean-variable :name 'x) t)))

    ;; Entailed?
    (is (odo:entailed-p n1))
    (is (not (odo:entailed-p n2)))
    (is (not (odo:entailed-p n3)))
    (is (odo:entailed-p (odo:make-bdd :expression c1
                                      :child-low n1
                                      :child-high n1)))
    ;; Satisifiable?
    (is (odo:satisfiable-p n1))
    (is (not (odo:satisfiable-p n2)))
    (is (odo:satisfiable-p n3))
    (is (not (odo:satisfiable-p (odo:make-bdd :expression c1
                                              :child-low n2
                                              :child-high n2))))

    ;; Test BDD Child Low
    (is (eql (odo:child-low n3) n1))

    ;; Test BDD Child High
    (is (eql (odo:child-high n3) n2))
    (is (not (odo:child-low n1)))

    ;; Equality
    (is (odo:equal-p n3
                     (odo:make-bdd :name 3
                                   :expression c1
                                   :child-low n1
                                   :child-high n2)))))
