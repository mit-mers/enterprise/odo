(in-package #:odo/test)

(defun test-pddl-domain ()
  (odo:make-pddl-domain
   'martian
   '(:typing :durative-actions :numeric-fluents :duration-inequalities
     :control-variables)
   :predicates (loop
                 :for name :in '(recharged
                                 rover-separated rover-connected
                                 rover-still rover-navigating
                                 vehicle-still
                                 solar-panel-deployed solar-panel-collected
                                 ship-arrived
                                 sample-taken-a sample-taken-b sample-taken-c
                                 sample-taken-d)
                 :collect (odo:make-pddl-predicate name))
   :functions (loop
                :for name :in '(x-vehicle y-vehicle
                                x-rover y-rover
                                b-vehicle
                                b-rover)
                :collect (odo:make-pddl-function name))
   :control-variables (loop
                        :for (name lb ub) :in '((vb-rover 4 8)
                                                (vb-vehicle 10 20)
                                                (vb-charge-split 5 5)
                                                (vb-charge-vehicle 10 10)
                                                (vx-vehicle -5 5)
                                                (vy-vehicle -5 5)
                                                (vx-rover -2 2)
                                                (vy-rover -2 2))
                        :collect (odo:make-pddl-control-variable name (list lb ub)))
   :actions
   (list
    (odo:make-pddl-durative-action
     'navigate-vehicle-with-rover
     :duration (odo:and
                (odo:ge (odo:lookup '?duration) 1d-1)
                (odo:le (odo:lookup '?duration) 200))
     :condition
     (odo:and (odo:pddl-at :start (odo:@ 'rover-still))
              (odo:pddl-at :start (odo:@ 'vehicle-still))
              (odo:pddl-over :all (odo:@ 'rover-connected))
              (odo:pddl-over :all (odo:@ 'solar-panel-collected))
              (odo:pddl-over :all (odo:ge (odo:@ 'b-vehicle) 0)))
     :effect
     (odo:and (odo:pddl-at :start (odo:not (odo:@ 'vehicle-still)))
              (odo:pddl-at :end (odo:@ 'vehicle-still))
              (odo:pddl-at :start (odo:not (odo:@ 'rover-still)))
              (odo:pddl-at :end (odo:@ 'rover-still))
              (odo:pddl-increase (odo:@ 'x-vehicle)
                                 (odo:* (odo:@ 'vx-vehicle) (odo:lookup odo:+current-time+)))
              (odo:pddl-increase (odo:@ 'y-vehicle)
                                 (odo:* (odo:@ 'vy-vehicle) (odo:lookup odo:+current-time+)))
              (odo:pddl-increase (odo:@ 'x-rover)
                                 (odo:* (odo:@ 'vx-vehicle) (odo:lookup odo:+current-time+)))
              (odo:pddl-increase (odo:@ 'y-rover)
                                 (odo:* (odo:@ 'vy-vehicle) (odo:lookup odo:+current-time+)))
              (odo:pddl-decrease (odo:@ 'b-vehicle)
                                 (odo:* 3 (odo:lookup odo:+current-time+))))))))

(defun serialize-pddl-to-file (pathname)
  (with-open-file (s pathname :direction :output
                              :if-exists :supersede)
    (let ((pddl-domain (test-pddl-domain)))
      (odo:serialize-to-stream pddl-domain s :format :pddl))))

;; (serialize-pddl-to-file (asdf:system-relative-pathname :odo "pddl-domain-tmp.pddl"))
