(in-package #:odo/test)

(defparameter *simple-enumerated-domain-tests*
  `(((:members (a b c d e))
     :test equal
     :values (a b c d e)
     :size 5
     :assigned nil
     :type-spec (member a b c d e)
     :domain-tests ((a . t)
                    (b . t)
                    (c . t)
                    (g . nil)))
    ((:members (#1="a" #2="b" #3="c"))
     :test equal
     :values ("a" "b" "c")
     :size 3
     :assigned nil
     :type-spec (member #1# #2# #3#)
     :domain-tests (("a" . t)
                    ("b" . t)
                    ("c" . t)
                    ("g" . nil)
                    (g . nil)))
    ((:members (f))
     :test equal
     :values (f)
     :size 1
     :assigned t
     :assigned-value f
     :type-spec (member f)
     :domain-tests ((f . t)
                    (10 . nil)))
    ,(let ((mem-1 (list 'g 'b))
           (mem-2 (list 'c 'd)))
       `((:members (,mem-1 ,mem-2 e))
         :test equal
         :values (,mem-1 ,mem-2 e)
         :size 3
         :assigned nil
         :type-spec (member ,mem-1 ,mem-2 e)
         :domain-tests ((a . nil)
                        (b . nil)
                        ((c d) . t)
                        (g . nil))))))

(defparameter *enumerated-domain-tests*
  `(,@*simple-enumerated-domain-tests*
    ,(let ((mem-1 (list 'a 'b))
           (mem-2 (list 'a 'b)))
       `((:members (,mem-1 ,mem-2 e) :test ,#'eql)
         :test eql
         :values (,mem-1 ,mem-2 e)
         :size 3
         :assigned nil
         :type-spec (member ,mem-1 ,mem-2 e)
         :domain-tests ((a . nil)
                        (b . nil)
                        (,(list 'a 'b) . nil)
                        (,mem-1 . t)
                        (,mem-2 . t)
                        (g . nil))))))

(defparameter *enumerated-domain-error-tests*
  `((:members (a a))))

(defun test-enumerated-domain (dom &key
                                     test
                                     values
                                     size
                                     assigned
                                     assigned-value
                                     type-spec
                                     domain-tests)
  (setf test (symbol-function test))
  (is (typep (odo:domain-type dom) 'odo:pure-discrete-domain))
  (is (odo:discrete-domain-p dom))
  (is (odo:discrete-domain-p dom t))
  (when type-spec
    (is (subtypep type-spec (odo:domain-type-spec dom))))
  (is (alex:set-equal values (odo:value-list dom) :test test))
  (is (eql test (odo:domain-test dom)))
  (when values
    (iter
      (for val :in-domain dom)
      (for true-val :in values)
      (is (funcall test true-val val))))
  (is (eql size (odo:size dom)))
  (is (eql assigned (odo:assigned-p dom)))
  (if assigned
      (is (funcall test assigned-value (odo:assigned-value dom)))
      (signals odo:domain-error (odo:assigned-value dom)))
  (iter
    (for (val . result) :in domain-tests)
    (is (eql result (odo:in-domain-p dom val))))
  (is (alex:set-equal values (odo:value-list dom) :test test)))

(defun test-enumerated-domain-implementation (fun &key simple-only-p)
  (iter
    (for (spec . results) in (if simple-only-p
                                 *simple-enumerated-domain-tests*
                                 *enumerated-domain-tests*))
    (for domain = (apply fun spec))
    (for domain-2 = (apply fun spec))
    (is (odo:domain-equal domain domain-2))
    (apply #'test-enumerated-domain
           domain
           results))
  (unless simple-only-p
    (iter
      (for spec in *enumerated-domain-error-tests*)
      (signals error (apply fun spec)))))
