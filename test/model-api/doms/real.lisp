(in-package #:odo/test)

(defparameter *simple-real-domain-tests*
  `(((:min 0.0 :max 10.0)
     :min-val 0.0
     :max-val 10.0
     :median 5.0
     :width 10.0
     :ranges ((0.0 . 10.0))
     :assigned nil
     :type-spec (real 0 10)
     :tight-type-spec (real 0 10)
     :domain-tests ((0.0 . t)
                    (7.0 . t)
                    (11.0 . nil)))
    ((:min 5.0 :max 5.0)
     :min-val 5.0
     :max-val 5.0
     :median 5.0
     :width 0.0
     :ranges ((5.0 . 5.0))
     :assigned t
     :assigned-value 5.0
     :type-spec (real 5 5)
     :tight-type-spec (real 5 5)
     :domain-tests ((0.0 . nil)
                    (7.0 . nil)
                    (5.0 . t)
                    (11.0 . nil)))
    (nil
     :min-val ,odo:+-infinity+
     :max-val ,odo:+infinity+
     :median *
     :width *
     :ranges ((,odo:+-infinity+ . ,odo:+infinity+))
     :assigned nil
     :type-spec real
     :tight-type-spec real
     :domain-tests ((-1.0 . t)
                    (0.0 . t)
                    (1.5 . t)))
    ((:min -3.0 :max *)
     :min-val -3.0
     :max-val ,odo:+infinity+
     :median *
     :width *
     :ranges ((-3.0 . ,odo:+infinity+))
     :assigned nil
     :type-spec (real -3)
     :tight-type-spec (real -3)
     :domain-tests ((-4.0 . nil)
                    (-3.0 . t)
                    (0.0 . t)
                    (100000.0 . t)))))

(defparameter *real-domain-tests*
  `(,@*simple-real-domain-tests*
    ((:ranges ((0.0 . 3.0) (9.0 . 10.0) (12.0 . 12.0)))
     :min-val 0.0
     :max-val 12.0
     :median 3.0
     :width 12.0
     :ranges ((0.0 . 3.0) (9.0 . 10.0) (12.0 . 12.0))
     :assigned nil
     :type-spec (real 0 12)
     :tight-type-spec (or (real 0 3)
                          (real 9 10)
                          (real 12 12))
     :domain-tests ((0.0 . t)
                    (7.0 . nil)
                    (10.0 . t)
                    (12.0 . t)
                    (11.0 . nil)
                    (14.0 . nil)))))

(defparameter *real-domain-error-tests*
  `((:ranges 3)
    (:ranges (1 2 3))
    (:ranges ((4 . 5) (* . 10)))
    (:ranges ((4 . *) (10 . 20)))
    (:ranges ((4 . *) (10 . *)))
    (:ranges (((a . t) . *)))
    (:ranges ((* . (a . t))))
    (:ranges (((* . t) . 70)))
    (:ranges ((-60 . (* . t))))
    (:min 10 :max 0)
    (:ranges ((0 . 30) (20 . 40)))))

(defun format-real-ranges (ranges)
  (mapcar (lambda (x)
            (list (car x)
                  (cdr x)
                  (if (eql (car x) odo:+-infinity+)
                      nil
                      t)
                  (if (eql (cdr x) odo:+infinity+)
                      nil
                      t)))
          ranges))

(defun test-real-domain (real &key
                                min-val
                                max-val
                                median
                                width
                                ranges
                                assigned
                                assigned-value
                                type-spec
                                tight-type-spec
                                domain-tests)
  (is (typep (odo:domain-type real) 'odo:pure-real-domain))
  (is (odo:real-domain-p real))
  (is (odo:real-domain-p real t))
  (when type-spec
    (is (subtypep type-spec (odo:domain-type-spec real))))
  (when tight-type-spec
    (is (subtypep tight-type-spec (odo:domain-type-spec real t))))
  (is (eql min-val (odo:min-val real)))
  (is (eql max-val (odo:max-val real)))
  (is (eql median (odo:median real)))
  (is (eql width (odo:width real)))
  (iter
    (for (values lb ub) :in-ranges real)
    (for true-range :in ranges)
    (is (equal true-range (cons lb ub))))
  (is (eql assigned (odo:assigned-p real)))
  (if assigned
      (is (eql assigned-value (odo:assigned-value real)))
      (signals odo:domain-error (odo:assigned-value real)))
  (iter
    (for (val . result) :in domain-tests)
    (is (eql result (odo:in-domain-p real val))))
  (is (equal (format-real-ranges ranges) (odo:range-list real))))

(defun format-real-ranges-for-print-test (ranges)
  (loop
     :for range :in ranges
     :collecting (cons (first range) (second range))))

(defun real-domain-string (dom)
  (let ((ranges (odo:range-list dom)))
    (if (= (length ranges) 1)
        (format nil "(real :min ~s :max ~s)"
                (odo:min-val dom)
                (odo:max-val dom))
        (format nil "(real :ranges ~A)"
                (format-real-ranges-for-print-test ranges)))))

(defun test-real-domain-pprint (dom)
  (is (string-equal (real-domain-string dom)
                    (%pprint-odo-object dom))))

(defun test-real-domain-implementation (fun &key simple-only-p)
  (iter
    (for (spec . results) in (if simple-only-p
                                 *simple-real-domain-tests*
                                 *real-domain-tests*))
    (for domain = (apply fun spec))
    (for domain-2 = (apply fun spec))
    (is (odo:domain-equal domain domain-2))
    (apply #'test-real-domain
           domain
           results)
    (test-real-domain-pprint domain))
  (unless simple-only-p
    (iter
      (for spec in *real-domain-error-tests*)
      (signals error (apply fun spec)))))
