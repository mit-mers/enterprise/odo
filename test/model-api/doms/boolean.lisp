(in-package #:odo/test)

(defparameter *boolean-domain-tests*
  '((nil
     :size 2
     :assigned nil
     :values (nil t)
     :type-spec boolean
     :tight-type-spec boolean
     :domain-tests ((t . t)
                    (nil . t)))
    ((:true t :false nil)
     :size 1
     :assigned t
     :assigned-value t
     :values (t)
     :type-spec boolean
     :tight-type-spec (eql t)
     :domain-tests ((t . t)
                    (nil . nil)))
    ((:value t)
     :size 1
     :assigned t
     :assigned-value t
     :values (t)
     :type-spec boolean
     :tight-type-spec (eql t)
     :domain-tests ((t . t)
                    (nil . nil)))
    ((:true nil :false t)
     :size 1
     :assigned t
     :assigned-value nil
     :values (nil)
     :type-spec boolean
     :tight-type-spec (eql nil)
     :domain-tests ((t . nil)
                    (nil . t)))
    ((:value nil)
     :size 1
     :assigned t
     :assigned-value nil
     :values (nil)
     :type-spec boolean
     :tight-type-spec (eql nil)
     :domain-tests ((t . nil)
                    (nil . t)))))

(defparameter *boolean-domain-error-tests*
  '((:true nil :false nil)))

(defun test-boolean-domain (bool &key
                                   size
                                   assigned
                                   assigned-value
                                   values
                                   domain-tests
                                   type-spec
                                   tight-type-spec)
  (is (typep (odo:domain-type bool) 'odo:pure-boolean-domain))
  (is (odo:boolean-domain-p bool))
  (is (odo:boolean-domain-p bool t))
  (is (odo:discrete-domain-p bool))
  (is (subtypep type-spec (odo:domain-type-spec bool)))
  (is (subtypep tight-type-spec (odo:domain-type-spec bool t)))
  (iter
    (for val :in-domain bool)
    (for true-val :in values)
    (is (eql true-val val)))
  (is (= size (odo:size bool)))
  (is (eql assigned (odo:assigned-p bool)))
  (when assigned
    (eql assigned-value (odo:assigned-value bool)))
  (iter
    (for (val . result) :in domain-tests)
    (is (eql result (odo:in-domain-p bool val))))
  (is (equal values (odo:value-list bool))))

(defun boolean-domain-string (bool)
  (cond
    ((= (odo:size bool) 2)
     "(boolean)")
    ((odo:in-domain-p bool t)
     "(boolean :value t)")
    (t
     "(boolean :value nil)")))

(defun test-boolean-domain-pprint (bool)
  (is (string-equal (boolean-domain-string bool) (%pprint-odo-object bool))))

(defun test-boolean-domain-implementation (fun)
  (iter
    (for (spec . results) in *boolean-domain-tests*)
    (for domain = (apply fun spec))
    (for domain-2 = (apply fun spec))
    (is (odo:domain-equal domain domain-2))
    (apply #'test-boolean-domain
           domain
           results)
    (test-boolean-domain-pprint domain))
  (iter
    (for spec in *boolean-domain-error-tests*)
    (signals odo:domain-error (apply fun spec))))
