(in-package #:odo/test)

(defparameter *simple-integer-domain-tests*
  `(((:min 0 :max 10)
     :regret-min 1
     :regret-max 1
     :min-val 0
     :max-val 10
     :median 5
     :width 10
     :ranges ((0 . 10))
     :values ,(alex:iota 11)
     :size 11
     :assigned nil
     :type-spec (integer 0 10)
     :tight-type-spec (integer 0 10)
     :domain-tests ((0 . t)
                    (7 . t)
                    (11 . nil)))
    ((:min 5 :max 5)
     :regret-min 0
     :regret-max 0
     :min-val 5
     :max-val 5
     :median 5
     :width 0
     :ranges ((5 . 5))
     :values (5)
     :size 1
     :assigned t
     :assigned-value 5
     :type-spec (integer 5 5)
     :tight-type-spec (integer 5 5)
     :domain-tests ((0 . nil)
                    (7 . nil)
                    (5 . t)
                    (11 . nil)))
    (nil
     :regret-min 1
     :regret-max 1
     :min-val ,odo:+-infinity+
     :max-val ,odo:+infinity+
     :median *
     :width *
     :ranges ((,odo:+-infinity+ . ,odo:+infinity+))
     :size *
     :assigned nil
     :type-spec integer
     :tight-type-spec integer
     :domain-tests ((-1 . t)
                    (0 . t)
                    (1.5 . nil)))
    ((:min -3 :max *)
     :regret-min 1
     :regret-max 1
     :min-val -3
     :max-val ,odo:+infinity+
     :median *
     :width *
     :ranges ((-3 . ,odo:+infinity+))
     :size *
     :assigned nil
     :type-spec (integer -3)
     :tight-type-spec (integer -3)
     :domain-tests ((-4 . nil)
                    (-3 . t)
                    (0 . t)
                    (100000 . t)))
    ((:min -3 :max ,odo:+infinity+)
     :regret-min 1
     :regret-max 1
     :min-val -3
     :max-val ,odo:+infinity+
     :median *
     :width *
     :ranges ((-3 . ,odo:+infinity+))
     :size *
     :assigned nil
     :type-spec (integer -3)
     :tight-type-spec (integer -3)
     :domain-tests ((-4 . nil)
                    (-3 . t)
                    (0 . t)
                    (100000 . t)))))

(defparameter *integer-domain-tests*
  `(,@*simple-integer-domain-tests*
    ((:ranges ((0 . 3) (9 . 10) (12 . 12)))
     :regret-min 1
     :regret-max 2
     :min-val 0
     :max-val 12
     :median 3
     :width 12
     :ranges ((0 . 3) (9 . 10) (12 . 12))
     :values (0 1 2 3 9 10 12)
     :size 7
     :assigned nil
     :type-spec (integer 0 12)
     :tight-type-spec (or (integer 0 3)
                          (integer 9 10)
                          (integer 12 12))
     :domain-tests ((0 . t)
                    (7 . nil)
                    (10 . t)
                    (12 . t)
                    (11 . nil)
                    (14 . nil)))
    ((:values (3 2 1 0 12 9 10))
     :regret-min 1
     :regret-max 2
     :min-val 0
     :max-val 12
     :median 3
     :width 12
     :ranges ((0 . 3) (9 . 10) (12 . 12))
     :values (0 1 2 3 9 10 12)
     :size 7
     :assigned nil
     :type-spec (integer 0 12)
     :tight-type-spec (or (integer 0 3)
                          (integer 9 10)
                          (integer 12 12))
     :domain-tests ((0 . t)
                    (7 . nil)
                    (10 . t)
                    (12 . t)
                    (11 . nil)
                    (14 . nil)))
    ((:ranges ((-3 . -3) (-1 . 8)))
     :regret-min 2
     :regret-max 1
     :min-val -3
     :max-val 8
     :median 2
     :width 11
     :ranges ((-3 . -3) (-1 . 8))
     :values (-3 -1 0 1 2 3 4 5 6 7 8)
     :size 11
     :assigned nil
     :type-spec (integer -3 8)
     :tight-type-spec (or (integer -3 -3)
                          (integer -1 8))
     :domain-tests ((-3 . t)
                    (-2 . nil)
                    (7 . t)
                    (9 . nil)))))

(defparameter *integer-domain-error-tests*
  `((:ranges 3)
    (:ranges (1 2 3))
    (:ranges ((4 . 5) (* . 10)))
    (:min -5.0)
    (:max -200.0)
    (:ranges ((4 . *) (10 . 20)))
    (:ranges ((4 . *) (10 . *)))
    (:min 10 :max 0)
    (:ranges ((0 . 30) (20 . 40)))))

(defun format-int-ranges (ranges)
  (mapcar (lambda (x)
            (list (car x) (cdr x) t t))
          ranges))

(defun test-integer-domain (int &key
                                  regret-min
                                  regret-max
                                  min-val
                                  max-val
                                  median
                                  width
                                  ranges
                                  values
                                  size
                                  assigned
                                  assigned-value
                                  type-spec
                                  tight-type-spec
                                  domain-tests)
  (is (typep (odo:domain-type int) 'odo:pure-integer-domain))
  (is (odo:integer-domain-p int))
  (is (odo:integer-domain-p int t))
  (is (odo:real-domain-p int))
  (is (odo:discrete-domain-p int))
  (when type-spec
    (is (subtypep type-spec (odo:domain-type-spec int))))
  (when tight-type-spec
    (is (subtypep tight-type-spec (odo:domain-type-spec int t))))
  (is (eql regret-min (odo:regret-min int)))
  (is (eql regret-max (odo:regret-max int)))
  (is (eql min-val (odo:min-val int)))
  (is (eql max-val (odo:max-val int)))
  (is (eql median (odo:median int)))
  (is (eql width (odo:width int)))
  (iter
    (for (values lb ub) :in-ranges int)
    (for true-range :in ranges)
    (is (equal true-range (cons lb ub))))
  (when values
    (iter
      (for val :in-domain int)
      (for true-val :in values)
      (is (= true-val val))))
  (is (eql size (odo:size int)))
  (is (eql assigned (odo:assigned-p int)))
  (if assigned
      (is (eql assigned-value (odo:assigned-value int)))
      (signals odo:domain-error (odo:assigned-value int)))
  (iter
    (for (val . result) :in domain-tests)
    (is (eql result (odo:in-domain-p int val))))
  (is (equal (format-int-ranges ranges) (odo:range-list int))))

(defun format-int-ranges-for-print-test (ranges)
  (loop
     :for range :in ranges
     :collecting (cons (first range) (second range))))

(defun integer-domain-string (dom)
  (let ((ranges (odo:range-list dom)))
    (if (= (length ranges) 1)
        (format nil "(integer :min ~s :max ~s)"
                (odo:min-val dom)
                (odo:max-val dom))
        (format nil "(integer :ranges ~A)"
                (format-int-ranges-for-print-test ranges)))))

(defun test-integer-domain-pprint (dom)
  (is (string-equal (integer-domain-string dom)
                    (%pprint-odo-object dom))))

(defun test-integer-domain-implementation (fun &key simple-only-p)
  (iter
    (for (spec . results) in (if simple-only-p
                                 *simple-integer-domain-tests*
                                 *integer-domain-tests*))
    (for domain = (apply fun spec))
    (for domain-2 = (apply fun spec))
    (is (odo:domain-equal domain domain-2))
    (apply #'test-integer-domain
           domain
           results)
    (test-integer-domain-pprint domain))
  (unless simple-only-p
    (iter
      (for spec in *integer-domain-error-tests*)
      (signals error (apply fun spec)))))
