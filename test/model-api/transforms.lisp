(in-package #:odo/test)

(defun test-identity-transform (csp)
  (odo:state-space-add! csp (odo:make-variable '(integer 0 5)
                                               :name 'a))
  (odo:state-space-add! csp (odo:make-variable '(integer 0 5)
                                               :name 'b))
  (odo:add-constraint! csp (odo:make-constraint (odo:lt (odo:state-space-get csp 'a)
                                                        (odo:state-space-get csp 'b))))
  (let ((copy (odo:transform-csp 'odo:identity-transform csp)))
    (is (not (eql copy csp)))
    (is (odo:csp-equal copy csp))))

(defun test-reset-domains-transform (csp-1 csp-2)
  (let ((a (odo:make-variable '(integer 0 5)
                              :name 'a))
        (b (odo:make-variable '(integer 0 5)
                              :name 'b))
        (a-2 (odo:make-variable '(integer 0 2)
                                :name 'a))
        (b-2 (odo:make-variable '(integer 1 1)
                                :name 'b)))
    (odo:state-space-add! csp-1 a)
    (odo:state-space-add! csp-1 b)
    (odo:add-constraint! csp-1 (odo:make-constraint (odo:lt a b)))

    (odo:state-space-add! csp-2 a-2)
    (odo:state-space-add! csp-2 b-2)
    (odo:add-constraint! csp-2 (odo:make-constraint (odo:lt a-2 b-2)))

    (let ((csp-3 (odo:transform-csp (make-instance 'odo:reset-domains
                                                   :source-csp csp-1 :variables '(a))
                                    csp-2)))
      (is (odo:assigned-p (odo:state-space-get csp-3 'b)))
      (is (= (odo:min-val (odo:state-space-get csp-3 'a)) 0))
      (is (= (odo:max-val (odo:state-space-get csp-3 'a)) 5)))))

(defun test-booleanize-conjunctions-transform (csp)
  (let ((a (odo:make-variable 'integer
                              :min 0
                              :max 10
                              :name 'a))
        (b (odo:make-variable 'integer
                              :min 20
                              :max 40
                              :name 'b))
        (c (odo:make-variable '(member a b)
                              :name 'c)))
    (odo:state-space-add! csp a)
    (odo:state-space-add! csp b)
    (odo:state-space-add! csp c)

    (odo:add-constraint! csp (odo:make-constraint (odo:and
                                                   (odo:equal a 3)
                                                   (odo:not (odo:equal b 21)))
                                                  :name 'c1))

    (odo:add-constraint! csp
                         (odo:make-constraint
                          (odo:-> (odo:equal c 'b)
                                  (odo:and (odo:equal b 22) (not (odo:equal a 4))))
                          :name 'c2))
    (let* ((new-csp (odo:transform-csp 'odo:booleanize-conjunctions csp))
           (vars (odo:state-space-list new-csp))
           (new-vars (set-difference vars (list a b c) :key #'odo:name))
           (constraints (odo:constraint-list new-csp))
           (new-constraints (set-difference constraints
                                            (list (odo:get-constraint new-csp 'c1)
                                                  (odo:get-constraint new-csp 'c2))
                                            :key #'odo:name)))
      ;; This should result four new boolean variables, and four new full reif
      ;; constraints.
      (is (= 7 (length vars)))
      (is (odo:equal-p a (odo:state-space-get new-csp 'a)))
      (is (odo:equal-p b (odo:state-space-get new-csp 'b)))
      (is (odo:equal-p c (odo:state-space-get new-csp 'c)))

      (is (every (alex:rcurry #'odo:boolean-domain-p t)
                 new-vars))

      (is (= 4 (length new-constraints)))
      (is (every (lambda (x)
                   (and (odo:expression-p x)
                        (typep (odo:type x) 'odo:application-expression)
                        (eql 'odo:<-> (odo:operator x))))
                 (mapcar #'odo:expression new-constraints)))
      (is (every (lambda (c)
                   (member (odo:argument-value c 'odo:right) new-vars))
                 (mapcar #'odo:expression new-constraints))))))

(defun test-booleanize-disjunctions-transform (csp)
  (let ((a (odo:make-variable 'integer
                              :min 0
                              :max 10
                              :name 'a))
        (b (odo:make-variable 'integer
                              :min 20
                              :max 40
                              :name 'b))
        (c (odo:make-variable '(member a b)
                              :name 'c)))
    (odo:state-space-add! csp a)
    (odo:state-space-add! csp b)
    (odo:state-space-add! csp c)

    (odo:add-constraint! csp (odo:make-constraint (odo:or
                                                   (odo:equal a 3)
                                                   (not (odo:equal b 21)))
                                                  :name 'c1))

    (odo:add-constraint! csp
                         (odo:make-constraint
                          (odo:-> (odo:equal c 'b)
                                  (odo:or (odo:equal b 22) (not (odo:equal a 4))))
                          :name 'c2))
    (let* ((new-csp (odo:transform-csp 'odo:booleanize-disjunctions csp))
           (vars (odo:state-space-list new-csp))
           (new-vars (set-difference vars (list a b c) :key #'odo:name))
           (constraints (odo:constraint-list new-csp))
           (new-constraints (set-difference constraints
                                            (list (odo:get-constraint new-csp 'c1)
                                                  (odo:get-constraint new-csp 'c2))
                                            :key #'odo:name)))
      ;; This should result four new boolean variables, and four new full reif
      ;; constraints.
      (is (= 7 (length vars)))
      (is (odo:equal-p a (odo:state-space-get new-csp 'a)))
      (is (odo:equal-p b (odo:state-space-get new-csp 'b)))
      (is (odo:equal-p c (odo:state-space-get new-csp 'c)))

      (is (every (alex:rcurry #'odo:boolean-domain-p t)
                 new-vars))

      (is (= 4 (length new-constraints)))
      (is (every (lambda (x)
                   (and (odo:expression-p x)
                        (typep (odo:type x) 'odo:application-expression)
                        (eql 'odo:<-> (odo:operator x))))
                 (mapcar #'odo:expression new-constraints)))
      (is (every (lambda (c)
                   (member (odo:argument-value c 'odo:right) new-vars))
                 (mapcar #'odo:expression new-constraints))))))

(defun test-booleanize-reifs-transform (csp)
  (let ((a (odo:make-variable '(member a b c)
                              :name 'a))
        (b (odo:make-variable '(member a b c)
                              :name 'b)))
    (odo:state-space-add! csp a)
    (odo:state-space-add! csp b)

    (odo:add-constraint! csp
                         (odo:make-constraint (odo:-> (odo:equal a 'a)
                                                      (odo:equal b 'c))
                                              :name 'h))

    (odo:add-constraint! csp
                         (odo:make-constraint (odo:<-> (odo:equal a 'a) (odo:equal b 'c))
                                              :name 'f))
    (let* ((new-csp (odo:transform-csp 'odo:booleanize-reifs csp))
           (vars (odo:state-space-list new-csp))
           (new-vars (set-difference vars (list a b) :key #'odo:name))
           (constraints (odo:constraint-list new-csp))
           (new-constraints (set-difference constraints
                                            (list (odo:get-constraint new-csp 'h)
                                                  (odo:get-constraint new-csp 'f))
                                            :key #'odo:name)))
      ;; This should result in two new boolean vars and two new full reifs with
      ;; the booleans on the right side.
      (is (= 4 (length vars)))
      (is (odo:equal-p a (odo:state-space-get new-csp 'a)))
      (is (odo:equal-p b (odo:state-space-get new-csp 'b)))

      (is (every (alex:rcurry #'odo:boolean-domain-p t)
                 new-vars))

      (is (= 2 (length new-constraints)))
      (is (every (lambda (x)
                   (and (odo:expression-p x)
                        (typep (odo:type x) 'odo:application-expression)
                        (eql 'odo:<-> (odo:operator x))))
                 (mapcar #'odo:expression new-constraints)))
      (is (every (lambda (c)
                   (member (odo:argument-value c 'odo:right) new-vars))
                 (mapcar #'odo:expression new-constraints))))))

(defun test-enum-transform (csp)
  (let ((a (odo:make-variable '(member a b c)
                              :name 'a))
        (b (odo:make-variable '(member a b c)
                              :name 'b))
        (c (odo:make-variable '(member a b c)
                              :name 'c))
        (d (odo:make-variable '(member a b c)
                              :name 'd)))
    (odo:state-space-add! csp a)
    (odo:state-space-add! csp b)
    (odo:state-space-add! csp c)

    (odo:add-constraint! csp (odo:make-constraint (odo:equal a 'b)
                                                  :name 'c1))

    (odo:add-constraint! csp (odo:make-constraint (odo:all-diff-except (list a b) 'a)
                                                  :name 'c2))

    (odo:add-constraint! csp (odo:make-constraint (odo:-> (odo:equal b 'c)
                                                          (odo:equal a 'a))
                                                  :name 'c3))

    (odo:add-constraint! csp (odo:make-constraint (odo:circuit (list a c b)
                                                               :sparse-vector-indices (list 'a 'b 'c))
                                                  :name 'c4))

    (odo:add-constraint! csp (odo:make-constraint (odo:element (list a c) b d :sparse-vector-indices (list 'a 'c))
                                                  :name 'c5))

    (odo:add-constraint! csp (odo:make-constraint (odo:element (list a c) b 'a :sparse-vector-indices (list 'a 'c))
                                                  :name 'c6))

    (let* ((new-csp (odo:transform-csp 'odo:enums-to-ints csp))
           (vars (odo:state-space-list new-csp))
           (constraints (odo:constraint-list new-csp)))
      ;; There should be four integer variables and every reference to an
      ;; element of the domains in constraints should be integers.
      (is (alex:set-equal '(a b c d) (mapcar #'odo:name vars)))
      (is (every (alex:rcurry #'odo:integer-domain-p t) vars))

      (is (alex:set-equal '(c1 c2 c3 c4 c5 c6) (mapcar #'odo:name constraints)))
      (is (integerp (odo:argument-value (odo:expression (odo:get-constraint new-csp 'c1))
                                        'odo:right)))
      (is (integerp (odo:argument-value (odo:expression (odo:get-constraint new-csp 'c2))
                                        'odo:except)))
      (is (integerp (odo:argument-value
                     (odo:argument-value (odo:expression (odo:get-constraint new-csp 'c3))
                                         'odo:right)
                     'odo:right)))
      (is (integerp (odo:argument-value
                     (odo:argument-value (odo:expression (odo:get-constraint new-csp 'c3))
                                         'odo:left)
                     'odo:right))))))

(defun test-expression-to-references-transform (csp)
  (let ((x (odo:make-variable 'integer :name :x)))
    (odo:state-space-add! csp x)
    (setf (odo:objective-goal csp) :maximize)
    (setf (odo:objective-function csp) (odo:* 5 x))

    (let ((new-csp (odo:transform-csp 'odo:expression-to-references csp)))
      (is (equal '(odo:* 5 (odo:lookup :x))
                 (odo:expression-to-sexp (odo:objective-function new-csp)))))))

(defun test-temporal-to-linear-transform (csp)
  (let ((from-event (odo:make-variable 'real
                                       :name :from))
        (to-event (odo:make-variable 'real
                                     :name :to)))

    (odo:state-space-add! csp from-event)
    (odo:state-space-add! csp to-event)

    (odo:add-constraint! csp (odo:make-constraint (odo:simple-temporal from-event to-event
                                                                       :lower-bound 0
                                                                       :upper-bound 10)))

    (let* ((new-csp (odo:transform-csp 'odo:temporal-to-linear csp))
           (vars (odo:state-space-list new-csp))
           (constraints (odo:constraint-list new-csp)))
      (is (alex:set-equal vars (list from-event to-event) :test #'odo:equal-p))

      (is (= 1 (length constraints)))
      (is (and (typep (odo:type (odo:expression (first constraints))) 'odo:application-expression)
               (eql 'odo:and (odo:operator (odo:expression (first constraints)))))))))
