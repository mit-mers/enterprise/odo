(in-package #:odo/test)

(defun variable-string (var)
  (let ((name (with-output-to-string (s)
                (write (odo:name var) :stream s)))
        (controllable-string
         (etypecase (odo:type var)
           (odo:controllable-variable "controllable")
           (odo:uncontrollable-variable "uncontrollable"))))
    (cond
      ((odo:boolean-domain-p var)
       (format nil "(~A (~A) ~A)" name controllable-string
               (boolean-domain-string var)))
      ((odo:integer-domain-p var)
       (format nil "(~A (~A) ~A)" name controllable-string
               (integer-domain-string var)))
      ((odo:real-domain-p var)
       (format nil "(~A (~A) ~A)" name controllable-string
               (real-domain-string var))))))

(defun test-integer-variable-implementation
    (fun &key uncontrollable-p simple-only-p)
  (iter
    (for (dom-spec . results) in (if simple-only-p
                                     *simple-integer-domain-tests*
                                     *integer-domain-tests*))
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (for var = (apply fun var-spec))
    (for var-2 = (apply fun var-spec))
    (for var-3 = (odo:copy var))
    (is (odo:equal-p var var-2))
    (is (odo:equal-p var var-3))
    (if uncontrollable-p
        (is (typep (odo:type var) 'odo:uncontrollable-variable))
        (is (typep (odo:type var) 'odo:controllable-variable)))
    (apply #'test-integer-domain
           var
           results)
    (is (eql name (odo:name var)))
    (is (string-equal (variable-string var)
                      (%pprint-odo-object var))))
  (unless simple-only-p
    (iter
      (for spec in *integer-domain-error-tests*)
      (signals error (apply fun spec)))))

(defun test-real-variable-implementation
    (fun &key uncontrollable-p simple-only-p)
  (iter
    (for (dom-spec . results) in (if simple-only-p
                                     *simple-real-domain-tests*
                                     *real-domain-tests*))
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (for var = (apply fun var-spec))
    (for var-2 = (apply fun var-spec))
    (for var-3 = (odo:copy var))
    (is (odo:equal-p var var-2))
    (is (odo:equal-p var var-3))
    (if uncontrollable-p
        (is (typep (odo:type var) 'odo:uncontrollable-variable))
        (is (typep (odo:type var) 'odo:controllable-variable)))
    (apply #'test-real-domain
           var
           results)
    (is (eql name (odo:name var)))
    (is (string-equal (variable-string var)
                      (%pprint-odo-object var))))
  (unless simple-only-p
    (iter
      (for spec in *real-domain-error-tests*)
      (signals error (apply fun spec)))))

(defun test-boolean-variable-implementation
    (fun &key uncontrollable-p)
  (iter
    (for (dom-spec . results) in *boolean-domain-tests*)
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (for var = (apply fun var-spec))
    (for var-2 = (apply fun var-spec))
    (for var-3 = (odo:copy var))
    (is (odo:equal-p var var-2))
    (is (odo:equal-p var var-3))
    (if uncontrollable-p
        (is (typep (odo:type var) 'odo:uncontrollable-variable))
        (is (typep (odo:type var) 'odo:controllable-variable)))
    (apply #'test-boolean-domain
           var
           results)
    (is (eql name (odo:name var)))
    (is (string-equal (variable-string var)
                      (%pprint-odo-object var))))
  (iter
    (for spec in *boolean-domain-error-tests*)
    (signals odo:domain-error (apply fun spec))))

(defun test-enumerated-variable-implementation
    (fun &key uncontrollable-p simple-only-p)
  (iter
    (for (dom-spec . results) in (if simple-only-p
                                     *simple-enumerated-domain-tests*
                                     *enumerated-domain-tests*))
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (for var = (apply fun var-spec))
    (for var-2 = (apply fun var-spec))
    (for var-3 = (odo:copy var))
    (is (odo:equal-p var var-2))
    (is (odo:equal-p var var-3))
    (if uncontrollable-p
        (is (typep (odo:type var) 'odo:uncontrollable-variable))
        (is (typep (odo:type var) 'odo:controllable-variable)))
    (apply #'test-enumerated-domain
           var
           results)
    (is (eql name (odo:name var))))
  (unless simple-only-p
    (iter
      (for spec in *enumerated-domain-error-tests*)
      (signals error (apply fun spec)))))
