(in-package #:odo/test)

(defun conditional-variable-string (var)
  (let ((name (with-output-to-string (s)
                (write (odo:name var) :stream s)))
        (controllable-string
         (etypecase (odo:type var)
           (odo:controllable-conditional-variable "controllable")
           (odo:uncontrollable-conditional-variable "uncontrollable"))))
    (cond
      ((odo:boolean-domain-p var)
       (format nil "(~A (~A) ~A :conditional t :activation-status ~S)"
               name controllable-string
               (boolean-domain-string var)
               (odo:variable-activation-status var)))
      ((odo:integer-domain-p var)
       (format nil "(~A (~A) ~A :conditional t :activation-status ~S)"
               name controllable-string
               (integer-domain-string var)
               (odo:variable-activation-status var)))
      ((odo:real-domain-p var)
       (format nil "(~A (~A) ~A :conditional t :activation-status ~S)"
               name controllable-string
               (real-domain-string var)
               (odo:variable-activation-status var))))))

(defun test-conditional-integer-variable-implementation
    (fun &key uncontrollable-p)
  (iter
    (for (dom-spec . results) in *integer-domain-tests*)
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (iter
      (for activation-status in '(:unknown :inactive :active))
      (for var = (apply fun :activation-status activation-status var-spec))
      (for var-2 = (apply fun :activation-status activation-status var-spec))
      (for var-3 = (odo:copy var))
      (is (odo:equal-p var var-2))
      (is (odo:equal-p var var-3))
      (is (odo:variable-p var))
      (is (odo:conditional-variable-p var))
      (if uncontrollable-p
          (is (typep (odo:type var) 'odo:uncontrollable-conditional-variable))
          (is (typep (odo:type var) 'odo:controllable-conditional-variable)))
      (is (eql activation-status (odo:variable-activation-status var)))
      (ecase activation-status
        (:unknown
         (apply #'test-integer-domain
                var
                :assigned nil
                results))
        (:inactive
         (apply #'test-integer-domain
                var
                :assigned t
                :assigned-value odo:+variable-inactive-value+
                results))
        (:active
         (apply #'test-integer-domain var results)))
      (is (eql name (odo:name var)))
      (is (string-equal (conditional-variable-string var)
                        (%pprint-odo-object var))))))

(defun test-conditional-real-variable-implementation
    (fun &key uncontrollable-p)
  (iter
    (for (dom-spec . results) in *real-domain-tests*)
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (iter
      (for activation-status in '(:unknown :inactive :active))
      (for var = (apply fun :activation-status activation-status var-spec))
      (for var-2 = (apply fun :activation-status activation-status var-spec))
      (for var-3 = (odo:copy var))
      (is (odo:equal-p var var-2))
      (is (odo:equal-p var var-3))
      (is (odo:variable-p var))
      (is (odo:conditional-variable-p var))
      (if uncontrollable-p
          (is (typep (odo:type var) 'odo:uncontrollable-conditional-variable))
          (is (typep (odo:type var) 'odo:controllable-conditional-variable)))
      (is (eql activation-status (odo:variable-activation-status var)))
      (ecase activation-status
        (:unknown
         (apply #'test-real-domain
                var
                :assigned nil
                results))
        (:inactive
         (apply #'test-real-domain
                var
                :assigned t
                :assigned-value odo:+variable-inactive-value+
                results))
        (:active
         (apply #'test-real-domain var results)))
      (is (eql name (odo:name var)))
      (is (string-equal (conditional-variable-string var)
                        (%pprint-odo-object var))))))

(defun test-conditional-boolean-variable-implementation
    (fun &key uncontrollable-p)
  (iter
    (for (dom-spec . results) in *boolean-domain-tests*)
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (iter
      (for activation-status in '(:unknown :inactive :active))
      (for var = (apply fun :activation-status activation-status var-spec))
      (for var-2 = (apply fun :activation-status activation-status var-spec))
      (for var-3 = (odo:copy var))
      (is (odo:equal-p var var-2))
      (is (odo:equal-p var var-3))
      (is (odo:variable-p var))
      (is (odo:conditional-variable-p var))
      (if uncontrollable-p
          (is (typep (odo:type var) 'odo:uncontrollable-conditional-variable))
          (is (typep (odo:type var) 'odo:controllable-conditional-variable)))
      (is (eql activation-status (odo:variable-activation-status var)))
      (ecase activation-status
        (:unknown
         (apply #'test-boolean-domain
                var
                :assigned nil
                results))
        (:inactive
         (apply #'test-boolean-domain
                var
                :assigned t
                :assigned-value odo:+variable-inactive-value+
                results))
        (:active
         (apply #'test-boolean-domain var results)))
      (is (eql name (odo:name var)))
      (is (string-equal (conditional-variable-string var)
                        (%pprint-odo-object var))))))

(defun test-conditional-enumerated-variable-implementation
    (fun &key uncontrollable-p)
  (iter
    (for (dom-spec . results) in *enumerated-domain-tests*)
    (for name = (gensym))
    (for var-spec = `(:name ,name
                      :uncontrollable-p ,uncontrollable-p
                      ,@dom-spec))
    (iter
      (for activation-status in '(:unknown :inactive :active))
      (for var = (apply fun :activation-status activation-status var-spec))
      (for var-2 = (apply fun :activation-status activation-status var-spec))
      (for var-3 = (odo:copy var))
      (is (odo:equal-p var var-2))
      (is (odo:equal-p var var-3))
      (is (odo:variable-p var))
      (is (odo:conditional-variable-p var))
      (if uncontrollable-p
          (is (typep (odo:type var) 'odo:uncontrollable-conditional-variable))
          (is (typep (odo:type var) 'odo:controllable-conditional-variable)))
      (is (eql activation-status (odo:variable-activation-status var)))
      (ecase activation-status
        (:unknown
         (apply #'test-enumerated-domain
                var
                :assigned nil
                results))
        (:inactive
         (apply #'test-enumerated-domain
                var
                :assigned t
                :assigned-value odo:+variable-inactive-value+
                results))
        (:active
         (apply #'test-enumerated-domain var results)))
      (is (eql name (odo:name var))))))
