(in-package #:odo/test)

(defun minizinc-installed-p ()
  (ignore-errors
   (zerop (nth-value 2
                     (uiop:run-program
                      '("minizinc" "--help")
                      :ignore-error-status t)))))

(defun call-with-minizinc-installed (thunk)
  (if (minizinc-installed-p)
      (funcall thunk)
      (skip "minizinc not installed")))

(defmacro with-minizinc (() &body body)
  `(call-with-minizinc-installed
    (lambda ()
      ,@body)))

(defun test-minizinc-tcsp ()
  (with-minizinc ()
    (is (odo:deserialize (asdf:system-relative-pathname
                          :odo
                          "examples/mzn/tcsp.mzn")))))

(defun test-minizinc-cctp ()
  (with-minizinc ()
    (is (odo:deserialize (asdf:system-relative-pathname
                          :odo
                          "examples/mzn/cctp.mzn")))))

(defun test-minizinc-vrp ()
  (with-minizinc ()
    (is (odo:deserialize (asdf:system-relative-pathname
                          :odo
                          "examples/mzn/vrp.mzn")
                         :decoder `(:minizinc :data-pathname ,(asdf:system-relative-pathname
                                                               :odo
                                                               "examples/mzn/vrp.dzn"))))))

(defun test-minizinc-uncontrollable-var ()
  (with-minizinc ()
    (is (odo:deserialize (asdf:system-relative-pathname
                          :odo
                          "examples/mzn/uncontrollable-var.mzn")))))

(defun test-minizinc-state-plan ()
  (with-minizinc ()
    (is (odo:deserialize (asdf:system-relative-pathname
                          :odo
                          "examples/mzn/state-plan.mzn")))))
