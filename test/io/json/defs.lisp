(in-package #:odo/test)

(defun jsonschema-installed-p ()
  (ignore-errors
   (zerop (nth-value 2
                     (uiop:run-program
                      '("jsonschema" "--help")
                      :ignore-error-status t)))))

(defun call-with-jsonschema-installed (thunk)
  (if (jsonschema-installed-p)
      (funcall thunk)
      (skip "jsonschema not installed")))

(defmacro with-jsonschema (() &body body)
  `(call-with-jsonschema-installed
    (lambda ()
      ,@body)))

(defun validate-against-jsonschema (pathname)
  (multiple-value-bind (stdout stderr exit-code)
      (uiop:run-program
       `("jsonschema" "-i" ,(uiop:native-namestring pathname)
                      ,(uiop:native-namestring (asdf:system-relative-pathname
                                                :odo "schemas/json/v0.3.0/state-plan.json")))
       :ignore-error-status t
       :error-output '(:string :stripped t))
    (declare (ignore stdout))
    (if (zerop exit-code)
        (pass)
        (fail "Schema validation failed with message:~%~A" stderr))))

(defun test-json-round-trip (obj)
  (let (meta)
    (uiop:with-temporary-file (:stream stream
                               :pathname pathname)
      (setf meta (odo:serialize-to-stream obj stream :format :json))
      :close-stream
      (with-jsonschema ()
        (validate-against-jsonschema pathname))
      (let ((obj2 (odo:deserialize pathname :format :json :meta meta)))
        (is (odo:equal-p obj obj2))))))
