(in-package #:odo/test)

(defun test-state-plan-io ()
  (let* ((state-plan (odo:make-state-plan)))
    (test-json-round-trip state-plan)))
