(in-package #:odo/test)

(named-readtables:in-readtable pythonic-string-reader:pythonic-string-syntax)

(alex:define-constant +latex-empty-structure+
  """

\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{longtable}
\title{Odo CSP}
\author{}
\date{\today}
\begin{document}
\maketitle
\newpage\section{Variables}
\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
\end{longtable}
\end{center}
\newpage\section{Constraints}
\begin{enumerate}
\end{enumerate}
\newpage\section{Objective}
(\textit{SATISFY}) \verb|NIL|
\end{document}
"""
  :test #'equal)

(alex:define-constant +latex-misc-section+
  """

\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{longtable}
\title{Odo CSP}
\author{}
\date{\today}
\begin{document}
\maketitle
\newpage
HUZZAH!
\newpage\section{Variables}
\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
\end{longtable}
\end{center}
\newpage\section{Constraints}
\begin{enumerate}
\end{enumerate}
\newpage\section{Objective}
(\textit{SATISFY}) \verb|NIL|
\end{document}
"""
  :test #'equal)

(alex:define-constant +latex-variable-section-bool+
  """\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
(\textbf{bool}) & $\lbrace \top, \bot \rbrace$ & \verb|boolvar| \\
\end{longtable}
\end{center}
"""
  :test #'equal)

(alex:define-constant +latex-variable-section-real+
  """\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
(\textbf{real}) & $[2.0, 3.14159]$ & \verb|realvar| \\
\end{longtable}
\end{center}
"""
  :test #'equal)

(alex:define-constant +latex-variable-section-int+
  """\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
(\textbf{int}) & $\lbrace 1 \cdots 3 \rbrace$ & \verb|intvar| \\
\end{longtable}
\end{center}
"""
  :test #'equal)

(alex:define-constant +latex-variable-section-enum+
  """\begin{center}
\def\arraystretch{1.5}
\begin{longtable}{lll}
\textbf{Type} & \textbf{Scope} & \textbf{Name} \\
(\textbf{enum}) & (a 4 13) & \verb|enumvar| \\
\end{longtable}
\end{center}
"""
  :test #'equal)


(alex:define-constant +latex-constraint-section-leq+
  """\begin{enumerate}
\vspace{1em} \item (CONS1) \begin{itemize}
\item $x_{0}$: \verb|realvar|
\end{itemize}
\[2 \cdot x_0 \leq 13.0\]
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-conjunction+
  """\begin{enumerate}
\vspace{1em} \item (CONS) The following must be true:
\begin{itemize}
\itemBoolean variable \verb|b1| holds.
\itemNOT (Boolean variable \verb|b2| holds.
)
\end{itemize}
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-eq+
  """\begin{enumerate}
\vspace{1em} \item (CONS2) \verb|intvar| $=$ \verb|realvar|
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-half-reif+
  """\begin{enumerate}
\vspace{1em} \item (CONS1) (a) $\rightarrow$ (b) where...
\begin{enumerate}
\item \verb|b1| $=$ T
\item \verb|b2| $=$ T
\end{enumerate}
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-full-reif+
  """\begin{enumerate}
\vspace{1em} \item (CONS1) (a) $\leftrightarrow$ (b) where...
\begin{enumerate}
\item \verb|b1| $=$ T
\item \verb|b2| $=$ T
\end{enumerate}
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-boolean-pred+
  """\begin{enumerate}
\vspace{1em} \item (CONS) Boolean variable \verb|b1| holds.
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-constraint-section-boolean-and+
  """\begin{enumerate}
\vspace{1em} \item (CONS) The following must be true:
\begin{itemize}
\itemBoolean variable \verb|b1| holds.
\itemNOT (Boolean variable \verb|b2| holds.
)
\end{itemize}
\end{enumerate}
"""
  :test #'equal)

(alex:define-constant +latex-objective-section+
  """(\textit{MAXIMIZE}) \verb|Reward|
"""
  :test #'equal)

(defun test-latex-structure ()
  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo/io/latex/csp2latex:latex-print-csp s csp)
    (is (equal +latex-empty-structure+ (get-output-stream-string s)))))

(defun test-latex-misc ()
  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo/io/latex/csp2latex:latex-print-csp s csp "HUZZAH!")
    (is (equal +latex-misc-section+ (get-output-stream-string s)))))

(defun test-latex-variables ()
  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo:state-space-add! csp (odo:make-variable
                               (odo:make-integer-domain :min 1 :max 3)
                               :name "intvar"))
    (odo/io/latex/csp2latex:latex-print-variables s csp)
    (is (equal +latex-variable-section-int+ (get-output-stream-string s))))

  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo:state-space-add! csp (odo:make-variable
                               (odo:make-real-domain :min 2.0 :max 3.14159)
                               :name "realvar"))
    (odo/io/latex/csp2latex:latex-print-variables s csp)
    (is (equal +latex-variable-section-real+ (get-output-stream-string s))))

  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo:state-space-add! csp (odo:make-variable
                               (odo:make-boolean-domain)
                               :name "boolvar"))
    (odo/io/latex/csp2latex:latex-print-variables s csp)
    (is (equal +latex-variable-section-bool+ (get-output-stream-string s))))

  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (odo:state-space-add! csp (odo:make-variable
                               (odo:make-enumerated-domain :members (list "a" 4 13))
                               :name "enumvar"))
    (odo/io/latex/csp2latex:latex-print-variables s csp)
    (is (equal +latex-variable-section-enum+ (get-output-stream-string s)))))

(defun test-latex-constraints ()
  (flet ((make-bool-pair (csp)
           (let ((b1 (odo:make-variable 'boolean :name "b1"))
                 (b2 (odo:make-variable 'boolean :name "b2")))
             (odo:state-space-add! csp b1)
             (odo:state-space-add! csp b2)
             (list b1 b2))))

    (let* ((s (make-string-output-stream))
           (csp (odo:make-csp))
           (bools (make-bool-pair csp)))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:->
                             (odo:cl-equalp (first bools) t)
                             (odo:cl-equalp (second bools) t))
                            :name "CONS1"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-half-reif+ (get-output-stream-string s))))

    (let* ((s (make-string-output-stream))
           (csp (odo:make-csp))
           (bools (make-bool-pair csp)))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:<->
                             (odo:cl-equalp (first bools) t)
                             (odo:cl-equalp (second bools) t))
                            :name "CONS1"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-full-reif+ (get-output-stream-string s))))

    (let* ((s (make-string-output-stream))
           (csp (odo:make-csp))
           (bools (make-bool-pair csp)))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (first bools)
                            :name "CONS"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-boolean-pred+ (get-output-stream-string s))))

    (let* ((s (make-string-output-stream))
           (csp (odo:make-csp))
           (bools (make-bool-pair csp)))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:and (first bools) (odo:not (second bools)))
                            :name "CONS"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-boolean-and+ (get-output-stream-string s))))

    (let* ((s (make-string-output-stream))
           (csp (odo:make-csp))
           (bools (make-bool-pair csp)))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:and (first bools) (odo:not (second bools)))
                            :name "CONS"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-conjunction+ (get-output-stream-string s))))

    (let ((s (make-string-output-stream))
          (csp (odo:make-csp)))
      (odo:state-space-add! csp (odo:make-variable
                                 (odo:make-integer-domain :min 1 :max 3)
                                 :name "intvar"))
      (odo:state-space-add! csp (odo:make-variable
                                 (odo:make-real-domain :min 2.0 :max 3.14159)
                                 :name "realvar"))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:cl-equalp
                             (odo:state-space-get csp "intvar")
                             (odo:state-space-get csp "realvar"))
                            :name "CONS2"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-eq+ (get-output-stream-string s))))

    (let ((s (make-string-output-stream))
          (csp (odo:make-csp)))
      (odo:state-space-add! csp (odo:make-variable
                                 (odo:make-real-domain :min 2.0 :max 3.14159)
                                 :name "realvar"))
      (odo:add-constraint! csp
                           (odo:make-constraint
                            (odo:linear-<=
                             (make-array 1 :initial-contents '(2))
                             (list (odo:state-space-get csp "realvar"))
                             13.0)
                            :name "CONS1"))
      (odo/io/latex/csp2latex:latex-print-constraints s csp)
      (is (equal +latex-constraint-section-leq+ (get-output-stream-string s))))))

(defun test-latex-objective ()
  (let ((s (make-string-output-stream))
        (csp (odo:make-csp)))
    (setf (odo:objective-function csp)
          (odo:make-variable
           (list 'integer 0 100)
           :name (format nil "Reward")))
    (setf (odo:objective-goal csp) :maximize)
    (odo/io/latex/csp2latex:latex-print-objective s csp)
    (is (equal +latex-objective-section+ (get-output-stream-string s)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;   Ways to dump the latex representation
;
; (ql:quickload :odo/io/latex/csp2latex) (in-package :odo/io/latex/csp2latex)
; (setf *foo* (some-way-to-create-a-csp-object))
;
; For a file:
; (with-open-file (stream "/home/lisp/workspaces/foo.tex" :direction :output :if-exists :supersede) (odo/io/latex/csp2latex:latex-print-csp stream *foo*))
;
; For standard output:
; (odo/io/latex/csp2latex:latex-print-csp *standard-output* *foo*)
