(in-package #:odo/test)

(defun test-pddl-round-trip (&key pathname)
  (let* ((obj (odo:deserialize pathname :format :pddl))
         (obj2 (odo:deserialize (odo:serialize-to-string obj :format :pddl) :format :pddl)))
    (is (odo:equal-p obj obj2))))
