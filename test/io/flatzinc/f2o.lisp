(in-package #:odo/test)

(defun test-flatzinc-csp ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/tcsp.fzn"))))

(defun test-flatzinc-cctp ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/cctp.fzn"))))

(defun test-flatzinc-nqueens ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/queens.fzn"))))

(defun test-flatzinc-vrp ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/vrp.fzn"))))

(defun test-flatzinc-radmax ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/RADMAX.fzn"))))

(defun test-flatzinc-radmax-mzn-fzn ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/RADMAX-mzn-fzn.fzn"))))

(defun test-flatzinc-aust ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/aust.fzn"))))

(defun test-flatzinc-latin-squares-12 ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/latin-squares-12.fzn"))))

(defun test-flatzinc-owens-toy-problem ()
  (is (odo:deserialize (asdf:system-relative-pathname
                        :odo
                        "examples/fzn/owens-demo.fzn"))))

(defun test-flatzinc-var-expr ()
  (let* ((csp (odo:deserialize (asdf:system-relative-pathname
                                :odo
                                "examples/fzn/var-expr-test.fzn")))
         (x (odo:state-space-get csp "x")))
    (is (= 1 (odo:min-val x)))
    (is (= 3 (odo:max-val x)))))

(defun test-flatzinc-output ()
  (let ((s (make-string-output-stream)))
    (finishes (odo:write-flatzinc-solution (odo:deserialize (asdf:system-relative-pathname
                                                             :odo "examples/fzn/output-array.fzn"))
                                           :stream s))
    (is (string=
         "A = 1.0;
C = 1.0;
Z = 1.0;
array_W = array1d(1..2, [1, 2]);
array_X = array2d(1..2, 1..3, [1.0, 2.0, 3.55, 1.0, 2.0, 3.55]);
array_Y = array1d(1..2, [1.0, 2.0]);
array_Z = array1d(1..2, [1, 2]);
----------
"
         (get-output-stream-string s)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For targeted testing / debugging of the eraps model,
;; the following approach can be quite helpful:
                                        ;
;  1. Load f2o with (ql:quickload :odo/io/flatzinc/f2o)
;  2. Run a targeted parse such as:
;     (odo/io/flatzinc/f2o::parse 'odo/io/flatzinc/f2o::pred_decl "predicate subcircuit(array [int] of var int: xs);")
;
