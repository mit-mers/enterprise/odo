(in-package #:odo/test)

(defun %pprint-odo-object (obj)
  (let ((utils:*odo-pprint-unreadable* nil))
    (with-output-to-string (s)
      (write obj
             :stream s
             :escape t
             :pretty t
             :circle nil
             :readably t
             :miser-width nil
             :right-margin most-positive-fixnum))))
