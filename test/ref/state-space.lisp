(in-package #:odo/test)

(def-suite :odo/ref/state-space :in :odo)
(in-suite :odo/ref/state-space)

(test test/state-space/make-state-space
  (test-state-space-implementation #'odo:make-state-space))

(test test/state-space/state-space-assigned
  (test-state-space-assigned-p #'odo:make-state-space))
