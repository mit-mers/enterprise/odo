(in-package #:odo/test)

(def-suite :odo/ref/variable :in :odo/ref)
(in-suite :odo/ref/variable)

(defun run-twice-varying-controllability (test-fun make-fun &rest args)
  (apply test-fun make-fun args)
  (apply test-fun make-fun :uncontrollable-p t args))


;;; Integer variables
(def-suite :odo/ref/variable/integer :in :odo/ref/variable)
(in-suite :odo/ref/variable/integer)

(test test/make-integer-variable
  (run-twice-varying-controllability
   #'test-integer-variable-implementation #'odo:make-integer-variable))

(test test/make-variable/integer
  (run-twice-varying-controllability
   #'test-integer-variable-implementation (alex:curry #'odo:make-variable 'integer)))

(test test/make-variable/integer-domain
  (run-twice-varying-controllability
   #'test-integer-variable-implementation (alex:curry #'odo:make-variable 'odo:integer-domain)))

(test test/make-variable/integer-domain-keyword
  (run-twice-varying-controllability
   #'test-integer-variable-implementation (alex:curry #'odo:make-variable :integer-domain)))

(test test/make-variable/integer-domain-object
  (run-twice-varying-controllability
   #'test-integer-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-variable
            `(,(apply #'odo:make-integer-domain
                      (alex:remove-from-plist args :name :uncontrollable-p))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p))))))

(test test/make-variable/integer-spec
  (run-twice-varying-controllability
   #'test-integer-variable-implementation
   (lambda (&key min max name uncontrollable-p)
     (apply #'odo:make-variable
            `(,(if min (if max `(integer ,min ,max)
                               `(integer ,min))
                       (if max `(integer * ,max)
                               `(integer)))
              :name ,name
              :uncontrollable-p ,uncontrollable-p)))
   :simple-only-p t))


;;; Real variables
(def-suite :odo/ref/variable/real :in :odo/ref/variable)
(in-suite :odo/ref/variable/real)

(test test/make-real-variable
  (run-twice-varying-controllability
   #'test-real-variable-implementation #'odo:make-real-variable))

(test test/make-variable/real
  (run-twice-varying-controllability
   #'test-real-variable-implementation (alex:curry #'odo:make-variable 'real)))

(test test/make-variable/real-domain
  (run-twice-varying-controllability
   #'test-real-variable-implementation (alex:curry #'odo:make-variable 'odo:real-domain)))

(test test/make-variable/real-domain-keyword
  (run-twice-varying-controllability
   #'test-real-variable-implementation (alex:curry #'odo:make-variable :real-domain)))

(test test/make-variable/real-domain-object
  (run-twice-varying-controllability
   #'test-real-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-variable
            `(,(apply #'odo:make-real-domain
                      (alex:remove-from-plist args :name :uncontrollable-p))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p))))))

(test test/make-variable/real-spec
  (run-twice-varying-controllability
   #'test-real-variable-implementation
   (lambda (&key min max name uncontrollable-p)
     (apply #'odo:make-variable
            `(,(if min (if max `(real ,min ,max)
                               `(real ,min))
                       (if max `(real * ,max)
                               `(real)))
              :name ,name
              :uncontrollable-p ,uncontrollable-p)))
   :simple-only-p t))


;;; Boolean variables
(def-suite :odo/ref/variable/boolean :in :odo/ref/variable)
(in-suite :odo/ref/variable/boolean)

(test test/make-boolean-variable
  (run-twice-varying-controllability
   #'test-boolean-variable-implementation #'odo:make-boolean-variable))

(test test/make-variable/boolean
  (run-twice-varying-controllability
   #'test-boolean-variable-implementation (alex:curry #'odo:make-variable 'boolean)))

(test test/make-variable/boolean-domain
  (run-twice-varying-controllability
   #'test-boolean-variable-implementation (alex:curry #'odo:make-variable 'odo:boolean-domain)))

(test test/make-variable/boolean-domain-keyword
  (run-twice-varying-controllability
   #'test-boolean-variable-implementation (alex:curry #'odo:make-variable :boolean-domain)))

(test test/make-variable/boolean-domain-object
  (run-twice-varying-controllability
   #'test-boolean-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-variable
            `(,(apply #'odo:make-boolean-domain
                      (alex:remove-from-plist args :name :uncontrollable-p))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p))))))


;;; Enumerated variables
(def-suite :odo/ref/variable/enumerated :in :odo/ref/variable)
(in-suite :odo/ref/variable/enumerated)

(test test/make-enumerated-variable
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation #'odo:make-enumerated-variable))

(test test/make-variable/enumerated
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation (alex:curry #'odo:make-variable 'member)))

(test test/make-variable/enumerated-domain
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation (alex:curry #'odo:make-variable 'odo:discrete-domain)))

(test test/make-variable/enumerated-domain-keyword
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation (alex:curry #'odo:make-variable :discrete-domain)))

(test test/make-variable/enumerated-domain-object
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-variable
            `(,(apply #'odo:make-enumerated-domain
                      (alex:remove-from-plist args :name :uncontrollable-p))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p))))))

(test test/make-variable/enumerated-spec
  (run-twice-varying-controllability
   #'test-enumerated-variable-implementation
   (lambda (&key members name uncontrollable-p)
     (apply #'odo:make-variable
            `((member ,@members)
              :name ,name
              :uncontrollable-p ,uncontrollable-p)))
   :simple-only-p t))
