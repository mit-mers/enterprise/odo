(in-package #:odo/test)

(def-suite :odo/ref/doms/real :in :odo/ref/doms)
(in-suite :odo/ref/doms/real)

(test test/make-real-domain
  (test-real-domain-implementation #'odo:make-real-domain))

(test test/make-domain/real
  (test-real-domain-implementation (alex:curry #'odo:make-domain 'real)))

(test test/make-domain/real-domain
  (test-real-domain-implementation (alex:curry #'odo:make-domain 'odo:real-domain)))

(test test/make-domain/real-domain-keyword
  (test-real-domain-implementation (alex:curry #'odo:make-domain :real-domain)))

(test test/make-domain/real-spec
  (test-real-domain-implementation
   (lambda (&key min max)
     (odo:make-domain (if min
                          (if max
                              `(real ,min ,max)
                              `(real ,min))
                          (if max
                              `(real * ,max)
                              `(real)))))
   :simple-only-p t))
