(in-package #:odo/test)

(def-suite :odo/ref/doms/integer :in :odo/ref/doms)
(in-suite :odo/ref/doms/integer)

(test test/make-integer-domain
  (test-integer-domain-implementation #'odo:make-integer-domain))

(test test/make-domain/integer
  (test-integer-domain-implementation (alex:curry #'odo:make-domain 'integer)))

(test test/make-domain/integer-domain
  (test-integer-domain-implementation (alex:curry #'odo:make-domain 'odo:integer-domain)))

(test test/make-domain/integer-domain-keyword
  (test-integer-domain-implementation (alex:curry #'odo:make-domain :integer-domain)))

(test test/make-domain/integer-spec
  (test-integer-domain-implementation
   (lambda (&key min max)
     (odo:make-domain (if min
                          (if max
                              `(integer ,min ,max)
                              `(integer ,min))
                          (if max
                              `(integer * ,max)
                              `(integer)))))
   :simple-only-p t))
