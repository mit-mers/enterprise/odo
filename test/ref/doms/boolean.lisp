(in-package #:odo/test)

(def-suite :odo/ref/doms/boolean :in :odo/ref/doms)
(in-suite :odo/ref/doms/boolean)

(test test/make-boolean-domain
  (test-boolean-domain-implementation #'odo:make-boolean-domain))

(test test/make-domain/boolean
  (test-boolean-domain-implementation (alex:curry #'odo:make-domain 'boolean)))

(test test/make-domain/boolean-domain
  (test-boolean-domain-implementation (alex:curry #'odo:make-domain 'odo:boolean-domain)))

(test test/make-domain/boolean-domain-keyword
  (test-boolean-domain-implementation (alex:curry #'odo:make-domain :boolean-domain)))

;; (test test/make-domain/boolean-spec
;;   (test-boolean-domain-implementation
;;    (lambda (&key min max)
;;      (odo:make-domain (if min
;;                       (if max
;;                           `(boolean ,min ,max)
;;                           `(boolean ,min))
;;                       (if max
;;                           `(boolean * ,max)
;;                           `(boolean)))))
;;    :simple-only-p t))
