(in-package #:odo/test)

(def-suite :odo/ref/doms/enumerated :in :odo/ref/doms)
(in-suite :odo/ref/doms/enumerated)

(test test/make-enumerated-domain
  (test-enumerated-domain-implementation #'odo:make-enumerated-domain))

(test test/make-domain/member
  (test-enumerated-domain-implementation (alex:curry #'odo:make-domain 'member)))

(test test/make-domain/enumerated-domain
  (test-enumerated-domain-implementation (alex:curry #'odo:make-domain 'odo:discrete-domain)))

(test test/make-domain/enumerated-domain-keyword
  (test-enumerated-domain-implementation (alex:curry #'odo:make-domain :discrete-domain)))

(test test/make-domain/member-spec
  (test-enumerated-domain-implementation
   (lambda (&key members)
     (odo:make-domain `(member ,@members)))
   :simple-only-p t))
