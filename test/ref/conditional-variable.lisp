(in-package #:odo/test)

(def-suite :odo/ref/conditional-variable :in :odo/ref)
(in-suite :odo/ref/conditional-variable)


;;; Integer variables
(def-suite :odo/ref/conditional-variable/integer :in :odo/ref/conditional-variable)
(in-suite :odo/ref/conditional-variable/integer)

(test test/make-conditional-integer-variable
  (run-twice-varying-controllability
   #'test-conditional-integer-variable-implementation
   #'odo:make-conditional-integer-variable))

(test test/make-conditional-variable/integer
  (run-twice-varying-controllability
   #'test-conditional-integer-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'integer)))

(test test/make-conditional-variable/integer-domain
  (run-twice-varying-controllability
   #'test-conditional-integer-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'odo:integer-domain)))

(test test/make-conditional-variable/integer-domain-keyword
  (run-twice-varying-controllability
   #'test-conditional-integer-variable-implementation
   (alex:curry #'odo:make-conditional-variable :integer-domain)))

(test test/make-conditional-variable/integer-domain-object
  (run-twice-varying-controllability
   #'test-conditional-integer-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-conditional-variable
            `(,(apply #'odo:make-integer-domain
                      (alex:remove-from-plist args :name :uncontrollable-p
                                                   :activation-status))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p)
              :activation-status ,(getf args :activation-status))))))


;;; Real variables
(def-suite :odo/ref/conditional-variable/real :in :odo/ref/conditional-variable)
(in-suite :odo/ref/conditional-variable/real)

(test test/make-conditional-real-variable
  (run-twice-varying-controllability
   #'test-conditional-real-variable-implementation
   #'odo:make-conditional-real-variable))

(test test/make-conditional-variable/real
  (run-twice-varying-controllability
   #'test-conditional-real-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'real)))

(test test/make-conditional-variable/real-domain
  (run-twice-varying-controllability
   #'test-conditional-real-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'odo:real-domain)))

(test test/make-conditional-variable/real-domain-keyword
  (run-twice-varying-controllability
   #'test-conditional-real-variable-implementation
   (alex:curry #'odo:make-conditional-variable :real-domain)))

(test test/make-conditional-variable/real-domain-object
  (run-twice-varying-controllability
   #'test-conditional-real-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-conditional-variable
            `(,(apply #'odo:make-real-domain
                      (alex:remove-from-plist args :name :uncontrollable-p
                                                   :activation-status))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p)
              :activation-status ,(getf args :activation-status))))))


;;; Boolean variables
(def-suite :odo/ref/conditional-variable/boolean :in :odo/ref/conditional-variable)
(in-suite :odo/ref/conditional-variable/boolean)

(test test/make-conditional-boolean-variable
  (run-twice-varying-controllability
   #'test-conditional-boolean-variable-implementation
   #'odo:make-conditional-boolean-variable))

(test test/make-conditional-variable/boolean
  (run-twice-varying-controllability
   #'test-conditional-boolean-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'boolean)))

(test test/make-conditional-variable/boolean-domain
  (run-twice-varying-controllability
   #'test-conditional-boolean-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'odo:boolean-domain)))

(test test/make-conditional-variable/boolean-domain-keyword
  (run-twice-varying-controllability
   #'test-conditional-boolean-variable-implementation
   (alex:curry #'odo:make-conditional-variable :boolean-domain)))

(test test/make-conditional-variable/boolean-domain-object
  (run-twice-varying-controllability
   #'test-conditional-boolean-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-conditional-variable
            `(,(apply #'odo:make-boolean-domain
                      (alex:remove-from-plist args :name :uncontrollable-p
                                                   :activation-status))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p)
              :activation-status ,(getf args :activation-status))))))


;;; Enumerated variables
(def-suite :odo/ref/conditional-variable/enumerated :in :odo/ref/conditional-variable)
(in-suite :odo/ref/conditional-variable/enumerated)

(test test/make-conditional-enumerated-variable
  (run-twice-varying-controllability
   #'test-conditional-enumerated-variable-implementation
   #'odo:make-conditional-enumerated-variable))

(test test/make-conditional-variable/enumerated
  (run-twice-varying-controllability
   #'test-conditional-enumerated-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'member)))

(test test/make-conditional-variable/enumerated-domain
  (run-twice-varying-controllability
   #'test-conditional-enumerated-variable-implementation
   (alex:curry #'odo:make-conditional-variable 'odo:discrete-domain)))

(test test/make-conditional-variable/enumerated-domain-keyword
  (run-twice-varying-controllability
   #'test-conditional-enumerated-variable-implementation
   (alex:curry #'odo:make-conditional-variable :discrete-domain)))

(test test/make-conditional-variable/enumerated-domain-object
  (run-twice-varying-controllability
   #'test-conditional-enumerated-variable-implementation
   (lambda (&rest args)
     (apply #'odo:make-conditional-variable
            `(,(apply #'odo:make-enumerated-domain
                      (alex:remove-from-plist args :name :uncontrollable-p
                                                   :activation-status))
              :name ,(getf args :name)
              :uncontrollable-p ,(getf args :uncontrollable-p)
              :activation-status ,(getf args :activation-status))))))
