(in-package #:odo/test)

(def-suite :odo/ref/state-variable :in :odo/ref)
(in-suite :odo/ref/state-variable)

(test test/make-state-variable
  (test-state-variable-implementation #'odo:make-state-variable))
