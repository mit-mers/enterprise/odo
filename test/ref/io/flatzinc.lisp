(in-package #:odo/test)

(def-suite :odo/ref/io/flatzinc :in :odo/ref/io)
(in-suite :odo/ref/io/flatzinc)

(test test/io/flatzinc/csp
  (test-flatzinc-csp))

(test test/io/flatzinc/cctp
  (test-flatzinc-cctp))

(test test/io/flatzinc/queens
  (test-flatzinc-nqueens))

(test test/io/flatzinc/vrp
  (test-flatzinc-vrp))

(test test/io/flatzinc/radmax
  (test-flatzinc-radmax))

(test test/io/flatzinc/radmax-mzn-fzn
  (test-flatzinc-radmax-mzn-fzn))

(test test/io/flatzinc/aust
  (test-flatzinc-aust))

(test test/io/flatzinc/latin-squares-12
  (test-flatzinc-latin-squares-12))

(test test/io/flatzinc/owens-toy-problem
  (test-flatzinc-owens-toy-problem))

(test test/io/flatzinc/var-expr
  (test-flatzinc-var-expr))

(test test/io/flatzinc/output
  (test-flatzinc-output))
