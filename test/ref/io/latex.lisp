(in-package #:odo/test)

(def-suite :odo/ref/io/latex :in :odo/ref/io)
(in-suite :odo/ref/io/latex)

(test test/io/latex/structure
  (test-latex-structure))

(test test/io/latex/misc
  (test-latex-misc))

(test test/io/latex/variables
  (test-latex-variables))

(test test/io/latex/constraints
  (test-latex-constraints))

(test test/io/latex/objective
  (test-latex-objective))
