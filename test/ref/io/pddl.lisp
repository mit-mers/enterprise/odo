(in-package #:odo/test)

(def-suite :odo/ref/io/pddl :in :odo/ref/io)
(in-suite :odo/ref/io/pddl)

(test pddls-1
  (test-pddl-round-trip :pathname (asdf:system-relative-pathname :odo "examples/pddls/cps-domain-1.pddl")))
