(in-package #:odo/test)

(def-suite :odo/ref/io/minizinc :in :odo/ref/io)
(in-suite :odo/ref/io/minizinc)

(test test/io/minizinc/tcsp
  (test-minizinc-tcsp))

(test test/io/minizinc/cctp
  (test-minizinc-cctp))

(test test/io/minizinc/vrp
  (test-minizinc-vrp))

(test test/io/minizinc/uncontrollable-var
  (test-minizinc-uncontrollable-var))

(test test/io/minizinc/state-plan
  (test-minizinc-state-plan))
