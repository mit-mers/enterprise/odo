(in-package #:odo/test)

(def-suite :odo/csp :in :odo)
(in-suite :odo/csp)

(test test/csp/make-csp
  (test-csp-implementation #'odo:make-csp))
