(in-package #:odo/test)

(def-suite :odo/transforms :in :odo)
(in-suite :odo/transforms)

(test test/csp/transforms/booleanize-conjunctions
  (test-booleanize-conjunctions-transform (odo:make-csp)))

(test test/csp/transforms/booleanize-disjunctions
  (test-booleanize-disjunctions-transform (odo:make-csp)))

(test test/csp/transforms/booleanize-reifs
  (test-booleanize-reifs-transform (odo:make-csp)))

(test test/csp/transforms/enum
  (test-enum-transform (odo:make-csp)))

(test test/csp/transforms/expression-to-references
  (test-expression-to-references-transform (odo:make-csp)))

(test test/csp/transforms/identity
  (test-identity-transform (odo:make-csp)))

(test test/csp/transforms/reset-domains
  (test-reset-domains-transform (odo:make-csp) (odo:make-csp)))

(test test/csp/transforms/temporal-to-linear
  (test-temporal-to-linear-transform (odo:make-csp)))
