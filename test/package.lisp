(uiop:define-package #:odo/test
  (:use #:cl
        #:fiveam
        #:iterate)
  (:local-nicknames (#:alex #:alexandria)
                    (#:utils #:odo/utils)))

(in-package #:odo/test)

(def-suite :odo :description "odo test suite.")
(def-suite :odo/ref :in :odo)
(def-suite :odo/ref/doms :in :odo/ref)
(def-suite :odo/ref/io :in :odo/ref)
