# Odo Merge Request #

Add a description of the change here.

## Checklist ##

Complete all items on checklist. If something cannot be completed, state why.

- [ ] Merge request has at least one label associated with it. Feel free to add
      labels you feel are missing.

- [ ] New functions have documentation strings.

- [ ] Documentation strings updated for functions whose behavior or lambda lists
      have changed.

- [ ] For ~enhancement new behaviors have test cases.

- [ ] For ~bug added one or more tests that *failed* before the fix was
      implemented.

- [ ] No calls to `(format t ...)` or `(log-{info-fatal})` unless justified.

- [ ] Changes have been rebased onto the target branch.

- [ ] Changes have been squashed to an appropriate number of commits. There is
      no need to keep every single commit, especially "checkpoints". Make sure
      your commits tell a story. (Suggest you do this last).

