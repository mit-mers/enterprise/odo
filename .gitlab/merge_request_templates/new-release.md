# Merge Request to Release a new version of Odo #

Version to be released:

Perform the following steps:

+ [] Update version string in src/version.lisp
+ [] Update CHANGELOG.md
+ [] Update `asdf:version-satisfies` in odo.asd if there have been any API breaking changes
+ [] Tag the release
