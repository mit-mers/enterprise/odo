;;;; Odo System Definition

#-:asdf3.1
(error "Requires ASDF >=3.1")

(defsystem #:odo
  :version (:read-file-form "src/utils/version.lisp" :at (1 2))
  :description "An API for describing variables, constraints,
automata, and optimization problems."
  :pathname "src"
  :in-order-to ((test-op (load-op "odo/test")))
  :perform (test-op (op c) (unless (symbol-call :fiveam :run! :odo)
                             (error "Test failure")))
  ;; Escalate non-deferrable style warnings to errors.
  #-ecl :around-compile
  #-ecl (lambda (thunk)
          (handler-bind ((style-warning (lambda (c)
                                          (error c))))
            (funcall thunk)))
  :depends-on (#:40ants-doc
               #:alexandria
               #:anaphora
               #:cl-coroutine
               #:cl-semver
               #:cl-singleton-mixin
               #:esrap
               #:jsown
               #:iterate
               #:lisp-namespace
               #:log4cl
               #:parse-number
               #:split-sequence
               #:trivia
               #:type-r)
  :components
  ( ;; Define all the packages used within Odo.
   (:module "packages"
    :pathname ""
    :components
    ((:file "package")
     (:file "shadows" :depends-on ("package"))
     (:file "utils/package")
     (:file "impl/package" :depends-on ("utils/package"))
     (:file "model-api/package" :depends-on ("impl/package" "utils/package"))
     (:file "io/package" :depends-on ("utils/package"))
     (:file "io/jsown/package")
     (:file "manual/package" :depends-on ("io/package" "model-api/package" "utils/package"))))

   (:module "manual"
    :depends-on ("packages")
    :components
    ((:file "manual")))

   (:module "utils"
    :depends-on ("packages")
    :components
    ((:file "copy" :depends-on ("version")) ; deprecation macro
     (:file "equal" :depends-on ("version")) ; deprecation macro
     (:file "iter")
     (:file "log")
     (:file "manual")
     (:file "plist")
     (:file "pretty-printing")
     (:file "printing")
     (:file "ranges")
     (:file "version")))

   (:module "model-api"
    :depends-on ("packages" "utils")
    :components
    ((:module "action-models"
      :depends-on ("types" "functions" "expressions")
      :components
      ((:file "pddl")
       (:file "pddl-grounding" :depends-on ("pddl"))))
     (:file "annotation")
     (:file "automata" :depends-on ("types"))
     (:file "bdd" :depends-on ("types"))
     (:file "chance-constraint" :depends-on ("types"))
     (:file "conditional-variable" :depends-on ("doms" "infinity" "types" "variable"))
     (:file "conditional")
     (:file "constraint" :depends-on ("types"))
     (:file "csp" :depends-on ("objectives" "types"))
     (:module "doms"
      :depends-on ("types" "infinity")
      :components
      ((:file "all" :depends-on ("standard"))
       (:file "standard")))
     (:file "episode" :depends-on ("types"))
     (:file "event" :depends-on ("conditional-variable" "infinity" "types"))
     (:module "expressions"
      :depends-on ("conditional-variable" "constraint" "doms" "episode" "namespaces" "state-variable" "types" "variable")
      :components
      ((:file "application" :depends-on ("defs"))
       (:file "defs")
       (:file "time" :depends-on ("defs"))))
     (:module "functions"
      :depends-on ("conditional" "expressions" "infinity" "namespaces" "types")
      :components
      ((:file "derivative" :depends-on ("function-defs"))
       (:file "function-defs")
       (:file "helpers")
       (:file "math" :depends-on ("function-defs"))
       (:module "preds"
        :depends-on ("function-defs" "helpers")
        :components
        ((:file "activep")
         (:file "all-diff-except")
         (:file "all-diff")
         (:file "circuit")
         (:file "conjunction")
         (:file "cumulative")
         (:file "disjunction")
         (:file "element")
         (:file "equality")
         (:file "equivalence")
         (:file "every")
         (:file "geometry")
         (:file "implication")
         (:file "linear")
         (:file "negation")
         (:file "normal")
         (:file "numeric-equality")
         (:file "numeric-inequality")
         (:file "obstacle")
         (:file "separation")
         (:file "some")
         (:file "subcircuit")
         (:file "temporal")
         (:file "xor")))
       (:file "pred-docs" :depends-on ("preds"))))
     (:file "generics")
     (:file "infinity")
     (:file "manual")
     (:file "namespaces")
     (:module "objectives"
      :depends-on ("types")
      :components
      ((:file "attribute" :depends-on ("defs"))
       (:file "defs")))
     (:file "state-constraint" :depends-on ("types"))
     (:file "state-plan" :depends-on ("types"))
     (:file "state-space" :depends-on ("types"))
     (:file "state-variable" :depends-on ("doms" "types"))
     (:module "transforms"
      :depends-on ("expressions" "variable")
      :components
      ((:file "booleanize-conjunctions" :depends-on ("defs"))
       (:file "booleanize-disjunctions" :depends-on ("defs"))
       (:file "booleanize-reifs" :depends-on ("defs"))
       (:file "defs")
       (:file "enums-to-ints" :depends-on ("defs"))
       (:file "expression-to-references" :depends-on ("defs"))
       (:file "identity" :depends-on ("defs"))
       (:file "reset-domains" :depends-on ("defs"))
       (:file "temporal-to-linear" :depends-on ("defs"))))
     (:file "transition-constraint" :depends-on ("types"))
     (:file "types" :depends-on ("infinity"))
     (:file "variable-vector" :depends-on ("doms" "types"))
     (:file "variable" :depends-on ("doms" "infinity" "types"))))

   (:module "io"
    :depends-on ("model-api")
    :components
    ((:file "csp" :depends-on ("defs" "latex" "minizinc" "flatzinc"))
     (:file "defs")
     (:file "id-helper" :depends-on ("defs"))
     (:module "flatzinc"
      :depends-on ("defs")
      :components
      ((:file "f2o")
       (:file "o2f")))
     (:module "jsown"
      :depends-on ("id-helper")
      :components
      ((:file "camel-case")
       (:file "decoder" :depends-on ("camel-case"))
       (:file "encoder" :depends-on ("camel-case" "decoder"))))
     (:module "latex"
      :depends-on ("defs")
      :components
      ((:file "csp2latex")))
     (:file "manual")
     (:module "minizinc"
      :depends-on ("defs" "flatzinc")
      :components
      ((:file "m2o")))
     (:module "pddl"
      :depends-on ("defs")
      :components
      ((:file "decoder")
       (:file "encoder")))))

   (:module "impl"
    :depends-on ("packages" "utils" "model-api")
    :components
    ((:file "all" :depends-on ("action-models"
                               "automata" "bdd"
                               "chance-constraint" "conditional-variable"
                               "constraint" "csp" "doms" "episode" "event"
                               "expressions" "objectives" "state-constraint"
                               "state-plan" "state-space" "state-variable"
                               "variable"))
     (:module "action-models"
      :depends-on ("annotation")
      :components
      ((:file "pddl")))
     (:file "annotation" :depends-on ("defs"))
     (:file "automata" :depends-on ("annotation"))
     (:file "bdd" :depends-on ("annotation"))
     (:file "chance-constraint" :depends-on ("annotation"))
     (:file "conditional-variable" :depends-on ("annotation"))
     (:file "constraint" :depends-on ("annotation"))
     (:file "csp" :depends-on ("annotation"))
     (:file "defs")
     (:module "doms"
      :depends-on ("annotation")
      :components
      ((:file "boolean" :depends-on ("defs"))
       (:file "defs")
       (:file "enumerated" :depends-on ("defs"))
       (:file "integer" :depends-on ("defs"))
       (:file "real" :depends-on ("defs"))))
     (:file "episode" :depends-on ("annotation"))
     (:file "event" :depends-on ("variable"))
     (:file "expressions" :depends-on ("defs"))
     (:module "objectives"
      :depends-on ("annotation")
      :components
      ((:file "attribute")))
     (:file "state-constraint" :depends-on ("annotation"))
     (:file "state-plan" :depends-on ("annotation"))
     (:file "state-space" :depends-on ("annotation"))
     (:file "state-variable" :depends-on ("annotation"))
     (:file "variable" :depends-on ("annotation"))))))

(defsystem "odo/test"
  :version (:read-file-form "src/utils/version.lisp" :at (2 2))
  :description "Tests for Odo"
  :pathname "test"
  :depends-on (#:fiveam
               #:named-readtables
               #:odo
               #:pythonic-string-reader)
  :components
  ((:module "io"
    :depends-on ("model-api")
    :components
    ((:module "flatzinc"
      :components
      ((:file "f2o")))
     (:module "json"
      :components
      ((:file "defs")
       (:file "state-plan")))
     (:module "latex"
      :components
      ((:file "csp2latex")))
     (:module "minizinc"
      :components
      ((:file "m2o")))
     (:module "pddl"
      :components
      ((:file "defs")))))

   (:module "model-api"
    :depends-on ("utils")
    :components
    ((:module "action-models"
      :components
      ((:file "pddl")))
     (:file "bdd" :depends-on ("doms"))
     (:file "conditional-variable" :depends-on ("doms"))
     (:file "csp" :depends-on ("doms"))
     (:file "expressions")
     (:module "doms"
      :components
      ((:file "boolean")
       (:file "enumerated")
       (:file "integer")
       (:file "real")))
     (:file "state-space" :depends-on ("doms"))
     (:file "state-variable" :depends-on ("doms"))
     (:file "transforms" :depends-on ("doms"))
     (:file "variable" :depends-on ("doms"))))

   (:file "package")

   (:module "utils"
    :depends-on ("package")
    :components
    ((:file "pretty-printing")))

   (:module "ref"
    :depends-on ("model-api")
    :components
    ((:file "bdd")
     (:file "conditional-variable")
     (:file "csp")
     (:module "doms"
      :components
      ((:file "boolean")
       (:file "enumerated")
       (:file "integer")
       (:file "real")))
     (:file "expressions")
     (:module "io"
      :components
      ((:file "flatzinc")
       (:file "json")
       (:file "latex")
       (:file "minizinc")
       (:file "pddl")))
     (:file "state-space")
     (:file "state-variable")
     (:file "transforms")
     (:file "variable")))))

(defsystem #:odo/doc
  :version (:read-file-form "src/utils/version.lisp" :at (2 2))
  :pathname "src"
  :depends-on ("40ants-doc-full" "odo")
  :components ((:file "doc")))

;; We include this so that other projects can depend on specific versions
(defmethod version-satisfies ((component (eql (find-system "odo"))) version)
  (let ((actual-version (component-version component)))
    (and
     ;; The last known API breakage happened in v0.5.0
     (version<= "0.5.0" version)
     (version<= version actual-version))))
