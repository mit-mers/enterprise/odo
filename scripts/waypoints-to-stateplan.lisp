(uiop:define-package #:odo/scripts/waypoints-to-stateplan
    (:use #:cl
          #:anaphora
          #:odo)
  (:export #:odo/scripts/make-state-plan-from-waypoints))

(in-package #:odo/scripts/waypoints-to-stateplan)

(defun make-glider-state-plan (&key
                                 (average-depth 20)
                                 (depth-band 10)
                                 waypoints)
  "Return a state plan consisting of the following activities in series:

+ set-average-depth
+ set-depth-band
+ goto every waypoint
+ surface"
  (aprog1 (make-state-plan :name "glider-mission"
                           :features '(:activities))
    (let ((start-event (state-plan-start-event it))
          (end-event (make-event)))
      (add-goal-episode! it (make-episode :start-event start-event
                                          :end-event end-event
                                          :activity-name "set-average-depth"
                                          :activity-args (list average-depth)))
      (shiftf start-event end-event (make-event))
      (add-goal-episode! it (make-episode :start-event start-event
                                          :end-event end-event
                                          :activity-name "set-depth-band"
                                          :activity-args (list depth-band)))
      (shiftf start-event end-event (make-event))
      (dolist (wp waypoints)
        (add-goal-episode! it (make-episode :start-event start-event
                                            :end-event end-event
                                            :activity-name "goto"
                                            :activity-args wp))
        (shiftf start-event end-event (make-event)))
      (add-goal-episode! it (make-episode :start-event start-event
                                          :end-event end-event
                                          :activity-name "surface")))))

(defun make-state-plan-from-waypoints (waypoints)
  "Returns a state plan containing a series of episodes for each waypoint (x y z) in a list of waypoints (w1 w2 w3 ...)

Example usage: (make-state-plan-from-waypoints (list '(1 2 3) '(4 5 6) '(7 8 9)))
"
  (let* ((start-event (make-event :value 0 :name "start")))
    (aprog1 (make-state-plan :name "goal"
                           :start-event start-event
                           :features '(:activities)) ; TODO: is this how features are added?
    (let* ((end-event (make-event :name "end")))
      ; make two episodes for each waypoint: one for the set-depth(z) activity, one for the follow-waypoint(x, y) activity
      (do ((waypoint (car waypoints) (car waypoints)) ; loop through all waypoints
           (current-start-event start-event current-end-event) ; keep track of the current start event for this episode
           (current-end-event (make-event) (make-event)))
          ((not waypoints)) ; loop until the waypoints list is empty
        (setq waypoints (cdr waypoints)) ; delete the first element of waypoints
        (if (not waypoints) (setq current-end-event end-event)) ; if on the last waypoint, the end event should be the overall end event
        (let ((x (nth 0 waypoint))
              (y (nth 1 waypoint))
              (z (nth 2 waypoint)))
          (add-goal-episode! it (make-episode :start-event current-start-event
                                              :end-event current-end-event
                                              :activity-name "set-depth"
                                              :activity-args (list z)))
          (add-goal-episode! it (make-episode :start-event current-start-event
                                              :end-event current-end-event
                                              :activity-name "follow-xy-waypoint"
                                              :activity-args (list x y)))))))))
